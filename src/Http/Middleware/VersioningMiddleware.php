<?php

namespace Pixelvide\IFMISTS\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VersioningMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        // same on client
        $response->headers->set('app-version', env('APP_VERSION', '0'));

        return $response;
    }

}
