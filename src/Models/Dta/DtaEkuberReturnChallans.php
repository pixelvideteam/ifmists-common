<?php

namespace App\Models\Dta;

use App\BankIfsc;
use App\BillMultipleParty;
use App\User;
use Illuminate\Database\Eloquent\Model;

class DtaEkuberReturnChallans extends Model
{
    protected $table = 'dta_ekuber_return_challans';

    public function old_bank()
    {
        return $this->hasOne(BankIfsc::class, 'ifsccode', 'old_ifsc');
    }

    public function new_bank()
    {
        return $this->hasOne(BankIfsc::class, 'ifsccode', 'new_ifsc');
    }

    public function ddo()
    {
        return $this->hasOne(User::class, 'username', 'ddocode');
    }

    public function bill_multiple_party()
    {
        return $this->hasOne(BillMultipleParty::class, 'id', 'bill_multiple_party_id');
    }
}
