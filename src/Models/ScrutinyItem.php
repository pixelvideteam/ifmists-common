<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ScrutinyItem extends Model {

	protected $table='scrutiny_items';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	public function billscrutinyans() {
		return $this->hasOne('App\BillScrutinyAns','scrutinyitems_id','id');
	}
}
