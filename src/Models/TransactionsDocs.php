<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionsDocs extends Model
{
    protected $table = "transactions_docs";
    protected $guarded = ['id'];
}
