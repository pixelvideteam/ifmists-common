<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class EmpHOD extends Model{

		protected $table="emp_hod_list";
		// public $timestamps = false;

		protected $guarded=['id'];

		public function hodddos()
		{	
			return $this->hasMany('App\EmpDDOHod','hod_list_table_id','id');
		}

		// public function schemes()
		// {	
		// 	return $this->belongsTo('Schemes','hoa','hoa');
		// }

		// public function accounts()
		// {	
		// 	return $this->belongsTo('Pdaccount','hoa','hoa');
		// }
	}