<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 17-01-2019
 * Time: 18:43
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyNameHistory extends Model
{
    protected $table = 'agency_name_history';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
