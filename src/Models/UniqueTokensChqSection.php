<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UniqueTokensChqSection extends Model
{
    protected $table = "uniquetokens_chqsection";
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
