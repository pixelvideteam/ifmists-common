<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarkedTransaction extends Model
{
    protected $table = 'marked_transactions';

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id')
            ->select('id', 'username', 'name');
    }

    public function transaction()
    {
        return $this->hasOne(Transactions::class, 'id', 'transaction_id')
            ->select('id', 'chequeno', 'passtogovtdate', 'ddocode', 'hoa', 'transdate', 'partyamount', 'purpose',
                'partyname', 'urn', 'partybranch', 'created_at', 'category_id', 'transid', 'partybranch',
                'partyifsc', 'marked_important', 'marked_remarks', 'remarks_user', 'marked_date', 'gross', 'net',
                'dedn', 'tokenno', 'adjustment_bill_flag');
    }

    public function pao_cheque()
    {
        return $this->hasOne(PaoCheques::class, 'id', 'pao_cheque_id');
    }

}
