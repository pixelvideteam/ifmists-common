<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MHod extends Model {

    protected $table='mhod';
    protected $guarded = ['id'];
    public $timestamps=false;
}
