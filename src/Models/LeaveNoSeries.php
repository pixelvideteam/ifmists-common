<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveNoSeries extends Model {

	protected $table = "leave_no_series";
	protected $guarded = ["id"];

}