<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestSurrenderDetail extends Model
{
    protected $table = 'budget_request_surrender_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function budsurrender()
    {
        return $this->hasOne('App\BudgetRequestSurrender', 'id', 'budget_request_surrender_id');
    }
}
