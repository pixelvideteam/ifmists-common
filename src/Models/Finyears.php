<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Finyears extends Model
{
//    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'finyears';

    protected $table = 'finyears';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
