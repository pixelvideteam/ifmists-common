<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanDetails extends Model {

	protected $table='loan_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function billmul() 
	{
		return $this->hasOne('App\BillMultipleParty','id','bill_multiple_party_id');
	}
}
