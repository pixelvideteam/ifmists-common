<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AudEmpApprovals extends Model
{
    protected $table = "aud_emp_approvals";
    protected $guarded = ['id'];

    public function audempearndedn()
    {
        return $this->hasMany('App\AudEmpApprovalsEarndedn', 'aud_emp_approvals_id', 'id');
    }

    public function desg()
    {
        return $this->hasOne('App\DesgCode', 'id', 'desgcode_id');
    }

    public function department()
    {
        return $this->hasOne('App\Department', 'id', 'dept_id');
    }

    public function scaletype()
    {
        return $this->hasOne('App\ScaleTypes', 'id', 'scale_type_id');
    }

    public function auduser()
    {
        return $this->hasOne('App\User', 'username', 'auduser');
    }

    public function docs()
    {
        return $this->hasMany('App\AudEmpApprovalsDocs', 'aud_emp_app_id', 'id');
    }

    ////////////////////
    public function cader()
    {
        return $this->hasOne('App\Cader', 'id', 'cadercode');
    }

    public function da()
    {
        return $this->hasOne('App\DAMasters', 'id', 'da_masters_id');
    }

    public function gpftype()
    {
        return $this->hasOne('App\GpfCatg', 'gpf_type', 'gpf_type');
    }

    public function prc()
    {
        return $this->hasOne('App\Prc', 'id', 'prc_id');
    }

    public function workingstatus()
    {
        return $this->hasOne('App\WorkingStatus', 'id', 'status');
    }

    public function billid()
    {
        return $this->hasOne('App\DdoBillIds', 'id', 'bill_id');
    }

    public function ddoallbillids()
    {
        return $this->hasMany('App\DdoBillIds', 'ddocode', 'ddocode');
    }

    // public function emplocation()
    // {
    // 	return $this->hasOne('App\EmployeeLocationList','id','emp_loc_id');
    // }

    public function scalesubtype()
    {
        return $this->hasOne('App\ScaleSubTypes', 'id', 'scale_sub_type_id');
    }

    public function ifscdetail()
    {
        return $this->hasOne('App\BankIfsc', 'ifsccode', 'ifsccode');
    }

    public function genders()
    {
        return $this->hasOne('App\Genders', 'id', 'gender_id');
    }

    public function scale()
    {
        return $this->hasOne('App\Scale', 'id', 'scale_id');
    }

    public function emplocation()
    {
        return $this->hasOne('App\EmployeeLocationList', 'id', 'emp_loc_id');
    }

    public function mar_status()
    {
        return $this->hasOne('App\MaritalStatus', 'id', 'maritalstatus');
    }

    public function empmaster()
    {
        return $this->hasOne('App\EmployeeMaster', 'bankacno', 'bankacno');
    }
}
