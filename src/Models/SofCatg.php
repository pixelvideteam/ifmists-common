<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 17-06-2019
 * Time: 20:52
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class SofCatg extends Model
{
    protected $table = 'sof_catg';

    public function sof()
    {
        return $this->hasOne(SourceOfFunding::class, 'id', 'sof_id');
    }
    public function sofsubcatg(){
        return $this->hasMany(SofSubCatg::class,'sof_catg_id','id');
    }
}