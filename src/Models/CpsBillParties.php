<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class CpsBillParties extends Model
{
    protected $table = 'cps_bill_parties';
}
