<?php
/**
 * Created by PhpStorm.
 * User: shreyan
 * Date: 6/11/2019
 * Time: 4:18 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helper\QueryHelper;

class LoginAttempt extends Model
{
    protected $table = 'login_attempts';
    protected $guarded = ['id'];
    public $timestamps = false;


    public static function add_invalid_login_attempt($username){
        $ip = QueryHelper::get_client_ip();
        $val=self::where('date',Carbon::today()->toDateString())->where('ip',$ip)->where('username',$username)->first();
        if(!empty($val)){
            $val->count=$val->count+1;
            $val->save();
        }else{
            $val = new self();
            $val->ip=$ip;
            $val->date=Carbon::today()->toDateString();
            $val->count=1;
            $val->username=$username;
            $val->save();
        }
        return $val;
    }

    public static function delete_invalid_login_attempt($username){
        $ip = QueryHelper::get_client_ip();
        self::where('date',Carbon::today()->toDateString())->where('ip',$ip)->where('username',$username)->delete();
    }


}

