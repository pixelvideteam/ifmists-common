<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DrawnMasterDetailsTypes extends Model 
{
	protected $table='drawn_master_details_types';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
