<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class EmpSalBillRemovals extends Model{

	protected $table="emp_sal_billremovals";
	protected $guarded=['id'];
	
	// public function empmaster()
	// {	
	// 	return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	// }

	// public function querydata()
	// {
	// 	return $this->belongsTo('App\Query','query_id','id');
	// }
}