<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPhonenos extends Model
{
    protected $table = 'user_phonenos';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
