<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 27-12-2018
 * Time: 17:39
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;


class EkuberMultiplePartyTemp extends Model
{
    protected $table="ekuber_multiple_party_temp";
    protected $guarded=['id'];
    protected $fillable=['ekuber_transaction_id','msg_id','message','txt_id'];


}