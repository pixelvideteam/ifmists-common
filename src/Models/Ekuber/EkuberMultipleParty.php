<?php

namespace App\Models\Ekuber;

use App\EkuberMultipleParty as EMP;

class EkuberMultipleParty extends EMP
{
    /**
     * Get the Bill Multiple Party record associated with the e-Kuber Multiple Party.
     *
     * @return BelongsTo
     */
    public function billMultipleParty()
    {
        return $this->belongsTo('App\Models\Ekuber\BillMultipleParty');
    }
}
