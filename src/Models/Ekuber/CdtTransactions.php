<?php

namespace App\Models\Ekuber;

class CdtTransactions
{
    private $InstrId;
    private $EndToEndId;
    private $Amt;
    private $CdtrIFSC;
    private $CdtrName;
    private $CdtrAcctNo;
    private $billMultipleParyId = null;

    public function __construct($cdtTransaction)
    {
        $this->InstrId = get_object_vars($cdtTransaction->PmtId)['InstrId'];
        $this->EndToEndId = get_object_vars($cdtTransaction->PmtId)['EndToEndId'];
        $this->Amt = get_object_vars($cdtTransaction->Amt->InstdAmt)['Amt'];
        $this->CdtrIFSC = get_object_vars($cdtTransaction->CdtrAgt->FinInstnId->ClrSysMmbId)['MmbId'];
        if(isset($cdtTransaction->Cdtr->Nm)){
            $this->CdtrName=$cdtTransaction->Cdtr->Nm;
        }else{
            $this->CdtrName = get_object_vars($cdtTransaction->Cdtr)['Nm'];
        }
        $this->CdtrAcctNo = get_object_vars($cdtTransaction->CdtrAcct->Id->Othr)['Id'];
    }

    public function getInstrId()
    {
        return $this->InstrId;
    }

    public function getCdtrEndToEndId()
    {
        return $this->EndToEndId;
    }

    public function getCdtrAmt()
    {
        return $this->Amt;
    }

    public function getCdtrIFSC()
    {
        return $this->CdtrIFSC;
    }

    public function getCdtrName()
    {
        return $this->CdtrName;
    }

    public function getCdtrAcctNo()
    {
        return $this->CdtrAcctNo;
    }

    public function getBillMuliplePartyId()
    {
        if ($this->billMultipleParyId) {
            return $this->billMultipleParyId;
        }

        if ($this->EndToEndId) {
            return intval(substr(str_replace("EP0130", "", $this->EndToEndId), 0, 12));
        }

        return null;
    }

    public function getTokenNo()
    {
        $res = \App\BillMultipleParty::where('id', '=', $this->getBillMuliplePartyId())
            ->with([
                'tokendets' => function ($query) {
                    $query->select('id', 'tokenno');
                }
            ])
            ->first();
        return $res['tokendets']->tokenno;
    }
}
