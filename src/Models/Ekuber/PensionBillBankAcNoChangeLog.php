<?php

namespace App\Models\Ekuber;

use Illuminate\Database\Eloquent\Model;

class PensionBillBankAcNoChangeLog extends Model
{
    protected $table = 'pension_bill_bankacno_edit_logs';
}
