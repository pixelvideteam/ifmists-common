<?php

namespace App\Models\Ekuber;

use App\Models\Ekuber\CdtTransactions;

class PaymentFileModel
{
    private $AppHdr = [
        'BizMsgIdr' => null,
        'MsgDefIdr' => null,
        'CreDt' => null
    ];

    private $GrpHdr = [
        'MsgId' => null,
        'CreDtTm' => null,
        'Prtry' => null,
        'NbOfTxs' => 0,
        'CtrlSum' => 0 // total
    ];

    private $PmtInf = [
        'PmtInfId' => null,
        'NbOfTxs' => 0,
        'Dbtr' => [
            'Name' => null,
            'IFSC' => null,
            'AccountNo' => null
        ],
        'CdtTrfTxInf' => [],
    ];

    public function __construct($xmlObj)
    {
        $this->setAppHdr(get_object_vars($xmlObj->AppHdr));
        $this->setGrpHdr(get_object_vars($xmlObj->Document->CstmrCdtTrfInitn->GrpHdr));
        $this->setPmtInf(get_object_vars($xmlObj->Document->CstmrCdtTrfInitn->PmtInf));
    }

    private function setAppHdr($appHdr)
    {
        $this->AppHdr = [
            'BizMsgIdr' => $appHdr['BizMsgIdr'],
            'MsgDefIdr' => $appHdr['MsgDefIdr'],
            'CreDt' => $appHdr['CreDt']
        ];
    }

    public function getAppHdr()
    {
        return $this->AppHdr;
    }

    private function setGrpHdr($grpHdr)
    {
        $this->GrpHdr = [
            'MsgId' => $grpHdr['MsgId'],
            'CreDtTm' => $grpHdr['CreDtTm'],
            'Prtry' => get_object_vars($grpHdr['Authstn'])['Prtry'],
            'NbOfTxs' => $grpHdr['NbOfTxs'],
            'CtrlSum' => $grpHdr['CtrlSum'],
        ];
    }

    public function getGrpHdr()
    {
        return $this->GrpHdr;
    }

    private function setPmtInf($pmtInf)
    {
        $this->PmtInf['PmtInfId'] = $pmtInf['PmtInfId'];
        $this->PmtInf['NbOfTxs'] = $pmtInf['NbOfTxs'] ? $pmtInf['NbOfTxs'] : 0;
        if(isset($pmtInf['Dbtr']->Nm)){
            $this->PmtInf['Dbtr']['Name'] = $pmtInf['Dbtr']->Nm;

        }else{
            $this->PmtInf['Dbtr']['Name'] = get_object_vars($pmtInf['Dbtr'])['Nm'];
        }
        $this->PmtInf['Dbtr']['IFSC'] = get_object_vars($pmtInf['DbtrAgt']->FinInstnId->ClrSysMmbId)['MmbId'];
        $this->PmtInf['Dbtr']['AccountNo'] = get_object_vars($pmtInf['DbtrAcct']->Id->Othr)['Id'];

        if (gettype($pmtInf['CdtTrfTxInf']) === 'array') {
            // file contains only multiple transactions
            foreach ($pmtInf['CdtTrfTxInf'] as $CdtTrfTxInf) {
                $this->addCdtTransaction($CdtTrfTxInf);
            }
        } else {
            // file contains only one transaction
            $this->addCdtTransaction($pmtInf['CdtTrfTxInf']);
        }
    }

    public function getPmtInf()
    {
        return $this->PmtInf;
    }

    private function xml2array($xmlObject, $out = array())
    {
        foreach ((array) $xmlObject as $index => $node) {
            $out[$index] = (is_object($node)) ? $this->xml2array($node) : $node;
        }

        return $out;
    }

    private function addCdtTransaction($cdtTransaction)
    {
        if (gettype($cdtTransaction) === 'object' && isset($cdtTransaction->PmtId)) {
            // $this->xml2array($cdtTransaction);

            $this->PmtInf['CdtTrfTxInf'][] = new CdtTransactions($cdtTransaction);
        }
    }

    public function getCdtTransactions()
    {
        return $this->PmtInf['CdtTrfTxInf'];
    }

}
