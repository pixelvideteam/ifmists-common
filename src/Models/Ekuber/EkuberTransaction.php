<?php

namespace App\Models\Ekuber;

use App\DTACheques;
use App\EkuberPaymentFiles;
use App\EkuberTransactions as EKT;
use App\Transactions;

class EkuberTransaction extends EKT
{
    /**
     * Get the e-Kuber client record associated with the billMultiplePartye-Kuber transaction.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ekuberClient()
    {
        return $this->belongsTo('App\EkuberClients', 'client_id');
    }

    /**
     * Get the e-Kuber Return Multiple Parties associated with the e-Kuber transaction.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ekuberReturnMultipleParty()
    {
        return $this->hasMany('App\EkuberMultiplePartyNew', 'ekuber_ack_rjct_transaction_id', 'id');
    }

    /**
     * Get the e-Kuber Transaction Type Status associated with the e-Kuber transaction.
     *
     * @return BelongsTo
     */
    public function ekuberTransTypeStatus()
    {
        return $this->belongsTo('App\EkuberTransStatus', 'transtype');
    }

    public function ekuberpaymentfilesnotready()
    {

        return $this->hasMany('App\EkuberPaymentFiles', 'ekuber_filename', 'orgnl_msg_id')->whereIn('reprocess_status',[0,1])->whereIn('invalidate_type',[0,1]);
    }
    public function ekuberpaymentfilesready()
    {

        return $this->hasMany('App\EkuberPaymentFiles', 'ekuber_filename', 'orgnl_msg_id')->whereIn('reprocess_status',[2])->whereIn('invalidate_type',[0,1]);
    }
    public function dtachequeekuber()
    {
        return $this->hasMany(DTACheques::class, 'ekuber_filename', 'orgnl_msg_id')->where('client_id',4);
    }
    public function pdtrans(){
        return $this->hasMany(Transactions::class, 'ekuber_pd_filename', 'orgnl_msg_id')->select('id','partyamount as net','ddocode','transid');
    }
    public function dtatrans(){
        return $this->hasMany(Transactions::class, 'ekuber_dta_filename', 'orgnl_msg_id')->select('id','net','ddocode','transid','urn');
    }
    public function dtachq(){
        return $this->hasMany(DTACheques::class, 'ekuber_filename', 'orgnl_msg_id')->select('id','chequeno as urn','amount','ekuber_filename');
    }
}
