<?php

namespace App\Models\Ekuber;

use App\BillMultipleParty as BMP;
use App\PaoChequesTransactions;
use App\Transactions;

class BillMultipleParty extends BMP
{
    /**
     * Get the transaction record associated with the Bill Multiple Party.
     *
     * @return BelongsTo
     */
    public function transaction()
    {
        return $this->belongsTo(Transactions::class, 'transaction_id');
    }

    public function pao_cheque_transactions()
    {
        return $this->hasMany(PaoChequesTransactions::class, 'transactions_id', 'transaction_id');
    }
}
