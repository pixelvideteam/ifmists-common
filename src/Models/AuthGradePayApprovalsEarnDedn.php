<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthGradePayApprovalsEarnDedn extends Model {

	protected $table = "auth_grade_pay_approvals_earndedn";
	protected $guarded = ["id"];

	public function earndedn() 
	{
		return $this->hasOne('App\EarnDednList','id','earndedn_id');
	}

	// public function employee() 
	// {
	// 	return $this->hasOne('App\EmployeeMaster','id','employee_id');
	// }

	// public function attachments() 
	// {
	// 	return $this->hasMany('App\AudEarnDednApprovalsAttachments','aud_earndedn_approvals_id','id');
	// }
}