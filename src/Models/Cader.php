<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Cader extends Model
{

    const CADRE_IAS_IPS = 1;
    const CADRE_GAZETTED = 2;
    const CADRE_NON_GAZETTED = 3;
    const CADRE_CLASS_IV = 4;
    const CADRE_WORK_CHARGE_INFERIOR = 11;
    const CADRE_WORK_CHARGE_SUPERIOR = 10;

    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'cader';

    protected $table = 'cader';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function employees()
    {
        return $this->hasMany('App\EmployeeMaster', 'cadercode', 'id');
    }

    public function hoa()
    {
        return $this->hasOne('App\Hoa', 'hoa', 'hoa');
    }

    public function hoadesc()
    {
        return $this->hasOne('App\HeadsDesc', 'hoa', 'hoa');
    }
}
