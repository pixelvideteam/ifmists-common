<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class FundsTransfer extends Model{

		protected $table="funds_transfer";
		public $timestamps = false;

		protected $guarded=['id'];

		public function creator(){	
			return $this->belongsTo('User','username','created_by');
		}

		public function usernames(){	
			return $this->belongsTo('User','ddo','username');
		}

		public function scheme()
		{
			return $this->belongsTo('Schemes','hoa','hoa');
		}
		public function credit_ddo()
		{
			return $this->belongsTo('User','username','credit_ddo');
		}
		public function credit_hoa()
		{
			return $this->belongsTo('Schemes','hoa','credit_hoa');
		}

	}


