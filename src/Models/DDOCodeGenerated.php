<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DDOCodeGenerated extends Model
{
    protected $table = "ddocode_generated";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getDDOCodeIncremented($areaCode, $departmentCode)
    {
        $first8digit = $areaCode . $departmentCode;
        $lastDDOCodeSequence = DB::table('users')
            ->selectRaw('MAX(substring(username,9,11)) as last_ddocode')
            ->where('username', 'LIKE', $first8digit . '%')
            ->whereRaw('length(username) = 11')
            ->value('last_ddocode');
        if (!$lastDDOCodeSequence) {
            $lastDDOCodeSequence = 0;
        }
        $lastDDOCodeSequence++;
        $ddocodeGenerated = $first8digit . str_pad($lastDDOCodeSequence, 3, 0, STR_PAD_LEFT);
        return $ddocodeGenerated;
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'ddo_dept_id', 'id');
    }
}
