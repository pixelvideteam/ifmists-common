<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HoaBe extends Model
{
    protected $table = "hoa_be";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    const COVID_19_HEAD_OF_ACCOUNT = '2245808002514500503NVN'; //amount => 3837500000.00
}
