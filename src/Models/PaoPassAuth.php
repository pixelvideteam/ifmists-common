<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoPassAuth extends Model {

	protected $table='pao_pass_auth';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
