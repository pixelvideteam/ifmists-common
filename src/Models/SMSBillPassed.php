<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SMSBillPassed extends Model
{
    protected $table = 'sms_bills_passed';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    
  
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'userid');
    }
}
