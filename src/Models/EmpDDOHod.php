<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class EmpDDOHod extends Model{

		protected $table="emp_ddohod";
		// public $timestamps = false;

		protected $guarded=['id'];

		public function ddohoddets()
		{	
			return $this->hasMany('App\EmpDDOHodDets','ddohod_table_id','id');
		}

		public function hoddets()
		{	
			return $this->belongsTo('App\EmpHOD','hod_list_table_id','id');
		}

		public function ddodets()
		{	
			return $this->belongsTo('App\EmpDDO','ddo_list_table_id','id');
		}
	}
