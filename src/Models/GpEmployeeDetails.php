<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class GpEmployeeDetails extends Model
{
    protected $table = 'gp_employee_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function gp_sal_unique_bill()
    {
        return $this->hasMany('App\GpSalUniqueBill', 'gp_employee_details_id', 'id');
    }
    public function bankifsc()
    {
        return $this->hasOne('App\BankIfsc', 'ifsccode', 'ifsccode');
    }

}
