<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PdaccountDocUploads extends Model {

	protected $table='pdaccount_doc_uploads';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function pdacc()
	{	
		return $this->belongsTo('App\Pdaccount','pdaccountinfo_id','id');
	}

}
