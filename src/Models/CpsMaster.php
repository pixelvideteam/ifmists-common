<?php
/**
 * Created by PhpStorm.
 * User: yoges
 * Date: 19-09-2019
 * Time: 19:20
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class CpsMaster extends Model {

    protected $table='cps_master';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function docs(){
        return $this->hasMany('App\CPSMasterDocs','cps_master_id','id');
    }

    public function employeemaster(){
        return $this->hasMany('App\EmployeeMaster','gpf_no','cpsno');
    }
}
