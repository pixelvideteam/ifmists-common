<?php namespace App;

use Illuminate\Database\Eloquent\Model;

	class Banks extends Model{

		protected $table="bankifsc";
		public $timestamps = false;

		protected $guarded=['id'];
	}	
