<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 23-10-2019
 * Time: 19:05
 */
declare(strict_types=1);

namespace App;


use App\Models\Ekuber\TransTypeStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DTACheques extends Model
{
    protected $table = "dta_cheques";
    protected $fillable = ['ekuber_verification_topic', 'ekuber_generation_status'];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function chequeTransactions()
    {
        return $this->hasMany(DTAChequeTransactions::class, 'dta_cheques_id', 'id');
    }

    public function transactions()
    {
        return $this->belongsToMany(Transactions::class,'dta_cheques_transactions','dta_cheques_id','transaction_id');
    }
    public function transactionbilldet()
    {
        return $this->belongsToMany(Transactions::class,'dta_cheques_transactions','dta_cheques_id','transaction_id')->select('ddocode','tokenno','transid','hoa','partyamount','gross','dedn','net');
    }

    public function chequePaymentFileMapping()
    {
        return $this->hasOne(ChequePaymentFileMapping::class, 'cheque_id', 'id');
    }

    public function ekuber_payment_files(){
        return $this->hasOne(EkuberPaymentFiles::class, 'ekuber_filename', 'ekuber_filename');
    }
    public function ekuber_tranasctions(){
        return $this->hasMany('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')
            ->whereIn('client_id',[2,4])
            ->orderBy('created_at');
    }
    public function ekuber_sent()
    {
        return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')
            ->whereIn('client_id',[2,4])
            ->where('transtype', '=', TransTypeStatus::SENT)->orderBy('created_at');
    }
    public function ekuber_ack_accept(){
        return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')
            ->whereIn('client_id',[2,4])
            ->where('transtype','=',TransTypeStatus::ACK_ACCP);
    }
    public function ekuber_ack_reject(){
        return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')
            ->whereIn('client_id',[2,4])
            ->where('transtype','=',TransTypeStatus::ACK_RJCT);
    }
    public function ekuber_dn()
    {
        return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')
            ->whereIn('client_id',[2,4])
            ->where('transtype', '=', TransTypeStatus::DN)->orderBy('created_at');
    }
    public function ekuber_ack_reject_dta()
    {
        return $this->hasOne('App\EkuberFilesStatus','ekuber_filename','ekuber_filename')
            ->whereIn('client_id',[4])
            ->whereRaw("ack_filename::text not like '%ACCP%'")->whereNotNull('ack_filename')->whereNull('dn_filename');
    }
    public function ekuber_ack_reject_pd()
    {
        return $this->hasOne('App\EkuberFilesStatus','ekuber_filename','ekuber_filename')
            ->whereIn('client_id',[2])
            ->whereRaw("ack_filename::text not like '%ACCP%'")->whereNotNull('ack_filename')->whereNull('dn_filename');
    }
    public function ekuber_transactions_all()
    {
        return $this->hasMany(EkuberTransactions::class, 'orgnl_msg_id', 'ekuber_filename');
    }
    public function ekuber_transactions()
    {
        return $this->hasMany(EkuberTransactions::class, 'orgnl_msg_id', 'ekuber_filename')->where('client_id',4);
    }
    public function ekuberpaymentfilesready()
    {
        return $this->hasMany('App\EkuberPaymentFiles',trim('ekuber_filename'),trim('ekuber_filename'))->where('client_id','=','4')->where('reprocess_status',2)->whereIn('invalidate_type',[0,1]);
    }
    public function ekuberpaymentfilesreadygen()
    {
        return $this->hasMany('App\EkuberPaymentFiles',trim('ekuber_filename'),trim('ekuber_filename'))->where('client_id','=','4')->whereIn('reprocess_status',[3,4,5])->whereIn('invalidate_type',[0,1]);
    }
    public function ekuberpaymentfilesnotready()
    {

        return $this->hasMany('App\EkuberPaymentFiles', 'ekuber_filename', 'ekuber_filename')->where('client_id','=','4')->whereIn('reprocess_status',[0,1])->whereIn('invalidate_type',[0,1]);
    }
    public function ekuberpaymentfilesreadygenpd()
    {
        return $this->hasMany('App\EkuberPaymentFiles',trim('ekuber_filename'),trim('ekuber_filename'))->where('client_id','=','2')->whereIn('reprocess_status',[3,4,5])->whereIn('invalidate_type',[0,1]);
    }
    public function ekuberpaymentfilesreadypd()
    {
        return $this->hasMany('App\EkuberPaymentFiles',trim('ekuber_filename'),trim('ekuber_filename'))->where('client_id','=','2')->where('reprocess_status',2)->whereIn('invalidate_type',[0,1]);
    }
    public function ekuberpaymentfilesnotreadypd()
    {

        return $this->hasMany('App\EkuberPaymentFiles', 'ekuber_filename', 'ekuber_filename')->whereIn('client_id',[2])->whereIn('reprocess_status',[0,1])->whereIn('invalidate_type',[0,1]);
    }
    public function ekuber_transactionspd()
    {
        return $this->hasMany(EkuberTransactions::class, 'orgnl_msg_id', 'ekuber_filename')->where('client_id',2);
    }
    public function ekuber_transactionsdwa()
    {
        return $this->hasMany(EkuberTransactions::class, 'orgnl_msg_id', 'ekuber_filename')->where('client_id',3);
    }
    public function ekuberpaymentfilesnotreadydwa()
    {

        return $this->hasMany('App\EkuberPaymentFiles', 'ekuber_filename', 'ekuber_filename')->whereIn('client_id',[3])->whereIn('reprocess_status',[0,1])->whereIn('invalidate_type',[0,1]);
    }
}
