<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartyChallanMapping extends Model
{
    protected $table='party_challan_mapping';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
