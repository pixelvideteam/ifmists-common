<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DateCheck extends Model {

    protected $table = "datecheck";
    public $timestamps = false;
    protected $guarded = ["id"];
}
