<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class OnlineBankScroll extends Model
{
    protected $table = 'onlinebankscroll';
    const TYPE_PAYMENTS = '1';
    const TYPE_RECEIPTS = '2';

}
