<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MDesg extends Model {

    protected $table='mdesg';
    protected $guarded = ['id'];
    public $timestamps=false;
}
