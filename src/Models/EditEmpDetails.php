<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class EditEmpDetails extends Model{

	protected $table="edit_empdetails";
	protected $guarded=['id'];
	
	public function empmaster()
	{	
		return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	}
}