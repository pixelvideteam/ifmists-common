<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeYearlyLeaves extends Model {

	protected $table='employee_yearly_leaves';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	public function leave() 
	{
		return $this->hasOne('App\LeavesMaster','id','leaves_id');
	}
    public function empleavelist()
    {
        return $this->hasMany('App\EmployeeLeavesList','employee_id','employee_id');
    }
}
