<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoChequesEKuberParties extends Model
{

    protected $table = "pao_cheques_billmultipleparty_ekuber";
    // public $timestamps = false;

    // protected $guarded = ["id"];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function ekuberddoform()
    {
        return $this->hasOne('App\EKuberReturnsDdoform', 'id', 'ekuber_returns_ddoform_id');
    }

    public function paochqdet()
    {
        return $this->hasOne('App\PaoChequesEKuber', 'id', 'pao_cheques_ekuber_id');
    }

    public function multipleparty()
    {
        return $this->hasOne(BillMultipleParty::class, 'id', 'bill_multiple_party_id');
    }

}
