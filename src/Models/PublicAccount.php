<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 15-06-2019
 * Time: 14:44
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class PublicAccount extends Model
{
    protected $table = 'public_accounts';
}