<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GenericSuppFormEmployees extends Model {

	protected $table='generic_supp_form_employees';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
