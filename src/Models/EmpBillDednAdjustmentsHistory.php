<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpBillDednAdjustmentsHistory extends Model {
	protected $table='emp_bill_dedn_adjustments_history';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    
}