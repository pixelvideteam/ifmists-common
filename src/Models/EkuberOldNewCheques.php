<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class EkuberOldNewCheques extends Model{

	protected $table="ekuber_old_new_cheques";
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function old_chq_details()
    {
        return $this->hasOne(Transactions::class, 'id', 'old_transactions_id');
    }

}
