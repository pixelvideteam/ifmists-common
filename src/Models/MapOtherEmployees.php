<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class MapOtherEmployees extends Model
{
	protected $table="map_other_employees";
	protected $guarded=['id'];
	
	public function aud()
	{	
		return $this->belongsTo('App\User','auduser','username');
	}

	public function empmaster()
	{	
		return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	}

	public function ddo()
	{	
		return $this->belongsTo('App\User','auduser','username');
	}

	// public function empearndedn()
	// {	
	// 	return $this->hasMany('App\AddChargeEarnings','add_charge_id','id');
	// }	
}