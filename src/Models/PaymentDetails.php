<?php
namespace App\Models;

use App\Models\Common\PgResult;
use DB;
use App\User;
use App\HeadsDesc;
use App\BankUrl;
use App\TReceiptsD;
use App\Models\Mdept;


class PaymentDetails
{
    public $hdn = "";
    public $deptCode = "";
    public $deptTransId = "";
    public $ddocode = "";
    public $hoa = "";
    public $remittersName = "";
    public $amount = "";
    public $bankname = "";
    public $phoneno = "";
    public $email = "";
    public $redirectUrl = "";
    public $challnno = '';
    public $dru = '';
    public $bankList = null;
    public $tin = '';

    /**
     * @param $deptCode
     * @param $deptTransId
     * @param $ddocode
     * @param $hoa
     * @param $remittersName
     * @param $amount
     * @param $bankname
     * @param $phoneno
     * @param $email
     */
    public function setData($deptCode, $deptTransId, $ddocode, $hoa, $remittersName, $amount, $redirectUrl, $tin)
    {
//        echo "<br />AAA : ".$deptCode."::".$deptTransId."::".$ddocode."::".$hoa."::".$remittersName."::".$amount."::".$redirectUrl."::".$tin."<br />";
        if (empty($deptCode)) {
            return new PgResult(1, "Department code is empty", null);
        } else {
            $isDeptValid = MDept::where('deptcode', $deptCode)->exists();
            if (!$isDeptValid) {
                return new PgResult(1, "Department code is not valid :$deptCode", null);
            } else {
                $this->deptCode = $deptCode;
            }
        }

        if (empty($deptTransId) || is_null($deptTransId)) {
            return new PgResult(1, "Department Transaction ID is empty", null);
        }
        $isTransIdPresent = TReceiptsD::where('depttransid', $deptTransId)->exists();
        if ($isTransIdPresent) {
            return new PgResult(1, "Duplicate Department Transaction Id :$deptTransId", null);
        } else {
            $this->deptTransId = $deptTransId;
        }
        if (empty($ddocode)) {
            return new PgResult(1, "DDo Code code is empty", null);
        } else {
            $isDDOValid = User::where(['username' => $ddocode, 'user_role' => '2'])->exists();
            if (!$isDDOValid) {
                return new PgResult(1, "DDO code is not valid :$ddocode", null);
            } else {
                $this->ddocode = $ddocode;
            }
        }
        if (empty($hoa)) {
            return new PgResult(1, "HOA is empty", null);
        } else {
            $isHoaValid = HeadsDesc::where('hoa', $hoa)->exists();
            if (!$isHoaValid) {
                return new PgResult(1, "HOA is not valid:" . $hoa, null);
            } else {
                $this->hoa = $hoa;
            }
        }
        if (empty($remittersName)) {
            return new PgResult(1, "Remitters is empty", null);
        } else {
            $this->remittersName = $remittersName;
        }
        if (empty($amount)) {
            return new PgResult(1, "Amount is empty", null);
        } else {
            if (intval($amount) > 0) {
                $this->amount = intval($amount);
            } else {
                return new PgResult(1, "Amount is not valid :" . $amount, null);

            }
        }

        /*if (!empty($bankname)) {
            $isBankValid = BankUrl::where('bank', $bankname)->exists();
            if (!$isBankValid) {
                return new PgResult(1, "Bank is not valid :" . $bankname, null);
            } else {
                $this->bankname = $bankname;
            }
        }*/

        /* if (strlen($phoneno) != 10) {
             return new PgResult(1, "Phone number is not valid :" . $phoneno, null);
         } else {
             $this->phoneno = $phoneno;
         }*/
        /* if (!preg_match(
             "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email)) {
             return new PgResult(1, "Email is not valid :" . $email, null);
         } else {
             $this->email = $email;
         }*/
        $this->redirectUrl = $redirectUrl;
        $this->dru = $redirectUrl;
        $seqName = 'challanno_cybertreasury_' . date('Y'); // we need to cchange on 1st Jan
//        $seqName = 'receipts_seq_2020_2021';
        $getNextSequence = DB::select(DB::raw("select nextval('$seqName')"));

        //0200000000

        $seqCheck = json_decode(json_encode($getNextSequence), true);

        $seq_value = $seqCheck[0]['nextval'];
        if (!empty($seq_value)) {
            $this->challnno = $seq_value;
        } else {
            return new PgResult(1, "Challan number not found", null);

        }
        $this->tin = $tin;
//        $this->bankList = BankUrl::select('bank', 'bankname')->get()->toArray();

    }
}
