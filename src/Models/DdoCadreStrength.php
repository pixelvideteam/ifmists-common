<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DdoCadreStrength extends Model{

	protected $table="ddo_cadre_strength";
	// public $timestamps = false;

	protected $guarded=['id'];

	public function desg()
	{
		return $this->belongsTo('App\DesgCode','desg_id','id');
	}

	// public function querydata()
	// {
	// 	return $this->belongsTo('App\Query','query_id','id');
	// }

	public function bdwise()
	{
		return $this->hasMany('App\BDWiseCadreStrength','desg_id','desg_id');
	}
    public function ddo()
    {
        return $this->hasOne('App\User','username','ddocode');
    }

    public function dept()
    {
        return $this->belongsTo('App\Department','dept_id','id');
    }

}
