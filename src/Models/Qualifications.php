<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Qualifications extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'qualifications';

    protected $table = 'qualifications';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
