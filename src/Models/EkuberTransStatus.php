<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 27-12-2018
 * Time: 20:16
 */
declare(strict_types=1);

namespace App;
use Illuminate\Database\Eloquent\Model;


class EkuberTransStatus extends Model
{
    protected $table="ekuber_trans_type_statuses";
}