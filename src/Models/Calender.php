<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class Calender extends Model{

	protected $table="calender";
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function day() {

		return $this->hasOne('App\Days','id','day_id');
	}

	public function drawnmaster() {

		return $this->hasMany('App\DrawnMaster','date','full_date');
	}

}
