<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceDemotions extends Model
{
    protected $table = 'esr_service_demotions';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getAllEmployeeDemotionParticulars($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with([
                'olddept.designations.desg',
                'newdept.designations.desg',
                'olddesg',
                'newdesg',
                'oldhod',
                'newhod',
                'oldprc.scales.scaledetails',
                'newprc.scales.scaledetails',
                'oldscale.scaledetails',
                'newscale.scaledetails',
                'payidfuture', 'payidpresent'
            ])
            ->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)
            ->with([
                'olddept.designations.desg',
                'newdept.designations.desg',
                'olddesg',
                'newdesg',
                'oldhod',
                'newhod',
                'oldprc.scales.scaledetails',
                'newprc.scales.scaledetails',
                'oldscale.scaledetails',
                'newscale.scaledetails',
                'payidfuture', 'payidpresent'
            ])
            ->first();
    }

    public function olddept()
    {
        return $this->hasOne(Department::class, 'id', 'old_dept_id');
    }

    public function newdept()
    {
        return $this->hasOne(Department::class, 'id', 'new_dept_id');
    }

    public function olddesg()
    {
        return $this->hasOne(DesgCode::class, 'id', 'old_desg_id');
    }

    public function newdesg()
    {
        return $this->hasOne(DesgCode::class, 'id', 'new_desg_id');
    }

    public function oldhod()
    {
        return $this->hasOne(HODList::class, 'id', 'old_hod_id');
    }

    public function newhod()
    {
        return $this->hasOne(HODList::class, 'id', 'new_hod_id');
    }

    public function oldprc()
    {
        return $this->hasOne(Prc::class, 'id', 'old_prc_id');
    }

    public function newprc()
    {
        return $this->hasOne(Prc::class, 'id', 'new_prc_id');
    }

    public function oldscale()
    {
        return $this->hasOne(Scale::class, 'id', 'old_scale_id');
    }

    public function newscale()
    {
        return $this->hasOne(Scale::class, 'id', 'new_scale_id');
    }

    public function payidpresent()
    {
        return $this->hasOne(ScaleDetails::class, 'id', 'old_pay_id');
    }

    public function payidfuture()
    {
        return $this->hasOne(ScaleDetails::class, 'id', 'new_pay_id');
    }
}
