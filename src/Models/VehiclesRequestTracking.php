<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehiclesRequestTracking extends Model
{
    protected $table = 'vehicles_request_tracking';
    protected $guarded = ['id'];

    public function primary_user_details() {
        return $this->hasOne('App\User', 'username', 'primary_user');
    }

    public function primary_user_role_details() {
        return $this->hasOne('App\UserRoles', 'role', 'primary_user_role');
    }

    public function secondary_user_details() {
        return $this->hasOne('App\User', 'username', 'primary_user');
    }

    public function secondary_user_role_details() {
        return $this->hasOne('App\UserRoles', 'role', 'secondary_user_role');
    }
}
