<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavesAvailedHPL extends Model
{
    protected $table='esr_leaves_availed_hpl';
    protected $guarded = ['id','created_at','updated_at'];

    public function leavetype(){
        return $this->hasOne(LeavesMaster::class, 'id', 'leave_type');
    }
}
