<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FormTypeUploads extends Model 
{
	protected $table='formtype_uploads';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
