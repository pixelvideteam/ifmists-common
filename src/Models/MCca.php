<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MCca extends Model {

	protected $table='mcca';
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
