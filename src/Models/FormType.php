<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class FormType extends Model
{

    use QueryCacheable;

    const FORM_PRC_ARREARS = 124;
    const FORM_PRC_ARREARS_LUMPSUM = 331;
    const FORM_PROMOTION_ARREARS = 102;
    const FORM_TRANSFER_ARREARS = 137;
    const FORM_INCREMENT_ARREARS = 101;
    const FORM_STEP_UP = 168;
    const FORM_GRADEPAY_ARREARS = 136;
    const FORM_DA_ARREARS = 95;
    const FORM_DIFFERENCE_DA_ARREARS = 497;
    const FORM_SALARY = 94;
    const FORM_SUPP_SALARY_ARREARS = 98;
    const FORM_ADDITIONAL_CHARGE_ARREARS = 118;
    const FORM_EDUCATION_CONCESSION = 134;
    const FORM_PAY_FIXATION_ARREARS = 148;
    const FORM_OTHER_ARREARS = 152;
    const FORM_ALLOWANCE_ARREARS = 151;
    const FORM_DA_EL_ENCASH_ARREARS = 150;
    const FORM_ANTICIPATORY_PENSION = 133;
    const FORM_SUPERANNUATION_RETIREMENT = 130;
    const FORM_LIFELONG_SERVICES = 157;
    const FORM_FESTIVAL_ADVANCE = 35;
    const FORM_SURRENDER_LEAVE = 97;
    const FORM_ADDL_SURRENDER_LEAVE = 158;
    const LEGAL_HEIR = 163;
    const ELECTION_DUTY = 202;
    const FINAL_EL_ENCASHMENT = 103;
    const FORM_SUSPENSION_ARREARS = 167;
    const FORM_LEGAL_HEIR = 163;
    const FORM_REFUND_BILLS = 172;
    const FORM_REFUND_REVENUE_BILLS = 162;
    const FORM_ANTICIPATORY_GRATUITY = 129;
    const FORM_DC = 142;
    const FORM_PENSION_BILL = 366;
    const FORM_CONVEYNANCE_ALLOWANCE = 11;
    const FOREIGN_SERVICE = 397;
    const FORM_FOREIGN_SERVICE = 397;
    const FORM_CSS_INTEREST_ARREARS = 496;
    const FORM_CSS_ARREARS = 467;
    const FORM_SCHOLARSHIPS_AND_STIPENDS = 16;
    const FORM_RYTHU_BANDHU = 529;
    const FORM_COVID_INCENTIVE = 601;
    const FORM_COVID_WEF_DATE = '2020-04-13';
    const FORM_COVID_ADJUSTMENT = 667;
    const FORM_DEFER_AMOUNT = 668;
    const FORM_CPS_TO_GPF = 701;
    const FORM_MILK_BILLS = 765;
    const FORM_RETURNS = 164;
    const GPF_CLASS_IV_LOAN = 113;
    const GPF_CLASS_IV_WITHDRAWL_PART_FINAL = 126;
    const FORM_DA_ARREARS_CPS = 801;
    const FORM_DA_ARREARS_CPS_ADJUST = 802;
    const FORM_ELECTION_DUTY = 202;
    const COVID_ADJUSTMENT_BILL = 667;
    const FORM_ADDITIONAL_ARREAR_CHARGE = 118;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = __CLASS__;

    protected $table = 'formtype';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function formno()
    {
        return $this->hasOne('App\FormNo', 'id', 'formno_id');
    }

    public function detailhead()
    {

        return $this->hasMany('App\Hoa', 'detailed_head', 'detailed_head');
    }

    public function subdetailhead()
    {

        return $this->hasMany('App\Hoa', 'sub_detailed_head', 'subdetailed_head');
    }

    public function scrutinyitems()
    {

        return $this->hasMany('App\ScrutinyItem', 'formtype_id', 'id');
    }

    public function fparent()
    {

        return $this->hasOne('App\FormType', 'parent', 'id');
    }

    public function leave()
    {

        return $this->hasOne('App\LeavesMaster', 'id', 'leaves_id');
    }

    public function subscription()
    {

        return $this->hasMany('App\FormTypeSubscription', 'formtype_id', 'id');
    }

    public function fuploads()
    {

        return $this->hasMany('App\FormTypeUploads', 'formtype_id', 'id');
    }

    public function frules()
    {

        return $this->hasMany('App\FormRules', 'formtype_id', 'id');
    }

    public function frulesuploads()
    {

        return $this->hasMany('App\FormRulesUploads', 'formtype_id', 'id');
    }

    public function earndednlist()
    {

        return $this->hasOne('App\EarnDednList', 'id', 'earndedn_id');
    }

}
