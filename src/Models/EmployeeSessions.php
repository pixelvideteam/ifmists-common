<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeSessions extends Model
{

    protected $table = 'employee_sessions';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
