<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Exptype extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'modelCache_';

    public $timestamps = false;

    public function subtypes()
    {
        return $this->hasMany('App\ExpSubTypes', 'exp_types_id', 'id');
    }
}
