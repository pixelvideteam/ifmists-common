<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Inctype extends Model {
	public $timestamps = false;

	public function subtypes()
	{	
		return $this->hasMany('App\IncSubTypes','inc_types_id','id');
	}

}
