<?php
/**
 * Created by PhpStorm.
 * User: yoges
 * Date: 27-07-2019
 * Time: 13:05
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuppBillSubmitDataPreview extends Model
{
    protected $table = 'supp_bill_submit_data_preview';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
