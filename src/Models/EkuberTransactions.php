<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkuberTransactions extends Model
{
    const DWA_CLIENT_ID = 3;
    const DN_TRANS_TYPE = 3;
    const RN_TRANS_TYPE = 4;

    protected $table = "ekuber_transactions";
    protected $guarded = ['id'];

    public function transTypeStatus()
    {
        return $this->hasOne(EkuberTransStatus::class, 'id', 'transtype');
    }

    public function returnmultipleParty()
    {
        return $this->hasMany(EkuberMultipleParty::class, 'ekuber_transaction_id', 'id');
    }


    public function dnmultipleParty()
    {
        return $this->hasMany(EkuberMultipleParty::class, 'ekuber_dn_transaction_id', 'id');
    }

    public function sentmultipleParty()
    {
        return $this->hasMany(EkuberMultipleParty::class, 'ekuber_sent_transaction_id', 'id');
    }

    public function returnmultiplePartynew()
    {
        return $this->hasMany(EkuberMultiplePartyNew::class, 'ekuber_transaction_id', 'id');
    }

    public function dnmultiplePartynew()
    {
        return $this->hasMany(EkuberMultiplePartyNew::class, 'ekuber_dn_transaction_id', 'id');
    }

    public function sentmultiplePartynew()
    {
        return $this->hasMany(EkuberMultiplePartyNew::class, 'ekuber_sent_transaction_id', 'id');
    }

    public function ackaccptmultiplePartyNew()
    {
        return $this->hasMany(EkuberMultiplePartyNew::class, 'ekuber_ack_rjct_transaction_id', 'id')
            ->where('message', '=', 'ACCP');
    }

    public function ackrjctmultiplePartyNew()
    {
        return $this->hasMany(EkuberMultiplePartyNew::class, 'ekuber_ack_rjct_transaction_id', 'id')
            ->where('message', 'like', '%TV0003%');
    }

    public function ackrjctmultiplePartyNewPd()
    {
        return $this->hasMany(EkuberMultiplePartyNew::class, 'ekuber_ack_rjct_transaction_id', 'id')
            ->where('message','like','%TV0003%');
    }


    public function paymentFile()
    {
        return $this->hasOne(EkuberPaymentFiles::class, 'ekuber_payment_file_id', 'id');
    }

    public function paocheque()
    {
        return $this->hasMany(PaoCheques::class, 'ekuber_filename', 'orgnl_msg_id');
    }

    public function paochequeekuber()
    {
        return $this->hasMany(PaoChequesEKuber::class, 'ekuber_filename', 'orgnl_msg_id');
    }

    public function multipleParty()
    {
        return $this->hasMany(EkuberMultipleParty::class, 'ekuber_transaction_id', 'id');
    }

    public function multiplePartyNew()
    {
        return $this->hasMany(EkuberMultiplePartyNew::class, 'ekuber_transaction_id', 'id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transactions', 'orgnl_msg_id', 'ekuber_dwa_filename');
    }
    /*public function ackrjctmultiplePartyNewDWA()
    {
        return $this->hasMany(EkuberMultiplePartyNew::class, 'ekuber_ack_rjct_transaction_id', 'id')
            ->where('message','like','%TV0003%');
    }*/

    public function rnMultiplePartyNew()
    {
        return $this->hasMany(EkuberMultiplePartyNew::class, 'ekuber_transaction_id', 'id');
    }
}
