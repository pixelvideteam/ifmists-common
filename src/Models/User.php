<?php namespace App;

use App\Models\Concerns;
use App\Models\Dwa\DwaAreas;
use Illuminate\Support\Facades\Cache;

class User extends Contracts\Authenticatable
{
    use
        Concerns\QueryBuilder,
        Concerns\PhoneNumber,
        Concerns\BlindIndex;

    const SESSION_PREFIX = 'sessions_';
    protected $table = 'users';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'password',
        'refreshtoken'
    ];

    /**
     * @var string[]
     * Blind Indexes columns in DB
     */
    protected $encrypted = [
        'phoneno' => 'phoneno_bidx',
    ];

    const ROLE_GOVT = 37;
    const ROLE_DTA_DIR = 18;
    const ROLE_GOVT_PS = 81;
    const USER_STATUS_ACTIVE = 1;
    const USER_STATUS_INACTIVE = 0;
    const USER_STATUS_ACTIVE_IN_WORDS = 'Active';
    const USER_STATUS_INACTIVE_IN_WORDS = 'In Active';
    const DWA_ADMIN_ROLE_ID = 14;
    const DWA_HOD_ROLE_ID = 1;
    const DWA_SUPERINTENDANT_ROLE_ID = 55;
    const DWA_PAO_ROLE_ID = 5;
    const DWA_AUDITOR_ROLE_ID = 6;
    const DWA_GOVT_ROLE_ID = 15;
    const DWA_DATA_ENTRY_ROLE_ID = 13;
    const DWA_JOINTDIRECTOR_ROLE_ID = 16;
    const DWA_DDO_ROLE_ID = 85;
    const PAO_DDO_ROLE_ID = 2;
    const DTA_SA_ROLE_ID = 11;
    const DTA_STO_ROLE_ID = 10;
    const DTA_ATO_ROLE_ID = 9;
    const DTA_DD_ROLE_ID = 8;
    const DTA_AUDITOR_ROLE_ID = 33;
    const PAO_AUDITOR_ROLE_ID = 11;
    const OPERATIONS_MANAGER = 112;
    const BULK_BILLS = 113;
    const JDPPO_JAO_ROLE_ID = 124;
    const JDPPO_APPO_ROLE_ID = 123;
    const JDPPO_PPO_ROLE_ID = 122;
    const MK_USERS = [
        'mk-staging',
        '20129999999w',
        'mk-production',
    ];
    const DWA_ADMIN_JOINT_DIRECTOR_ROLE_ID = 109;
    const DWA_HOD_USER_ROLE = 41;
    const DWA_TSE_LEAD_ROLE = 135;

    const PENSION_LIVE_STOS = [
        '2101', '2102', '2104', '2105', '2170',
        '1201', '1202', '1203', '1270',
        '2701', '2702', '2770',
        '0601', '0670', '0602', '0603', '0604',
        '1901', '1970', '1902', '1903',
        '0201', '0202', '0203', '0204', '0270',
        '0901', '0902', '0903', '0904', '0905', '0970',
        '1301', '1302', '1303', '1304', '1370',
        '0501', '0502', '0503', '0504', '0570',
        '1601', '1602', '1602', '1603', '1604', '1605', '1606', '1670',
        '2001', '2002', '2003', '2004', '2005', '2070',
        '2201', '2202', '2270',
        '2301', '2302', '2303', '2304', '2305', '2306', '2307', '2370',
        '2801', '2802', '2803', '2804', '2870',
        '2901', '2902', '2903', '2904', '2905', '2906', '2907', '2970',
        '3101', '3102', '3103', '3104', '3105', '3170',
        '2501','2560','2569','2571','2561','2562','2563','2564','2565','2566','2567','2568','2570',
        '0301','0302','0303','0304','0370',
        '0801','0803','0870',
        '1001','1002','1003','1070',
        '1101','1102','1103','1104','1170',
        '1501','1502','1503','1504','1570',
        '1701','1702','1703','1704','1770',
        '2601','2602','2670',
        '3501','3502','3503','3570',
        '0101','0102','0103','0170',
        '0401','0402','0403','0404','0470',
        '0701','0702','0703','0704','0770',
        '1401','1402','1470',
        '1801','1802','1803','1804','1805','1806','1870',
        '2401','2402','2403','2404','2405','2470',
        '3001','3002','3003','3004','3070',
        '3401','3402','3403','3470'
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($m) {
            if (!empty($m->refreshtoken)) {
                Cache::forget(self::SESSION_PREFIX . $m->refreshtoken);
                Session::fetch($m->refreshtoken);
            }
        });
    }

    public function modules()
    {
        return $this->hasMany('App\Modules', 'role', 'user_role')
            ->orderBy('modules.priority', 'asc');
    }

    public function store()
    {
        return $this->hasOne('App\Store', 'user_id', 'id');
    }

    public function ddodept()
    {
        return $this->hasOne('App\Department', 'id', 'ddo_dept_id');
    }

    public function company()
    {
        return $this->hasOne('App\Company', 'id', 'company_id');
    }

    public function ddohoas()
    {
        return $this->hasMany('App\PaoBudget', 'ddocode', 'username');
    }

    public function billids()
    {
        return $this->hasMany('App\DdoBillIds', 'ddocode', 'username');
    }

    public function audbills()
    {
        return $this->hasMany('App\Transactions', 'audempcode', 'empcode');
    }

    public function supbills()
    {
        return $this->hasMany('App\Transactions', 'supempcode', 'empcode');
    }

    public function offbills()
    {
        return $this->hasMany('App\Transactions', 'offempcode', 'empcode');
    }

    public function paobills()
    {
        return $this->hasMany('App\Transactions', 'offempcode', 'empcode');
    }

    public function chqcounterbills()
    {
        return $this->hasMany('App\Transactions', 'chqcounter_userid', 'id');
    }

    public function mappedto()
    {
        return $this->belongsTo('App\MapTable', 'username', 'currentuser');
    }

    public function auditors()
    {
        return $this->hasMany('App\PaoDdoAuditorMap', 'ddocode', 'username');
    }

    public function ddocadrestr()
    {
        return $this->hasMany('App\DdoCadreStrength', 'ddocode', 'username');
    }

    public function chequemap()
    {
        return $this->belongsTo('App\ChequeBookUsers', 'username', 'sauser');
    }

    public function ddoifsc()
    {
        return $this->hasOne('App\BankIfsc', 'ifsccode', 'bankifsc');
    }

    public function updates()
    {
        return $this->hasMany('App\UserUpdates', 'user_id', 'id');
    }

    public function userotp()
    {
        return $this->hasMany('App\UserOtp', 'userid', 'id');
    }

    public function userfp()
    {
        return $this->hasMany('App\UserFingerPrint', 'user_id', 'id');
    }

    public function depthoa()
    {
        return $this->hasOne('App\SchemesUsers', 'user_id', 'id');
    }

    public function ddoempdet()
    {
        return $this->hasOne('App\EmployeeMaster', 'id', 'ddo_emp_id');
    }

    public function branch()
    {
        return $this->hasOne('App\PaoBranch', 'chqapao_userid', 'id');
    }

    public function pdaccount()
    {
        return $this->hasMany('App\Pdaccount', 'ddocode', 'username');
    }

    public function hod()
    {
        return $this->hasOne('App\HODList', 'id', 'hod_id');
    }

    public function ddoemp_count_all()
    {
        return $this->hasMany('App\EmployeeMaster', 'ddocode', 'username');
    }

    public function ddoemps()
    {
        return $this->hasMany('App\EmployeeMaster', 'ddocode', 'username');
    }

    public function useraltphoneno()
    {
        return $this->hasMany('App\UserPhonenos', 'user_id', 'id');
    }

    public function audddos()
    {
        return $this->hasMany('App\PaoDdoAuditorMap', 'auditor', 'username');
    }

    public function alternate_phone_no()
    {
        return $this->hasMany('App\AlternatePhone', 'user_id', 'id');
    }

    public function hoamap()
    {
        return $this->hasMany('App\PaoDdoAuditorMap', 'auditor', 'username')
            ->where('other_req_flag', '=', '0');
    }

    public function getName()
    {
        return ucwords($this->name);
    }

    public function otherreqmap()
    {
        return $this->hasOne('App\PaoDdoAuditorMap', 'ddocode', 'username')
            ->where('other_req_flag', '=', '1');
    }

    public function userbranch()
    {
        return $this->hasOne('App\PaoBranch', 'id', 'pao_branch_id');
    }

    public function userhoamap()
    {
        return $this->hasMany('App\MapTableHoaWise', 'mapped_user', 'username');
    }

    public function ifscdetail()
    {
        return $this->hasOne('App\BankIfsc', 'ifsccode', 'bankifsc');
    }

    public function transdet()
    {
        return $this->hasMany('App\Transactions', 'ddocode', 'username');
    }

    public function empApprovalState()
    {
        return $this->hasMany('App\AudEmpApprovals', 'ddocode', 'username');
    }

    public function sto()
    {
        return $this->hasOne('App\User', 'username', 'stocode')
            ->select('id', 'username', 'user_role', 'ddocode', 'designation', 'userdesc');
    }

    public function inputRules(\Illuminate\Http\Request $request)
    {
        return [
            'confirm' => 'required|max:255',
            'new' => 'required|max:255',
            'old' => 'required|max:255',
        ];
    }

    public function custommessages(\Illuminate\Http\Request $request)
    {
        return [
            'required' => 'The :attribute is required.',
            'regex' => 'The :attribute should only contain digits',
            'max' => 'The :attribute length should not exceed :value',
            'min' => 'The :attribute length should be less than :value',
            'size' => 'The :attribute length should be :size',
            'between' => 'The :attribute length should be between :min - :max',
        ];
    }

    public function userrole()
    {
        return $this->hasOne('App\UserRoles', 'role', 'user_role');
    }

    public function ddos()
    {
        return $this->hasMany(User::class, 'dwa_pao_code', 'dwa_pao_code');
    }

    public function headDesc()
    {
        return $this->hasManyThrough(
            'App\Models\Reports\HeadDesc',
            'App\Transactions',
            'ddocode',
            'hoa',
            'username',
            'hoa');
    }

    public function dwaSections()
    {
        return $this->belongsToMany('App\Sections', 'dwa_users_sections', 'user_id', 'dwa_section_id');
    }

    public function mddocumbud()
    {
        return $this->hasMany(MDdoCumBudget::class, 'ddocode', 'username');
    }

    public function arealist()
    {
        return $this->hasOne(DwaAreas::class, 'pao_code', 'dwa_pao_code_number');
    }
    public function heavyadjbillfiles()
    {
        return $this->hasMany(HeavyAdjBillsFilesStatus::class, 'users_id', 'id');
    }

}
