<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Department extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'departments';

    protected $table = 'departments';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $hidden = ["created_at", "updated_at"];

    const ANTI_CORRUPTION_BUREAU = 47;
    const POLICE = 60;
    const DIR_GENL_INSP_GENL_OF_POLICE = 311;
    const IRRIGATION_COMMAND_AREA_DEVELOPMENT = 71;


    public function designations()
    {
        return $this->hasMany('App\DeptDesgnMapping', 'department_id', 'id');
    }
}
