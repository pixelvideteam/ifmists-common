<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeSalMesgTrack extends Model {

	protected $table='employee_sal_mesg_track';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
