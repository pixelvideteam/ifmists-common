<?php namespace App;

use App\Models\Ekuber\TransTypeStatus;
use Illuminate\Database\Eloquent\Model;

class PAOReconciliationPermission extends Model
{

    const STATUS_APRROVED = 1;
    const STATUS_PENDING = 0;
   
	protected $table='pao_reconciliation_permissions';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    
}