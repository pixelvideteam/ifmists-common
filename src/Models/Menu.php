<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Menu extends Model
{
//    use QueryCacheable;

    public $cacheFor = 3600; // cache time, in seconds / month
    public $cachePrefix = 'menus_';

    protected $table = 'menus';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function submenus()
    {

        return $this->hasMany('App\SubMenus', 'menu_id', 'id')->orderBy('sub_menus.priority', 'asc');;
    }

    public function menus_user()
    {

        return $this->hasMany('App\MenusUser', 'menus_id', 'id');
    }

}
