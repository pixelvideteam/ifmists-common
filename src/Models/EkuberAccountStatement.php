<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 10-02-2019
 * Time: 20:55
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class EkuberAccountStatement extends Model
{
    protected $table="ekuber_account_statement";

}