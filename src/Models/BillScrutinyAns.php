<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BillScrutinyAns extends Model {

	protected $table='billscrutiny_ans';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function scrutinyquestion() {

		return $this->hasOne('App\ScrutinyItem','id','scrutinyitems_id');
	}
}
