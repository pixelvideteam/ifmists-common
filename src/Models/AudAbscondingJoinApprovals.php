<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AudAbscondingJoinApprovals extends Model
{
    protected $table = "aud_absconding_join_approvals";
    protected $guarded = ['id'];

    public function empmaster()
    {
        return $this->hasOne('App\EmployeeMaster', 'id', 'employee_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'username', 'auduser');
    }

    public function absconding()
    {
        return $this->hasOne('App\Absconding', 'id', 'absconding_id');
    }
}
