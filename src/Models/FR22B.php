<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class FR22B extends Model{

	protected $table="fr22b";
	protected $guarded=['id'];
	
	public function monthdesc()
	{	
		return $this->belongsTo('App\Months','month','month_no');
	}
}