<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SchemesUsers extends Model {

	protected $table = "schemes_users";
	public $timestamps = false;

	protected $guarded = ["id"];
	public function schemes()
	{	
		return $this->hasOne('App\Schemes','id','schemes_id');
	}

}