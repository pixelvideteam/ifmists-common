<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 09-04-2019
 * Time: 09:12
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArcticSchedulerLog extends Model
{
    protected $table = "arctic_scheduler_log";

    public function result()
    {
        return $this->hasMany(ArcticSchedulerResults::class, 'scheduler_run_time_id', 'id');
    }

    public function script()
    {
        return $this->belongsTo(ArcticScripts::class, 'script_id', 'id');
    }
}
