<?php namespace App;

use Illuminate\Database\Eloquent\Model;
 
class RbiData extends Model {
    protected $table='rbi_datas';
    protected $guarded = ['id', 'created_at', 'updated_at'];
	public function datadetails() {
		return $this->hasMany('App\RbiDataDetails','data_id','id');
	}

}
