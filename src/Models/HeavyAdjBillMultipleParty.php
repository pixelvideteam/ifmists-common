<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class HeavyAdjBillMultipleParty extends Model
{
    protected $table = "heavy_adj_bill_multiple_party";
    public $timestamps = true;
    protected $guarded = ['id'];

    public function tokendets()
    {
        return $this->hasOne('App\Transactions', 'id', 'transactions_id');
    }
}
