<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChallanDetails extends Model
{
    protected $table='challan_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
