<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class HeadsDescBudget extends Model {

    protected $table='heads_desc_budget';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function hod()
    {
        return $this->hasOne(HODList::class, 'id', 'hod_id');
    }
}
