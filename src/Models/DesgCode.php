<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DesgCode extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'desgcode';

    protected $table = 'desgcode';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $hidden = ["created_at", "updated_at"];

    // public function scale() {

    // 	return $this->hasOne('App\Scale','id','scale_id');
    // }

    public function desgscales()
    {
        return $this->hasMany('App\DesgScaleMapping', 'desgcode_id', 'id');
    }

    public function ddocaderstr()
    {
        return $this->hasMany('App\DdoCadreStrength', 'desg_id', 'id');
    }
}
