<?php

namespace App;

use App\Repository\CommonRepository\FinancialYearRepository;
use Illuminate\Database\Eloquent\Model;

class EmployeeChangeElHplLog extends Model
{

    protected $table = 'employee_change_el_hpl_logs';

    const TYPE_EL = 1;
    const TYPE_HPL = 2;

    public static function canEditEL ($employee_id)
    {
        $fin_year = FinancialYearRepository::getCurrentFinancialYear();

        if(self::where('employee_id', $employee_id)->where('status', 1)->where('type', self::TYPE_EL)->where('fin_year', $fin_year)->exists()){
            return false;
        } else {
            return true;
        }

    }

    public static function canEditHPL ($employee_id)
    {
        $fin_year = FinancialYearRepository::getCurrentFinancialYear();

        if(self::where('employee_id', $employee_id)->where('status', 1)->where('type', self::TYPE_HPL)->where('fin_year', $fin_year)->exists()){
            return false;
        } else {
            return true;
        }

    }

    public static function store($employee_id, $type, $ddocode)
    {
        $a = new self();
        $a->ddocode = $ddocode;
        $a->employee_id = $employee_id;
        $a->type = $type;
        $a->status = 1;
        $a->fin_year = FinancialYearRepository::getCurrentFinancialYear();
        $a->save();

    }


}
