<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class BillFilterFormTypeMapping extends Model{

	protected $table="bill_filter_formtype_mapping";
	protected $guarded=['id'];
	
	public function formtype()
	{	
		return $this->hasOne('App\FormType','id','formtype_id');
	}

}