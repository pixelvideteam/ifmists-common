<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniqueImpTbr extends Model
{
    protected $table = "unique_imp_tbr";
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
