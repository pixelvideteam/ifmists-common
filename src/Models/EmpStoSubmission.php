<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class EmpStoSubmission extends Model{

		protected $table="emp_stosubmission";
		// public $timestamps = false;

		protected $guarded=['id'];

		public function ddohod()
		{	
			return $this->hasMany('App\EmpDDOHod','stosubmission_table_id','id');
		}
	}
