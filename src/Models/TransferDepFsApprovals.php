<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferDepFsApprovals extends Model
{
    protected $table = "transfer_depfs_approvals";
    protected $guarded = ['id'];

    public function empmaster()
    {
        return $this->belongsTo('App\EmployeeMaster', 'employee_id', 'id');
    }

    public function loc()
    {
        return $this->belongsTo('App\EmployeeLocationList', 'emp_new_loc_id', 'id');
    }
}
