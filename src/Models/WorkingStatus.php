<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class WorkingStatus extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'working_statuses';

    const STATUS_WORKING = 1;
    const STATUS_TRANSFERRED = 2;
    const STATUS_RETIRED = 3;
    const STATUS_EXPIRED = 4;
    const STATUS_SUSPENDED = 5;
    const STATUS_DEPUTED = 6;
    const STATUS_FOREIGN_SERVICES = 8;
    const STATUS_ABSCONDING = 9;

    protected $table = 'working_statuses';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
    protected $hidden = [
        "created_at",
        "updated_at",
    ];
}
