<?php

namespace App;

use App\Repository\CommonRepository\FinancialYearRepository;
use Illuminate\Database\Eloquent\Model;

class EmployeeChangeDesgLog extends Model
{
    protected $table = 'employee_designation_change_logs';

    const CHANGE_FORM_EMPLOYEE_MASTER = 1;
    const CHANGE_FORM_PROMOTION = 2;


    public static function canEditDesg ($employee_id)
    {
        $fin_year = FinancialYearRepository::getCurrentFinancialYear();
        $number_of_changes=self::where('employee_id', $employee_id)->where('status', 1)->where('fin_year', $fin_year)->count();
        if($number_of_changes >= 3){
            return false;
        } else {
            return true;
        }

    }
}
