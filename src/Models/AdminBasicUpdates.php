<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminBasicUpdates extends Model
{
    protected $table = 'admin_basic_updates';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
