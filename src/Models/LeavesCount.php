<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavesCount extends Model
{
    protected $table = 'esr_leaves_totals';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getEmployeeLeavesCount($employeeId)
    {
        return self::where('employee_id', $employeeId)->first();
    }

    public static function createBlankTotals($employeeId)
    {
        return LeavesCount::create([
            'el_total_credited' => 0,
            'el_total_used' => 0,
            'hpl_total_credited' => 0,
            'hpl_total_used' => 0,
            'employee_id' => $employeeId,
        ]);
    }

    public function addToELCredited($count = 0)
    {
        return $this->el_total_credited += $count;
    }

    public function subtractFromELCredited($count = 0)
    {
        return $this->el_total_credited -= $count;
    }

    public function addToELUsed($count = 0)
    {
        return $this->el_total_used += $count;
    }

    public function subtractFromELUsed($count = 0)
    {
        return $this->el_total_used -= $count;
    }

    public function addToHPLCredited($count = 0)
    {
        return $this->hpl_total_credited += $count;
    }

    public function subtractFromHPLCredited($count = 0)
    {
        return $this->hpl_total_credited -= $count;
    }

    public function addToHPLUsed($count = 0)
    {
        return $this->hpl_total_used += $count;
    }

    public function subtractFromHPLUsed($count = 0)
    {
        return $this->hpl_total_used -= $count;
    }

    public function hasSufficientELLeavesForAvailment($availedTotalCount)
    {
        return ($availedTotalCount + $this->el_total_used) <= $this->el_total_credited;
    }

    public function hasSufficientHPLLeavesForAvailment($availedTotalCount)
    {
        return ($availedTotalCount + $this->hpl_total_used) <= $this->hpl_total_credited;
    }
}
