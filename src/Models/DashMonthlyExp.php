<?php namespace App;

use Illuminate\Database\Eloquent\Model;
 
class DashMonthlyExp extends Model {

	//
	protected $table = "dashmonthlyexp";
	// public $timestamps = false;rans

	protected $guarded = ["id"];

	public function dailyexp()
	{	
		return $this->hasMany('App\DashDailyExp','dash_monthly_table_id','id');
	}

}
