<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class RythubandhuHbmp extends Model
{
    protected $table='heavy_bill_multiple_party';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    
    public function getAadharNoAttribute($value)
    {
        if (empty($value)) {
            return '';
        }
        return \App\Fms\Document\AadhaarCardDocument::hashed($value);
    }
}
