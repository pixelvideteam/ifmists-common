<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BillEarnDedn extends Model
{
    protected $table = 'bill_earndedn';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function empbilldetails()
    {
        return $this->hasMany('App\EmployeeBillDetails', 'bill_earndedn_id', 'id');
    }

    public function catwise()
    {
        return $this->hasMany('App\BillScheduleCatWise', 'bill_earndedn_id', 'id');
    }

    public function earndednlist()
    {
        return $this->hasOne('App\EarnDednList', 'id', 'earndedn_id');
    }

    public function earndedn()
    {
        return $this->hasOne('App\EarnDednList', 'id', 'earndedn_id');
    }

    public function earndednType2List()
    {
        return $this->hasOne('App\EarnDednList', 'id', 'earndedn_id')->where('type', 2);
    }

    public function billschedulecat()
    {
        return $this->hasMany('App\BillScheduleCatWise', 'bill_earndedn_id', 'id');
    }

    public function paybillsdh()
    {
        return $this->hasOne('App\PayBillSdhDesc', 'sdh', 'sdh');
    }

    public function otherbillearndedn()
    {
        return $this->hasOne('App\OtherBillEarndedn', 'id', 'other_bill_earndedn_id');
    }

    public function transaction()
    {
        return $this->hasOne('App\Transactions', 'id', 'transaction_id');
    }

    // LOCAL SCOPES
    public function scopeEarndednListByTypeAndCode($query, $type, $earndedncode)
    {
        /*return $query->with(['earndedn' => function($query2) use ($type, $earndedncode) {
			$query2->earndednType($type)->earndednCode($earndedncode);
		}]);*/
        return $query->whereHas('earndedn', function ($query2) use ($type, $earndedncode) {
            $query2->earndednType($type)->earndednCode($earndedncode);
        });
    }

    public function scopeWithAndWhereHas($query, $relation, $constraint)
    {
        return $query->whereHas($relation, $constraint)->with([$relation => $constraint]);
    }
}
