<?php
/**
 * Created by PhpStorm.
 * User: shreyan
 * Date: 6/11/2019
 * Time: 4:18 PM
 */

namespace App;
use App\Models\Concerns;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LoginOtpDetails extends Model
{
    use
        Concerns\QueryBuilder,
        Concerns\PhoneNumber,
        Concerns\BlindIndex;

    protected $table = 'login_otp_details';
    protected $guarded = ['id'];

    /**
     * @var string[]
     * Blind Indexes columns in DB
     */
    protected $encrypted = [
        'phoneno' => 'phoneno_bidx',
    ];

    public function users() {

        return $this->hasOne('App\User','id','user_id');
    }


}
