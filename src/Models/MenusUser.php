<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MenusUser extends Model 
{
	protected $table='menus_users';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
