<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pdaccount extends Model {

	protected $table = "pdaccountinfo";
	public $timestamps = false;

	protected $guarded = ["id"];

	public function usernames()
	{	
		return $this->belongsTo('App\User','ddocode','username');
	}

	public function scheme()
	{
		return $this->belongsTo('App\Schemes','hoa','hoa');
	}

	public function arealist()
	{
		return $this->belongsTo('App\Areas','areacode','areacode');
	}

	public function pdaccdocuploads() {

		return $this->hasMany('App\PdaccountDocUploads','pdaccountinfo_id','id');
	}
	public function pdacc_obalances() {

		return $this->hasMany('App\PdaccountObalances','pdaccountinfo_id','id');
	}
}