<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AgMhSubacno extends Model
{
    protected $table = 'ag_mh_subacno';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
