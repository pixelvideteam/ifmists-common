<?php


namespace App\Models\AgencyVerification;


use Illuminate\Database\Eloquent\Model;

class OutsourcingEmployees extends Model
{
    protected $table = 'outsourcing_employees';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
