<?php

namespace App\Models\AgencyVerification;

use Illuminate\Database\Eloquent\Model;

class ContractEmployees extends Model
{
    protected $table = 'contract_employees';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
