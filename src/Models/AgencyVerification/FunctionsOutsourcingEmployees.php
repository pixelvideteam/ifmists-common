<?php


namespace App\Models\AgencyVerification;


use Illuminate\Database\Eloquent\Model;

class FunctionsOutsourcingEmployees extends Model
{
    protected $table = 'functions_outsourcing_employees';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
