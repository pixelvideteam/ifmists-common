<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class GPFClassIVEmployeeExistance extends Model
{
    use QueryCacheable;
    
    public $cachePrefix = 'gpf_class_iv_employee_existance';

    protected $table = 'gpf_class_iv_employee_existance';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function employee()
    {
        return $this->hasOne(GPFOpeningBalance::class, 'empcode', 'empcode');
    }

    public function nonemployee()
    {
        return $this->hasOne(GPFClassIVChallanEmployees::class, 'empcode', 'empcode');
    }
}
