<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeDaUpdate extends Model 
{
	protected $table='employee_da_update';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function da() 
	{
		return $this->hasOne('App\DAMasters','id','da_masters_id');
	}
}
