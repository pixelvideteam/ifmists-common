<?php

namespace App\Models\HOA;

use App\HODList;
use App\PublicAccount;
use App\SofCatg;
use App\SofSubCatg;
use App\SourceOfFunding;
use Illuminate\Database\Eloquent\Model;

class HeadDescDashboard extends Model
{
    protected $table = 'heads_desc_dashboard';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function detailed()
    {
        return $this->hasOne('App\Formtype', 'detailed_head', 'dh');
    }
    public function subdetailed()
    {
        return $this->hasOne('App\Formtype', 'subdetailed_head', 'sdh');
    }
    public function paobudget()
    {
        return $this->hasMany('App\PaoBudget', 'hoa', 'hoa');
    }
    public function hoabebudget()
    {
        return $this->hasMany('App\HoaBeBudget', 'heads_desc_id', 'id');
    }
    public function ddobillidhoa()
    {
        return $this->hasOne('App\DdoBillIdsHoa', 'hoa', 'hoa');
    }
    public function hod()
    {
        return $this->hasOne(HODList::class, 'id', 'hod_id');
    }
    public function transactions() {
        return $this->hasMany('App\Transactions', 'hoa','hoa');
    }
    public function publicacc()
    {
        return $this->hasOne(PublicAccount::class, 'id', 'public_acc_id');
    }
    public function getGH($hoa)
    {
        $gh =substr($hoa,10,2);
        return $gh;
    }
    public function getDH($hoa)
    {
        $dh = substr($hoa,14,3);
        return $dh;
    }
    public function getSDH($hoa)
    {
        $sdh = substr($hoa,17,3);
        return $sdh;
    }
    public function sof(){
        return $this->hasOne(SourceOfFunding::class,'id','sof_id');
    }
    public function sofcatg(){
        return $this->hasOne(SofCatg::class,'id','sof_catg_id');
    }
    public function sofsubcatg(){
        return $this->hasOne(SofSubCatg::class,'id','sof_sub_catg_id');
    }
    public function publicAccount(){
        return $this->hasOne(PublicAccount::class,'id','public_acc_id');
    }
    public function restrictedDdo() {
        return $this->hasMany('App\FormTypeRestrictedDdocode', 'hoa','hoa');
    }
    public function mjhhoamap()
    {
        return $this->hasMany('App\MapTableHoaWise', 'major_head', 'mjh')->orderBy("user_role", "desc");
    }
    public function hoamap()
    {
        return $this->hasMany('App\MapTableHoaWise', 'hoa', 'hoa')->orderBy("user_role", "desc");
    }

}
