<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class MonthAccountsTransaction extends Model
{
    protected $table='month_accounts_transaction';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
?>
