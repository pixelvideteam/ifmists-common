<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class SalBillEmpDaywiseEarnings extends Model{

		protected $table="sal_bill_emp_daywise_earnings";
		protected $guarded=['id'];
		
		public function daydets() 
		{
			return $this->hasOne('App\SalBillEmpDays','id','sal_bill_emp_days_id');
		}

		public function earndedn()
		{
			return $this->belongsTo('App\EarnDednList','earndedn_id','id');
		}

		// public function querydata()
		// {
		// 	return $this->belongsTo('App\Query','query_id','id');
		// }
	}