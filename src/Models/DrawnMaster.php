<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DrawnMaster extends Model {

	protected $table = 'drawn_master';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function caltable()
	{	
		return $this->hasMany('App\EarnDednCal','earndedn_id','earndedn_id');
	}

	public function dets()
	{	
		return $this->hasMany('App\DrawnMasterDetails','drawn_master_id','id');
	}

	public function daybasic()
	{	
		return $this->hasMany('App\DrawnMaster','date','date')->where("earndedn_id", "=", 300)->where("type", "=", 1);
	}

	public function earndedn()
	{	
		return $this->hasOne('App\EarnDednList','id','earndedn_id');
	}

	public function da()
	{	
		return $this->hasOne('App\DAMasters','id','da_masters_id');
	}

	public function basic()
	{	
		return $this->hasOne('App\MasterScaleDetails','id','master_scale_details_id');
	}

	public function hra()
	{	
		return $this->hasOne('App\HraCatg','id','hracatg');
	}

	public function paytype()
	{	
		return $this->hasOne('App\DrawnMasterTypes','type','type');
	}
	public function details()
	{	
		return $this->hasMany('App\DrawnMasterDetails','drawn_master_id','id');
	}
}
