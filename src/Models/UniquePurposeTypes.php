<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniquePurposeTypes extends Model
{
    protected $table = 'unique_purpose_types';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
