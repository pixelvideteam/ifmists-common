<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemotionDocs extends Model
{
    protected $table = 'demotion_docs';
}
