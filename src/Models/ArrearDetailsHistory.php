<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ArrearDetailsHistory extends Model
{
    protected $table = 'arrear_details_history';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
