<?php namespace App;

use App\Fms\Document\PanCardNumberEncryption;
use Illuminate\Database\Eloquent\Model;

class EmployeeBillDetails extends Model
{

    protected $table = 'employee_bill_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function earndednlist()
    {
        return $this->hasOne('App\EarnDednList', 'id', 'earndedn_id');
    }

    public function earndedn()
    {
        return $this->hasOne('App\EarnDednList', 'id', 'earndedn_id');
    }

    public function month()
    {
        return $this->hasOne('App\Months', 'id', 'month');
    }

    public function billmultiple()
    {
        return $this->hasOne('App\BillMultipleParty', 'id', 'bill_multiple_party_id');
    }

    public function empmaster()
    {
        return $this->hasOne('App\EmployeeMaster', 'id', 'employee_id');
    }

    public function loandets()
    {
        return $this->hasMany('App\Loans', 'employee_id', 'employee_id')->where("close_flag", "=", 0);
    }

    public function allloandets()
    {
        return $this->hasMany('App\Loans', 'employee_id', 'employee_id');
    }

    public function empearndedn()
    {
        return $this->hasMany('App\EmployeeEarnDedn', 'employee_id', 'employee_id');
    }

    public function dateranges()
    {
        return $this->hasMany('App\SuppBillDateranges', 'bill_multiple_party_id', 'bill_multiple_party_id');
    }

    public function transactions()
    {
        return $this->hasOne('App\Transactions', 'id', 'transaction_id');
    }

    public function otherbillearndedn()
    {
        return $this->hasOne('App\OtherBillEarndedn', 'id', 'other_bill_earndedn_id');
    }

    public function billearndedn()
    {
        return $this->hasOne('App\BillEarnDedn', 'id', 'bill_earndedn_id');
    }

    public function empbasic()
    {
        return $this->hasOne('App\EmployeeBillDetails', 'bill_multiple_party_id', 'bill_multiple_party_id')
            ->where('earndedn_id', 300);
    }

    public function getEmpMasterColValAttribute($value)
    {
        // its a encrypted Pan
        if (strlen($value) > 30) {
            try {
                $value = PanCardNumberEncryption::decrypted($value);
            } catch (\Exception $e) {

            }
        }

        return $value;
    }
}
