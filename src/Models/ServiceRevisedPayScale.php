<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRevisedPayScale extends Model
{
    protected $table = 'esr_revised_pay_scale';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getAllEmployeeRevisedPayScaleDetails($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with([
                'dept.designations.desg',
                'desg',
                'hod',
                'oldprc.scales.scaledetails',
                'newprc.scales.scaledetails',
                'oldscale.scaledetails',
                'newscale.scaledetails',
                'payidfuture', 'payidpresent'
            ])
            ->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)
            ->with([
                'dept.designations.desg',
                'desg',
                'hod',
                'oldprc.scales.scaledetails',
                'newprc.scales.scaledetails',
                'oldscale.scaledetails',
                'newscale.scaledetails',
                'payidfuture', 'payidpresent'
            ])
            ->first();
    }

    public function dept()
    {
        return $this->hasOne(Department::class, 'id', 'dept_id');
    }

    public function desg()
    {
        return $this->hasOne(DesgCode::class, 'id', 'desg_id');
    }

    public function hod()
    {
        return $this->hasOne(HODList::class, 'id', 'hod_id');
    }

    public function oldprc()
    {
        return $this->hasOne(Prc::class, 'id', 'old_prc_id');
    }

    public function newprc()
    {
        return $this->hasOne(Prc::class, 'id', 'new_prc_id');
    }

    public function oldscale()
    {
        return $this->hasOne(Scale::class, 'id', 'old_scale_id');
    }

    public function newscale()
    {
        return $this->hasOne(Scale::class, 'id', 'new_scale_id');
    }

    public function payidpresent()
    {
        return $this->hasOne(ScaleDetails::class, 'id', 'old_pay_id');
    }

    public function payidfuture()
    {
        return $this->hasOne(ScaleDetails::class, 'id', 'new_pay_id');
    }
}
