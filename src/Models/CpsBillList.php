<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class CpsBillList extends Model
{
    protected $table = 'cps_bill_list';

    public function cpsbillparties()
    {
        return $this->hasMany(CpsBillParties::class, 'cps_bill_list_id', 'id');
    }
    public function cpsbilltransactions()
    {
        return $this->hasMany(CpsBillTransactions::class, 'cps_bill_list_id', 'id');
    }
}
