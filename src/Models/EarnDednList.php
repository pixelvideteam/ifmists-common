<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class EarnDednList extends Model
{
    const EARN_BASIC_PAY_ID = '300';
    const EARN_HRA_ID = '306';
    const EARN_DA_ID = '305';
    const EARN_CCA_ID = '307';
    const EARN_GIS_ID = '398';
    const EARN_GIS_ID_LIVE = '498';
    const EARN_TEL_INC_ID = '382';
    const EARN_ADDL_HRA_ID = '356';
    const EARN_ADDL_HRA2_ID = '378';
    const EARN_PRC_ARREAR_ID = '504';
    const DEDN_CPS_ID = '435';
    const EARN_PA_ALLOW = '521';
    const EARN_OFC_SUB_ORD_ALLOW = '522';

    const DEDN_TSGLI_ID = '396';
    const DEDN_NG_LIC = '779';
    const DWA_WORK_GPF_CODES = ['2074'];

    const DWA_REGULAR_GPF_CODES = ['1111', '2045', '2050', '1111', '2001', '2002', '2100', '2094', '2093', '2066'];
    const CM_INCENTIVE_COVID = '1058';
    const GPF_IV_SUBSCRIPTION = '433';
    const GPF_IV_LOAN = '455';
    const DEFERRED_AMOUNT=1057;
    
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'earndedn_list';

    protected $table = 'earndedn_list';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $hidden = ["created_at", "updated_at"];

    const CAN_ADD_UPDATE_EARNDEDN_LIST = [921, 535, 436, 498, 373, 459, 386, 375, 387, 320, 384, 300, 513, 305, 378, 374, 372, 434, 548, 435];
    public function interest()
    {
        return $this->hasOne('App\EarnDednList', 'parent_earndedn_id', 'id');
    }

    public function emploan()
    {
        return $this->hasOne('App\Loans', 'earndedn_id', 'id')->where('close_flag', '=', '0');
    }

    public function sdhdesc()
    {
        return $this->hasOne('App\PayBillSdhDesc', 'sdh', 'sdh');
    }

    public function earndedn()
    {
        return $this->hasOne('App\EarnDednList', 'id', 'id');
    }

    public function billEarnDedn()
    {
        return $this->hasMany('App\BillEarnDedn', 'earndedn_id');
    }

    //Aliasing to keep reports consistent
    public function billMultipleParties()
    {
        return $this->billEarnDedn();
    }

    public function headsdesc()
    {
        return $this->hasOne('App\HeadsDesc', 'hoa', 'hoa');
    }


    // LOCAL SCOPES
    public function scopeEarndednType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopeEarndednCode($query, $earndedncode)
    {
        return $query->where('earndedncode', $earndedncode);
    }

    public function scopeWithAndWhereHas($query, $relation, $constraint)
    {
        return $query->whereHas($relation, $constraint)->with([$relation => $constraint]);
    }

    public static function canAddUpdateEarndednList()
    {
        return self::where('loan', '!=', '1')->where("active", '1')->whereNotIn('id', self::CAN_ADD_UPDATE_EARNDEDN_LIST)->get();
    }

    public static function canDeleteEarndednList()
    {
        return self::where('loan', '!=', '1')->where("active", '1')->whereNotIn('id', [921, 535, 436, 498, 373, 459, 386, 375, 387, 320, 384, 300, 513, 305, 378, 374, 372, 434, 548, 435, 396])->get();
    }

    public static function canAddUpdateLoanEarndednList()
    {
        return self::where('loan', '1')->where("active", '1')->get();
    }

    public static function getActiveEarndednList()
    {
        return self::where("active", '1')->where('sal_rec_flag', '!=', '1')->get();
    }
}
