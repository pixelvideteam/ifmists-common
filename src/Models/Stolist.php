<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stolist extends Model
{
    protected $table='stolist';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
