<?php

namespace App\Models\BMS;

use App\Repository\ApiRepository\BMS\Helpers;
use App\Models\BMS\BmsDB;

class Bms
{
    protected $helpers;

    public function __construct()
    {
        $this->helpers = new Helpers;
    }

    public function isAuthorized($transaction)
    {
        $db = new BmsDB();

        $paoCode = $transaction->users->dwa_pao_code;
        $financialYear = $this->helpers->getFinancialStartYearFromBillMonthYear($transaction->bill_month, $transaction->bill_year);
        $query = 'SELECT * FROM factstg.TOKEN WHERE EXPNO=' . $transaction->tokenno . ' AND PAOCODE=' . $paoCode . ' AND EXPYEAR=' . $financialYear;
        $result = $db->query($query)->execute();

        if ($result->status == 1) {
            return count($result->data['ISAUTHORIZED']) ? $result->data['ISAUTHORIZED'][0] : 0;
        }

        return 0;
    }

    public function hasBudget($transaction, $withBills = true)
    {
        $be = $this->getBE($transaction);
        $reApp = $this->getReApp($transaction);
        $quaterlyRelaxation = $this->getQuaterlyRelaxation($transaction);

        $totalExpenditure = $this->getTotalExpenditure($transaction);
        $totalBudget = $be + $reApp + $quaterlyRelaxation;

        $balance = ($be + $reApp + $quaterlyRelaxation - $totalExpenditure);

        $response = new \stdClass();
        $response->totalBe = $be;
        $response->totalBeWithAll = $totalBudget;
        $response->totalBudgetAvailable = $balance;
        if ($withBills) {
            $bmsReadyBills = $this->getBillsFromHoa($transaction);
            $response->bmsBills = $bmsReadyBills;
        }

        return $response;
    }

    protected function getBE($transaction)
    {
        $db = new BmsDB();
        $hoa = $this->helpers->getFormattedHoa($transaction->hoa) . '-' . substr($transaction->hoa, -2, 1);
        $financialYear = 2019;//$this->helpers->getFinancialStartYearFromBillMonthYear($transaction->bill_month, $transaction->bill_year);
        $query = 'SELECT SUM(NVL(EBP,0)+ NVL(EBNP,0)) FROM factstg.objective_new WHERE YEAR=' . $financialYear . ' AND HOA=\'' . $hoa . '\'';
        $result = $db->query($query)->execute();
        if ($result->status == 1) {
            $data = array_values($result->data);
            return count($data[0]) ? $data[0][0] * 1000 : 0;
        }

        return 0;
    }

    protected function getReApp($transaction)
    {
        $db = new BmsDB();
        $hoa = $this->helpers->getFormattedHoa($transaction->hoa);
        $financialYear = 2019;//$this->helpers->getFinancialStartYearFromBillMonthYear($transaction->bill_month, $transaction->bill_year);
        $query = 'SELECT SUM(CASE WHEN TYPE IN (\'A\', \'R\') THEN AMOUNT ELSE 0 END) FROM factstg.reapp WHERE FINYEAR=' . $financialYear . ' AND HOA=\'' . $hoa . '\'';
        $result = $db->query($query)->execute();
        if ($result->status == 1) {
            $data = array_values($result->data);
            return count($data[0]) ? $data[0][0] * 100000 : 0;
        }

        return 0;
    }

    protected function getQuaterlyRelaxation($transaction)
    {
        $db = new BmsDB();
        $hoa = $this->helpers->getFormattedHoa($transaction->hoa) . '-' . substr($transaction->hoa, -2, 1);
        $financialYear = 2019;//$this->helpers->getFinancialStartYearFromBillMonthYear($transaction->bill_month, $transaction->bill_year);
        $query = 'SELECT SUM(AMOUNT) FROM factstg.QUATERLYRELAXATION WHERE EXPYEAR=' . $financialYear . ' AND HEADOFACCOUNT=\'' . $hoa . '\'';
        $result = $db->query($query)->execute();

        if ($result->status == 1) {
            $data = array_values($result->data);
            return count($data[0]) ? $data[0][0] * 100000 : 0;
        }

        return 0;
    }

    public function getTotalExpenditure($transaction, $mode = 'DDH')
    {
        $exp = 0;
        $db = new BmsDB();
        $vc = substr($transaction->hoa, -2, 1);
        $hoa = $this->helpers->getFormattedHoa($transaction->hoa) . '-' . $vc;
        $financialYear = $this->helpers->getFinancialStartYearFromBillMonthYear($transaction->bill_month, $transaction->bill_year);
        $bmsDdoCode = $transaction->users->dwa_ddocode;
        $query = 'SELECT SUM(DETS.AMOUNT) FROM factstg.expendituredet DETS WHERE EXISTS( SELECT 1 FROM factstg.EXPENDITURE EXP1 WHERE DETS.EXPNO = EXP1.EXPNO AND DETS.EXPYEAR = EXP1.EXPYEAR AND DETS.EXPTYPECODE = EXP1.EXPTYPECODE AND DETS.PAOCODE = EXP1.PAOCODE  AND EXP1.VOUCHERDATE >=\'01-APR-19\' AND EXP1.VOUCHERDATE <=\'31-JAN-20\' AND EXP1.BILLSTATUSCODE=\'C\') AND DETS.HEADOFACCOUNT=\'' . $hoa . '\'AND DETS.DCFLAG=\'D\' AND DETS.CVRFLAG=\'' . $vc . '\'';;

        if ($mode == 'LOC') {
            $query .= ' DETS.DDOCODE = \'' . $bmsDdoCode . '\'';
        }

        $result = $db->query($query)->execute();

        if ($result->status == 1) {
            $data = array_values($result->data);
            $exp += count($data[0]) ? $data[0][0] : 0;
        }

        $query = 'SELECT SUM(GROSSAMOUNT) FROM factstg.TOKEN WHERE ISAUTHORIZED=1 AND AUTHORISATIONDATE >= \'01-FEB-2020\' AND HOA=\'' . $this->helpers->getFormattedHoa($transaction->hoa) . '\'';

        if ($mode == 'LOC') {
            $query .= ' DETS.DDOCODE = \'' . $bmsDdoCode . '\'';
        }

        $result = $db->query($query)->execute();

        if ($result->status == 1) {
            $data = array_values($result->data);
            $exp += count($data[0]) ? $data[0][0] : 0;
        }

        $query = 'SELECT SUM(GROSSAMOUNT) FROM factstg.TOKEN WHERE ISAUTHORIZED=1 AND DATALOCK = \'U\' AND TRUNC(AUTHORISATIONDATE) BETWEEN \'01-APR-2019\' AND \'31-JAN-2020\' AND HOA=\'' . $this->helpers->getFormattedHoa($transaction->hoa) . '\'';

        if ($mode == 'LOC') {
            $query .= ' DETS.DDOCODE = \'' . $bmsDdoCode . '\'';
        }

        $result = $db->query($query)->execute();

        if ($result->status == 1) {
            $data = array_values($result->data);
            $exp += count($data[0]) ? $data[0][0] : 0;
        }

        return $exp;
    }

    public function getBillsFromHoa($transaction)
    {
        $vc = substr($transaction->hoa, -2, 1);
        $hoa = $this->helpers->getFormattedHoa($transaction->hoa);
        $query = "SELECT TOKEN.*, TS.REMARKS_DATE FROM factstg.TOKEN JOIN factstg.TOKEN_DOC_SCRUTINY TS ON TS.EXPNO = TOKEN.EXPNO AND TS.EXPYEAR = TOKEN.EXPYEAR AND TS.PAOCODE = TOKEN.PAOCODE AND TS.TOKEN_STATUS = 3 AND TS.IS_LIVE=1 AND TOKEN.HOA='" . $hoa . "' AND TOKEN.CVR_FLAG='" . $vc . "' AND TOKEN.ISAUTHORIZED=0 AND BILLUNDERSCRUTINY='Y' and TOKEN.BILLSTATUSCODE ='U' ORDER BY TS.REMARKS_DATE ASC";
        $db = new BmsDB();
        $result = $db->query($query)->execute();
        if ($result->status == 1) {
            $bmsReadyBills = $result->data;
        } else {
            throw new \Exception('Unable to get bills');
        }

        $transactions = [];
        $totalBills = count($bmsReadyBills['EXPNO']);
        $columnMapping = [
            'EXPNO' => 'tokenno',
            'EXPYEAR' => 'bill_year',
            'PAOCODE' => 'pao_code',
            'GROSSAMOUNT' => 'gross',
            'REMARKS_DATE' => 'govtdate'
        ];
        foreach ($bmsReadyBills as $key => $bill) {

            if (array_key_exists($key, $columnMapping)) {
                foreach ($bill as $key2 => $value) {
                    $transactions[$key2][$columnMapping[$key]] = $value;
                }
            }
//            foreach ($bill as $value) {
//                if ($key == 'EXPNO') {
//                    for ($i = 0; $i < $totalBills; $i++) {
//                        $transactions[$i]['tokenno'] = $value;
//                    }
//                } elseif ($key == 'EXPYEAR') {
//                    for ($i = 0; $i < $totalBills; $i++) {
//                        $transactions[$i]['bill_year'] = $value;
//                    }
//                } elseif ($key == 'PAOCODE') {
//                    for ($i = 0; $i < $totalBills; $i++) {
//                        $transactions[$i]['pao_code'] = $value;
//                    }
//                } elseif ($key == 'GROSSAMOUNT') {
//                    for ($i = 0; $i < $totalBills; $i++) {
//                        $transactions[$i]['gross'] = $value;
//                    }
//                } elseif ($key == 'REMARKS_DATE') {
//                    for ($i = 0; $i < $totalBills; $i++) {
//                        $transactions[$i]['govtdate'] = $value;
//                    }
//                }
//            }

        }

//        dd($transactions);
        return $transactions;
    }

    public function hasBudgetLoc($transaction)
    {
        $totalLocBudget = $this->getTotalLocBudget($transaction);
        $totalExpenditure = $this->getTotalExpenditure($transaction, 'LOC');

        $available = $totalLocBudget - $totalExpenditure;

        return ($available - $transaction->gross) >= 0;

    }

    public function getTotalLocBudget($transaction)
    {
        $exp = 0;
        $db = new BmsDB();
        $vc = substr($transaction->hoa, -2, 1);
        $hoa = substr($transaction->hoa, 0, 4) . ' ' . substr($transaction->hoa, 4, 15);
        $dwaDdoCode = $transaction->users->dwa_ddocode;
        $query = 'SELECT SUM(AMOUNT) from factstg.DDOREC where HOA =\'' . $hoa . '\' and DDOCODE =\'' . $dwaDdoCode . '\' and FINYEAR=2019 and VC =\'' . $vc . '\'';
        $result = $db->query($query)->execute();

        if ($result->status == 1) {
            $data = array_values($result->data);
            $exp += count($data[0]) ? $data[0][0] : 0;
        }

        return $exp * 100000;

    }

}
