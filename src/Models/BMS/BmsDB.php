<?php

namespace App\Models\BMS;

class BmsDB
{
    /**
     * @var Oracle Connection
     */
    protected $conn;

    protected $oracleQuery;

    public function __construct()
    {
        $this->conn = \oci_connect("factstg_readonly", "factstg_readonly", '//dwabmstg.cgg.gov.in:1521/cggulb');
        if (!$this->conn) {
            $m = \oci_error();
            throw new \Exception($m['message'] . PHP_EOL);
        }
        return $this;
    }

    public function query($query)
    {
        $this->oracleQuery = $query;
        return $this;
    }

    public function execute()
    {
        $response = new \stdClass();
        try {
            if (empty($this->oracleQuery)) {
                throw new \Exception('Query cannot be empty');
            }

            $s = \oci_parse($this->conn, $this->oracleQuery);
            \oci_execute($s);
            \oci_fetch_all($s, $output);

            $response->status = 1;
            $response->data = $output;
            $response->message = 'Success';
        } catch (\Exception $e) {
            $response->status = 0;
            $response->data = 0;
            $response->message = $e->getMessage();
        }

        \oci_close($this->conn);

        return $response;
    }
}
