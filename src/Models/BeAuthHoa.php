<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeAuthHoa extends Model
{
    protected $table='be_auth_hoa';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
