<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryEditedData extends Model
{
    protected $table = 'salary_edited_data';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function billmultiple()
    {
        return $this->hasMany('App\BillMultipleParty', 'emp_id', 'employee_id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transactions', 'transaction_id', 'id');
    }
}
