<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 23-10-2019
 * Time: 19:30
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class DTAChequeTransactions extends Model
{
    protected $table = 'dta_cheques_transactions';

    public function chqtransdet()
    {
        return $this->hasOne(Transactions::class, 'id', 'transaction_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transactions::class, 'id', 'transaction_id');
    }

    public function dta_cheques()
    {
        return $this->hasOne(DTACheques::class, 'id', 'dta_cheques_id');
    }
}
