<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CPSMasterDocs extends Model
{
    protected $table='cps_master_docs';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
