<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestReappropriationDetail extends Model
{
    protected $table = 'budget_request_reappropriation_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function budreq()
    {
        return $this->hasOne('App\BudgetRequestReappropriation', 'id', 'budget_request_reappropriation_id');
    }
}
