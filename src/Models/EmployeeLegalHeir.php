<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeLegalHeir extends Model
{
    protected $table = 'employee_legal_heir';

    public function agency()
    {
        return $this->hasOne('App\AgencyDetails', 'id', 'agency_details_id');
    }
}
