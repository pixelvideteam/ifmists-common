<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 09-04-2019
 * Time: 09:13
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArcticSchedulerResults extends Model
{
    protected $table = "arctic_scheduler_results";
}