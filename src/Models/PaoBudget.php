<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoBudget extends Model {

	protected $table='pao_budget';
	protected $guarded = ['id', 'created_at', 'updated_at', 'budrelaxtn_flag'];

	public function hoadesc() {

		return $this->hasOne('App\Hoa','hoa','hoa');
	}

	public function billsddo() {

		return $this->hasMany('App\Transactions','ddocode','ddocode');
	}
	public function headsdesc() {

		return $this->hasOne('App\HeadsDesc','hoa','hoa');
	}
}
