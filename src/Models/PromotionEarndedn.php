<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class PromotionEarndedn extends Model
{
	protected $table="promotion_earndedn";
	protected $guarded=['id'];
	
	public function earndedn()
	{	
		return $this->belongsTo('App\EarnDednList','earndedn_id','id');
	}

	
}