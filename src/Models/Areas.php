<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Areas extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'arealist';

    protected $table = "arealist";
    public $timestamps = false;
    protected $guarded = ["id"];

    /**
     * @param string $areacode
     * @return Areas|mixed|null
     */
    public static function getArea(string $areacode)
    {
        $areas = static::getAllAreas();

        if (!empty($areas[$areacode])) {
            return $areas[$areacode];
        }

        return null;
    }

    /**
     * @return Areas[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public static function getAllAreas()
    {
        $cacheKey = __METHOD__;
        $result = Cache::get($cacheKey);
        if (!$result) {
            $result = static::all()
                ->keyBy('areacode');

            Cache::put($cacheKey, $result, 86400);
        }
        return $result;
    }
}
