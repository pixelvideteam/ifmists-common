<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class IncrementApprovalsEarndedn extends Model
{
	protected $table="increment_approvals_earndedn";
	protected $guarded=['id'];
	
	public function earndedn()
	{	
		return $this->belongsTo('App\EarnDednList','earndedn_id','id');
	}
}