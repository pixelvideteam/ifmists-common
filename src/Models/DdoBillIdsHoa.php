<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DdoBillIdsHoa extends Model
{
//    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'ddo_bill_ids_hoa';

    protected $table = "ddo_bill_ids_hoa";
    protected $guarded = ['id'];

    public function hoadesc()
    {
        return $this->hasOne('App\HeadsDesc', 'hoa', 'hoa');
    }

    public function hoadetails()
    {
        return $this->hasOne('App\Hoa', 'hoa', 'hoa');
    }

    public function ddobillids()
    {
        return $this->hasMany('App\DdoBillIds', 'ddo_bill_ids_hoa_id', 'id');
    }

    public function paoBudget()
    {
        return $this->hasMany('App\PaoBudget', 'hoa', 'hoa');
    }

    public function headsdescdet()
    {
        return $this->hasOne('App\HeadsDesc', 'hoa', 'hoa');
    }

    // public function querydata()
    // {
    // 	return $this->belongsTo('App\Query','query_id','id');
    // }
}
