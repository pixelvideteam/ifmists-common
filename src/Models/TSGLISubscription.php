<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GisCatg;


class TSGLISubscription extends Model
{
    protected $table = 'esr_tsgli_subscriptions';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getEmployeeTSGLISubscriptions($employeeId)
    {
        return self::where('employee_id', $employeeId)->get();
    }
}
