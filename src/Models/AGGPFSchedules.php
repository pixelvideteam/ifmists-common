<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 15-02-2019
 * Time: 19:51
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class AGGPFSchedules extends Model
{
    protected $table = "aggpfschedules";
    protected $guarded = ['id'];
}
