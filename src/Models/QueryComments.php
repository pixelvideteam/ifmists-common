<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class QueryComments extends Model {

    protected $table = "query_comments";
    public $timestamps = false;

    protected $guarded = ["id"];
    protected $dateFormat = 'Y-m-d H:i:s';

    public function getDateFormat()
    {
        return 'Y-m-d H:i:s';
    }

    public function commented_user() {

        return $this->hasOne('App\User','id','comment_by')->select(['id', 'name']);
        //return $this->belongsTo('id','comment_by','App\User');
    }

    public function query_docs(){
        return $this->hasMany('App\QueriesDocs','comment_id','id');
    }

}