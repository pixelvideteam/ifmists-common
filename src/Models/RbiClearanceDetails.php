<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RbiClearanceDetails extends Model {

	//
    protected $table='rbi_clearance_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];
	public function inctypedet() {
		return $this->hasOne('App\Inctype','id','inctype');
	}

	// public function inctypedet()
	// {	
	// 	return $this->belongsTo('Inctype','inctype','id');
	// }


	public function exptypedet() {
		return $this->hasOne('App\Exptype','id','exptype');
	}

}
