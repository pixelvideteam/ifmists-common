<?php namespace App;

use App\Models\BRO\BroSections;
use App\Models\BRO\SectionOfficerHodMapping;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Rennokki\QueryCache\Traits\QueryCacheable;

class HODList extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'hod_list';

    protected $table = "hod_list";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function department()
    {
        return $this->belongsTo('App\Department', 'dept_id', 'id');
    }

    public function heads()
    {
        return $this->hasMany('App\HeadsDesc', 'hod_id', 'id');
    }

    public function parentDepartment()
    {
        return $this->hasOne(HODList::class, 'sdeptcode', 'sdeptcode')
            ->where('deptcode', DB::raw("concat(sdeptcode, '01')"));
    }


    // public function empmaster()
    // {
    // 	return $this->belongsTo('App\EmployeeMaster','employee_id','id');
    // }

    public function section()
    {
        return $this->hasOneThrough(BroSections::class, SectionOfficerHodMapping::class, 'hod_id', 'id');
    }
}
