<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class EmployeeLocationList extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'employee_location_list';

    protected $table = "employee_location_list";
    protected $guarded = ["id"];
}
