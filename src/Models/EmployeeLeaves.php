<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class EmployeeLeaves extends Model{

	protected $table="employee_leaves";
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function details() 
	{
		return $this->hasMany('App\EmployeeLeavesDetails','employee_leaves_id','id');
	}

}
