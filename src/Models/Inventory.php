<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class Inventory extends Model{

		protected $table="inventory";
		public $timestamps = false;

		protected $guarded=['id'];
	}