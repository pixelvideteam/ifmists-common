<?php namespace App;

use App\Models\Dwa\DwaAreas;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PaoDdoAuditorMap extends Model {

	protected $table='pao_ddo_auditor';
	//Add more migrated codes here if more than 10 create a DB Table for this
	const MIGRATED_AUDITORS = [
        '3401SA01' => '3201SA01', // new => old
    ];
    const MIGRATED_DDO = [
        '34011002001' => '32011002001', // new => old
        '35012301001' => '33012301001',
        '35012301005' => '33012301005',
        '35012301006' => '33012301006',
        '35012202002' => '33012202002',
        '35012202003' => '33012202003',
        '34012302002' => '32012302002',
        '34012302001' => '32012302001',
    ];

	public function auditordesg() {

		return $this->hasOne('App\User','username','auditor');
	}

	public function ddodesg() {

		return $this->hasOne('App\User','username','ddocode');
	}
	public function transtrack() {

		return $this->hasMany('App\TransactionTracking','primary_user','auditor');
	}
	public function ddo_cadre_strength(){
	    return $this->hasMany('App\DdoCadreStrength', 'ddocode','ddocode');
    }
    public function pao() {
        return $this->belongsTo(DwaAreas::class, 'dwa_area_id','id');
    }

    public function ddo() {
        return $this->belongsTo(User::class, 'ddocode','username');
    }
}
