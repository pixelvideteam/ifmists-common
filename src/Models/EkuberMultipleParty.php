<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 27-12-2018
 * Time: 17:39
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;


class EkuberMultipleParty extends Model
{
    protected $table = "ekuber_multiple_party";
    protected $guarded = ['id'];
    protected $fillable = ['ekuber_transaction_id', 'msg_id', 'message', 'txt_id'];

    public function bill_multiple()
    {
        return $this->hasOne(BillMultipleParty::class, 'id', 'bill_multiple_party_id');
    }

    public function pd_multiple()
    {
        return $this->hasOne(Multipleparty::class, 'id', 'multiple_party_id');
    }

    public function ekuber_transaction()
    {
        return $this->hasOne(EkuberTransactions::class, 'id', 'ekuber_transaction_id');
    }

    public function ekuber_payment_file()
    {
        return $this->hasOne(EkuberPaymentFiles::class, 'id', 'ekuber_payment_file_id');
    }

    public function ekuber_dn_transaction()
    {
        return $this->hasOne(EkuberTransactions::class, 'id', 'ekuber_dn_transaction_id');
    }

    public function return_ddoform()
    {
        return $this->hasOne(EKuberReturnsDdoform::class, 'bill_multiple_party_id', 'bill_multiple_party_id');
    }

    public function return_cheques()
    {
        return $this->hasOne(PaoChequesEKuberParties::class, 'bill_multiple_party_id', 'bill_multiple_party_id');
    }
}
