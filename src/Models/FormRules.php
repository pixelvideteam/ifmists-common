<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FormRules extends Model {

	protected $table='form_rules';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
