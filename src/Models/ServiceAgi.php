<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceAgi extends Model
{
    protected $table = 'esr_service_agi';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getAllEmployeeAgiParticulars($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with([
                'dept.designations.desg',
                'prc.scales.scaledetails',
                'desg', 'scale.scaledetails', 'oldpay', 'newpay'
            ])
            ->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)
            ->with([
                'dept.designations.desg',
                'prc.scales.scaledetails',
                'desg', 'scale.scaledetails', 'oldpay', 'newpay'
            ])
            ->first();
    }

    public function dept()
    {
        return $this->hasOne(Department::class, 'id', 'dept_id');
    }

    public function desg()
    {
        return $this->hasOne(DesgCode::class, 'id', 'desg_id');
    }

    public function prc()
    {
        return $this->hasOne(Prc::class, 'id', 'prc_id');
    }

    public function scale()
    {
        return $this->hasOne(Scale::class, 'id', 'scale_id');
    }

    public function oldpay()
    {
        return $this->hasOne(ScaleDetails::class, 'id', 'pay_id_old');
    }

    public function newpay()
    {
        return $this->hasOne(ScaleDetails::class, 'id', 'pay_id_new');
    }
}
