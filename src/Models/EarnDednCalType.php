<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class EarnDednCalType extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'earndedn_cal_type';

    protected $table = 'earndedn_cal_type';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
