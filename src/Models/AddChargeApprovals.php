<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AddChargeApprovals extends Model
{
    protected $table = "add_charge_approvals";
    protected $guarded = ['id'];

    public function empmaster()
    {
        return $this->belongsTo('App\EmployeeMaster', 'employee_id', 'id');
    }

    public function prc()
    {
        return $this->belongsTo('App\Prc', 'prc_id', 'id');
    }

    public function fromdesg()
    {
        return $this->belongsTo('App\DesgCode', 'from_desg_id', 'id');
    }

    public function scale()
    {
        return $this->belongsTo('App\Scale', 'scale_id', 'id');
    }

    public function todesg()
    {
        return $this->belongsTo('App\DesgCode', 'to_desg_id', 'id');
    }

    public function fromdept()
    {
        return $this->belongsTo('App\Department', 'from_dept_id', 'id');
    }

    public function todept()
    {
        return $this->belongsTo('App\Department', 'to_dept_id', 'id');
    }

    public function aud()
    {
        return $this->belongsTo('App\User', 'auduser', 'username');
    }

    public function addearn()
    {
        return $this->hasMany('App\AddChargeEarnings', 'add_charge_approvals_id', 'id');
    }

    public function docs()
    {
        return $this->hasMany('App\AddChargeDocs', 'add_charge_approvals_id', 'id');
    }
}
