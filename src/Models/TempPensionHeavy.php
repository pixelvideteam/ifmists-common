<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempPensionHeavy extends Model
{
    protected $table = "temp_pension_heavy";
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
