<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LpcLoanDetails extends Model 
{
	protected $table='lpc_loan_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
