<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Lpc extends Model
{
	protected $table='lpc';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function empmaster()
	{
		return $this->hasOne('App\EmployeeMaster','id','employee_id');
	}

	public function empearndedn()
	{
		return $this->hasMany('App\LpcDetails','lpc_id','id');
	}

	public function loans()
	{
		return $this->hasMany('App\LpcLoanDetails','lpc_id','id');
	}

    public function transfer()
    {
        return $this->hasOne('App\Transfer', 'id', 'transfers_id');
    }
    public function dept_fs()
    {
        return $this->hasOne('App\DeputationFs', 'id', 'dep_fs_id');
    }
    public function retirement()
    {
        return $this->hasOne('App\EmployeeRetirement', 'id', 'retirement_id');
    }
    public function expired()
    {
        return $this->hasOne('App\EmployeeExpired', 'id', 'expired_id');
    }
}
