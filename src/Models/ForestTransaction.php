<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ForestTransaction extends Model {

    protected $table='dta_forest_transactions';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
