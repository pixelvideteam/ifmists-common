<?php

namespace App;

use App\Models\Concerns;
use Illuminate\Database\Eloquent\Model;

class AlternatePhone extends Model
{
    use
        Concerns\QueryBuilder,
        Concerns\PhoneNumber,
        Concerns\BlindIndex;

    protected $table = 'alternate_phones';

    /**
     * @var string[]
     * Blind Indexes columns in DB
     */
    protected $encrypted = [
        'phone' => 'phone_bidx',
    ];

    public function user()
    {
        $this->belongsTo('App\Users');
    }
}
