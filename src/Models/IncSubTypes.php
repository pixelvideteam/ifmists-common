<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class IncSubTypes extends Model{

	protected $table="inc_subtypes";
	public $timestamps = false;
	protected $guarded=['id'];
	
}