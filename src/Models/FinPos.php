<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FinPos extends Model{

	protected $table="finpos";
	// public $timestamps = false;
	protected $guarded=['id'];

	public function res()
	{	
		return $this->hasMany('App\FinRes','finposid','id');
	}

	public function outflows()
	{	
		return $this->hasMany('App\FinOutflows','finposid','id');
	}

	public function inflows()
	{	
		return $this->hasMany('App\FinInflows','finposid','id');
	}	
}