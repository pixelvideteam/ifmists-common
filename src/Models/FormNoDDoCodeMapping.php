<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 22-04-2019
 * Time: 07:54
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class FormNoDDoCodeMapping extends Model
{
    protected $table='formno_ddocode_mapping';
}