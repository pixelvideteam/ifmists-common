<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class SuspensionPercentages extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'suspension_percentages';

    protected $table = 'suspension_percentages';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
