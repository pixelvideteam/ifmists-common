<?php namespace App;

use App\Models\Ekuber\TransTypeStatus;
use Illuminate\Database\Eloquent\Model;

class PaoCheques extends Model
{

    const STATUS_APRROVED = 0;
    const STATUS_PENDING = 1;
    const STATUS_REJECTED = 21;

	protected $table='pao_cheques';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function paochqtrans()
	{
		return $this->hasMany('App\PaoChequesTransactions','pao_cheques_id','id');
	}

    public function transactions()
    {
        return $this->belongsToMany(Transactions::class,'pao_cheques_transactions','pao_cheques_id','transactions_id');
    }
	public function approvedby()
	{
		return $this->hasOne('App\User','id','approved_by');
	}
	public function branch()
	{
		return $this->hasOne('App\PaoBranch','id','branch_id');
	}
    public function paymentfilesmap()
    {
        return $this->hasMany('App\ChequePaymentFileMapping','cheque_id','id')->orderBy("id", "desc");
    }
    public function getdnfile()
	{
		return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')->where('transtype', '=', TransTypeStatus::DN)->orderBy('created_at');
	}
	public function ekuberfilesent()
	{
		return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')->where('transtype', '=', TransTypeStatus::SENT)->orderBy('created_at');
	}
	public function ekuberpaymentfile()
	{
		return $this->hasOne('App\EkuberPaymentFiles',trim('ekuber_filename'),trim('ekuber_filename'))
            ->where('ekuber_payment_files.client_id','=','1');
	}
	public function ekuberpaymentfilesready()
	{
		return $this->hasMany('App\EkuberPaymentFiles',trim('ekuber_filename'),trim('ekuber_filename'))->where('client_id','=','1')->where('reprocess_status',2);
	}
    public function ekuberpaymentfilesreadygen()
    {
        return $this->hasMany('App\EkuberPaymentFiles',trim('ekuber_filename'),trim('ekuber_filename'))->where('client_id','=','1')->where('reprocess_status',5)->where('is_sent_to_ekuber',0)->where('is_ready_for_download',1);
    }
    public function ekuber_tranasctions(){
        return $this->hasMany('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')
            ->where('client_id','=',1)
            ->orderBy('created_at');
    }
    public function ekuber_ack_accept(){
        return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')
            ->where('client_id','=',1)
            ->where('transtype','=',TransTypeStatus::ACK_ACCP);
    }
    public function ekuber_ack_reject(){
        return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')
            ->where('client_id','=',1)
            ->where('transtype','=',TransTypeStatus::ACK_RJCT);
    }
}
