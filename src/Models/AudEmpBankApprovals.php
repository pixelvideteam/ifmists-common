<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AudEmpBankApprovals extends Model
{
    protected $table = 'aud_emp_bank_approvals';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function ifscdetail()
    {
        return $this->hasOne('App\BankIfsc', 'ifsccode', 'ifsccode');
    }

    public function empmaster()
    {
        return $this->hasOne('App\EmployeeMaster', 'id', 'employee_id');
    }

    public function auduser()
    {
        return $this->hasOne('App\User', 'username', 'audempcode');
    }
}
