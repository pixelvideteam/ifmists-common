<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoBranchCounter extends Model {

	protected $table='pao_branch_counter';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function countermh() {

		return $this->hasMany('App\PaoCounterMh','pao_branch_counter_id','id');
	}
	public function branch() {
		return $this->hasOne('App\PaoBranch','id','branch_id');
	}

}
