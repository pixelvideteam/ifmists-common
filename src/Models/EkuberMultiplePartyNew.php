<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 22-04-2019
 * Time: 22:51
 */
declare(strict_types=1);

namespace App;

use App\Repository\CommonRepository\FinancialYearRepository;
use Illuminate\Database\Eloquent\Model;

class EkuberMultiplePartyNew extends Model
{
    protected $table="ekuber_multiple_party_new";
    protected $guarded=['id'];
    protected $fillable=['ekuber_transaction_id','msg_id','message','txt_id'];
    public function bill_multiple(){
        return $this->hasOne(BillMultipleParty::class,'id','bill_multiple_party_id');
    }
    public function pd_multiple(){
        return $this->hasOne(Multipleparty::class,'id','multiple_party_id');
    }

    public function ekuber_transaction(){
        return $this->hasOne(EkuberTransactions::class,'id','ekuber_transaction_id');
    }
    public function ekuber_payment_file(){
        return $this->hasOne(EkuberPaymentFiles::class,'id','ekuber_payment_file_id');
    }
    public function ekuber_dn_transaction(){
        return $this->hasOne(EkuberTransactions::class,'id','ekuber_dn_transaction_id');
    }
    public function return_ddoform(){
        return $this->hasOne(EKuberReturnsDdoform::class,'bill_multiple_party_id','bill_multiple_party_id');
    }
    public function return_cheques(){
        return $this->hasOne(PaoChequesEKuberParties::class,'bill_multiple_party_id','bill_multiple_party_id');
    }
    public function billMultipleParty()
    {
        return $this->belongsTo('App\Models\Ekuber\BillMultipleParty', 'bill_multiple_party_id');
    }
    public function multiple_party_id()
    {
        return $this->hasMany(Multipleparty::class,'id','multiple_party_id');

    }
    public function pd_multiple_withinfinyear(){
        $finYearDateArr = FinancialYearRepository::getFinYearDateForTransactions();
        
        return $this->hasOne(Multipleparty::class,'id','multiple_party_id')->where('created_at','>=',$finYearDateArr['from_date']);
    }
}
