<?php

namespace App\Models\Receipts;

use App\OnlineBankScroll;
use Illuminate\Database\Eloquent\Model;
use App\CyberScroll;

class ReceiptUploadedFile extends Model
{
    protected $table = 'receipts_uploaded_files';

    const FILE_TYPE_MANUAL_CHALLAN = 1;
    const FILE_TYPE_CYBER_TREASURY_FILE = 2;

    const CRON_RUNNING_FLAG_FREE = 0;
    const CRON_RUNNING_FLAG_FAILED = 21;
    const CRON_RUNNING_FLAG_IMPORTED_FILE_DATA = 1;
    const CRON_RUNNING_FLAG_IN_PROGRESS = 2;

    const VERIFICATION_RUNNING_CRON_FLAG_FREE = 0;
    const VERIFICATION_RUNNING_CRON_FLAG_VERIFYING = 1;

    const DONE_FLAG_PENDING_FILE_TO_READ = 0;
    const DONE_FLAG_DATA_IMPORTED_INTO_DB = 1;

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;

    public function cyberscroll()
    {

        return $this->hasMany(CyberScroll::Class, 'receipts_uploaded_file_id', 'id');
    }

    public function onlinebankscroll()
    {
        return $this->hasMany(OnlineBankScroll::Class, 'receipts_uploaded_file_id', 'id');

    }


}
