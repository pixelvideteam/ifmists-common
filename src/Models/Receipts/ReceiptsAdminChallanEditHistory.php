<?php

namespace App\Models\Receipts;

use Illuminate\Database\Eloquent\Model;

class ReceiptsAdminChallanEditHistory extends Model
{
    protected $table = 'receipts_admin_challan_edit_histories';
}
