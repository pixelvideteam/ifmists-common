<?php

namespace App\Models\Receipts;

use Illuminate\Database\Eloquent\Model;

class ReceiptsGstJsonFile extends Model
{
    protected $table = 'receipts_gst_json_files';
}
