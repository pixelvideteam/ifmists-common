<?php

namespace App\Models\Receipts;

use App\CyberScroll;
use App\OnlineBankScroll;
use Illuminate\Database\Eloquent\Model;

class Receipts extends Model
{
    protected $table = 'receipts_treceipts';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    const STATUS_PENDING_APPROVALS = 0;
    const STATUS_IMPACT_PAID = 1;
    const STATUS_REJECTED = 21;

    const TRANSTYPE_MANUAL_CHALLAN = 1;
    const TRANSTYPE_CYBER_TREASURY = 2;
    const TRANSTYPE_DTA_ADJUSTMENT = 3;
    const TRANSTYPE_PD_ADJUSTMENT = 4;
    const TRANSTYPE_AM = 5;
    const TRANSTYPE_RETURNS = 6;
    const TRANSTYPE_CHALLAN_RECEIPTS = 8;

    public function onlinebankscroll()
    {
        return $this->hasOne(OnlineBankScroll::class, 'transid', 'tokenno');
    }

    public function cyberscroll()
    {
        return $this->hasOne(CyberScroll::class, 'challanno', 'tokenno');
    }

}
