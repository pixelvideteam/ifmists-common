<?php

namespace App\Models\Receipts;

use Illuminate\Database\Eloquent\Model;

class ReceiptsEkuberGstFileData extends Model
{
    protected $table = 'receipts_ekuber_gst_file_data';
}
