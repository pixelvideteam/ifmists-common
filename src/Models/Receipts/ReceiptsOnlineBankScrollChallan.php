<?php

namespace App\Models\Receipts;

use App\Models\ReceiptsManualChallanDetails;
use Illuminate\Database\Eloquent\Model;

class ReceiptsOnlineBankScrollChallan extends Model
{
    protected $table = 'receipts_online_bank_scrolls';

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;
    const AUTHORISED_USER = 'rec-admin';

    public function manual_challan()
    {
        return $this->hasOne(ReceiptsManualChallanDetails::class, 'id', 'receipts_manula_challan_id');
    }

}
