<?php

namespace App\Models\Receipts;

use Illuminate\Database\Eloquent\Model;

class ReceiptBankScrollChallanEditApprovals extends Model
{
    protected $table = 'receipts_bank_scroll_challan_edit_approvals';

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;

    public function file_data()
    {
        return $this->hasOne(ReceiptFileData::class, 'id','receipts_file_data_id');
    }

}
