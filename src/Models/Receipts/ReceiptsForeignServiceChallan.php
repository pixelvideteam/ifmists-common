<?php

namespace App\Models\Receipts;

use Illuminate\Database\Eloquent\Model;

class ReceiptsForeignServiceChallan extends Model
{
    protected $table = 'receipts_foreign_service_challans';

    const TYPE_REGULAR = 1;
    const TYPE_ARREARS = 2;

    public function challandet() {
        return $this->hasOne('App\Models\ReceiptsManualChallanDetails', 'id', 'receipts_manual_challans_id');
    }
    public function empdet() {
        return $this->hasOne('App\FsEmployeeMaster', 'id', 'employee_id');
    }
}
