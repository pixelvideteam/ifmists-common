<?php

namespace App\Models\Receipts;

use Illuminate\Database\Eloquent\Model;

class ReceiptFileData extends Model
{
    protected $table = 'receipts_file_data';

    const STATUS_PENDING = 0;
    const STATUS_VERIFIED_FROM_IMPACT = 1;

    const TRANSACTION_TYPE_PAYMENT = 1;
    const TRANSACTION_TYPE_RECEIPT = 2;

    public function uploaded_file()
    {
        return $this->hasOne(ReceiptUploadedFile::class, 'id' , 'receipts_uploaded_file_id');
    }

    public function file_data()
    {
        return $this->hasMany(ReceiptFileData::class, 'receipts_uploaded_file_id');
    }

}
