<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class ChangeScaleType extends Model
{
	protected $table="change_scale_type";
	protected $guarded=['id'];
	
	public function empmaster()
	{	
		return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	}

	public function toscaletype()
	{	
		return $this->belongsTo('App\ScaleTypes','new_scale_type_id','id');
	}

	public function fromscaletype()
	{	
		return $this->belongsTo('App\ScaleTypes','old_scale_type_id','id');
	}

	public function fromdesg()
	{	
		return $this->belongsTo('App\DesgCode','old_desg_id','id');
	}

	public function todesg()
	{	
		return $this->belongsTo('App\DesgCode','new_desg_id','id');
	}

	// public function empearndedn()
	// {	
	// 	return $this->hasMany('App\AddChargeEarnings','add_charge_id','id');
	// }	
}