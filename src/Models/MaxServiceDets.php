<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class MaxServiceDets extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'max_service_details';

    protected $table = "max_service_details";
    // public $timestamps = false;

    protected $guarded = ["id"];
}
