<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SchemeDhSdh extends Model 
{
	protected $table='scheme_dh_sdh';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
