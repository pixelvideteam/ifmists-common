<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DesgScaleMapping extends Model {

	protected $table='desg_scale_mapping';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function scale()
	{	
		return $this->hasOne('App\Scale','id','scale_id');
	}
}
