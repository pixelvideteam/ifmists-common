<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  PDChequePaymentFileMapping extends Model
{
    //pd_cheque_payment_file_mapping
    protected $table = "pd_cheque_payment_file_mapping";
    public function cheque(){
        return $this->hasOne(Transactions::class,'id','pd_cheque_trans_id');
    }


    public function paymentfiles(){
        return $this->hasOne(EkuberPaymentFiles::class,'id','ekuber_pd_payment_file_id');
    }


}
