<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RbiDataDetails extends Model {
    protected $table='rbi_data_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];
	public function details() {
		return $this->belongsTo('App\RbiDetails','detail_id','id');
	}

}
