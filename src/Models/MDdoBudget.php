<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MDdoBudget extends Model
{
    protected $table='mddobudget';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
