<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormTypeRanges extends Model
{
    protected $table = 'formtype_ranges';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function hasRangeFor($formTypeId, $onDay)
    {
        $inRangeForTokenGeneration = false;
        $formTypeRanges = self::where('formtype_id', $formTypeId)->get();
        foreach ($formTypeRanges as $formTypeRange) {
            if (in_array($onDay, range($formTypeRange->open_day, $formTypeRange->close_day, 1))) {
                $inRangeForTokenGeneration = true;
                break;
            }
        }
        return $inRangeForTokenGeneration;
    }
}
