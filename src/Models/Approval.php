<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    protected $table = "approvals";
    // public $timestamps = false;

    protected $guarded = ["id"];

    public function dashtrans()
    {
        return $this->hasMany('App\DashTransaction', 'approval_id', 'id');
    }

    public function dashuser()
    {
        return $this->hasOne('App\DashUser', 'id', 'approved_by');
    }
}
