<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demotions extends Model
{
    protected $table = 'demotions';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function basic()
    {
        return $this->belongsTo('App\MasterScaleDetails', 'master_scale_details_id', 'id');
    }

    public function todesg()
    {
        return $this->belongsTo('App\DesgCode', 'to_desgcode_id', 'id');
    }

    public function fromdesg()
    {
        return $this->belongsTo('App\DesgCode', 'from_desgcode_id', 'id');
    }

    public function empearndedn()
    {
        return $this->hasMany(DemotionEarndedn::class, 'demotion_id', 'id');
    }

    public function toscaledets()
    {
        return $this->belongsTo('App\ScaleDetails', 'to_scale_details_id', 'id');
    }

    public function fromscaledets()
    {
        return $this->belongsTo('App\ScaleDetails', 'from_scale_details_id', 'id');
    }

    public function fromprc()
    {
        return $this->belongsTo(Prc::class, 'from_prc_id', 'id');
    }

    public function toprc()
    {
        return $this->belongsTo(Prc::class, 'to_prc_id', 'id');
    }

    public function fromscale()
    {
        return $this->belongsTo(Scale::class, 'from_scale_id', 'id');
    }

    public function toscale()
    {
        return $this->belongsTo(Scale::class, 'to_scale_id', 'id');
    }

    public function fromcader()
    {
        return $this->belongsTo(Cader::class, 'from_cader_id', 'id');
    }

    public function tocader()
    {
        return $this->belongsTo(Cader::class, 'to_cader_id', 'id');
    }
}
