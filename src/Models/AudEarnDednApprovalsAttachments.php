<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AudEarnDednApprovalsAttachments extends Model
{
    protected $table = "aud_earndedn_approvals_attachments";
    protected $guarded = ["id"];
}
