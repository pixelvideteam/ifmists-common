<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class FormNo extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'formno';

    protected $table = 'formno';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function formtypes()
    {
        return $this->hasMany('App\FormType', 'formno_id', 'id')->orderBy("priority", "asc");
    }
}
