<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;

class AppNotification extends Model
{
    protected $table="app_notifications";

    const APPROVE_REJECT_FLAG_APPROVED = 1;
    const APPROVE_REJECT_FLAG_REJECTED = 21;

    const NOTIFICATION_TYPE_MOBILE_PUSH_NOTIFICATION = 5;

    const USER_ROLE_ARRAY_GOVT_ACTION = [37];
}
