<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestReappropriationDoc extends Model
{
    protected $table = 'budget_request_reappropriation_docs';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
