<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeLocations extends Model 
{
	protected $table='employee_locations';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
