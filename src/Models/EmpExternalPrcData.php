<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpExternalPrcData extends Model {

	protected $table='emp_external_prc_data';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function earndedn()
    {
        return $this->hasOne('App\EarnDednList', 'id', 'earndedn_id');
    }
}
