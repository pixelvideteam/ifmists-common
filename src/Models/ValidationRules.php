<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 21-11-2019
 * Time: 21:28
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValidationRules extends Model
{
    protected $table = 'validation_rules';

    public function ruleclientmap()
    {
        return $this->hasone(RuleClientMapping::class, 'rule_id', 'id');
    }

    public function columnmap()
    {
        return $this->hasMany(ValidationRulesColumns::class, 'validation_rule_id', 'id');
    }
}
