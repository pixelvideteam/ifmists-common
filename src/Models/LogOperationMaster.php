<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogOperationMaster extends Model
{
    protected $table = 'log_operation_master';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function logOperationEach()
    {
        return $this->hasMany(LogOperationEach::class, 'log_operation_master_id', 'id');
    }
}
