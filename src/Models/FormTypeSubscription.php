<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FormTypeSubscription extends Model {

	protected $table='formtype_subscription';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
