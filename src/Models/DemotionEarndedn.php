<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemotionEarndedn extends Model
{
    protected $table = 'demotion_earndedn';


    public function earndedn()
    {
        return $this->belongsTo('App\EarnDednList','earndedn_id','id');
    }
}
