<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeBillIdLog extends Model
{
    protected $table = 'change_bill_id_logs';
}
