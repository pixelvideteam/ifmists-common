<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AMSalaryAdjustment extends Model
{

    protected $table = 'am_salary_adjustments';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
