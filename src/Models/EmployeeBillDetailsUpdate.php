<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeBillDetailsUpdate extends Model {

	protected $table='emp_bill_dets_update';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	
}
