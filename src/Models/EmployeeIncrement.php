<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeIncrement extends Model {

	protected $table='employee_increments';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function emp()
    {
        return $this->hasOne('App\EmployeeMaster', 'id', 'employee_id');
    }

    public function monthdesc()
    {
        return $this->hasOne('App\Months', 'month_no', 'month');
    }
}
