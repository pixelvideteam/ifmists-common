<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BillFormTypeUploads extends Model 
{
	protected $table='bill_formtype_uploads';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function formtypeuploads() {

		return $this->hasOne('App\FormTypeUploads','id','formtype_upload_id');
	}
}
