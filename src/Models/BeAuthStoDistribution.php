<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeAuthStoDistribution extends Model
{
    protected $table = 'be_auth_sto_distribution';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;

    public function ddodetails()
    {
        return $this->hasMany(BeAuthStoDistDdo::class, 'be_auth_sto_dist_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
