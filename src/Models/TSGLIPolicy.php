<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GisCatg;


class TSGLIPolicy extends Model
{
    protected $table = 'esr_tsgli_policies';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getEmployeeTSGLIPolicies($employeeId)
    {
        return self::where('employee_id', $employeeId)->get();
    }
}
