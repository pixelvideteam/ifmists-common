<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class LeavesMaster extends Model{

    const LEAVE_CASUAL = 1;
    const LEAVE_EARNED = 11;
    const LEAVE_HALFPAY = 12;
    const LEAVE_COMMUTED = 17;

    protected $table="leaves";
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function empleavedetails() {

		return $this->hasMany('App\EmployeeLeavesDetails','leaves_id','id');
	}
}
