<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillMultiplePartyTracking extends Model
{
    protected $table = 'bill_multiple_party_tracking';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
