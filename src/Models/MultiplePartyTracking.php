<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultiplePartyTracking extends Model
{
    protected $table = 'multiple_party_tracking';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
