<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FamilyDetails extends Model
{
    protected $table = 'family_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function maritalStat()
    {
        return $this->hasOne(MaritalStatus::class, 'id', 'marital_status');
    }

    public function familyRelations()
    {
        return $this->hasOne(FamilyRelations::class, 'id', 'relation_with_employee');
    }

    public static function getAllEmployeeRelations($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with(['maritalStat', 'familyRelations'])
            ->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)
            ->with(['maritalStat', 'familyRelations'])
            ->first();
    }
}
