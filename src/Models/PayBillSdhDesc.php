<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class PayBillSdhDesc extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'paybill_sdh_desc';

    protected $table = 'paybill_sdh_desc';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $hidden = ["created_at", "updated_at"];
}
