<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeAuthStoBudgetSurrender extends Model
{
    protected $table = 'be_auth_sto_budget_surrender';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
