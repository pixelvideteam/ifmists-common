<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class SubMenus extends Model
{

//    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'sub_menus_';

    protected $table = 'sub_menus';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function menuuser()
    {

        return $this->hasMany('App\SubMenusUser', 'sub_menus_id', 'id');
    }

}
