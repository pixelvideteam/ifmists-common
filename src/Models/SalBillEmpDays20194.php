<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SalBillEmpDays20194 extends Model{

    protected $table="sal_bill_emp_days_2019_4";
    protected $guarded=['id'];

    public function billmultiple()
    {
        return $this->belongsTo('App\BillMultipleParty','bill_multiple_party_id','id');
    }

}