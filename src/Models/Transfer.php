<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $table = "transfer";
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function empmaster()
    {
        return $this->hasMany('App\EmployeeMaster', 'employee_id', 'employee_code');
    }

    public function loc()
    {
        return $this->hasOne('App\EmployeeLocationList', 'id', 'newloc_id');
    }

    public function oldhoa()
    {
        return $this->hasOne('App\DdoBillIds', 'id', 'prev_bill_id');
    }

    public function desg()
    {
        return $this->hasOne('App\DesgCode', 'id', 'old_desg_id');
    }
}
