<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NmrEngEmployee extends Model
{
    protected $table = 'dwa_nmr_eng_employees';
}
