<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    const CONTINGENT_BILL = 3;
    const SALARY_BILL = 4;
    const SUPPLEMENTARY_BILL = 5;
    const PENSION_BILL = 11;
    const WORKS = 6;
    const LAND_ACQUISITION_RESETTLEMENT_REHABILITATION = 7;
    const HAND_RECEIPT = 8;
    const GST = 9;
    const HAND_RECEIPT_OTHER = 10;
    const MISSION_KAKATIYA = 12;
    const CPS = 13;
    const RECOVERY_BILL = 14;
    const HEAVY_ADJ_BILL = 15;
    const CORPORATION_BILL = 20;
    protected $table = 'transaction_types';
}
