<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmployeeTraining extends Model
{
    protected $table = 'esr_employee_training';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getEmployeeTraining($employeeId)
    {
        return self::where('employee_id', $employeeId)->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)->first();
    }

}
