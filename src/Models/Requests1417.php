<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class Requests1417 extends Model{

		protected $table="chequerequest_1417";
		public $timestamps = false;

		protected $guarded=['id'];

		public function requser()
		{	
			return $this->belongsTo('App\User','requestuser','username');
		}

		public function leafs()
		{
			return $this->hasMany('App\Leaves','user','requestuser');
		}

		public function bookdata()
		{
			return $this->hasOne('App\Cheques','requestid','id');
		}
	}