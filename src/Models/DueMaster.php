<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DueMaster extends Model {

	protected $table='due_master';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function caltable()
	{	
		return $this->hasMany('App\EarnDednCal','earndedn_id','earndedn_id');
	}
	public function earndedn()
	{	
		return $this->hasOne('App\EarnDednList','id','earndedn_id');
	}
	// public function newInstance($attributes = [], $exists = false)
	// {
	//     $model = parent::newInstance($attributes, $exists);

	//     $model->setTable($this->getTable());

	//     return $model;
	// }
	// public function getTable()
	// {
	//     return 'due_master';
	// }
}
