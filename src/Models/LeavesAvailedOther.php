<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavesAvailedOther extends Model
{
    protected $table = 'esr_leaves_availed_other';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getEmployeeAllOtherLeaves($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with('leavetype')
            ->orderBy('from_date', 'ASC')
            ->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)
            ->with('leavetype')
            ->first();
    }

    public function leavetype()
    {
        return $this->hasOne(LeavesMaster::class, 'id', 'leave_type');
    }
}
