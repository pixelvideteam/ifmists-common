<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class IncrementArrearsMain extends Model {

	protected $table='increment_arrears_main';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
