<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class GPFOpeningBalance extends Model
{
    use QueryCacheable;

    protected $table = 'gpf_opening_balances';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function employeemaster()
    {
        return $this->hasOne(EmployeeMaster::class, 'id', 'employee_id');
    }

    public function empBillDet()
    {
        return $this->hasMany('App\EmployeeBillDetails', 'employee_id', 'employee_id');
    }
    
    public function bms_multiple()
    {
        return $this->hasMany('App\BillMultipleParty', 'emp_id', 'employee_id');
    }
}
