<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DuplicateSuppSalaryLog extends Model
{
    protected $table = 'duplicate_supp_salary_logs';
}
