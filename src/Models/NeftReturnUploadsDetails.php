<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NeftReturnUploadsDetails extends Model {

	protected $table = "neftreturn_uploads_details";
	public $timestamps = false;

	protected $guarded = ["id"];
	public function neftreturnddoformdets() {
		return $this->hasOne('App\NeftReturnDdoformDetails','id','neftreturn_ddoform_details_id');
	}
}