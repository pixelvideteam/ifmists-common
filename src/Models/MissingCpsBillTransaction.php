<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class MissingCpsBillTransaction extends Model
{
    protected $table = "missing_cps_bills_transactions";
    protected $fillable = ['id','created_at','updated_at'];

    public function cpsbill()
    {
        return $this->hasOne(MissingCpsBill::class, 'id', 'cps_bill_list_id');
    }

    public function transactions()
    {
        return $this->hasOne(Transactions::class, 'id', 'transaction_id');
    }

}
