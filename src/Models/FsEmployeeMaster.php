<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class FsEmployeeMaster extends Model
{
    protected $table = 'fs_employee_master';
}
