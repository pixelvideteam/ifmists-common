<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class SuspensionEarndedn extends Model{

	protected $table="suspension_earndedn";

	protected $guarded=['id'];
	
	// public function admintrans()
	// {	
	// 	return $this->belongsTo('App\Transactions','transid','id');
	// }

	public function earndedn()
	{
		return $this->belongsTo('App\EarnDednList','earndedn_id','id');
	}
}