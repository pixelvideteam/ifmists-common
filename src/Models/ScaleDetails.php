<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class ScaleDetails extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'scale_details';

    protected $table = 'scale_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function scale()
    {
        return $this->hasOne('App\Scale', 'id', 'scale_id');
    }
}
