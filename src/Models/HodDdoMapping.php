<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HodDdoMapping extends Model
{
    protected $table="hod_ddo_mapping";
    protected $guarded=['id'];
    public function ddo()
    {
        return $this->hasOne('App\User','username','ddocode');
    }
}
