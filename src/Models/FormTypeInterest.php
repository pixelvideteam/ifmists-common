<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FormTypeInterest extends Model {

	protected $table='formtype_interest';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
