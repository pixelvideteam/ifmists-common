<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoBudget201819 extends Model
{

    protected $table = 'pao_budget_201819';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function hoadesc() {

        return $this->hasOne('App\Hoa','hoa','hoa');
    }

    public function billsddo() {

        return $this->hasMany('App\Transactions','ddocode','ddocode');
    }
    public function headsdesc() {

        return $this->hasOne('App\HeadsDesc','hoa','hoa');
    }
}