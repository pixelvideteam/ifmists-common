<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestDdoChangeLogs extends Model
{
    protected $table = 'budget_request_ddo_change_logs';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
