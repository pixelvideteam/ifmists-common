<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpcodeValidationException extends Model
{
    protected $table = 'empcode_validation_exception';
}
