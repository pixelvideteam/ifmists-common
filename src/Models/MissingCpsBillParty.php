<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class MissingCpsBillParty extends Model
{
    protected $table = 'missing_cps_bills_parties';

}
