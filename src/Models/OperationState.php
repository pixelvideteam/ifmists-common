<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperationState extends Model
{
    protected $table = 'operation_states';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function logOperationMaster()
    {
        return $this->hasMany(LogOperationMaster::class, 'id', 'operation_states_id');
    }
}
