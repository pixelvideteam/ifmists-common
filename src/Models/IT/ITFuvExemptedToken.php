<?php

namespace App\Models\IT;

use App\Transactions;
use Illuminate\Database\Eloquent\Model;

class ITFuvExemptedToken extends Model
{
    protected $table = 'it_fuv_file_exempted_tokens';

    public function transaction(){
        return $this->hasOne(Transactions::class, 'id', 'transaction_id');
    }
}
