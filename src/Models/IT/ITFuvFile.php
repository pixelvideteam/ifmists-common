<?php

namespace App\Models\IT;

use Illuminate\Database\Eloquent\Model;

class ITFuvFile extends Model
{
    protected $table = 'it_fuv_files';

    public function details()
    {
        return $this->hasMany(ITFuvFileDetail::class, 'it_fuv_file_id', 'id');
    }
}
