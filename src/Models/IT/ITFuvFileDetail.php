<?php

namespace App\Models\IT;

use App\Transactions;
use Illuminate\Database\Eloquent\Model;

class ITFuvFileDetail extends Model
{
    protected $table = 'it_fuv_file_details';

    public function transaction()
    {
        return $this->hasOne(Transactions::class, 'id', 'it_fuv_file_id');
    }
}
