<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MPrcMaster extends Model {

	protected $table='mprc_master';
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
