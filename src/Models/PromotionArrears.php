<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionArrears extends Model {

	protected $table='promotion_arrears';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
