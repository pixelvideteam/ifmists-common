<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SuspensionArrears extends Model {

	protected $table='suspension_arrears';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
