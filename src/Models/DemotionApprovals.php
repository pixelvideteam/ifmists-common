<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemotionApprovals extends Model
{
    protected $table = 'demotion_approvals';

    public function empmaster()
    {
        return $this->belongsTo('App\EmployeeMaster','employee_id','id');
    }

    public function prc()
    {
        return $this->belongsTo('App\Prc','prc_id','id');
    }

    public function desg()
    {
        return $this->belongsTo('App\DesgCode','desg_id','id');
    }

    public function scale()
    {
        return $this->belongsTo('App\Scale','scale_id','id');
    }

    public function cader()
    {
        return $this->belongsTo('App\Cader','cader_id','id');
    }

    public function docs()
    {
        return $this->hasMany('App\DemotionDocs','demotion_approval_id','id');
    }

    public function demo_empearndedn()
    {
        return $this->hasMany('App\DemotionEarndedn','demotion_approval_id','id');
    }


}
