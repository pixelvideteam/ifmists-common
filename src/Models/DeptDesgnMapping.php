<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DeptDesgnMapping extends Model {

	protected $table='department_designation_mapping';
	protected $guarded = ['id', 'created_at', 'updated_at'];


	public function desg()
	{	
		return $this->hasOne('App\DesgCode','id','desgcode_id');
	}


}
