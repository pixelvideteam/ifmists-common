<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class DdoCadStrengthApprovalsDets extends Model
{
	protected $table="ddo_cad_strength_approvals_dets";
	protected $guarded=['id'];
	
	public function desg()
	{	
		return $this->belongsTo('App\DesgCode','desg_id','id');
	}
}