<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class MongoLoginLogs extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'login_logs';
    protected $hidden=["created_at","updated_at"];
}
