<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AudEmpApprovalsDocs extends Model
{
    protected $table = "aud_emp_approvals_docs";
    protected $guarded = ['id'];
}
