<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PartyFiles extends Model {

	protected $table = "party_files";
	public $timestamps = false;

	protected $guarded = ["id"];

	public function trans()
	{	
		return $this->hasOne('App\Transactions','id','transid');
	}
}