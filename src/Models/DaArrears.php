<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DaArrears extends Model {

	protected $table='da_arrears';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
