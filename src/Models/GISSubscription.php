<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GisCatg;


class GISSubscription extends Model
{
    protected $table = 'esr_subscriptions_gis';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getEmployeeGISSubscriptions($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with('category')
            ->orderBy('from_date', 'ASC')
            ->get();
    }

    public function category()
    {
        return $this->hasOne(GisCatg::class, 'id', 'category_id');
    }
}
