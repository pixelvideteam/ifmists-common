<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeSubscriptionsFunds extends Model {

	protected $table='employee_subscriptions_funds';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}