<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class BankIfsc extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'bankifsc';

    protected $table = 'bankifsc';
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $hidden = ["created_at", "updated_at"];
    // protected $guarded = ['id', 'created_at', 'updated_at'];


}
