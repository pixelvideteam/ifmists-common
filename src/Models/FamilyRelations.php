<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class FamilyRelations extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'family_relations';

    protected $table = 'family_relations';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
