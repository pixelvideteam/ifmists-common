<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loans extends Model {

	protected $table='loans';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	protected $hidden = ["created_at", "updated_at"];

	public function loandetails() 
	{
		return $this->hasMany('App\LoanDetails','loan_id','id');
	}

	public function loaninst() 
	{
		return $this->hasMany('App\LoanInstallments','loans_id','id');
	}

	public function earndedn() {

		return $this->hasOne('App\EarnDednList','id','earndedn_id');
	}

	public function bill() 
	{
		return $this->hasOne('App\Loans','id','bill_multiple_party_id');
	}

	public function interest() {

		return $this->hasOne('App\Loans','principal_loan_id','id');
	}
}
