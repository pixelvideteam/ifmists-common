<?php
/**
 * Created by PhpStorm.
 * User: Srikanth Reddy
 * Date: 26-02-2020
 * Time: 07:38
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class DtaEkuberRetChallanLedger extends Model
{
    protected $table = 'dta_ekuber_return_challans_ledger';
    protected $fillable = ['challan_id', 'ddocode', 'transaction_id', 'bill_generation_time'];
}
