<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SalUniqueBill extends Model
{

    protected $table="sal_unique_bill";
    protected $guarded=['id'];
}