<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogOperationEach extends Model
{
    protected $table='log_operation_each';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
