<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class EmpDep extends Model{

		protected $table="emp_deplist";
		// public $timestamps = false;

		protected $guarded=['id'];

		public function hods()
		{	
			return $this->hasMany('App\EmpHOD','depid','id');
		}

		public function ddohods()
		{	
			return $this->hasMany('App\EmpDDOHod','deplist_table_id','id');
		}

		// public function accounts()
		// {	
		// 	return $this->belongsTo('Pdaccount','hoa','hoa');
		// }
	}