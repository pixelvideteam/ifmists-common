<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsNotification extends Model {

	protected $table='sms_notifications';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
