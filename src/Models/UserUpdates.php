<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserUpdates extends Model
{
    protected $table = 'user_updates';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
