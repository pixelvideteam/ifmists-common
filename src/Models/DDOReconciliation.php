<?php
/**
 * Created by PhpStorm.
 * User: shreyan
 * Date: 8/8/2019
 * Time: 1:57 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class DDOReconciliation extends Model {

    protected $table='ddo_reconciliations';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}