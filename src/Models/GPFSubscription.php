<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GisCatg;


class GPFSubscription extends Model
{
    protected $table = 'esr_subscriptions_gpf';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getEmployeeGPFSubscriptions($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->orderBy('date_of_withdrawl', 'ASC')
            ->get();
    }
}
