<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FormRulesUploads extends Model {

	protected $table='form_rules_uploads';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
