<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class SpecialGradePayTypes extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'esr_service_special_grade_pay_types';

    protected $table = 'esr_service_special_grade_pay_types';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
