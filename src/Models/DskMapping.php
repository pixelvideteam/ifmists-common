<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DskMapping extends Model
{
    protected $table='dsk_mapping';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
