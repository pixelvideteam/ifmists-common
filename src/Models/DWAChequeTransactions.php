<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 23-10-2019
 * Time: 19:30
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class DWAChequeTransactions extends Model
{
    protected $table = 'dwa_cheques_transactions';
    public function chqtransdet()
    {
        return $this->hasOne(Transactions::class,'id','transaction_id')
            ->where('ddocode', 'LIKE', '%w')
            ->where('created_at', '>=', '2019-03-01 00:00:00');
    }

    public function dwachqdet()
    {
        return $this->hasOne(DWACheques::class,'id','dwa_cheques_id');
    }
}
