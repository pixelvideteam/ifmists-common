<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class AdditionalFundAddresses extends Model
{
    protected $table = 'additional_funds_addresses';
}
