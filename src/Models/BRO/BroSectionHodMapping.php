<?php

namespace App\Models\BRO;

use App\HODList;
use Illuminate\Database\Eloquent\Model;

class BroSectionHodMapping extends Model
{
    protected $table = "bro_section_hod_mapping";

    public function user_section() {
        return $this->hasMany(BroSectionUsersMapping::class, 'section_id', 'section_id');
    }

    public function section()
    {
        return $this->hasOne(BroSections::class, 'id', 'section_id');
    }

    public function hod()
    {
        return $this->hasOne(HODList::class, 'id', 'hod_id');
    }
}
