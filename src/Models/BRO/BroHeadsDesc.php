<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class BroHeadsDesc extends Model
{
    protected $table = 'bro_heads_desc';

    public function additionalFunds()
    {
        return $this->hasMany(AdditionalFundHoas::class, 'hoa_id', 'id');
    }

    public function reappropriatedAmounts()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'to_hoa_id', 'id')
            ->where('type', ReappropriationResumption::TYPE_REAPPROPRIATION)
            ->whereIn('status', [ReappropriationResumption::STATUS_APPROVED]);
    }

    public function resumptionAmounts()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'from_hoa_id', 'id')
            ->whereIn('type', [ReappropriationResumption::TYPE_RESUMPTION, ReappropriationResumption::TYPE_REAPPROPRIATION])
            ->whereNull('to_hoa_id')
            ->whereNotIn('status', [ReappropriationResumption::STATUS_REJECTED]);
    }

    public function authorizedReappropriatedAmount()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'to_hoa_id', 'id')
            ->where('type', ReappropriationResumption::TYPE_REAPPROPRIATION)
            ->where('status', ReappropriationResumption::STATUS_APPROVED);
    }

    public function authorizedResumptionAmount()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'from_hoa_id', 'id')
            ->whereIn('type', [ReappropriationResumption::TYPE_RESUMPTION, ReappropriationResumption::TYPE_REAPPROPRIATION])
            ->whereNull('to_hoa_id')
            ->where('status', ReappropriationResumption::STATUS_APPROVED);
    }

    public function pendingReappropriatedAmount()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'to_hoa_id', 'id')
            ->where('type', ReappropriationResumption::TYPE_REAPPROPRIATION)
            ->whereIn('status', [ReappropriationResumption::STATUS_PENDING, ReappropriationResumption::STATUS_FORWARDED, ReappropriationResumption::STATUS_RETURNED, ReappropriationResumption::STATUS_EDITTED]);
    }

    public function pendingResumptionAmount()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'from_hoa_id', 'id')
            ->whereIn('type', [ReappropriationResumption::TYPE_RESUMPTION, ReappropriationResumption::TYPE_REAPPROPRIATION])
            ->whereNull('to_hoa_id')
            ->whereIn('status', [ReappropriationResumption::STATUS_PENDING, ReappropriationResumption::STATUS_FORWARDED, ReappropriationResumption::STATUS_RETURNED, ReappropriationResumption::STATUS_EDITTED]);
    }

    public function authorizedBros()
    {
        return $this->hasMany(BroHoas::class, 'hoa_id', 'id')
            ->whereHas('authorizedBros', function ($query) {
                $query->select('id', 'status');
            });
//            ->where('status', BudgetReleaseOrder::STATUS_APPROVED);
    }

    public function pendingBros()
    {
        return $this->hasMany(BroHoas::class, 'hoa_id', 'id')
            ->whereHas('pendingBros', function ($query) {
                $query->select('id', 'status');
            });
//            ->where('status', BudgetReleaseOrder::STATUS_PENDING);
    }

    public function broOldData () {
        return $this->hasMany('App\Models\BRO\BroOldData', 'hod_id', 'hod_id');
    }

    public function department() {
        return $this->hasOne('App\HODList', 'id', 'hod_id');
    }

    public function budgetYears() {
        return $this->hasOne('App\Models\BRO\BudgetFinYear', 'id', 'budget_year');
    }
}
