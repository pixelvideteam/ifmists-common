<?php

namespace App\Models\BRO;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AdditionalFundsTracking extends Model
{
    protected $table = 'additional_funds_tracking';

    public function primary_user () {
        return $this->hasOne(User::class, 'id', 'primary_user_id');
    }

    public function secondary_user () {
        return $this->hasOne(User::class, 'id', 'secondary_user_id');
    }
}
