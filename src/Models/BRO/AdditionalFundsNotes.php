<?php

namespace App\Models\BRO;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AdditionalFundsNotes extends Model
{
    protected $table = 'bro_additional_funds_notes';

    const STATUS_ADDED = 1;
    const STATUS_REMOVED = 21;

    public function addedBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
