<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class BudgetFinYear extends Model
{
    protected $table = 'budget_fin_year';
}
