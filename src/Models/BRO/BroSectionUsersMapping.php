<?php

namespace App\Models\BRO;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BroSectionUsersMapping extends Model
{
    protected $table = "bro_section_users_mapping";

    public function section_users() {
        return $this->hasMany(self::class, 'section_id', 'section_id');
    }

    public function section()
    {
        return $this->hasOne(BroSections::class, 'id', 'section_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
