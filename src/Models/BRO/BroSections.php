<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class BroSections extends Model
{
    protected $table = 'bro_sections';
}
