<?php

namespace App\Models\BRO;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ReappropriationResumptionHistory extends Model
{
    public $timestamps = false;
    protected $table = 'reappropriation_resumption_history';

    public function performedBy()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }
}
