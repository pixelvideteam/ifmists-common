<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class BroHoas extends Model
{
    protected $table = 'bro_hoas';

    const STATUS_AUTHORIZED = 1;
    const STATUS_PENDING = 0;
    const STATUS_REJECTED = 21;
    const STATUS_FORWARDED = 2;
    const STATUS_RETURNED = 3;
    const STATUS_EDITTED = 4;


    public function hoa()
    {
        return $this->hasOne(BroHeadsDesc::class, 'id', 'hoa_id');
    }

    public function pendingBros()
    {
        return $this->hasMany(BudgetReleaseOrder::class, 'id', 'bro_id')
            ->whereIn('status', [BudgetReleaseOrder::STATUS_PENDING, BudgetReleaseOrder::STATUS_FORWARDED, BudgetReleaseOrder::STATUS_EDITTED, BudgetReleaseOrder::STATUS_RETURNED]);
    }

    public function authorizedBros()
    {
        return $this->hasMany(BudgetReleaseOrder::class, 'id', 'bro_id')
            ->where('status', BudgetReleaseOrder::STATUS_APPROVED);
    }

    public function additionalFunds()
    {
        return $this->hasMany(AdditionalFundHoas::class, 'hoa_id', 'hoa_id');
    }

    public function reappropriatedAmounts()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'to_hoa_id', 'hoa_id')
            ->where('type', ReappropriationResumption::TYPE_REAPPROPRIATION)
            ->where('status', ReappropriationResumption::STATUS_APPROVED);
    }

    public function resumptionAmounts()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'from_hoa_id', 'hoa_id')
            ->whereIn('type', [ReappropriationResumption::TYPE_RESUMPTION, ReappropriationResumption::TYPE_REAPPROPRIATION])
            ->whereNull('to_hoa_id')
            ->where('status', ReappropriationResumption::STATUS_APPROVED);
    }

    public function authorizedBroHoas()
    {
        return $this->hasMany(BroHoas::class, 'hoa_id', 'hoa_id')
            ->where('status', self::STATUS_PENDING);
    }
}
