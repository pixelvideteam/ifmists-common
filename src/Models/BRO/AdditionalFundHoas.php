<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class AdditionalFundHoas extends Model
{
    protected $table = 'additional_funds_hoas';

    public function hoa()
    {
        return $this->hasOne(BroHeadsDesc::class, 'id', 'hoa_id');
    }
}
