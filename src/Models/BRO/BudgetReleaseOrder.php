<?php

namespace App\Models\BRO;

use App\HODList;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BudgetReleaseOrder extends Model
{
    protected $table = 'budget_release_order';
    public static $tablename = 'budget_release_order';

    const PROVISIONS = [
        1 => 'From the BE Provision 2020-21',
        2 => 'As Additional Funds',
        3 => 'As Reappropriated'
    ];

    const SHARE_OF_STATE = 1;
    const SHARE_OF_CENTRAL = 2;

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;
    const STATUS_FORWARDED = 2;
    const STATUS_RETURNED = 3;
    const STATUS_EDITTED = 4;
    const STATUS_REVERTED = 5;

    const STATUSES = [
      0 => 'INITIATED',
      1 => 'APPROVED',
      2 => 'FORWARDED',
      3 => 'RETURNED',
      4 => 'EDITED',
      21 => 'REJECTED',
        5 => 'REVERTED'
    ];

    const ALIASNAMES = [
        'updated_by' => 'Updated By',
        'status' => 'Status',
    ];

    const BRO_AMOUNT_CONVERTED_BY = 100000;

    const BRO_AMOUNT_DISPLAY_IN = '(Rs. in Lakhs)';
    const ADDL_FUND_AMOUNT_DISPLAY_IN = '(Rs. in Lakhs)';
    const REAPP_AMOUNT_DISPLAY_IN = '(Rs. in Lakhs)';

    const BRO_MODULE = 1;
    const ADDITIONAL_FUNDS_MODULE = 2;
    const REAPPROPRIATION_RESUMPTION_MODULE = 3;

    /*public function getShareOfAttribute($value)
    {
        if (empty($value)) {
            return '';
        }
        return ($value == 'C') ? 'Central' : 'State';
    }*/

    /*public function setProvisionAttribute($value)
    {
        if (empty($value) && !in_array($value, array_keys(self::PROVISIONS))) {
            return '';
        }
        $this->attributes['provision'] = $value;
    }*/
    /*public function getProvisionAttribute($value)
    {
        if (empty($value)) {
            return '';
        }

        return is_string($value)? $value : self::PROVISIONS[$value];
    }*/


    public function initiatedBy()
    {
        return $this->hasOne(User::class, 'id', 'initiated_by');
    }

    public function currUser()
    {
        return $this->hasOne(User::class, 'id', 'curr_user');
    }
    public function department()
    {
        return $this->hasOne(HODList::class, 'id', 'hod_id');
    }

    public function section()
    {
        return $this->hasOne(BroSections::class, 'id', 'section_id');
    }

    public function amount()
    {
        return $this->hasMany(BroHoas::class, 'bro_id', 'id')
            ->select('bro_id', 'amount');
//            ->sum('amount');
    }

    public function references()
    {
        return $this->hasMany(BroReferences::class, 'bro_id', 'id');
    }

    public function addresses()
    {
        return $this->hasMany(BroAddresses::class, 'bro_id', 'id');
    }

    public function hoas()
    {
        return $this->hasMany(BroHoas::class, 'bro_id', 'id');
    }

    public function forwardedByMe()
    {
        $user = Auth::user();
        return $this->hasMany(BudgetReleaseOrderHistory::class, 'bro_id', 'id')
            ->where('updated_by', $user->id);
    }

    public function broOldData () {
        return $this->hasMany('App\Models\BRO\BroOldData', 'hod_id', 'hod_id');
    }

    public function forwardedBy() {
        return $this->hasMany(BudgetReleaseOrderTracking::class, 'bro_id', 'id')
            ->with(['primary_user' => function ($pqry) {
            $pqry->select('id', 'name', 'username');
        }]);
    }

}
