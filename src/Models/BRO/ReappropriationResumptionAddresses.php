<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class ReappropriationResumptionAddresses extends Model
{
    protected $table = 'reappropriation_resumption_addresses';
}
