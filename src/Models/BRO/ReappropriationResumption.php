<?php

namespace App\Models\BRO;

use App\HODList;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ReappropriationResumption extends Model
{
    protected $table = 'reappropriation_resumption';

    public $timestamps = false;

    public static $tablename = 'reappropriation_resumption';

    protected $fillable = ['remarks'];

    const TYPE_REAPPROPRIATION = 1;
    const TYPE_RESUMPTION = 2;

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;
    const STATUS_FORWARDED = 2;
    const STATUS_RETURNED = 3;
    const STATUS_EDITTED = 4;
    const STATUS_REVERTED = 5;

    const STATUSES = [
        0 => 'INITIATED',
        1 => 'APPROVED',
        2 => 'FORWARDED',
        3 => 'RETURNED',
        4 => 'EDITED',
        21 => 'REJECTED',
        5 => 'REVERTED'
    ];

    const AMOUNT_CONVERTED_BY = 100000;

    public function fromDepartment()
    {
        return $this->hasOne(HODList::class, 'id', 'from_hod_id');
    }

    public function toDepartment()
    {
        return $this->hasOne(HODList::class, 'id', 'to_hod_id');
    }

    public function amount ()
    {
        return $this->hasMany('');
    }
    public function resumptionAmount()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'reapp_resump_id', 'id')
            ->where('type', ReappropriationResumption::TYPE_RESUMPTION);
    }

    public function reappropriatedAmount()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'reapp_resump_id', 'id')
            ->where('type', ReappropriationResumption::TYPE_REAPPROPRIATION)
            ->whereNull('from_hoa_id');
    }

    public function initiatedBy()
    {
        return $this->hasOne(User::class, 'id', 'initiated_by');
    }

    public function currUser()
    {
        return $this->hasOne(User::class, 'id', 'curr_user');
    }

    public function fromHoas()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'reapp_resump_id', 'id')
            ->whereNull('to_hoa_id');
    }

    public function toHoas()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'reapp_resump_id', 'id')
            ->whereNull('from_hoa_id');
    }

    public function hoas()
    {
        return $this->hasMany(ReappropriationResumptionHoas::class, 'reapp_resump_id', 'id')
            ->where(function ($where) {
                $where->whereNull('from_hoa_id')
                    ->where('type', ReappropriationResumption::TYPE_REAPPROPRIATION);
            })
            ->whereIn('type' ,[ReappropriationResumption::TYPE_REAPPROPRIATION, ReappropriationResumption::TYPE_RESUMPTION]);
    }

    public function addresses()
    {
        return $this->hasMany(ReappropriationResumptionAddresses::class, 'reapp_resump_id', 'id');
    }

    public function forwardedByMe()
    {
        $user = Auth::user();
        return $this->hasMany(ReappropriationResumptionHistory::class, 'reapp_resump_id', 'id')
            ->where('updated_by', $user->id);
    }

    public function from_section() {
        return $this->hasOne(BroSections::class, 'id', 'from_section_id');
    }

    public function to_section() {
        return $this->hasOne(BroSections::class, 'id', 'to_section_id');
    }

}
