<?php

namespace App\Models\BRO;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AdditionalFundsHistory extends Model
{
    public $timestamps = false;
    protected $table = 'bro_additional_funds_history';

    public function performedBy()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }
}
