<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class ReappropriationResumptionHoas extends Model
{
    protected $table = 'reappropriation_resumption_hoas';

    public function from_hoa()
    {
        return $this->hasOne(BroHeadsDesc::class, 'id' , 'from_hoa_id');
    }

    public function to_hoa()
    {
        return $this->hasOne(BroHeadsDesc::class, 'id' , 'to_hoa_id');
    }

    public function fromHoaAuthorizedBroHoas()
    {
        return $this->hasMany(BroHoas::class, 'hoa_id', 'from_hoa_id')
            ->where('status', BudgetReleaseOrder::STATUS_APPROVED);
    }

    public function toHoaAuthorizedBroHoas()
    {
        return $this->hasMany(BroHoas::class, 'hoa_id', 'to_hoa_id')
            ->where('status', BudgetReleaseOrder::STATUS_APPROVED);
    }
    public function fromHoaPendingBroHoas()
    {
        return $this->hasMany(BroHoas::class, 'hoa_id', 'from_hoa_id')
            ->where('status', BudgetReleaseOrder::STATUS_PENDING);
    }

    public function toHoaPendingBroHoas()
    {
        return $this->hasMany(BroHoas::class, 'hoa_id', 'to_hoa_id')
            ->where('status', BudgetReleaseOrder::STATUS_PENDING);
    }


    public function fromHoaAdditionalFunds()
    {
        return $this->hasMany(AdditionalFundHoas::class, 'hoa_id', 'from_hoa_id');
    }

    public function toHoaAdditionalFunds()
    {
        return $this->hasMany(AdditionalFundHoas::class, 'hoa_id', 'to_hoa_id');
    }
}
