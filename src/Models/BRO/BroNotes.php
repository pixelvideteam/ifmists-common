<?php

namespace App\Models\BRO;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BroNotes extends Model
{
    protected $table = 'bro_notes';

    const STATUS_ADDED = 1;
    const STATUS_REMOVED = 21;

    public function addedBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
