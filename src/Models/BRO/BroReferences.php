<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class BroReferences extends Model
{
    protected $table = 'bro_references';
}
