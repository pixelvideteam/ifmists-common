<?php

namespace App\Models\BRO;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BudgetReleaseOrderTracking extends Model
{
    protected $table = 'budget_release_order_tracking';

    public function primary_user () {
        return $this->hasOne(User::class, 'id', 'primary_user_id');
    }

    public function secondary_user () {
        return $this->hasOne(User::class, 'id', 'secondary_user_id');
    }
}
