<?php

namespace App\Models\BRO;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BroLogs extends Model
{
    protected $table = 'bro_logs';


    public function initiated_by_user()
    {
        return $this->hasOne(User::class, 'id', 'initiated_by');
    }

    public function initiated_for_user()
    {
        return $this->hasOne(User::class, 'id', 'initiated_for');
    }

    public function initiated_to_user()
    {
        return $this->hasOne(User::class, 'id', 'initiated_to');
    }

    public function dataType()
    {
        return '';
    }

    public function bro()
    {
        return $this->hasOne(BudgetReleaseOrder::class, 'id', 'bro_id');
    }

    public function additional_funds()
    {
        return $this->hasOne(AdditionalFunds::class, 'id', 'additional_fund_id');
    }

    public function reapp()
    {
        return $this->hasOne(ReappropriationResumption::class, 'id', 'reapp_id');
    }

    public function action_type()
    {
        return '';
    }
}
