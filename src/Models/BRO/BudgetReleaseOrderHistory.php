<?php

namespace App\Models\BRO;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BudgetReleaseOrderHistory extends Model
{

    public $timestamps = false;
    protected $table = 'budget_release_order_history';
    public static $tablename = 'budget_release_order_history';

    const ALLOWED_FIELDS_TO_DISPLAY = [
        'status',
        'provision',
        'order',
        'current_no',
        'amount',
        'issuer',
        'share_of',
        'remarks',
        'is_revised',
        'inpursuance_text',
        'towards_text',
        'order_hod_id'
    ];

    public function performedBy()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }
}
