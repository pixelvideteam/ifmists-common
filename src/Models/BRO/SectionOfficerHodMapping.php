<?php

namespace App\Models\BRO;

use App\HODList;
use App\User;
use Illuminate\Database\Eloquent\Model;

class SectionOfficerHodMapping extends Model
{
    protected $table = 'section_officer_hod_mapping';

    public function section()
    {
        return $this->hasOne(BroSections::class, 'id', 'section_id');
    }

    public function hod()
    {
        return $this->hasOne(HODList::class, 'id', 'hod_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
