<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class BroNextUserMapping extends Model
{
    protected $table = "bro_nextuser_mapping";

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function next_return_user() {
        return $this->hasOne('App\User', 'id', 'next_return_user_id');
    }

    public function childusers() {
        return $this->hasMany(BroNextUserMapping::class,  'next_return_user_id', 'user_id')
            ->where('type', 1);
    }

    public function parentusers() {
        return $this->hasMany(BroNextUserMapping::class, 'user_id',  'next_return_user_id')
            ->where('type', 1);
    }

    public function sections() {
        return $this->hasMany(BroSectionUsersMapping::class, 'user_id', 'next_return_user_id');
    }
}
