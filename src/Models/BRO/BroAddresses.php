<?php

namespace App\Models\BRO;

use Illuminate\Database\Eloquent\Model;

class BroAddresses extends Model
{
    protected $table = 'bro_addresses';
}
