<?php

namespace App\Models\BRO;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BroUserPermission extends Model
{
    protected $table = 'bro_user_permissions';

    const BRO_MODULE = 1;
    const ADDITIONAL_FUNDS_MODULE = 2;
    const REAPPROPRIATION_RESUMPTION_MODULE = 3;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
