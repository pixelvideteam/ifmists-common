<?php

namespace App\Models\BRO;

use App\HODList;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AdditionalFunds extends Model
{
    protected $table = 'bro_additional_funds';

    public static $tablename = 'bro_additional_funds';

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;
    const STATUS_FORWARDED = 2;
    const STATUS_RETURNED = 3;
    const STATUS_EDITTED = 4;
    const STATUS_REVERTED = 5;

    const STATUSES = [
        0 => 'INITIATED',
        1 => 'APPROVED',
        2 => 'FORWARDED',
        3 => 'RETURNED',
        4 => 'EDITED',
        21 => 'REJECTED',
        5 => 'REVERTED'
    ];

    public function department()
    {
        return $this->hasOne(HODList::class, 'id', 'hod_id');
    }

    public function initiatedBy()
    {
        return $this->hasOne(User::class, 'id', 'initiated_by');
    }

    public function currUser()
    {
        return $this->hasOne(User::class, 'id', 'curr_user');
    }

    public function addresses()
    {
        return $this->hasMany(AdditionalFundAddresses::class, 'additional_fund_id', 'id');
    }

    public function hoas()
    {
        return $this->hasMany(AdditionalFundHoas::class, 'additional_fund_id', 'id');
    }

    public function forwardedByMe()
    {
        $user = Auth::user();
        return $this->hasMany(AdditionalFundsHistory::class, 'additional_fund_id', 'id')
            ->where('updated_by', $user->id);
    }

    public function amount()
    {
        return $this->hasMany(AdditionalFundHoas::class, 'additional_fund_id', 'id')
            ->select('additional_fund_id', 'amount');
    }

    public function section() {
        return $this->hasOne(BroSections::class, 'id', 'section_id');
    }
}
