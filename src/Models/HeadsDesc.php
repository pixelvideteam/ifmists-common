<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class HeadsDesc extends Model
{
    //
    //    use QueryCacheable;
    //
    //    public $cacheFor = 1800; // cache time, in seconds / month
    //    public $cachePrefix = 'heads_desc';

    protected $table = 'heads_desc';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static $suspenseHeadOfAccounts = [
        [
            'id' => 1,
            'desc' => 'Irrigation',
            'hoa' => '8658001020021001000NVN'
        ],
        [
            'id' => 2,
            'desc' => 'Projects',
            'hoa' => '8658001020021002000NVN'
        ],
        [
            'id' => 3,
            'desc' => 'Roads & Buildings',
            'hoa' => '8658001020021003000NVN'
        ],
        [
            'id' => 4,
            'desc' => 'Panchayat Raj',
            'hoa' => '8658001020021004000NVN'
        ],
        [
            'id' => 5,
            'desc' => 'Public Health',
            'hoa' => '8658001020021005000NVN'
        ],
        [
            'id' => 6,
            'desc' => 'Forest',
            'hoa' => '8658001020021006000NVN'
        ],
        [
            'id' => 7,
            'desc' => 'Finance & Corporations',
            'hoa' => '8658001020021007000NVN'
        ],
        [
            'id' => 8,
            'desc' => 'National Highways',
            'hoa' => '8658001020021008000NVN'
        ],
    ];

    CONST kaleswaramHOA = '0000000000000000000NVN';

    public function detailed()
    {
        return $this->hasOne('App\Formtype', 'detailed_head', 'dh');
    }

    public function subdetailed()
    {

        return $this->hasOne('App\Formtype', 'subdetailed_head', 'sdh');
    }

    public function paobudget()
    {

        return $this->hasMany('App\MDdoCumBudget', 'hoa', 'hoa');
    }

    public function hoabebudget()
    {

        return $this->hasMany('App\HoaBeBudget', 'heads_desc_id', 'id');
    }

    public function ddobillidhoa()
    {

        return $this->hasOne('App\DdoBillIdsHoa', 'hoa', 'hoa');
    }

    public function hod()
    {
        return $this->hasOne(HODList::class, 'id', 'hod_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transactions', 'hoa', 'hoa');
    }

    public function transactionsDuringPeriod()
    {
        return $this->hasMany('App\Transactions', 'hoa', 'hoa');
    }

    public function transactionsProgressive()
    {
        return $this->hasMany('App\Transactions', 'hoa', 'hoa');
    }

    public function publicacc()
    {
        return $this->hasOne(PublicAccount::class, 'id', 'public_acc_id');
    }

    public function getGH($hoa)
    {
        $gh = substr($hoa, 10, 2);
        return $gh;
    }

    public function getDH($hoa)
    {
        $dh = substr($hoa, 14, 3);
        return $dh;
    }

    public function getSDH($hoa)
    {
        $sdh = substr($hoa, 17, 3);
        return $sdh;
    }

    public function sof()
    {
        return $this->hasOne(SourceOfFunding::class, 'id', 'sof_id');
    }

    public function sofcatg()
    {
        return $this->hasOne(SofCatg::class, 'id', 'sof_catg_id');
    }

    public function sofsubcatg()
    {
        return $this->hasOne(SofSubCatg::class, 'id', 'sof_sub_catg_id');
    }

    public function publicAccount()
    {
        return $this->hasOne(PublicAccount::class, 'id', 'public_acc_id');
    }

    public function restrictedDdo()
    {
        return $this->hasMany('App\FormTypeRestrictedDdocode', 'hoa', 'hoa');
    }

    public function mjhhoamap()
    {
        return $this->hasMany('App\MapTableHoaWise', 'major_head', 'mjh')->orderBy("user_role", "desc");
    }

    public function hoamap()
    {
        return $this->hasMany('App\MapTableHoaWise', 'hoa', 'hoa')->orderBy("user_role", "desc");
    }

    public function hoaBe()
    {
        return $this->hasOne('App\HoaBe', 'hoa', 'hoa');
    }

    public function reappropriationDebit()
    {
        /*return $this->hasMany(BudgetRequestReappropriation::class, 'debit_hoa', 'hoa');*/
        return $this->hasMany(BudgetRequestReappropriationDetail::class, 'hoa', 'hoa')->where('cr_dr_flag', '=', '1');;
    }

    public function reappropriationCredit()
    {
        return $this->hasMany(BudgetRequestReappropriationDetail::class, 'hoa', 'hoa')->where('cr_dr_flag', '=', '2');;
    }

    public function surrenderedDebit()
    {
        return $this->hasmany(BudgetRequestSurrender::class, "hoa", "hoa");
    }

    public function surrenderedCredit()
    {
        return $this->hasmany(BudgetRequestSurrenderDetail::class, "hoa", "hoa");
    }

    public function authorized()
    {
        return $this->hasmany(BudgetRequestHoa::class, "hoa", "hoa");
    }
}
