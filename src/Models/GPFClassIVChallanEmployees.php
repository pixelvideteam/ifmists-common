<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class GPFClassIVChallanEmployees extends Model
{
    use QueryCacheable;

    protected $table = 'gpf_class_iv_challan_employees';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
