<?php namespace App;

use App\Models\Concerns;
use Illuminate\Database\Eloquent\Model;

class UserOtp extends Model
{
    use
        Concerns\QueryBuilder,
        Concerns\PhoneNumber,
        Concerns\BlindIndex;

    protected $table = 'user_otp';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     * Blind Indexes columns in DB
     */
    protected $encrypted = [
        'phone' => 'phone_bidx',
    ];
}
