<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PdReportFiles extends Model
{
    protected $table = 'pd_report_files';
    const TYPE_DEFAULT = 0;
    const TYPE_LOCAL_FUNDS_REPORT = 1;

    const STATUS_DEFAULT = 0;
    const STATUS_GENERATED = 1;
    const STATUS_REJECTED = 21;
}
