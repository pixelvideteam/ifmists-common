<?php namespace App;

use Illuminate\Database\Eloquent\Model;

	class Loc1417 extends Model{

		protected $table="locrequest_1417";
		public $timestamps = false;

		protected $guarded=['id'];

		public function requser()
		{	
			return $this->belongsTo('App\User','requestuser','username');
		}

		public function schemes()
		{	
			return $this->belongsTo('App\Schemes','hoa','hoa');
		}

		public function accounts()
		{	
			return $this->belongsTo('App\Pdaccount','hoa','hoa');
		}
	}