<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryNotifications extends Model
{

    protected $table = 'salary_notifications';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
