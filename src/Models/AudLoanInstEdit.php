<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AudLoanInstEdit extends Model 
{
	protected $table='aud_loan_inst_edit';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	
	public function earndedn() 
	{
		return $this->hasOne('App\EarnDednList','id','earndedn_id');
	}

	public function loan() 
	{
		return $this->hasOne('App\Loans','id','loan_id');
	}

	public function aud() 
	{
		return $this->hasOne('App\User','username','auduser');
	}

	public function emp() 
	{
		return $this->hasOne('App\EmployeeMaster','id','emp_id');
	}

}
