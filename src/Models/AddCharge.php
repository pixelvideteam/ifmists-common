<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AddCharge extends Model
{
    protected $table = "additional_charge";
    protected $guarded = ['id'];

    public function todesg()
    {
        return $this->belongsTo('App\DesgCode', 'to_desgcode_id', 'id');
    }

    public function empearndedn()
    {
        return $this->hasMany('App\AddChargeEarnings', 'add_charge_id', 'id');
    }
}
