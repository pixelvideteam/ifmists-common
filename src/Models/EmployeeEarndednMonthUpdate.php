<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeEarndednMonthUpdate extends Model {

	protected $table='employee_earndedn_month_update';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	protected $hidden = ["created_at", "updated_at"];
	
	public function earndedn() 
	{
		return $this->hasOne('App\EarnDednList','id','earndedn_id');
	}

}
