<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class Lapsing extends Model{

		protected $table="lapsing";
		public $timestamps = false;

		protected $guarded=['id'];

		public function creator(){	
			return $this->belongsTo('App\User','username','created_by');
		}

		public function usernames(){	
			return $this->belongsTo('App\User','ddo','username');
		}

		public function scheme()
		{
			return $this->belongsTo('App\Schemes','hoa','hoa');
		}

	}


