<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OtherBillEarndedn extends Model
{
    use SoftDeletes;

    protected $table = 'other_bill_earndedn';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function stomapping()
    {
        return $this->hasMany('App\OtherBillEarndednMapping', 'other_bill_earndedn_id', 'id');
    }

    public function billEarnDedn()
    {
        return $this->hasMany('App\BillEarnDedn', 'other_bill_earndedn_id');
    }

    //Aliasing to keep reports consistent
    public function billMultipleParties()
    {
        return $this->billEarnDedn();
    }
}
