<?php

namespace App\Models\BulkBill;

use Illuminate\Database\Eloquent\Model;

class BulkBillGroups extends Model
{
    const PENDING = 0;
    const CRON_RUNNING = 1;
    const FILE_INVALID = 2;
    const DUPLICATE_PARTIES_IN_FILE = 3;
    const DUPLICATE_PARTIES_WITH_OTHER_FILES = 4;
    const FILE_VALID = 5;
    const BILL_DONE = 6;
    protected $guarded = ['id'];
    protected $fillable = ['status', 'created_at', 'updated_at'];
    protected $table = 'bulk_bill_groups';
}
