<?php

namespace App\Models\BulkBill;

use Illuminate\Database\Eloquent\Model;

class BulkBillTempParties extends Model
{
    protected $table = 'bulk_bill_parties';
}
