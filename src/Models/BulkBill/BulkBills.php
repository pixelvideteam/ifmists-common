<?php

namespace App\Models\BulkBill;

use Illuminate\Database\Eloquent\Model;

class BulkBills extends Model
{
    protected $guarded = ['id'];
    protected $fillable = ['status', 'created_at', 'updated_at'];
    protected $table = 'bulk_bills';
}
