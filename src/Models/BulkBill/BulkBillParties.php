<?php

namespace App\Models\BulkBill;

use Illuminate\Database\Eloquent\Model;

class BulkBillParties extends Model
{
    const PARTY_INVALID = 0;
    const PARTY_VALID = 1;
    protected $guarded = ['id'];
    protected $fillable = ['status', 'created_at', 'updated_at'];
    protected $table = 'bulk_bill_parties';
}
