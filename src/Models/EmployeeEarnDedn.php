<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeEarnDedn extends Model {

	protected $table='employee_earndedn';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	// protected $hidden = ["created_at", "updated_at"];

	public function earndedn() 
	{
		return $this->hasOne('App\EarnDednList','id','earndedn_id')->orderBy("sdh");
	}

	public function empmaster() 
	{
		return $this->hasOne('App\EmployeeMaster','id','employee_id');
	}

	public function loan() {

		return $this->hasMany('App\Loans','earndedn_id','earndedn_id');
	}

	public function loans() {

		return $this->hasMany('App\Loans','earndedn_id','earndedn_id');
	}
	public function emploans() {

		return $this->hasMany('App\Loans','employee_id','employee_id');
	}
}
