<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 09-04-2019
 * Time: 09:11
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArcticScripts extends Model
{
    protected $table = "arctic_scripts";

    public function schedulerlog()
    {
        return $this->hasMany(ArcticSchedulerLog::class, 'script_id', 'id');
    }
}
