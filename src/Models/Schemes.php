<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Schemes extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'schemes';

    protected $table = "schemes";
    public $timestamps = false;

    protected $guarded = ["id"];

    public function pdexptypes()
    {
        return $this->hasMany('App\PdExpTypes', 'schemes_id', 'id');
    }

    public function schemeusers()
    {
        return $this->hasMany('App\SchemesUsers', 'schemes_id', 'id');
    }
}
