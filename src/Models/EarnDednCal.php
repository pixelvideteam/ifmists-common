<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class EarnDednCal extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'earn_dedn_cal';

    protected $table = 'earn_dedn_cal';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
