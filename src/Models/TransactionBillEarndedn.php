<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionBillEarndedn extends Model
{
    protected $table = 'transaction_bill_earndedn';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
