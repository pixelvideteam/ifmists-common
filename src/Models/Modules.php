<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Modules extends Model {

//    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'modules';

	protected $table = 'modules';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    const KALESHWARAM_WORKS_SECTION_MODULE = 1188;

	public function menus() {
		return $this->hasMany('App\Menu','module_id','id')->orderBy('menus.priority', 'asc');
	}

	public function tutorials() {
		return $this->hasMany('App\Tutorials','module_id','id')->orderBy('tutorials.rank', 'asc');
	}
}
