<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class Suspension extends Model{

	protected $table="suspension";
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function empmaster()
	{	
		return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	}

	public function empearndedn() 
	{
		return $this->hasMany('App\SuspensionEarndedn','suspension_id','id');
	}
}
