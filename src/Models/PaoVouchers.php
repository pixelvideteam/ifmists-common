<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoVouchers extends Model 
{
	protected $table='pao_vouchers';
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
