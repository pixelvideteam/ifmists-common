<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MDdoCumBudget extends Model
{
    protected $table='mddocumbudget';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function ddo()
    {
        return $this->hasOne('App\User', 'username', 'ddocode')->where("hod_id", "!=", 0)->latest();
    }
    public function headsdesc() {

        return $this->hasOne('App\HeadsDesc','hoa','hoa');
    }
    public function hoadesc() {

        return $this->hasOne('App\Hoa','hoa','hoa');
    }

    public function billsddo() {

        return $this->hasMany('App\Transactions','ddocode','ddocode');
    }

}
