<?php namespace App;

use App\Helper\Storage\StorageHelper;
use App\Models\Concerns;
use App\VRO\VroEmployeeDetail;
use Illuminate\Database\Eloquent\Model;

class EmployeeMaster extends Model
{

    use Concerns\PhoneNumber,
        Concerns\AadharNumber,
        Concerns\PanCardNumber;

    const STATUS_WORKING = 1;
    const STATUS_TRANSFERRED = 2;
    const STATUS_RETIRED = 3;
    const STATUS_EXPIRED = 4;
    const STATUS_SUSPENDED = 5;
    const STATUS_DEPUTED = 6;
    const STATUS_FOREIGN_SERVICES = 8;
    const STATUS_ABSCONDING = 9;
    const GPF_TYPE = 1;
    const CPS_TYPE = 5;

    const STATUS_ALL_ARRAY = [
        EmployeeMaster::STATUS_WORKING,
        EmployeeMaster::STATUS_TRANSFERRED,
        EmployeeMaster::STATUS_RETIRED,
        EmployeeMaster::STATUS_EXPIRED,
        EmployeeMaster::STATUS_SUSPENDED,
        EmployeeMaster::STATUS_DEPUTED,
        EmployeeMaster::STATUS_FOREIGN_SERVICES,
        EmployeeMaster::STATUS_ABSCONDING,
    ];

    const BANKACNO = 'bankacno';

    protected $table = 'employee_master';
    protected $guarded = ['id', 'created_at', 'updated_at'];
//    protected $hidden = ["created_at", "updated_at"];

    protected $encrypted = [
        'aadharno' => 'aadharno_bidx',
        'phoneno' => 'phoneno_bidx',
        'pan' => 'pan_bidx',
    ];


    public function cader()
    {
        return $this->hasOne(Cader::class, 'id', 'cadercode');
    }

    public function updateEmployeeName()
    {
        return $this->hasOne('App\UpdateEmployeeName', 'employee_master_id', 'id');
    }

    public function empearndedn()
    {
        return $this->hasMany('App\EmployeeEarnDedn', 'employee_id', 'id');
    }

    public function latestBillMultipleParty()
    {
        return $this->hasOne('App\BillMultipleParty', 'emp_id', 'id')->latest()->limit(1);
    }

    public function basicPay()
    {
        return $this->hasOne('App\EmployeeEarnDedn', 'employee_id', 'id')->where('earndedn_id', '=', 300)
            ->select('employee_id', 'amount');
    }

    public function empearndedn_copy()
    {
        return $this->hasMany('App\EmployeeEarnDedn', 'employee_id', 'id');
    }

    public function monthupdate()
    {
        return $this->hasMany('App\EmployeeEarndednMonthUpdate', 'employee_id', 'id');
    }

    public function desg()
    {

        return $this->hasOne('App\DesgCode', 'id', 'desgcode_id');
    }

    public function retirement()
    {
        return $this->hasOne('App\EmployeeRetirement', 'employee_id', 'id');
    }

    public function da()
    {

        return $this->hasOne('App\DAMasters', 'id', 'da_masters_id');
    }

    public function gpftype()
    {
        return $this->hasOne('App\GpfCatg', 'gpf_type', 'gpf_type');
    }

    public function prc()
    {
        return $this->hasOne('App\Prc', 'id', 'prc_id');
    }

    public function workingstatus()
    {
        return $this->hasOne('App\WorkingStatus', 'id', 'status');
    }

    public function department()
    {

        return $this->hasOne('App\Department', 'id', 'dept_id');
    }

    public function scaletype()
    {

        return $this->hasOne('App\ScaleTypes', 'id', 'scale_type_id');
    }

    public function scalesubtype()
    {

        return $this->hasOne('App\ScaleSubTypes', 'id', 'scale_sub_type_id');
    }

    public function ifscdetail()
    {

        return $this->hasOne('App\BankIfsc', 'ifsccode', 'ifsccode');
    }

    public function genders()
    {

        return $this->hasOne('App\Genders', 'id', 'gender_id');
    }

    public function leaveslist()
    {

        return $this->hasMany('App\EmployeeLeavesList', 'employee_id', 'id');
    }

    public function promotions()
    {

        return $this->hasMany('App\Promotion', 'employee_id', 'id');
    }

    public function demotion()
    {

        return $this->hasMany('App\Demotion', 'employee_id', 'id');
    }

    public function leavestaken()
    {

        return $this->hasMany('App\EmployeeLeaves', 'employee_id', 'id');
    }

    public function transfer()
    {

        return $this->hasMany('App\Transfer', 'employee_id', 'id')->where("status", "=", 0);
    }

    public function latesttransfer()
    {

        return $this->hasOne('App\Transfer', 'employee_id', 'id')->where("status", "=", 1)->orderBy('wef_date', 'desc')->limit(1);
    }

    public function bms_multiple()
    {

        return $this->hasMany('App\BillMultipleParty', 'emp_id', 'id');
    }

    public function loans()
    {

        return $this->hasMany('App\Loans', 'employee_id', 'id')->where("close_flag", "=", 0);
    }

    public function allloans()
    {

        return $this->hasMany('App\Loans', 'employee_id', 'id');
    }

    public function ddo()
    {

        return $this->hasOne('App\User', 'username', 'ddocode');
    }

    public function scaledetails()
    {
        return $this->hasOne('App\ScaleDetails', 'id', 'scale_details_id');
    }

    public function scale()
    {
        return $this->hasOne('App\Scale', 'id', 'scale_id');
    }

    public function emplocation()
    {
        return $this->hasOne('App\EmployeeLocationList', 'id', 'emp_loc_id');
    }

    public function mar_status()
    {
        return $this->hasOne('App\MaritalStatus', 'id', 'maritalstatus');
    }

    public function suspension()
    {
        return $this->hasMany('App\Suspension', 'employee_id', 'id');
    }

    public function deptdesglist()
    {

        return $this->hasMany('App\DeptDesgnMapping', 'department_id', 'dept_id');
    }

    public function allscales()
    {
        return $this->hasMany('App\Scale', 'prc_id', 'prc_id');
    }

    public function lastpromo()
    {
        return $this->hasOne('App\Promotion', 'employee_id', 'id')->orderBy("promotion_wef_date", "desc");
    }

    public function lastdemo()
    {
        return $this->hasOne('App\Demotion', 'employee_id', 'id')->orderBy("demotion_wef_date", "desc");
    }

    public function empddocourse()
    {
        return $this->hasMany('App\EmployeeDdoCourse', 'employee_id', 'id');
    }

    public function allda()
    {
        return $this->hasMany('App\DAMasters', 'prc_id', 'prc_id')->orderBy('wef_date', 'desc');
    }

    ////
    public function absjoinapproval()
    {
        return $this->hasMany('App\AudAbscondingJoinApprovals', 'employee_id', 'id');
    }

    public function promapprovals()
    {
        return $this->hasMany('App\PromotionApprovals', 'employee_id', 'id');
    }

    public function demoapprovals()
    {
        return $this->hasMany('App\DemotionApprovals', 'employee_id', 'id');
    }

    public function suspapprovals()
    {
        return $this->hasMany('App\SuspensionApprovals', 'employee_id', 'id');
    }

    public function earndednapprovals()
    {
        return $this->hasMany('App\AudEarnDednApprovals', 'employee_id', 'id');
    }

    public function incapprovals()
    {
        return $this->hasMany('App\IncrementApprovals', 'employee_id', 'id');
    }

    public function gradepayapprovals()
    {
        return $this->hasMany('App\AuthGradePayApprovals', 'employee_id', 'id');
    }

    public function empbillid()
    {
        return $this->belongsTo('App\DdoBillIds', 'bill_id', 'id');
    }

    public function emptransferdets()
    {
        return $this->hasMany('App\Transfer', 'employee_id', 'id');
    }

    public function hractg()
    {
        return $this->hasOne('App\HraCatg', 'id', 'hracatg');
    }

    public function empprclocation()
    {
        return $this->hasMany('App\EmployeeLocationList', 'prc_id', 'prc_id');
    }

    public function basicdets()
    {
        return $this->hasOne('App\MasterScaleDetails', 'id', 'master_scale_details_id');
    }


    public function empyearlyleave()
    {
        return $this->hasMany('App\EmployeeYearlyLeaves', 'employee_id', 'id');
    }

    public function paomemp()
    {
        return $this->hasOne('App\PaoMemp', 'empcode', 'empcode');
    }

    public function audempapprovals()
    {
        return $this->hasOne('App\AudEmpApprovals', 'empcode', 'empcode');
    }

    public function genformemps()
    {
        return $this->hasMany('App\GenericSuppFormEmployees', 'employee_id', 'id');
    }

    public function empdaupdate()
    {
        return $this->hasMany('App\EmployeeDaUpdate', 'employee_id', 'id');
    }

    public function empdednrefno()
    {
        return $this->hasMany('App\EmployeeDeductionRefno', 'employee_id', 'id');
    }

    public function empExtPrcData()
    {
        return $this->hasMany('App\EmpExternalPrcData', 'employee_id', 'id');
    }

    public function editempdets()
    {
        return $this->hasOne('App\EditEmpDetails', 'employee_id', 'id');
    }

    public function increments()
    {
        return $this->hasMany('App\EmployeeIncrement', 'employee_id', 'id');
    }

    public function gradepay()
    {

        return $this->hasMany('App\EmployeeGradePay', 'employee_id', 'id');
    }

    public function fr22b()
    {
        return $this->hasMany('App\FR22B', 'employee_id', 'id');
    }

    public function exemptdays()
    {
        return $this->hasMany('App\ExemptEmpSalDays', 'employee_id', 'id');
    }

    public function ddoBillIds()
    {
        return $this->HasMany('App\DdoBillIds', 'ddocode', 'ddocode');
    }

    public function saldaywiseearnings()
    {
        return $this->HasMany('App\SalBillEmpDaywiseEarnings', 'employee_id', 'id');
    }

    public function legalheir()
    {
        return $this->hasMany('App\EmployeeLegalHeir', 'employee_id', 'id');
    }

    public function foreignservice()
    {
        return $this->hasMany('App\EmployeeForeignService', 'employee_id', 'id');
    }

    public function totalBills()
    {
        return $this->hasOne(BillMultipleParty::class, 'bankacno', 'bankacno')->whereHas('transaction', function ($query) {
            $query->where('transstatus', '!=', '21');
        })->distinct('transaction_id');
    }

    public function status()
    {
        return $this->hasOne('App\WorkingStatus', 'id', 'status');
    }

    public function saltrack()
    {
        return $this->hasMany('App\EmployeeSalMesgTrack', 'employee_id', 'id');
    }

    /**
     * @param $value
     * @return string
     */
    public function getPhonenoDecoded()
    {
        if (!empty($this->attributes['phoneno'])) {
            return \App\Fms\Document\PhoneNumberEncryption::decrypted($this->attributes['phoneno']);
        }
        return "";
    }

    public function getPhonenoDecrypted($phoneno)
    {
        if (!empty($phoneno)) {
            return \App\Fms\Document\PhoneNumberEncryption::decrypted($phoneno);
        }
        return "";
    }
    /**
     * @param $value
     * @return string
     */
    //  public function getSurnameAttribute($value)
    //  {
    //      return ucfirst($value);
    //  }

//    /**
//     * @param $value
//     * @return string
//     */
//    public function getNameAttribute($value)
//    {
//        $name = ucfirst($value);
//
//        # User is expired
//        if (!empty($this->status) && $this->status == 4) {
//            $name = 'Late ' . $name;
//        }
//
//        return $name;
//    }

    /**
     * @param $value
     * @return string
     */
    //  public function getFathernameAttribute($value)
    //  {
    //      return ucfirst($value);
    //  }

    /**
     * @param $value
     * @return string
     */
    //  public function getSpousenameAttribute($value)
    //  {
    //      return ucfirst($value);
    //  }
    public function inputRules(\Illuminate\Http\Request $request)
    {
        return [
            'empcode' => "size:7|regex:/^[0-9]*$/"
        ];
    }

    public function custommessages(\Illuminate\Http\Request $request)
    {
        return [
            'required' => 'The :attribute is required.',
            'regex' => 'The :attribute should only contain digits',
            'max' => 'The :attribute length should not exceed :value',
            'min' => 'The :attribute length should be less than :value',
            'size' => 'The :attribute length should be :size',
            'between' => 'The :attribute length should be between :min - :max'
        ];
    }


    public function familydetails()
    {
        return $this->hasMany('App\FamilyDetails', 'employee_id', 'id');
    }

    public function religion()
    {
        return $this->hasOne('App\Religions', 'id', 'religion_id');
    }

    public function qualification_before()
    {
        return $this->hasOne('App\Qualifications', 'id', 'qualification_before_entry_id');
    }

    public function qualification_after()
    {
        return $this->hasOne('App\Qualifications', 'id', 'qualification_after_entry_id');
    }

    /**
     * Dynamically get profile image URL on the fly for employee
     * check in ESRController->get_esr_personal_details() for usage
     * EmployeeMaster::first() or get() then $employeeMaster->setAppends(['profile_url']);
     * @return string URl of profile picture from S3
     */

    public function getProfileUrlAttribute()
    {
        $url = "";
        if ($this->image_path) {
            $imagePath = ltrim($this->image_path, '/');// /uploads/profile/ => uploads/profile/
            try {
                $fileExists = StorageHelper::getFile($imagePath);
            } catch (\Exception $e) {
                $fileExists = [];
            }
            if (count($fileExists)) {
                $url = StorageHelper::getTempURLForFile($imagePath);
            }
        }
        return $url;
    }

    public function empBillDet()
    {
        return $this->hasMany('App\EmployeeBillDetails', 'employee_id', 'id');
    }

    public function cpsMaster()
    {
        return $this->hasOne(CpsMaster::class, 'cpsno', 'gpfno');
    }

    public function vro()
    {
        return $this->hasOne(VroEmployeeDetail::class, 'employee_id', 'id');
    }

    public function salaryEditedData()
    {
        return $this->hasMany(SalaryEditedData::class, 'employee_id', 'id');
    }

    public function esr_downloads()
    {
        return $this->hasMany('App\EsrDownloadHistory', 'employee_id', 'id')->where('status', '=', 1);
    }

    public function isFinalSubmit()
    {
        return $this->hasMany('App\EsrDownloadHistory', 'employee_id', 'id')->where('status', '=', 2);
    }
}
