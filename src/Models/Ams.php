<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ams extends Model
{
    protected $table = "ams";
    public $timestamps = false;
    protected $guarded = ['id'];

    public function creator()
    {
        return $this->belongsTo('App\User', 'username', 'created_by');
    }

    public function cr_usernames()
    {
        return $this->belongsTo('App\User', 'cr_ddo', 'username');
    }

    public function cr_scheme()
    {
        return $this->belongsTo('App\Schemes', 'cr_hoa', 'hoa');
    }

    public function dbt_usernames()
    {
        return $this->belongsTo('App\User', 'dbt_ddo', 'username');
    }

    public function dbt_scheme()
    {
        return $this->belongsTo('App\Schemes', 'dbt_hoa', 'hoa');
    }
}
