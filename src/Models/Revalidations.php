<?php namespace App;

use Illuminate\Database\Eloquent\Model;

	class Revalidations extends Model{

		protected $table="revalidations";
		public $timestamps = false;

		protected $guarded=['id'];
	}