<?php

namespace App\Models\Reports;

use App\HeadsDesc as HD;
use Illuminate\Support\Facades\DB;

class HeadDesc extends HD
{
    protected $appends = ['mjh_to_mih', 'mjh_to_dh'];
    //protected $successStatus = [204, 310];

    /**
     * Get the Major Head to Minor Head String.
     *
     * @return string
     */
    public function getMjhToMihAttribute()
    {
        return "{$this->mjh}-{$this->smjh}-{$this->mih}";
    }

    public function getMjhToDhAttribute()
    {
        return "{$this->mjh}-{$this->smjh}-{$this->mih}-{$this->gsh}-{$this->sh}-{$this->dh}";
    }

    public function dwaChallanItems()
    {
        return $this->hasMany('App\Models\Dwa\Accounts\DwaChallanItem', 'hoa', 'hoa');
    }

    public function dwaTeoAdjustmentItems()
    {
        return $this->hasMany('App\Models\Dwa\Accounts\DwaTeoAdjustmentItem', 'hoa', 'hoa');
    }

    public function billEarndednHasManyThroughTransactions()
    {
        return $this->hasManyThrough(
            'App\Transactions',
            'App\BillEarnDedn',
            'transaction_id', // Foreign key on BillEarnDedn table...
            'id', // Foreign key on Transactions table...
            'id', // Local key on countries table...
            'id' // Local key on BillEarnDedn table...
        );
    }

    public function scopeAmountDuringPeriod($query, $filters)
    {
        return $query->with(['transactionsDuringPeriod' => function ($query) use ($filters) {
            //$query->select(DB::raw("SUM(gross) as amt_during_period"))
            //$query->select(DB::raw("SUM(amount) as amt_during_period from billearndedn where type=1"));
            //$query->select('transtype');
            $query->whereIn('transtype', [4, 5])
                ->whereBetween('voucherdate', [$filters['from_date'], $filters['to_date']])
                ->whereIn('transstatus', [204, 310])
                ->where('ddocode', 'LIKE', '%w')
                ->where('created_at', '>=', '2019-03-01 00:00:00')
                ->whereHas('users', function ($qu) use ($filters) {
                    $qu->where('dwa_pao_code', $filters['pao']);
                });

            if ($filters['accountTypes']) {
                $query->where('dwa_account_type_id', $filters['accountTypes']);
            }
            $query->withCount(['billearndedn AS amt_during_period' => function ($bq) {
                $bq->select(DB::raw("SUM(amount) as amt_during_period"))
                    ->where('type', 1);
            }]);
        }]);
    }

    public function scopeAmountProgressive($query, $filters)
    {
        return $query->with(['transactionsProgressive' => function ($query) use ($filters) {
                //$query->select(DB::raw("SUM(gross) as amt_progressive"))
                //$query->select(DB::raw("SUM(amount) as amt_progressive from billearndedn where type=1"));
                //$query->select('transtype');
                $query->whereIn('transtype', [4, 5])
                    ->whereBetween('voucherdate', [$filters['from_financial_year'], $filters['to_financial_year']])
                    ->whereIn('transstatus', [204, 310])
                    ->where('ddocode', 'LIKE', '%w')
                    ->where('created_at', '>=', '2019-03-01 00:00:00')
                    ->whereHas('users', function ($qu) use ($filters) {
                        $qu->where('dwa_pao_code', $filters['pao']);
                    });

                if ($filters['accountTypes']) {
                    $query->where('dwa_account_type_id', $filters['accountTypes']);
                }
                $query->withCount(['billearndedn AS amt_progressive' => function ($bq) {
                    $bq->select(DB::raw("SUM(amount) as amt_progressive"))
                        ->where('type', 1);
                }]);
            }
        ]);
    }
}
