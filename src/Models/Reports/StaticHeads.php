<?php
namespace App\Models\Reports;

use App\Repository\CommonRepository\FinancialYearRepository;

class StaticHeads
{
    const DEFAULT_CHEQUE_HEAD = '8782001020002002000NVN';
    const FINANCIAL_YEAR_CHEQUE_HEADS = [
        '2019-2020' => '8782001020002002000NVN',
    ];
    const DEFAULT_CHALLAN_HEAD = '8782001020001000000NVN';
    const FINANCIAL_YEAR_CHALLAN_HEADS = [
        '2019-2020' => '8782001020001000000NVN',
    ];

    public static function currentFinanacialYearChequeHead()
    {
        $finYear = FinancialYearRepository::getCurrentFinancialYear();
        if(!empty(self::FINANCIAL_YEAR_CHEQUE_HEADS[$finYear])){
            return self::FINANCIAL_YEAR_CHEQUE_HEADS[$finYear];
        }
        return self::DEFAULT_CHEQUE_HEAD;
    }

    public static function currentFinanacialYearChallanHead()
    {
        $finYear = FinancialYearRepository::getCurrentFinancialYear();
        if(!empty(self::FINANCIAL_YEAR_CHALLAN_HEADS[$finYear])){
            return self::FINANCIAL_YEAR_CHALLAN_HEADS[$finYear];
        }
        return self::DEFAULT_CHALLAN_HEAD;
    }
}
