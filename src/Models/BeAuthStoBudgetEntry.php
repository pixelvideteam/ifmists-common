<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeAuthStoBudgetEntry extends Model
{
    protected $table = 'be_auth_sto_budget_entry';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
