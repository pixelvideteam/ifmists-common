<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 23-10-2019
 * Time: 19:05
 */
declare(strict_types=1);

namespace App;


use App\Models\Dwa\Accounts\DwaChequeTransaction;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\EKuber\Models\TransTypeStatuses;

class DWACheques extends Model
{
    const STATUS_ZERO = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 21;

    protected $table = "dwa_cheques";

    public function dwachqtrans()
    {
        return $this->hasMany(DWAChequeTransactions::class, 'dwa_cheques_id', 'id');
    }

    public function createdby()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function dwaChequePaymentFileMapping()
    {
        return $this->hasOne(DWAChequePaymentFileMapping::class, 'dwa_cheques_id', 'id');
    }

    public function scopeAllEkuberTrans($query, $areacode = null, $accountType = null)
    {
        return $query->with(['dwaChequePaymentFileMapping.paymentfiles' => function ($innerquery) {
            $innerquery->where('invalidate_type', '!=', '21')
                ->where('is_sent_to_ekuber', 1)
                ->where('client_id', 3)
                ->with('ekubertrans');
        }])
            ->whereHas('dwaChequePaymentFileMapping.paymentfiles', function ($innerquery) {
                $innerquery->where('invalidate_type', '!=', '21')
                    ->where('is_sent_to_ekuber', 1)
                    ->where('client_id', 3)
                    ->whereHas('ekubertrans');
            })
            ->whereHas('transactions', function ($tq) use ($accountType) {
                if ($accountType) {
                    $tq->where('dwa_account_type_id', $accountType);
                }
            })
            ->with(['transactions' => function ($cquery) use ($accountType) {
                if ($accountType) {
                    $cquery->where('dwa_account_type_id', $accountType);
                }
                $cquery->with('accountType:id,name,account_type_code,sdept_code');
            }])
            ->whereHas('createdby', function ($cquery) use ($areacode) {
                if ($areacode) {
                    $cquery->where('dwa_pao_code', $areacode);
                }
            })
            ->with(['createdby' => function ($cquery) use ($areacode) {
                if ($areacode) {
                    $cquery->where('dwa_pao_code', $areacode);
                }
            }])
            ->whereNotNull('ekuber_filename')
            ->orderBy('chequeno', 'desc')
            ->get();
    }

    public function scopePendingCheques($query, $areacode = null, $accountType = null)
    {
        return $query->with(['dwaChequePaymentFileMapping.paymentfiles' => function ($innerquery) {
            $innerquery->where('invalidate_type', '!=', '21')
                ->where('is_sent_to_ekuber', 1)
                ->with(['ekubertrans' => function ($innerquery2) {
                    $innerquery2
                        ->where('transtype', TransTypeStatuses::SENT)
                        ->where('transtype', '!=', TransTypeStatuses::ACK_ACCP)
                        ->where('transtype', '!=', TransTypeStatuses::DN);
                }]);
        }])
            ->whereHas('dwaChequePaymentFileMapping.paymentfiles', function ($innerquery) {
                $innerquery->where('invalidate_type', '!=', '21')
                    ->where('is_sent_to_ekuber', 1)
                    ->whereHas('ekubertrans', function ($innerquery2) {
                        $innerquery2
                            ->where('transtype', TransTypeStatuses::SENT)
                            ->where('transtype', '!=', TransTypeStatuses::ACK_ACCP)
                            ->where('transtype', '!=', TransTypeStatuses::DN);
                    });
            })
            ->whereHas('transactions', function ($tq) use ($accountType) {
                if ($accountType) {
                    $tq->where('dwa_account_type_id', $accountType);
                }
            })
            ->whereHas('createdby', function ($cquery) use ($areacode) {
                if ($areacode) {
                    $cquery->where('dwa_pao_code', $areacode);
                }
            })
            ->with(['createdby' => function ($cquery) use ($areacode) {
                if ($areacode) {
                    $cquery->where('dwa_pao_code', $areacode);
                }
            }])
            ->whereNotNull('ekuber_filename')
            ->get();
    }

    public function scopePendingChequesWithAckWithoutDn($query, $areacode = null, $accountType = null)
    {
        return $query->with(['dwaChequePaymentFileMapping.paymentfiles' => function ($innerquery) {
            $innerquery->where('invalidate_type', '!=', '21')
                ->where('is_sent_to_ekuber', 1)
                ->with(['ekubertrans' => function ($innerquery2) {
                    $innerquery2
                        ->where('transtype', TransTypeStatuses::SENT)
                        ->where('transtype', TransTypeStatuses::ACK_ACCP)
                        ->where('transtype', '!=', TransTypeStatuses::DN);
                }]);
        }])
            ->whereHas('dwaChequePaymentFileMapping.paymentfiles', function ($innerquery) {
                $innerquery->where('invalidate_type', '!=', '21')
                    ->where('is_sent_to_ekuber', 1)
                    ->whereHas('ekubertrans', function ($innerquery2) {
                        $innerquery2
                            ->where('transtype', TransTypeStatuses::SENT)
                            ->where('transtype', TransTypeStatuses::ACK_ACCP)
                            ->where('transtype', '!=', TransTypeStatuses::DN);
                    });
            })
            ->whereHas('transactions', function ($tq) use ($accountType) {
                if ($accountType) {
                    $tq->where('dwa_account_type_id', $accountType);
                }
            })
            ->whereHas('createdby', function ($cquery) use ($areacode) {
                if ($areacode) {
                    $cquery->where('dwa_pao_code', $areacode);
                }
            })
            ->with(['createdby' => function ($cquery) use ($areacode) {
                if ($areacode) {
                    $cquery->where('dwa_pao_code', $areacode);
                }
            }])
            ->whereNotNull('ekuber_filename')
            ->get();
    }

    public function scopePendingChequesWithDnWithoutAck($query, $areacode = null, $accountType = null)
    {
        return $query->with(['dwaChequePaymentFileMapping.paymentfiles' => function ($innerquery) {
            $innerquery->where('invalidate_type', '!=', '21')
                ->where('is_sent_to_ekuber', 1)
                ->with(['ekubertrans' => function ($innerquery2) {
                    $innerquery2
                        ->where('transtype', TransTypeStatuses::SENT)
                        ->where('transtype', '!=', TransTypeStatuses::ACK_ACCP)
                        ->where('transtype', TransTypeStatuses::DN);
                }]);
        }])
            ->whereHas('dwaChequePaymentFileMapping.paymentfiles', function ($innerquery) {
                $innerquery->where('invalidate_type', '!=', '21')
                    ->where('is_sent_to_ekuber', 1)
                    ->whereHas('ekubertrans', function ($innerquery2) {
                        $innerquery2
                            ->where('transtype', TransTypeStatuses::SENT)
                            ->where('transtype', '!=', TransTypeStatuses::ACK_ACCP)
                            ->where('transtype', TransTypeStatuses::DN);
                    });
            })
            ->whereHas('transactions', function ($tq) use ($accountType) {
                if ($accountType) {
                    $tq->where('dwa_account_type_id', $accountType);
                }
            })
            ->whereHas('createdby', function ($cquery) use ($areacode) {
                if ($areacode) {
                    $cquery->where('dwa_pao_code', $areacode);
                }
            })
            ->with(['createdby' => function ($cquery) use ($areacode) {
                if ($areacode) {
                    $cquery->where('dwa_pao_code', $areacode);
                }
            }])
            ->whereNotNull('ekuber_filename')
            ->get();
    }

    public function chequeTransactions()
    {
        return $this->hasMany(DwaChequeTransaction::class, 'dwa_cheques_id', 'id');
    }

    public function dwaTransaction()
    {
        return $this->hasOne('App\Transactions', 'ekuber_dwa_filename', 'ekuber_filename');
    }

    public function transactions()
    {
        return $this->belongsToMany(
            Transactions::class,
            'dwa_cheques_transactions',
            'dwa_cheques_id',
            'transaction_id'
        );
    }

    public function transactionssingle()
    {
        return $this->belongsToMany(
            Transactions::class,
            'dwa_cheques_transactions',
            'dwa_cheques_id',
            'transaction_id'
        );
    }


    public function getDnFile()
    {
        return $this->hasOne('App\EkuberTransactions', 'orgnl_msg_id', 'ekuber_filename')
            ->where('transtype', '=', '3')
            ->where('client_id', 3)
            ->orderBy('created_at');
    }

    public function ekuberPaymentFiles()
    {
        return $this->hasOne('App\EkuberPaymentFiles', trim('ekuber_filename'), trim('ekuber_filename'))
            ->where('ekuber_payment_files.client_id', '=', '3');
    }

    public function approvedby()
    {
        return $this->hasOne(User::class, 'id', 'approved_by')->select('id', 'username', 'name');
    }

    public function dwachqonetrans()
    {
        return $this->hasOne(DWAChequeTransactions::class, 'dwa_cheques_id', 'id');
    }

    public function scopeChequeDataForBmsInsertWorkVoucher($query, $chequeId)
    {
        return $query->whereHas('dwaChequePaymentFileMapping.paymentfiles', function ($innerquery) {
            $innerquery->where('invalidate_type', '!=', '21')
                ->where('is_sent_to_ekuber', 1)
                ->where('client_id', 3)
                ->whereHas('ekubertrans');
        })
            ->whereHas('dwachqonetrans.chqtransdet', function ($tq) {
                $tq->whereNotNull('voucherno')
                    ->whereNotNull('voucherdate')
                    ->whereNotNull('dwa_account_type_id')
                    // ->whereIn('transtype', [6, 12])
                    ->whereHas('headDesc');
            })
            ->with(['dwachqonetrans.chqtransdet' => function ($tq) {
                $tq->whereNotNull('voucherno')
                    ->whereNotNull('voucherdate')
                    ->whereNotNull('dwa_account_type_id')
                    // ->whereIn('transtype', [6, 12])
                    ->whereHas('headDesc')
                    ->with('headDesc')
                    ->with('beneficieries.dwaBillDeductions');
            }])
            ->whereHas('createdby')
            ->with('createdby')
            ->where('id', $chequeId)
            ->whereNotNull('voucher_date')
            ->where('status', 1)
            ->whereNotNull('ekuber_filename');
    }

    public function ekuber_tranasctions()
    {
        return $this->hasMany('App\EkuberTransactions', 'orgnl_msg_id', 'ekuber_filename')
            ->where('client_id', '=', 3)
            ->orderBy('created_at');
    }

    public function ekuber_sent()
    {
        return $this->hasOne('App\EkuberTransactions', 'orgnl_msg_id', 'ekuber_filename')
            ->where('client_id', '=', 3)
            ->where('transtype', '=', TransTypeStatuses::SENT)->orderBy('created_at');
    }

    public function ekuber_ack_accept()
    {
        return $this->hasOne('App\EkuberTransactions', 'orgnl_msg_id', 'ekuber_filename')
            ->where('client_id', '=', 3)
            ->where('transtype', '=', TransTypeStatuses::ACK_ACCP);
    }

    public function ekuber_ack_reject()
    {
        return $this->hasOne('App\EkuberTransactions', 'orgnl_msg_id', 'ekuber_filename')
            ->where('client_id', '=', 3)
            ->where('transtype', '=', TransTypeStatuses::ACK_RJCT);
    }

    public function ekuber_dn()
    {
        return $this->hasOne('App\EkuberTransactions', 'orgnl_msg_id', 'ekuber_filename')
            ->where('client_id', '=', 3)
            ->where('transtype', '=', TransTypeStatuses::DN)->orderBy('created_at');
    }

    public function ekuber_transactionsdwa()
    {
        return $this->hasMany(EkuberTransactions::class, 'orgnl_msg_id', 'ekuber_filename')->where('client_id', 3);
    }

    public function ekuberpaymentfilesnotreadydwa()
    {

        return $this->hasMany('App\EkuberPaymentFiles', 'ekuber_filename', 'ekuber_filename')->whereIn('client_id', [3])->whereIn('reprocess_status', [0, 1]);
    }

    public function ekuber_ack_reject_dwa()
    {
        return $this->hasOne('App\EkuberFilesStatus', 'ekuber_filename', 'ekuber_filename')
            ->whereIn('client_id', [3])
            ->whereRaw("ack_filename::text not like '%ACCP%'")->whereNotNull('ack_filename')->whereNull('dn_filename');
    }

    public function ekuberpaymentfilesreadygen()
    {
        return $this->hasMany('App\EkuberPaymentFiles', trim('ekuber_filename'), trim('ekuber_filename'))->where('client_id', '=', '3')->whereIn('reprocess_status', [3, 4, 5]);
    }

    public function ekuberpaymentfilesready()
    {
        return $this->hasMany('App\EkuberPaymentFiles', trim('ekuber_filename'), trim('ekuber_filename'))->where('client_id', '=', '3')->where('reprocess_status', 2);
    }
}
