<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class ExpSubTypes extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'exp_subtypes';

    protected $table = "exp_subtypes";
    public $timestamps = false;
    protected $guarded = ['id'];

}
