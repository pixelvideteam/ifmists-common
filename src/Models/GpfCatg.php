<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class GpfCatg extends Model
{
    const CPS = 5;
    const ZP_GPF = 4;
    const GPF = 1;
    const GPF_IV = 3;
    const AG_GPF = 2;

    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'gpfcatg';

    protected $table = 'gpfcatg';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
