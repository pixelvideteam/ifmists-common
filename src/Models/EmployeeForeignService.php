<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeForeignService extends Model
{
   protected $table = 'employee_foreign_service';

    public function agency()
    {
        return $this->hasOne('App\AgencyDetails', 'id', 'agency_details_id');
    }
}
