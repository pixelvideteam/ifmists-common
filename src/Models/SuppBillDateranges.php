<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SuppBillDateranges extends Model {

	protected $table='supp_bill_dateranges';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function drawn() 
	{
		return $this->hasMany('App\SuppBillDaterangeDetails','supp_bill_dateranges_id','id')->where('drawn_due_flag','=','0');
	}

	public function due() 
	{
		return $this->hasMany('App\SuppBillDaterangeDetails','supp_bill_dateranges_id','id')->where('drawn_due_flag','=','1');
	}

	public function diff() 
	{
		return $this->hasMany('App\SuppBillDaterangeDetails','supp_bill_dateranges_id','id')->where('drawn_due_flag','=','2');
	}

    public function billmultiple()
    {
        return $this->belongsTo('App\BillMultipleParty','id', 'bill_multiple_party_id');
    }
}
