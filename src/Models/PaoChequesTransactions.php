<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoChequesTransactions extends Model
{
	protected $table='pao_cheques_transactions';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	public function chqtransdet()
	{
		return $this->hasOne('App\Transactions','id','transactions_id');
	}
	public function paochqdet()
	{
		return $this->hasOne('App\PaoCheques','id','pao_cheques_id');
	}
	public function multipleparty(){
	    return $this->hasOne(BillMultipleParty::class,'transaction_id','transactions_id');
    }
    public function multiplepartymany(){
        return $this->hasMany(BillMultipleParty::class,'transaction_id','transactions_id');
    }
}
