<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OldDesgCode extends Model {

	protected $table='desgcode_old';
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
