<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeDa extends Model 
{
	protected $table='employee_da';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
