<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class PromotionApprovals extends Model
{

	protected $table="promotion_approvals";
	protected $guarded=['id'];
	
	public function empmaster()
	{	
		return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	}

	public function prc()
	{
		return $this->belongsTo('App\Prc','prc_id','id');
	}

	public function desg()
	{
		return $this->belongsTo('App\DesgCode','desg_id','id');
	}

	public function scale()
	{
		return $this->belongsTo('App\Scale','scale_id','id');
	}

	public function cader()
	{
		return $this->belongsTo('App\Cader','cader_id','id');
	}

	public function docs()
	{	
		return $this->hasMany('App\PromotionDocs','promotion_approval_id','id');
	}

	public function prom_empearndedn()
	{	
		return $this->hasMany('App\PromotionEarndedn','promotion_approval_id','id');
	}

}