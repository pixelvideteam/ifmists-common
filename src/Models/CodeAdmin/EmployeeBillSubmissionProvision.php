<?php

namespace App\Models\CodeAdmin;

use Illuminate\Database\Eloquent\Model;

class EmployeeBillSubmissionProvision extends Model
{
    protected $table = 'employee_bill_submission_provisions';

    const PROVISION = 1;
    const STOP = 0;
}
