<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 25-02-2019
 * Time: 15:15
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class EkuberClients extends Model
{
    protected $table ="ekuber_clients";
}