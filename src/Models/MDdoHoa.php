<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MDdoHoa extends Model
{
    protected $table='mddohoa';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
