<?php

namespace App\Models\URN;

use Illuminate\Database\Eloquent\Model;

class UrnFinYear extends Model
{
    protected $table = 'urn_fin_years';
}
