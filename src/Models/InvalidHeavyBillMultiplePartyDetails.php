<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvalidHeavyBillMultiplePartyDetails extends Model
{
    protected $table = "invalid_heavy_bill_multiple_party_details";
    public $timestamps = false;
    protected $guarded = ["id"];
}
