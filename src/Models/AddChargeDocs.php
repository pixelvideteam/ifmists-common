<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AddChargeDocs extends Model
{
    protected $table = "add_charge_docs";
    protected $guarded = ['id'];
}
