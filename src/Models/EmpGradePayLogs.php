<?php namespace App;

use App\Helper\Storage\StorageHelper;
use Illuminate\Database\Eloquent\Model;

class EmpGradePayLogs extends Model
{
    protected $table = 'empgradepay_logs';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
