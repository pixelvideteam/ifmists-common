<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class MScroll extends Model
{
    protected $table = 'mscroll';
    public $timestamps = false;
}
