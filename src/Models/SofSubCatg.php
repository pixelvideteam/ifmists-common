<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 17-06-2019
 * Time: 20:53
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class SofSubCatg extends Model
{
    protected $table = 'sof_sub_catg';
    public function sofcatg(){
        return $this->hasOne(SofCatg::class,'id','sof_catg_id');
    }
}