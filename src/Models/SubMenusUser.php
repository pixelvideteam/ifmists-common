<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMenusUser extends Model 
{
	protected $table='sub_menus_users';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
