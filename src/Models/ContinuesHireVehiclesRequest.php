<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContinuesHireVehiclesRequest extends Model
{
    protected $table = 'continues_hire_vehicles_request';
    protected $guarded = ['id'];

    const StatusPending = 0;
    const StatusApproved = 1;
    const StatusReject = 2;
    const StatusInProcess = 3;
    const StatusCancelled = 4;
    const StatusReturned = 6;

    const DcmUserRole = 315;
    const AssistantSecOfficerUserRole = 310;
    const SecOfficerUserRole = 317;
    const AssistantSecretaryUserRole = 311;
    const DeputySecretaryUserRole = 312;
    const DWAJointSecretaryUserRole = 305;
    const PFSDUMMYUserRole = 313;
    const PFSUserRole = 37;
    const GovtUserRole = 37;
    const RequestSubmittedUserRole = self::DcmUserRole;
//    const FirstScrutinyUserRole = self::AssistantSecOfficerUserRole;
    const FirstScrutinyUserRole = self::SecOfficerUserRole;

    const GovtUserID = 63524;
    const PfsUserID = 63714;

    public function hod() {
        return $this->hasOne('App\HODList', 'id', 'hod_id');
    }

    public function department() {
        return $this->hasOne('App\Department', 'id', 'dept_id');
    }


    public static function nextUserRole($currUserRole) {
        $nextUserRole = null;
        if ($currUserRole) {
            switch ($currUserRole) {
                case ContinuesHireVehiclesRequest::DcmUserRole:
                    /*$nextUserRole = ContinuesHireVehiclesRequest::AssistantSecOfficerUserRole;
                    break;
                case ContinuesHireVehiclesRequest::AssistantSecOfficerUserRole:*/
                    $nextUserRole = ContinuesHireVehiclesRequest::SecOfficerUserRole;
                    break;
                case ContinuesHireVehiclesRequest::SecOfficerUserRole:
                    $nextUserRole = ContinuesHireVehiclesRequest::AssistantSecretaryUserRole;
                    break;
                case ContinuesHireVehiclesRequest::AssistantSecretaryUserRole:
                    /*$nextUserRole = ContinuesHireVehiclesRequest::DeputySecretaryUserRole;
                    break;
                case ContinuesHireVehiclesRequest::DeputySecretaryUserRole:*/
                    $nextUserRole = ContinuesHireVehiclesRequest::GovtUserRole;
                    break;
                /*case ContinuesHireVehiclesRequest::DWAJointSecretaryUserRole:
                    $nextUserRole = ContinuesHireVehiclesRequest::PFSUserRole;
                    break;*/
                case ContinuesHireVehiclesRequest::GovtUserRole:
                    $nextUserRole = ContinuesHireVehiclesRequest::PFSUserRole;
                    break;
                case ContinuesHireVehiclesRequest::PFSUserRole:
                    $nextUserRole = ContinuesHireVehiclesRequest::RequestSubmittedUserRole;
                    break;
                default:
                    break;
            }
        }

        return $nextUserRole;
    }

    public function docs() {
        return $this->hasMany('App\HireVehiclesRequestDocs', 'vehicles_request_id', 'id');
    }

    public function provision_of_vehicles() {
        return $this->hasMany('App\ProvisionOfVehicles', 'vehicles_request_id', 'id');
    }

    public function curr_user_role_details() {
        return $this->hasOne('App\UserRoles', 'role', 'curr_user_role');
    }

    public function curr_user() {
        return $this->hasOne('App\User', 'id', 'curr_user');
    }

    public function request_cycle() {
        return $this->hasMany('App\VehiclesRequestTracking', 'continues_hire_vehicles_request_id', 'id');
    }

}
