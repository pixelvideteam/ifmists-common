<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class UserFingerPrint extends Model
{
    protected $table = 'user_fingerprint';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    public static function createOrupdate($data = [])
    {
        $finger_print = self::where('user_id', $data['user_id'])
            ->first();
        if (empty($finger_print)) {
            $finger_print = new self();
            $finger_print->user_id = $data['user_id'];
        }
        if (empty($finger_print->r1)) {
            $finger_print->r1 = $data['fingerprint'];
            $finger_print->r1_bmp = $data['fingerprint_image'];
        } elseif (empty($finger_print->l1)) {
            $finger_print->l1 = $data['fingerprint'];
            $finger_print->l1_bmp = $data['fingerprint_image'];
        }
        $finger_print->save();
        if (!empty($finger_print->r1) && !empty($finger_print->l1)) {
            $user = User::find($data['user_id']);
            $user->fingerprint_login = 1;
            $user->redirect_flag = 0;
            $user->save();
            Cache::forget(Session::SESSION_PREFIX.$data['tkn']);
        }

        return $finger_print;
    }
}

