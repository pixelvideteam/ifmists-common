<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EKuberReturnsSecuritycodes extends Model {

	protected $table = "ekuber_returns_securitycodes";
	public $timestamps = false;

	protected $guarded = ["id"];
	public function multipleparty() {
		return $this->hasOne('App\BillMultipleParty','id','bill_multiple_party_id');
	}
}