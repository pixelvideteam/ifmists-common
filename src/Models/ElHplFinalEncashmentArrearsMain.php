<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ElHplFinalEncashmentArrearsMain extends Model {

	protected $table='el_hpl_final_encashment_arrears_main';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
