<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HireVehiclesRequestDocs extends Model
{
    protected $table = 'hire_vehicles_request_docs';
    protected $guarded = ['id'];

}
