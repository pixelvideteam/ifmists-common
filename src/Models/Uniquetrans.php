<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Uniquetrans extends Model
{
    protected $table = "uniquetrans";
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
