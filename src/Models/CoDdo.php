<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoDdo extends Model
{
    protected $table='coddo';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function ddo()
    {
        return $this->hasOne('App\User','username','coddo');
    }
}
