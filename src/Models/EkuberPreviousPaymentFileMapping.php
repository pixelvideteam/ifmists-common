<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 27-12-2018
 * Time: 17:39
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;


class EkuberPreviousPaymentFileMapping extends Model
{
    protected $table="ekuber_previous_file_mapping";
    protected $guarded=['id'];
}