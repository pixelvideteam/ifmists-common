<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Tutorials extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'tutorials';

    protected $table = "tutorials";
    protected $guarded = ['id'];

    public function modules()
    {
        return $this->hasOne(Modules::class, 'id', 'module_id');
    }

    // public function todesg()
    // {
    // 	return $this->belongsTo('App\DesgCode','to_desgcode_id','id');
    // }

    // public function empearndedn()
    // {
    // 	return $this->hasMany('App\AddChargeEarnings','add_charge_id','id');
    // }
}
