<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoCounterMh extends Model {

	protected $table='pao_counter_mh';
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
