<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveCreditDates extends Model {

	protected $table='leave_credit_dates';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
