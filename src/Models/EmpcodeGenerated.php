<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpcodeGenerated extends Model
{
    protected $table = "empcode_generated";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function getByEmpcode($empcode)
    {
        return self::where('empcode', $empcode)
            ->with(['gender' => function ($q) {
                $q->select('id', 'desc');
            }])
            ->with(['marital_status' => function ($q) {
                $q->select('id', 'desc');
            }])
            ->with(['cadre' => function ($q) {
                $q->select('id', 'description');
            }])
            ->with(['scale_type' => function ($q) {
                $q->select('id', 'type');
            }])
            ->with(['department' => function ($q) {
                $q->select('id', 'name');
            }])
            ->first();
    }

    public function gender()
    {
        return $this->belongsTo(Genders::class, 'gender_id', 'id');
    }

    public function marital_status()
    {
        return $this->belongsTo(MaritalStatus::class, 'marital_status_id', 'id');
    }

    public function cadre()
    {
        return $this->belongsTo(Cader::class, 'cadre_id', 'id');
    }

    public function scale_type()
    {
        return $this->belongsTo(ScaleTypes::class, 'scale_type_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }
}
