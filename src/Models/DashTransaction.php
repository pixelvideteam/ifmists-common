<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DashTransaction extends Model {

	//
	protected $table = "dash_transactions";
	// public $timestamps = false;rans

	protected $guarded = ["id"];

	public function approvedby() {

		return $this->hasOne('App\User','id','approved_by');
	}

}
