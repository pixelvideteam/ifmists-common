<?php namespace App;

use App\Helper\Storage\StorageHelper;
use App\SubscriptionNominees;
use Illuminate\Database\Eloquent\Model;

class EmployeeMasterESR extends EmployeeMaster
{
    public function leavescount()
    {
        return $this->hasOne('App\LeavesCount', 'employee_id', 'id');
    }

    public function familydetails()
    {
        return $this->hasMany('App\FamilyDetails', 'employee_id', 'id');
    }

    public function religion()
    {
        return $this->hasOne('App\Religions', 'id', 'religion_id');
    }

    public function qualification_before()
    {
        return $this->hasOne('App\Qualifications', 'id', 'qualification_before_entry_id');
    }

    public function qualification_after()
    {
        return $this->hasOne('App\Qualifications', 'id', 'qualification_after_entry_id');
    }

    public function gpfNominees()
    {
        return $this->hasMany(SubscriptionNominees::class, 'employee_id', 'id')->where('subscription_type', SubscriptionNominees::TYPE_GPF);
    }

    public function tsgliNominees()
    {
        return $this->hasMany(SubscriptionNominees::class, 'employee_id', 'id')->where('subscription_type', SubscriptionNominees::TYPE_TSGLI);
    }

    public function gisNominees()
    {
        return $this->hasMany(SubscriptionNominees::class, 'employee_id', 'id')->where('subscription_type', SubscriptionNominees::TYPE_GIS);
    }

    public function pensionNominees()
    {
        return $this->hasMany(SubscriptionNominees::class, 'employee_id', 'id')->where('subscription_type', SubscriptionNominees::TYPE_SERVICE_PENSION);
    }

    /* Pay Particular Relations Start */
    public function annualGradePay()
    {
        return $this->hasMany(ServiceAgi::class, 'employee_id', 'id');
    }

    public function demotion()
    {
        return $this->hasMany(ServiceDemotions::class, 'employee_id', 'id');
    }

    public function deputation()
    {
        return $this->hasMany(ServiceDeputations::class, 'employee_id', 'id');
    }

    public function probation()
    {
        return $this->hasMany(ServiceProbationDetails::class, 'employee_id', 'id');
    }

    public function promotion()
    {
        return $this->hasMany(ServicePromotions::class, 'employee_id', 'id');
    }

    public function revisedPayScale() // Pay Fixation => RevisedPayScale
    {
        return $this->hasMany(ServiceRevisedPayScale::class, 'employee_id', 'id');
    }

    public function automaticAdvanceScheme() // Automatic Advance Scheme => SpecialGradePay
    {
        return $this->hasMany(ServiceAutomaticAdvanceScheme::class, 'employee_id', 'id');
    }

    public function suspension()
    {
        return $this->hasMany(ServiceSuspensions::class, 'employee_id', 'id');
    }

    public function transfer()
    {
        return $this->hasMany(ServiceTransfers::class, 'employee_id', 'id');
    }
    /* Pay Particular Relations End */


}
