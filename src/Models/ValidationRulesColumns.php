<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 21-11-2019
 * Time: 21:29
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValidationRulesColumns extends Model
{
    protected $table = 'validation_rules_columns_tables';
}
