<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AddChargeEarnings extends Model
{
    protected $table = "add_charge_earnings";
    protected $guarded = ['id'];

    public function earndedn()
    {
        return $this->belongsTo('App\EarnDednList', 'earndedn_id', 'id');
    }

    // public function querydata()
    // {
    // 	return $this->belongsTo('App\Query','query_id','id');
    // }
}
