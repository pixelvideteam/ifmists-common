<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CovidEmployeeBillDetails extends Model
{
    protected $table = 'covid_employee_bill_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
