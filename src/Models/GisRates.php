<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GisRates extends Model {

	protected $table='gis_rates';
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
