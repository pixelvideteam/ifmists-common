<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class SupBillDaUpdates extends Model{

	protected $table="sup_bill_da_updates";
	protected $guarded=['id'];
	
	// public function empmaster()
	// {	
	// 	return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	// }

	// // public function querydata()
	// // {
	// // 	return $this->belongsTo('App\Query','query_id','id');
	// // }
	public function dateranges() 
	{
		return $this->hasMany('App\SuppBillDateranges','sup_bill_da_update_id','id');
	}

}