<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class TransactionDocs extends Model
{

	protected $table="transaction_docs";
	protected $guarded=['id'];
	
}