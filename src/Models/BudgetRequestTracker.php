<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestTracker extends Model
{
    protected $table = 'budget_request_tracker';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
