<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class EmpDDOHodDets extends Model{

		protected $table="emp_ddohoddets";
		// public $timestamps = false;

		protected $guarded=['id'];

		public function caddets()
		{	
			return $this->belongsTo('App\EmpCadre','cadre_list_table_id','id');
		}
	}
