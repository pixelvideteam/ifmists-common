<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DaArrearsMain extends Model {

	protected $table='da_arrears_main';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
