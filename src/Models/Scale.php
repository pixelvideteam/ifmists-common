<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Scale extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'scales';

    protected $table = 'scales';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function scaledetails()
    {
        return $this->hasMany('App\ScaleDetails', 'scale_id', 'id')->orderBy('start_basic');
    }

    public function masterscale()
    {
        return $this->hasOne('App\MasterScalesPrc', 'id', 'master_scale_id');
    }

    public function dapercentage()
    {
        return $this->hasMany('App\DAMasters', 'prc_id', 'prc_id');
    }
}
