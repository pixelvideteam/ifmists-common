<?php


namespace App;
use Illuminate\Database\Eloquent\Model;


class TransactionsAcbillsImpact extends Model
{

    protected $table = 'transactions_acbills_impact';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
