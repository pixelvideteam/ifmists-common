<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class QueriesDocs extends Model 
{
	protected $table='queries_docs';
	protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $dateFormat = 'Y-m-d H:i:s';

    public function getDateFormat()
    {
        return 'Y-m-d H:i:s';
    }
}
