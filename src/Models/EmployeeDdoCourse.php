<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeDdoCourse extends Model {

	protected $table='employee_ddo_course';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	protected $hidden = ["created_at", "updated_at"];

	public function empmaster() 
	{
		return $this->hasOne('App\EmployeeMaster','id','employee_id');
	}

	public function status() 
	{
		return $this->hasOne('App\WorkingStatus','id','working_status_id');
	}

    public function empbilldet()
    {
        return $this->hasMany('App\BillMultipleParty','emp_id','employee_id');
    }
}
