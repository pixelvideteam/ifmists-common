<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class EmployeeLeavesDetails extends Model{

	protected $table="employee_leaves_details";
	protected $guarded = ['id', 'created_at', 'updated_at'];
	public function empleave() {

		return $this->hasOne('App\EmployeeLeaves','id','employee_leaves_id');
	}
	public function leave() {

		return $this->hasOne('App\LeavesMaster','id','leaves_id');
	}
}
