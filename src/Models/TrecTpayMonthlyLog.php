<?php
/**
 * Created by PhpStorm.
 * User: Shesha Rao Puli
 * Date: 02-12-2020
 * Time: 11:37 AM
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;


class TrecTpayMonthlyLog extends Model
{
    protected $table = 'trec_tpay_monthly_log';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
