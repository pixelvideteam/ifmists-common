<?php

namespace App\Models\PD;

use Illuminate\Database\Eloquent\Model;

class KLTransactionDetails extends Model
{
    protected $table = 'kl_transaction_details';

    const kalyan_laxmi_hoa_arr = [
        '8443008000056000000NVN',
        '8443008000057000000NVN',
        '8443008000058000000NVN',
        '8443008000059000000NVN'
    ];
}
