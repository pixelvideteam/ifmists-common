<?php

namespace App\Models\PD;

use Illuminate\Database\Eloquent\Model;

class PDLocBankScroll extends Model
{
    protected $table = 'pd_loc_bank_scrolls';

    const TYPE_KALYAN_LAXMI = 1;
    const TYPE_CMRF = 2;
}
