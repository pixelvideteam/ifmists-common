<?php

namespace App\Models\PD;

use App\Transactions;
use App\User;
use Illuminate\Database\Eloquent\Model;

class KlBankList extends Model
{
    protected $table = 'kl_bank_lists';

    public function transaction()
    {
        return $this->hasOne(Transactions::class, 'id', 'transaction_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
