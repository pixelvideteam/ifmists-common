<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class BDWiseCadreStrength extends Model{

	protected $table="bill_id_wise_cadre_strength";
	protected $guarded=['id'];
	
	public function desg()
	{	
		return $this->belongsTo('App\DesgCode','desg_id','id');
	}

	// public function querydata()
	// {
	// 	return $this->belongsTo('App\Query','query_id','id');
	// }
}