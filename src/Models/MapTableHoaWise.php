<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MapTableHoaWise extends Model {

    protected $table='maptable_hoawise';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
