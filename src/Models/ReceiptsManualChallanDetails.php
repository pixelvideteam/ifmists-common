<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ReceiptsManualChallanDetails extends Model
{

    protected $table = 'receipts_manual_challans';

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;

    const CUSTOMER_TYPE_ORG = 1;
    const CUSTOMER_TYPE_INDI = 2;
    const CUSTOMER_TYPE_FOREIGN_SERVICE = 3;
    const CUSTOMER_TYPE_RTA = 4;
    const CUSTOMER_TYPE_MANUAL_CHALLAN_NUMBER_GENERATION = 5;

    const CUST_ID_TYPE_ARR = [
        'adhaar' => 1,
        'pan' => 2,
        'Other' => 3,
    ];

    public function ddo_details()
    {
        return $this->hasOne(\App\User::class, 'username', 'ddocode');
    }

}
