<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MDaImpCode extends Model {

	protected $table='mdaimpcode';
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
