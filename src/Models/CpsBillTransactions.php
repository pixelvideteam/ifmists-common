<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class CpsBillTransactions extends Model
{
    protected $table = "cps_bill_transactions";
    protected $fillable = ['id','created_at','updated_at'];
    public function tokendets()
    {
        return $this->hasOne('App\Transactions', 'id', 'transaction_id')->select('id','ddocode','bill_month','bill_year','transid','tokenno','hoa','gross','net','purpose');
    }
}
