<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Years extends Model
{
    protected $table = 'years';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
