<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class TransactionsTransstatuses extends Model
{
    const DWA_BILL_EDIT_MODE = 306;
    const DWA_BILL_XML_FILE_GENERATED = 308;
    const BILL_PENDING_FOR_APPROVAL = 0; // for DWA supdt will generate token
    const DWA_BILL_AT_AUDITOR = 301;
    const DWA_BILL_AT_SUPDT = 300;
    const DWA_BILL_AT_PAO = 302;
    const DWA_BILL_AT_GOVT = 303;
    const BILL_WITH_AUDITOR_FOR_SCRUTINY_STATUS = 1;
    const BILL_WITH_SUPT_FOR_SCRUTINY_STATUS = 2;
    const BILL_WITH_OFFICER_FOR_SCRUTINY_STATUS = 3;
    const BILL_WITH_PAO_FOR_SCRUTINY_STATUS = 4;
    const BILL_WITH_APAOCHEQ_FOR_SCRUTINY_STATUS = 13;
    const BILL_WITH_CHEQCOUNTER_FOR_SCRUTINY_STATUS = 10;
    const DWA_BILL_READY_FOR_PAYMENT_AT_PAO = 304; //XML generation
    const DWA_BILL_PENDING_AT_CASHIER = 305;
    const DWA_BILL_PENDING_FOR_XML_AT_PAO = 306;
    const DWA_BILL_PAID = 310;
    const DWA_BILL_PENDING_FOR_TOKEN_GENERATION = 0;
    const  DWA_BILL_REJECT = 21;
    const BILL_SUBMITTED = 0;
    const PAO_BILL_PENDING_WITH_AUDITOR = 1;
    const DWA_VOUCHER_GENERATED = 307;
    const DWA_BMS_BILL_PAID = 204;
    const DTA_BILL_PENDING_FOR_XML = 150;
    const DTA_BILL_XML_GENERATED = 155;
    const RECEIPTS_CONFIRMED = 405;
    const BILL_PENDING_APPO = 113;
    const PAO_BILL_PAID = 14;
    const DTA_BILL_PAID = 160;
    const DWA_CORP_BILL_AT_PAO = 311; // for PAY ORDER GENERATION
    const DWA_CORP_BILL_FORWARDED_TO_MD = 312;

    const PENSION_BILL_INSERTED_IN_IMPACT = '992';

    const ALL_PENDING_STATUSES = [
        self::BILL_PENDING_FOR_APPROVAL,
        self::DWA_BILL_AT_SUPDT,
        self::DWA_BILL_AT_AUDITOR,
        self::DWA_BILL_AT_PAO,
        self::DWA_BILL_AT_GOVT,
        self::DWA_CORP_BILL_AT_PAO,
        self::DWA_CORP_BILL_FORWARDED_TO_MD
    ];


    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'transactions_transstatuses';

    protected $table = 'transactions_transstatuses';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
