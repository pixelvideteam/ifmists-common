<?php

namespace App\Models\GovtApp;

use Illuminate\Database\Eloquent\Model;

class PfsMobileAppUpdate extends Model
{
    protected $table = 'pfs_mobile_app_updates';
}
