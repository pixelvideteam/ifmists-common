<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceProbationDetails extends Model
{
    protected $table = 'esr_service_probation_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getAllEmployeeProbationDetails($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with(['cadre', 'hod'])
            ->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)
            ->with(['cadre', 'hod'])
            ->first();
    }

    public function cadre()
    {
        return $this->hasOne(Cader::class, 'id', 'cadre_id');
    }

    public function hod()
    {
        return $this->hasOne(HODList::class, 'id', 'hod_id');
    }
}
