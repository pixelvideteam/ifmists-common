<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeEarnDednHistory extends Model {

	protected $table='employee_earndedn_history';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function earndedn() 
	{
		return $this->hasOne('App\EarnDednList','id','earndedn_id')->orderBy("sdh");
	}
}
