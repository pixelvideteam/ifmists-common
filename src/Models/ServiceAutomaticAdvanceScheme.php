<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceAutomaticAdvanceScheme extends Model
{
    protected $table = 'esr_automatic_advance_scheme';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getAllEmployeeAutomaticAdvanceScheme($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with([
                'dept.designations.desg', 'desg', 'hod',
                'prc.scales.scaledetails', 'oldscale.scaledetails', 'newscale.scaledetails',
                'payidfuture', 'payidpresent', 'paytype'
            ])
            ->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)
            ->with([
                'dept.designations.desg', 'desg', 'hod',
                'prc.scales.scaledetails', 'oldscale.scaledetails', 'newscale.scaledetails',
                'payidfuture', 'payidpresent', 'paytype'
            ])
            ->first();
    }

    public function dept()
    {
        return $this->hasOne(Department::class, 'id', 'dept_id');
    }

    public function desg()
    {
        return $this->hasOne(DesgCode::class, 'id', 'desg_id');
    }

    public function hod()
    {
        return $this->hasOne(HODList::class, 'id', 'hod_id');
    }

    public function prc()
    {
        return $this->hasOne(Prc::class, 'id', 'prc_id');
    }

    public function oldscale()
    {
        return $this->hasOne(Scale::class, 'id', 'old_scale_id');
    }

    public function newscale()
    {
        return $this->hasOne(Scale::class, 'id', 'new_scale_id');
    }

    public function payidpresent()
    {
        return $this->hasOne(ScaleDetails::class, 'id', 'old_pay_id');
    }

    public function payidfuture()
    {
        return $this->hasOne(ScaleDetails::class, 'id', 'new_pay_id');
    }

    public function paytype()
    {
        return $this->hasOne(SpecialGradePayTypes::class, 'id', 'pay_type');
    }
}
