<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class EmployeeLeavesList extends Model{

	protected $table="employee_leaves_list";
	protected $guarded = ['id', 'created_at', 'updated_at'];
	public function leave() {

		return $this->hasOne('App\LeavesMaster','id','leaves_id');

	}
	public function empdetails() {

		return $this->hasOne('App\EmployeeMaster','id','employee_id');
	}
}
