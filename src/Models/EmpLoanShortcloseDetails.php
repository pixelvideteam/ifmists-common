<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpLoanShortcloseDetails extends Model {

	protected $table='emp_loan_shortclose_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
