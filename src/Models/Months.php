<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Months extends Model
{

    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'months';

    protected $table = 'months';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
