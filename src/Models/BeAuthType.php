<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BeAuthType extends Model
{
    const Normal = 1;
    const TreasuryRelaxed = 2;
    const Normal_Relaxed = 3;
    protected $table="be_auth_type";
    protected $guarded=['id'];
}
