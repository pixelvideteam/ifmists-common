<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BillScheduleCatWise extends Model {

	protected $table='bill_schedule_catwise';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
