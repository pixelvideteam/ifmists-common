<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class GpSalUniqueBill extends Model
{
    protected $table = 'gp_sal_unique_bill';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function gp_emp_dets()
    {
        return $this->hasOne('App\GpEmployeeDetails', 'id', 'gp_employee_details_id');
    }
}
