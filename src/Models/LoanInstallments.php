<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanInstallments extends Model 
{
	protected $table='loan_installments';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	protected $hidden = ["created_at", "updated_at"];

	public function billmul() 
	{
		return $this->hasOne('App\BillMultipleParty','id','bill_multiple_party_id');
	}
}
