<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class HraCatg extends Model {

	protected $table='hracatg';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
