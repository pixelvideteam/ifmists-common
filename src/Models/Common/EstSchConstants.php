<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 06-08-2019
 * Time: 18:44
 */
declare(strict_types=1);

namespace App\Models\Common;


class EstSchConstants
{
    public static $estSchContants = array(
        0 => array('dh' => '010', 'sdh' => '011', 'desc' => 'Pay', 'type' => '1'),
        1 => array('dh' => '010', 'sdh' => '012', 'desc' => 'Allowances', 'type' => '1'),
        2 => array('dh' => '010', 'sdh' => '013', 'desc' => 'Dearness Allowance', 'type' => '1'),
        3 => array('dh' => '010', 'sdh' => '014', 'desc' => 'Sumptuary Allowance', 'type' => '1'),
        4 => array('dh' => '010', 'sdh' => '015', 'desc' => 'Interim Relief', 'type' => '1'),
        5 => array('dh' => '010', 'sdh' => '016', 'desc' => 'House Rent Allowance', 'type' => '1'),
        6 => array('dh' => '010', 'sdh' => '017', 'desc' => 'Medical Reimbursement', 'type' => '1'),
        7 => array('dh' => '010', 'sdh' => '018', 'desc' => 'Encashment of Earned Leave', 'type' => '1'),
        8 => array('dh' => '010', 'sdh' => '019', 'desc' => 'Leave Travel Concession', 'type' => '1'),
        9 => array('dh' => '020', 'sdh' => '000', 'desc' => 'Wages', 'type' => '1'),
        10 => array('dh' => '020', 'sdh' => '021', 'desc' => 'Daily Wage Employees', 'type' => '1'),
        11 => array('dh' => '020', 'sdh' => '022', 'desc' => 'Full Time contingent Employees', 'type' => '1'),
        12 => array('dh' => '020', 'sdh' => '023', 'desc' => 'Part Time contingent Employees', 'type' => '1'),
        13 => array('dh' => '270', 'sdh' => '273', 'desc' => 'Wort charged Establishment', 'type' => '1'),
        14 => array('dh' => '310', 'sdh' => '311', 'desc' => 'Grants In Aid Towards Salary', 'type' => '1'),
        15 => array('dh' => '110', 'sdh' => '1ll', 'desc' => 'Travelling Allowance', 'type' => '1'),
        16 => array('dh' => '110', 'sdh' => '112', 'desc' => 'Bus Warrants', 'type' => '1'),
        17 => array('dh' => '110', 'sdh' => '113', 'desc' => 'T.A/D.A to Non Official Members', 'type' => '1'),
        18 => array('dh' => '110', 'sdh' => '114', 'desc' => 'Fixed Travel Allowances', 'type' => '1'),
        19 => array('dh' => '110', 'sdh' => '115', 'desc' => 'Conveyance Mowance', 'type' => '1'),
        20 => array('dh' => '120', 'sdh' => '121', 'desc' => 'Foreign Travel Expenses', 'type' => '1'),
        21 => array('dh' => '120', 'sdh' => '122', 'desc' => 'T.A/D.A to Non Official Members', 'type' => '1'),
        22 => array('dh' => '200', 'sdh' => '000', 'desc' => 'Other Administrative Expenses', 'type' => '1'),
        23 => array(
            'dh' => '130',
            'sdh' => '131',
            'desc' => 'Service Postage, Telegram and Telephone Charges',
            'type' => '1'
        ),
        24 => array('dh' => '130', 'sdh' => '132', 'desc' => 'Other Office Expenses', 'type' => '1'),
        25 => array('dh' => '130', 'sdh' => '133', 'desc' => 'Water & Electricity Charges', 'type' => '1'),
        26 => array('dh' => '130', 'sdh' => '134', 'desc' => 'Hiring of Private Vehicles', 'type' => '1'),
        27 => array('dh' => '140', 'sdh' => '000', 'desc' => 'Rents Rates and Taxes', 'type' => '1'),
        28 => array('dh' => '240', 'sdh' => '000', 'desc' => 'Petrol,Oil and Lubricants', 'type' => '1'),
        29 => array('dh' => '280', 'sdh' => '281', 'desc' => 'Pleaders Fees', 'type' => '1'),
        30 => array('dh' => '280', 'sdh' => '282', 'desc' => 'Payment to Home Guards', 'type' => '1'),
        31 => array('dh' => '280', 'sdh' => '283', 'desc' => 'Payments to Anganwadi Workers', 'type' => '1'),
        32 => array('dh' => '280', 'sdh' => '285', 'desc' => 'Sanitation Services', 'type' => '1'),
        33 => array('dh' => '280', 'sdh' => '286', 'desc' => 'Payments to Village Revenue Assistants', 'type' => '1'),
        34 => array('dh' => '300', 'sdh' => '000', 'desc' => 'Other Contractual Services', 'type' => '1'),
        35 => array('dh' => '300', 'sdh' => '301', 'desc' => 'Contract Appointments', 'type' => '1'),
        36 => array('dh' => '300', 'sdh' => '302', 'desc' => 'Outsourcing Engagements', 'type' => '1'),
        37 => array('dh' => '510', 'sdh' => '511', 'desc' => 'Maintenance of Office Vehicles', 'type' => '1'),
        38 => array('dh' => '270', 'sdh' => '271', 'desc' => 'Other Expenditure', 'type' => '1'),
        39 => array('dh' => '270', 'sdh' => '272', 'desc' => 'Maintenance', 'type' => '1'),
        40 => array('dh' => '270', 'sdh' => '275', 'desc' => 'Buildings', 'type' => '1'),
        41 => array('dh' => '270', 'sdh' => '278', 'desc' => 'Emergency Repairs', 'type' => '1'),
        42 => array('dh' => '800', 'sdh' => '807', 'desc' => 'User Charges Maintenance', 'type' => '1'),
        43 => array('dh' => '310', 'sdh' => '313', 'desc' => 'Per capita Grants', 'type' => '1'),
        44 => array('dh' => '310', 'sdh' => '318', 'desc' => 'Obsequies Charges', 'type' => '1'),
        45 => array('dh' => '040', 'sdh' => '041', 'desc' => 'Pensions', 'type' => '1'),
        46 => array('dh' => '040', 'sdh' => '042', 'desc' => 'Gratuities', 'type' => '1'),
        47 => array('dh' => '320', 'sdh' => '000', 'desc' => 'Contributions', 'type' => '1'),
        48 => array(
            'dh' => '001',
            'sdh' => '000',
            'desc' => 'Loans to Govt Servants, Institutions, adjustments to Fund Accounts',
            'type' => '1'
        ),
        49 => array(
            'dh' => '002',
            'sdh' => '000',
            'desc' => 'Loans to Govt Servants, Institutions, adjustments to Fund Accounts',
            'type' => '1'
        ),
        50 => array('dh' => '450', 'sdh' => '000', 'desc' => 'Interest', 'type' => '1'),
        51 => array('dh' => '530', 'sdh' => '534', 'desc' => 'Work charged Establishment', 'type' => '1'),
        52 => array('dh' => '560', 'sdh' => '000', 'desc' => 'Repayment of Borrowings', 'type' => '1'),
        53 => array('dh' => '630', 'sdh' => '631', 'desc' => 'Inter Account Transfers', 'type' => '1'),
        54 => array('dh' => '160', 'sdh' => '000', 'desc' => 'Publications', 'type' => '2'),
        55 => array('dh' => '210', 'sdh' => '211', 'desc' => 'Materials', 'type' => '2'),
        56 => array('dh' => '210', 'sdh' => '212', 'desc' => 'Drugs And Medicines', 'type' => '2'),
        57 => array('dh' => '220', 'sdh' => '000', 'desc' => 'Arms And Ammunition', 'type' => '2'),
        58 => array('dh' => '230', 'sdh' => '000', 'desc' => 'Cost of Ration/Diet Charges', 'type' => '2'),
        59 => array('dh' => '250', 'sdh' => '000', 'desc' => 'Clothing And Tentage', 'type' => '2'),
        60 => array('dh' => '260', 'sdh' => '000', 'desc' => 'Advertising And Publicity', 'type' => '2'),
        61 => array('dh' => '280', 'sdh' => '284', 'desc' => 'Other Payments', 'type' => '2'),
        62 => array('dh' => '410', 'sdh' => '000', 'desc' => 'Secret Service Expenditure', 'type' => '2'),
        63 => array('dh' => '500', 'sdh' => '000', 'desc' => 'Other Charges', 'type' => '2'),
        64 => array('dh' => '500', 'sdh' => '501', 'desc' => 'Compensation', 'type' => '2'),
        65 => array('dh' => '500', 'sdh' => '502', 'desc' => 'Transport Facility', 'type' => '2'),
        66 => array('dh' => '500', 'sdh' => '503', 'desc' => 'Other Expenditure', 'type' => '2'),
        67 => array('dh' => '500', 'sdh' => '504', 'desc' => 'Cosmetic Charges', 'type' => '2'),
        68 => array('dh' => '510', 'sdh' => '000', 'desc' => 'Motor Vehicles', 'type' => '2'),
        69 => array('dh' => '510', 'sdh' => '512', 'desc' => 'Purchases of Motor Vehicles', 'type' => '2'),
        70 => array('dh' => '520', 'sdh' => '000', 'desc' => 'Machinery and Equipment', 'type' => '2'),
        71 => array('dh' => '520', 'sdh' => '521', 'desc' => 'Purchases', 'type' => '2'),
        72 => array('dh' => '520', 'sdh' => '522', 'desc' => 'Tools And Plant', 'type' => '2'),
        73 => array(
            'dh' => '520',
            'sdh' => '523',
            'desc' => 'Deduct Receipts & Recoveries Towards Maintenance',
            'type' => '2'
        ),
        74 => array('dh' => '610', 'sdh' => '000', 'desc' => 'Depreciation', 'type' => '2'),
        75 => array('dh' => '800', 'sdh' => '801', 'desc' => 'User Charges Other Expenditure', 'type' => '2'),
        76 => array('dh' => '800', 'sdh' => '802', 'desc' => 'User Charges Transport Facility', 'type' => '2'),
        77 => array('dh' => '930', 'sdh' => '803', 'desc' => 'User Charges Travelling Allowances', 'type' => '2'),
        78 => array('dh' => '800', 'sdh' => '804', 'desc' => 'User Charges Utility Payments', 'type' => '2'),
        79 => array('dh' => '800', 'sdh' => '805', 'desc' => 'User Charges Other Office Expenses', 'type' => '2'),
        80 => array(
            'dh' => '800',
            'sdh' => '806',
            'desc' => 'User Charges Advertisements, Sales and Publicity',
            'type' => '2'
        ),
        81 => array('dh' => '800', 'sdh' => '808', 'desc' => 'User Charges Other Payments', 'type' => '2'),
        82 => array(
            'dh' => '800',
            'sdh' => '810',
            'desc' => 'User Charges Other Administrative Expenses',
            'type' => '2'
        ),
        83 => array('dh' => '800', 'sdh' => '811', 'desc' => 'User Charges Materials and Supplies', 'type' => '2'),
        84 => array('dh' => '800', 'sdh' => '812', 'desc' => 'User Charges Petrol, Oil and Lubricants', 'type' => '2'),
        85 => array('dh' => '800', 'sdh' => '814', 'desc' => 'User Charges Purchases', 'type' => '2'),
        86 => array('dh' => '800', 'sdh' => '815', 'desc' => 'User Charges Publications', 'type' => '2'),
        87 => array('dh' => '270', 'sdh' => '274', 'desc' => 'HTCC Charges', 'type' => '2'),
        88 => array('dh' => '310', 'sdh' => '312', 'desc' => 'Other Grants in aid', 'type' => '2'),
        89 => array('dh' => '310', 'sdh' => '314', 'desc' => 'Seignorage Grant', 'type' => '2'),
        90 => array('dh' => '310', 'sdh' => '315', 'desc' => 'F.C Grants', 'type' => '2'),
        91 => array('dh' => '310', 'sdh' => '316', 'desc' => 'Maintenance Grant', 'type' => '2'),
        92 => array(
            'dh' => '310',
            'sdh' => '317',
            'desc' => 'Exgratia Payments (Accidental death/compassionate)',
            'type' => '2'
        ),
        93 => array('dh' => '310', 'sdh' => '319', 'desc' => 'Grants for creation of capital Assets', 'type' => '2'),
        94 => array('dh' => '330', 'sdh' => '000', 'desc' => 'Subsidies', 'type' => '2'),
        95 => array('dh' => '340', 'sdh' => '000', 'desc' => 'Scholarships and stipends', 'type' => '2'),
        96 => array('dh' => '800', 'sdh' => '809', 'desc' => 'User Charges Other Grants in Aid', 'type' => '2'),
        97 => array('dh' => '800', 'sdh' => '813', 'desc' => 'User Charges  Scholarships and Stipends', 'type' => '2'),
        98 => array('dh' => '150', 'sdh' => '000', 'desc' => 'Royalty', 'type' => '2'),
        99 => array('dh' => '530', 'sdh' => '000', 'desc' => 'Major Works', 'type' => '2'),
        100 => array('dh' => '530', 'sdh' => '531', 'desc' => 'Other Expenditure', 'type' => '2'),
        101 => array('dh' => '530', 'sdh' => '532', 'desc' => 'Lands', 'type' => '2'),
        102 => array('dh' => '530', 'sdh' => '533', 'desc' => 'Buildings', 'type' => '2'),
        103 => array('dh' => '530', 'sdh' => '535', 'desc' => 'Price Adjustment', 'type' => '2'),
        104 => array('dh' => '540', 'sdh' => '000', 'desc' => 'Investment', 'type' => '2'),
        105 => array('dh' => '640', 'sdh' => '000', 'desc' => 'Writes off and Losses', 'type' => '2'),
        106 => array('dh' => '060', 'sdh' => '000', 'desc' => 'PRC', 'type' => '1'),
        107 => array('dh' => '110', 'sdh' => '111', 'desc' => 'Travelling Allowance', 'type' => '1'),
        108 => array('dh' => '050', 'sdh' => '000', 'desc' => 'Rewards', 'type' => '2'),
        109 => array('dh' => '000', 'sdh' => '000', 'desc' => '', 'type' => '1'),
        110 => array('dh' => '010', 'sdh' => '000', 'desc' => '', 'type' => '1'),
        111 => array('dh' => '004', 'sdh' => '000', 'desc' => '', 'type' => '1'),
        112 => array('dh' => '005', 'sdh' => '000', 'desc' => '', 'type' => '1'),


    );
}
