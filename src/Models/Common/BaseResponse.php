<?php

/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 22-10-2019
 * Time: 09:17
 */

declare(strict_types=1);

namespace App\Models\Common;


class BaseResponse
{
    public $status;
    public $data;
    public $message;
    public $messageCode;
    public $error;

    const MESSAGE_SUCCESS = 'Success';
    const EXECUTION_ERROR_CODE = 0;
    const PARAMS_ERROR_CODE = 2;
    public function make($data, $message, $messageCode = null, $error = null)
    {
        if (!is_null($data)) {
            $this->status = true;
            $this->data = $data;
        } else {
            $this->status = false;
        }
        if ($message === self::MESSAGE_SUCCESS) {
            $this->messageCode = '1';
            $this->message = $message;
        } else {
            $this->messageCode = $messageCode;
            $this->message = $message;
            $this->error = $error;
        }
    }
}
