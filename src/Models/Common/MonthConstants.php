<?php
/**
 * Created by PhpStorm.
 * User: yoges
 * Date: 19-07-2019
 * Time: 09:00
 */

namespace App\Models\Common;


class MonthConstants
{
    const JAN = 1;
    const FEB = 2;
    const MAR = 3;
    const APR = 4;
    const MAY = 5;
    const JUN = 6;
    const JUL = 7;
    const AUG = 8;
    const SEP = 9;
    const OCT = 10;
    const NOV = 11;
    const DEC = 12;
}