<?php
/**
 * Created by PhpStorm.
 * User: Shesha Rao Puli
 * Date: 24-08-2019
 * Time: 06:46
 */

namespace App\Models\Common;


class EarndednConstants
{
    const EARNING_SUB_DETAIL_HEADS = [
        '011' => 'PAY',
        '012' => 'ALLOW',
        '013' => 'DA',
        '014' => 'SUMP',
        '016' => 'HRA',
        '8793' => 'INTER-STATE', // not available in DB so take major head
    ];
    const OTHERS = "OTHERS";
    const TYPE_EARNING = 1;
    const TYPE_DEDUCTION = 2;
    const EL_ENCASHMENT_ID = 352;
    const EL_SURR_LEAVE = 547;
    const MH_8793 = '8793';
    const EARNING_ID_MAPPING = [
        'IR' => [320, 540],
        'ELEncash' => [352, 381],
        'PRCArr' => [504],
        'LTC' => [354],
        'MED-R' => [355],
        'SURR Leave' => [547],
    ];
    const DEDUCTIONS_ID_MAPPING = [
        'GPF-S' => [394, 395, 500, 501, 464, 434],
        'GPF-C' => [437, 368, 369, 460],
        'GLI' => [396, 397],
        'GPF-4' => [433, 455],
        'GIS-S' => [398, 451, 498, 514, 436],
        'P-TAX' => [399],
        'HRD' => [400, 449, 450, 453],
        'FA' => [401, 402, 489, 445],
        'EA' => [403],
        'HBA-P' => [404, 413, 407, 410, 787, 788],
        'HBA-I' => [405, 414, 415, 408, 409, 411, 412, 406],
        'CAR-P' => [416],
        'CAR-I' => [417, 418],
        'MC-P' => [419],
        'MC-I' => [420, 421],
        'CYC-P' => [422],
        'CYC-I' => [423, 424],
        'MAR-P' => [425],
        'MAR-I' => [426, 427],
        'I-TAX' => [428],
        'EWF-L' => [430, 431, 432],
        'CPS' => [435, 457],
        'EWF' => [429, 508],
        'MISC' => [392, 507],
        '8443-00-800-00-02-000-000' => [441, 499],
        '8658-00-102-00-14-000-000' => [459],
        '8009-01-104-00-01-000-000' => [442, 482],
        '8658-00-123-00-00-000-000' => [443],
        '7610-00-201-00-04-001-000' => [497],
        '7610-00-204-00-12-001-000' => [440],
        '0049-04-800-00-01-005-000' => [458, 490, 491],
        '0070-60-800-00-16-000-000' => [446],
        '7610-00-202-00-06-001-000' => [492],
        '7610-00-202-00-07-001-000' => [493],
        '7610-00-202-00-07-002-000' => [494],
        '7610-00-800-00-81-001-000' => [470],
        '7610-00-800-00-80-001-000' => [471],
        '7610-00-204-00-13-001-000' => [495],
        '7610-00-204-00-14-001-000' => [496],
        '7610-00-202-00-05-001-000' => [536],
        '0049-04-800-00-01-003-000' => [537, 538],
        '8342-00-120-00-11-001-000' => [473, 485],
    ];
    const EARNINGS_ORDER = ['PAY', 'ALLOW', 'DA', 'SUMP', 'IR', 'HRA', 'MED-R', 'ELEncash', 'PRCArr', 'LTC'];
    const DEDUCTIONS_ORDER = [
        'GPF-S',
        'GPF-C',
        'GLI',
        'GPF-4',
        'GIS-S',
        'P-TAX',
        'HRD',
        'FA',
        'EA',
        'HBA-P',
        'HBA-I',
        'CAR-P',
        'CAR-I',
        'MC-P',
        'MC-I',
        'CYC-P',
        'CYC-I',
        'MAR-P',
        'MAR-I',
        'I-TAX',
        'EWF-L',
        'CPS',
        'EWF',
        'MISC',
        '8443-00-800-00-02-000-000',
        '8658-00-102-00-14-000-000',
        '8009-01-104-00-01-000-000',
        '8658-00-123-00-00-000-000',
        '7610-00-201-00-04-001-000',
        '7610-00-204-00-12-001-000',
        '0049-04-800-00-01-005-000',
        '0070-60-800-00-16-000-000',
        '7610-00-202-00-06-001-000',
        '7610-00-202-00-07-001-000',
        '7610-00-202-00-07-002-000',
        '7610-00-800-00-81-001-000',
        '7610-00-800-00-80-001-000',
        '7610-00-204-00-13-001-000',
        '7610-00-204-00-14-001-000',
        '7610-00-202-00-05-001-000',
        '0049-04-800-00-01-003-000',
        '8342-00-120-00-11-001-000'
    ];

    public static function fetchEarningCategoryByID($earndednId)
    {
        foreach (self::EARNING_ID_MAPPING as $name => $earnIdArray) {
            foreach ($earnIdArray as $earnId) {
                if ($earnId == $earndednId) {
                    return $name;
                }
            }
        }
        return self::OTHERS;
    }

    public static function fetchEarningCategoryBySubHead($subDetailHead)
    {
        foreach (self::EARNING_SUB_DETAIL_HEADS as $detailHead => $name) {
            if ($detailHead == $subDetailHead) {
                return $name;
            }
        }
    }

    public static function fetchDeductionCategoryByID($earndednId)
    {
        foreach (self::DEDUCTIONS_ID_MAPPING as $name => $dednIdArray) {
            foreach ($dednIdArray as $dednId) {
                if ($dednId == $earndednId) {
                    return $name;
                }
            }
        }
        return self::OTHERS;
    }
}
