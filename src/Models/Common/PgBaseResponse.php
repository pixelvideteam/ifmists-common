<?php
declare(strict_types=1);
namespace App\Models\Common;
class PgBaseResponse
{
    const DEFAULT_SUCCESS_MSG_CODE = 1;
    const DEFAULT_FAIL_MSG_CODE = 101;

    public $status;
    public $messageCode;
    public $message;
    public $data;

    /**
     * BaseResponse constructor.
     * @param $messageCode
     * @param $message
     * @param $data
     */
    public function __construct($messageCode, $message, $data)
    {
        if (intval($messageCode) <= 100) {
            $this->status = true;
        } else {
            $this->status = false;
        }
        $this->messageCode = $messageCode;
        $this->message = $message;
        $this->data = $data;
    }
}
