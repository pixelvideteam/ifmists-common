<?php

namespace App\Models\HeavyBill;

use Illuminate\Database\Eloquent\Model;

class HeavyBillFile extends Model
{
    const TYPE_RYTHU_BANDHU = 1;
    const TYPE_MILK = 2;

    const PENDING = 0;
    const CRON_RUNNING = 1;
    const CRON_EXCEPTION = 2;
    const FILE_INVALID = 3;
    const DUPLICATE_PARTIES_IN_FILE = 4;
    const DUPLICATE_PARTIES_WITH_OTHER_FILES = 5;
    const FILE_VALID_FOR_RECTIFICATIONS = 6;
    const READY_FOR_BILL = 7;
    const BILL_SUBMITTED = 8;
    const CHEQUE_GENERATED = 9;
    const PAYMENT_FILE_GEN = 10;
    const BILL_DONE = 11;

    protected $guarded = ['id'];
    protected $table = 'heavy_bill_file';

}
