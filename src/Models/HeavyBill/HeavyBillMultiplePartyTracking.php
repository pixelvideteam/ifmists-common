<?php


namespace App\Models\HeavyBill;

use Illuminate\Database\Eloquent\Model;


class HeavyBillMultiplePartyTracking extends Model
{
    protected $table = 'heavy_bill_multiple_party_tracking';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
