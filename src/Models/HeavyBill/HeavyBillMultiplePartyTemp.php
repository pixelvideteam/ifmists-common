<?php

namespace App\Models\HeavyBill;

use Illuminate\Database\Eloquent\Model;

class HeavyBillMultiplePartyTemp extends Model
{
    protected $table = 'heavy_bill_multiple_party_temp';
    protected $fillable = ['unique_reference','aadhar_no','bankacno','ppbno'];
}
