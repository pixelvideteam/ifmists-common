<?php


namespace App\Models\HeavyBill;


use App\BankIfsc;
use Illuminate\Database\Eloquent\Model;

class HeavyBillEkuberReturnChallans extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_BILL_SUBMITTED = 1;
    const STATUS_DETAILS_UPDATED = 2;
    const RYTHU_BANDHU_RETURN_HOA = '8658001020020003000NVN';
    const MILK_BILL_RETURN_HOA = '8658001020020002000NVN';
    protected $table = 'heavy_bill_ekuber_return_challans';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function old_bank()
    {
        return $this->hasOne(BankIfsc::class, 'ifsccode', 'old_ifsc');
    }

    public function new_bank()
    {
        return $this->hasOne(BankIfsc::class, 'ifsccode', 'new_ifsc');
    }

    public function heavy_bill_multiple_party()
    {
        return $this->hasOne(HeavyBillMultipleParty::class, 'id', 'heavy_bill_multiple_party_id');
    }

}
