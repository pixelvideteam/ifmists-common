<?php


namespace App\Models\HeavyBill;

use App\BankIfsc;
use App\Transactions;
use Illuminate\Database\Eloquent\Model;


class HeavyBillMultipleParty extends Model
{
    const PARTY_VALID = 1;
    const PARTY_INVALID = 0;
    protected $table = 'heavy_bill_multiple_party';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function transaction()
    {
        return $this->hasOne(Transactions::class, 'id', 'transaction_id');
    }

    public function return_challan_party(){
        return $this->hasOne(HeavyBillEkuberReturnChallans::class,'heavy_bill_multiple_party_id','id');
    }

    public function bankifsc(){
        return $this->hasOne(BankIfsc::class,'ifsccode','ifsccode');
    }

    public function tokendets()
    {
        return $this->hasOne(Transactions::class, 'id', 'transaction_id');
    }

    public function tracking_dets()
    {
        return $this->hasMany(HeavyBillMultiplePartyTracking::class, 'heavy_bill_multiple_party_id', 'id')
            ->orderBy('id','asc');
    }

    public function org_ifsc()
    {
        return $this->hasOne(HeavyBillMultiplePartyTracking::class, 'heavy_bill_multiple_party_id', 'id')
            ->where('type','=','ifsccode')->orderBy('id','asc');
    }

    public function org_bankacno()
    {
        return $this->hasOne(HeavyBillMultiplePartyTracking::class, 'heavy_bill_multiple_party_id', 'id')
            ->where('type','=','bankacno')->orderBy('id','asc');
    }
}
