<?php


namespace App\Models\HeavyBill;

use Illuminate\Database\Eloquent\Model;


class HeavyBillSmsErros extends Model
{
    protected $table = 'heavy_bill_sms_error_logs';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
