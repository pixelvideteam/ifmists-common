<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MDdoBudgetUpdateLog extends Model
{
    protected $table='mddobudget_update_log';
}
