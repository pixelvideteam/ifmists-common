<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class EsrDownloadHistory extends Model
{
    protected $table = 'esr_download_history';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    const STATUS_DOWNLOAD = 1;
    const STATUS_FINAL_SUBMIT = 2;


}
