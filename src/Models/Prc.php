<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Prc extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'prc';

    protected $table = "prc";
    protected $guarded = ["id"];

    public function scales()
    {
        return $this->hasMany('App\Scale', 'prc_id', 'id');
    }
}
