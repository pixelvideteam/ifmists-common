<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 28-04-2019
 * Time: 20:38
 */
//declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class DepartmentDesigationMapping extends Model
{
    protected $table = 'department_designation_mapping';

    protected $fillable = ['department_id', 'desgcode_id', 'created_at','updated_at'];
    public function department(){
        return $this->hasOne(Department::class,'id','department_id');
    }
    public function desgination(){
        return $this->hasOne(DesgCode::class,'id','desgcode_id');
    }
}