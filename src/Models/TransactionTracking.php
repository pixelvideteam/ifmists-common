<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionTracking extends Model
{
    protected $table = 'transaction_tracking';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

	const CHANGE_AUDITOR_TYPE = 5;
	const SENT_FOR_PFS_APPROVAL = 10;
    const SENT_FOR_XML_VERIFICATION = 11;

	//Type 5 is mapping from one aud to another aud

    public function primaryuser()
    {
        return $this->hasOne('App\User', 'username', 'primary_user');
    }

    public function secondaryuser()
    {
        return $this->hasOne('App\User', 'username', 'secondary_user');
    }

    public function transactions()
    {
        return $this->hasOne('App\Transactions', 'id', 'transaction_id');
    }
}
