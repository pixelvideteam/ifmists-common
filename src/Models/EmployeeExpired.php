<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class EmployeeExpired extends Model{

	protected $table="employee_expired";
	protected $guarded=['id'];
	
}