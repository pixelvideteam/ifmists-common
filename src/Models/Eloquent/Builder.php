<?php

namespace App\Models\Eloquent;

use App\Fms\Security\BlindIndexEncryption;
use Illuminate\Database\Eloquent\Builder as BaseBuilder;

class Builder extends BaseBuilder
{

    protected $_ignoreBlindIndex = false;

    /**
     * @return $this
     */
    public function ignoreBlindIndex(): Builder
    {
        $this->_ignoreBlindIndex = true;
        return $this;
    }

    /**
     * @param array|\Closure|string $column
     * @param null $operator
     * @param null $value
     * @param string $boolean
     * @return Builder
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        if (method_exists($this->model, 'getEncrypted') && $this->_ignoreBlindIndex === false) {
            $encryptedFields = $this->model->getEncrypted();

            if (is_string($column) && !empty($encryptedFields[$column])) {
                $column = $encryptedFields[$column];

                if (!empty($operator) && !$value && is_string($operator)) {
                    $operator = BlindIndexEncryption::getBlindIndex($operator);
                } elseif (!empty($value) && is_string($value)) {
                    $value = BlindIndexEncryption::getBlindIndex($value);
                }

//                print_r("$column = $operator $value");
            }
        }

        return parent::where($column, $operator, $value, $boolean);
    }


}
