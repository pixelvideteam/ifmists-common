<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BillMultiplePartyHistory extends Model {

	protected $table='bill_multiple_party_history';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function tokendets() {
		return $this->hasOne('App\Transactions','id','transaction_id');
	}
	public function empbilldets() {
		return $this->hasMany('App\EmployeeBillDetails','bill_multiple_party_id','id');
	}
	public function empmaster() {
		return $this->hasOne('App\EmployeeMaster','id','emp_id');
	}
	public function loandets() {
		return $this->hasOne('App\BillLoanDetails','bill_multiple_party_id','id');
	}

	public function desg() 
	{
		return $this->hasOne('App\DesgCode','id','desg_id');
	}
}
