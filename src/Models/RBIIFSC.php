<?php
/**
 * Created by PhpStorm.
 * User: shreyan
 * Date: 5/29/2019
 * Time: 12:11 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class RBIIFSC extends Model
{
    protected $table='rbi_ifsc_codes';
}