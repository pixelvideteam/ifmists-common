<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequest extends Model
{
    protected $table = 'budget_request';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    const PENDING_HOD_AUTH = 0;
    const PENDING_SA = 1;
    const PENDING_JAO = 2;
    const PENDING_DD = 3;
    const AUTHORISED = 4;
    const DD = 5;
    public function budReqHoa()
    {
        return $this->hasMany('App\BudgetRequestHoa', 'budget_request_id', 'id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function reqtype()
    {
        return $this->hasOne('App\BeAuthType', 'id', 'be_auth_type_id');
    }
    public function docs()
    {
        return $this->hasMany(BudgetRequestDoc::class, 'budget_request_id', 'id');
    }
}
