<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TPayParty extends Model
{
    protected $connection = 'impact1819';
    protected $table = "tpayparty_pd";
}