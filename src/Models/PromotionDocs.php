<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionDocs extends Model 
{
	protected $table='promotion_docs';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
