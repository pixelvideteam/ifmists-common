<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class MasterScalesPrc extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'master_scales_prc';

    protected $table = "master_scales_prc";
    protected $guarded = ['id'];

    // public function empmaster()
    // {
    // 	return $this->belongsTo('App\EmployeeMaster','employee_id','id');
    // }

    // // public function querydata()
    // // {
    // // 	return $this->belongsTo('App\Query','query_id','id');
    // // }
}
