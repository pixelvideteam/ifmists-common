<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavesCreditedEL extends Model
{
    protected $table = 'esr_leaves_credited_el';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    const LEAVES_LIST = [11, 84, 85, 86];

    public static function getEmployeeAllELLeaves($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with('leavesavailed.leavetype')
            ->orderBy('from_date','ASC')
            ->get();
    }

    public function leavesavailed()
    {
        return $this->hasMany(LeavesAvailedEL::class, 'el_credited_id', 'id');
    }

}
