<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DrawnMasterTypes extends Model 
{
	protected $table='drawn_master_types';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
