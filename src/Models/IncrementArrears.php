<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class IncrementArrears extends Model {

	protected $table='increment_arrears';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
