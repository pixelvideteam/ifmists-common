<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class EmployeeDeductionRefno extends Model
{

	protected $table="employee_deduction_refno";
	protected $guarded=['id'];
}