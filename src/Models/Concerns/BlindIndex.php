<?php


namespace App\Models\Concerns;


trait BlindIndex
{
    /**
     * @var array
     * Below is an example of implementation and reference
     */
//    protected $encrypted = [
////        secure_filed => secure_blind_index
//    ];

    /**
     * @param $method
     * @param $parameters
     * @return mixed
     *
     * @use We are overwriting the call method and replacing blind indexed fields
     * with blind indexed columsn
     *
     * for example if phoneno is encrypted, and the blind index field name is phoneno_bindx,
     * we dynamically replace phoneno with phoneno_bindx. This way we will not have to phone in
     * our application layer.
     *
     * Note: encrypted columns need to be TYPE TEXT
     */
//    public function __call($method, $parameters)
//    {
//        if (in_array($method, ['where', 'whereIn']) && !empty($this->encrypted)) {
//
//            $lookUpColumns = array_intersect($parameters, array_keys($this->encrypted));
//
//            if ($lookUpColumns && !in_array('!=', $parameters)) {
//                $parameters = array_replace($parameters, array_values($this->encrypted));
//            }
//        }
//
//        return $this->forwardCallTo($this->newQuery(), $method, $parameters);
//    }


    /**
     * @return array
     */
    public function getEncrypted(): array
    {
        return $this->encrypted;
    }


}
