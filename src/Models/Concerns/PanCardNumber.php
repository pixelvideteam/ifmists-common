<?php


namespace App\Models\Concerns;

use App\Fms\Document\PanCardNumberEncryption;

trait PanCardNumber
{
    /**
     * @param $value
     * @return void
     */
    public function setPanAttribute($value)
    {
        if (!empty($value)) {
            $value = str_replace(' ', '', $value);
            $value = strtoupper($value);
            $this->attributes['pan'] = PanCardNumberEncryption::encrypt($value);
        }
    }

    /**
     * @param string $value
     * @return string
     */

    public function getPanAttribute($value)
    {

        // unencrypted value
        try {
            if (strlen($value) > 20) {
                return PanCardNumberEncryption::decrypted($value);
            }
        } catch (\Exception $e) {
            // this is to support
            // SELECT id, surname, name, pan FROM employee_master where id IN (225959,87105,276165,330402,84862,85295,483580,80732,261232,184266,83440,86346,85196,79072,84959,78805,219767,249819,251016);
            // employees with invalid pan numbers
            // Talked to Yogesh and he said he will have this standardized to 10 char limit.
//            print_r($e->getMessage() . PHP_EOL);
//            print_r($value);
        }

        return $value;
    }

}
