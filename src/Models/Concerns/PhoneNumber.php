<?php


namespace App\Models\Concerns;

use App\Fms\Document\PhoneNumberEncryption;
use App\Fms\Security\BlindIndexEncryption;

trait PhoneNumber
{

    /**
     * @param $value
     * @return void
     */
    public function setPhonenoAttribute($value)
    {
        $value = trim($value);

        if (!empty($value) && strlen($value) <= 15) {
            $this->attributes['phoneno'] = PhoneNumberEncryption::encrypt($value);
            if (!empty($this->encrypted['phoneno']))
                $this->attributes[$this->encrypted['phoneno']] = BlindIndexEncryption::getBlindIndex($value);
        } else {
            $this->attributes['phoneno'] = $value;
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getPhonenoAttribute($value)
    {
        if (strlen($value) <= 15) {
            return $value;
        }

        if (empty($value)) {
            return '';
        }

        return PhoneNumberEncryption::decrypted($value);
    }

    /**
     * @param $value
     * @return void
     */
    public function setPhoneAttribute($value)
    {
        $value = trim($value);

        if (!empty($value) && strlen($value) <= 15) {
            $this->attributes['phone'] = PhoneNumberEncryption::encrypt($value);
            if (!empty($this->encrypted['phone']))
                $this->attributes[$this->encrypted['phone']] = BlindIndexEncryption::getBlindIndex($value);
        } else {
            $this->attributes['phone'] = $value;
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getPhoneAttribute($value)
    {
        if (strlen($value) <= 15) {
            return $value;
        }

        if (empty($value)) {
            return '';
        }

        return PhoneNumberEncryption::decrypted($value);
    }
}
