<?php


namespace App\Models\Concerns;


use App\Models\Eloquent\Builder;

trait QueryBuilder
{

    /**
     * @param $query
     * @return Builder
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }
}
