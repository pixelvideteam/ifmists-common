<?php


namespace App\Models\Concerns;

use App\Fms\Document\AadhaarCardDocument;

trait AadharNumber
{
    /**
     * @param $value
     * @return void
     */
    public function setAadharnoAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['aadharno'] = AadhaarCardDocument::encrypt($value);
        }
    }

    /**
     * @param string $value
     * @return string
     */

    public function getAadharnoAttribute($value)
    {
        // unencrypted value
        if (is_numeric($value) || strlen(trim($value)) == 12) {
            return $value;
        }
        if (empty($value)) {
            return '';
        }

        return AadhaarCardDocument::hashed($value);
    }

    /**
     * @return int|mixed|string
     */
    public function getAadharnoDecrypted()
    {
        $value = $this->attributes['aadharno'];

        // unencrypted value
        if (is_numeric($value) || strlen(trim($value)) == 12) {
            return $value;
        }

        if (empty($value)) {
            return '';
        }

        return AadhaarCardDocument::decrypt($value);
    }
}
