<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalCharge extends Model
{
    protected $table = "additional_charge";
    protected $guarded = ['id'];
    // public function admintrans()
    // {
    // 	return $this->belongsTo('App\Transactions','transid','id');
    // }

    // public function querydata()
    // {
    // 	return $this->belongsTo('App\Query','query_id','id');
    // }
}
