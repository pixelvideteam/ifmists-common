<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 24-12-2018
 * Time: 11:23
 */
declare(strict_types=1);


namespace App;

use App\Http\Controllers\EKuber\Models\TransTypeStatuses;
use Illuminate\Database\Eloquent\Model;

class EkuberPaymentFiles extends Model
{
    protected $table = "ekuber_payment_files";

    public function cheques()
    {
        return $this->hasMany(ChequePaymentFileMapping::class, 'ekuber_payment_file_id', 'id');
    }

    public function pdcheques()
    {
        return $this->hasMany(PDChequePaymentFileMapping::class, 'ekuber_pd_payment_file_id', 'id');
    }

    public function dwacheques()
    {
        return $this->hasMany(DWAChequePaymentFileMapping::class, 'ekuber_dwa_payment_file_id', 'id');
    }

    public function dwachequepaymentfilemapping()
    {
        return $this->hasMany(DWAChequePaymentFileMapping::class, 'ekuber_dwa_payment_file_id', 'id');
    }

    public function dwaChequeByEkuberFilename()
    {
        return $this->hasOne(DWACheques::class, 'ekuber_filename', 'ekuber_filename');
    }

    public function dwaTransaction()
    {
        return $this->hasOne(Transactions::class, 'ekuber_dwa_filename', 'ekuber_filename');
    }

    public function returncheques()
    {
        return $this->hasMany(ChequePaymentReturnMapping::class, 'ekuber_payment_file_id', 'id');
    }

    public function branch()
    {
        return $this->hasOne('App\PaoBranch', 'id', 'branch_id');
    }

    public function ekubertrans()
    {
        return $this->hasMany(EkuberTransactions::class, 'ekuber_payment_file_id', 'id');
    }

    public function ekuberfileparties()
    {
        return $this->hasMany(EkuberMultipleParty::class, 'ekuber_payment_file_id', 'id');
    }

    public function ekuberfilepartiesnew()
    {
        return $this->hasMany(EkuberMultiplePartyNew::class, 'ekuber_payment_file_id', 'id');
    }

    public function alltrans()
    {
        return $this->hasMany(EkuberTransactions::class, 'orgnl_msg_id', 'ekuber_filename');
    }

    public function allfilesforpayfile()
    {
        return $this->hasMany(EkuberTransactions::class, 'orgnl_msg_id', 'ekuber_filename');
    }

    public function pdtrans()
    {
        return $this->hasMany(Transactions::class, 'ekuber_pd_filename', 'ekuber_filename');
    }
    public function ekuberTranactionPending(){
        return $this->hasMany(EkuberTransactions::class,'orgnl_msg_id','ekuber_filename')->where('transtype' ,'!=',5);
    }

    public function paocheques()
    {
        return $this->hasMany(PaoCheques::class, 'ekuber_filename', 'ekuber_filename');
    }

    public function paoReturncheques()
    {
        return $this->hasMany(PaoChequesEKuber::class, 'ekuber_filename', 'ekuber_filename');
    }

    public function paoChequesTransaction()
    {
        return $this->hasMany(PaoChequesTransactions::class, 'pao_cheques_id', 'pao_cheques_id');
    }
    public function pdtransnor()
    {
        return $this->hasMany(Transactions::class, 'ekuber_pd_filename', 'ekuber_filename')->whereNull('govtdate');
    }
    public function pdtransgov()
    {
        return $this->hasMany(Transactions::class, 'ekuber_pd_filename', 'ekuber_filename')->whereNotNull('govtdate');
    }
    public function pdtransall()
    {
        return $this->hasMany(Transactions::class, 'ekuber_pd_filename', 'ekuber_filename');
    }
    public function dtacheques()
    {
        return $this->hasMany(DTACheques::class, 'ekuber_filename', 'ekuber_filename');
    }
    public function pdtransdtadown()
    {
        return $this->hasMany(Transactions::class, 'ekuber_pd_filename', 'ekuber_filename')->where('created_at','>=','2019-04-01')->select('id','partyamount','purpose','ekuber_pd_filename');
    }
    public function dtatransdtadown()
    {
        return $this->hasMany(Transactions::class, 'ekuber_dta_filename', 'ekuber_filename')->where('created_at','>=','2019-04-01')->select('id','partyamount','purpose','ekuber_dta_filename', 'tokenno', 'ddocode', 'hoa');
    }
    public function dtatransnor()
    {
        return $this->hasMany(Transactions::class, 'ekuber_dta_filename', 'ekuber_filename')->whereNull('govtdate');
    }
    public function dtatransgov()
    {
        return $this->hasMany(Transactions::class, 'ekuber_dta_filename', 'ekuber_filename')->whereNotNull('govtdate');
    }

    public function ekuber_transaction(){
        return $this->hasMany(EkuberTransactions::class,'orgnl_msg_id','ekuber_filename');
    }
    public function ekuber_transactions(){
        return $this->hasMany(EkuberTransactions::class,'ekuber_payment_file_id','id');
    }
    // Ack && No response
    public function no_response()
    {
        return $this->hasOne(EkuberMultiplePartyNew::class, 'ekuber_payment_file_id', 'id')
            ->whereNotNull('ekuber_sent_transaction_id')
            ->whereNull('ekuber_dn_transaction_id')
            ->whereNull('ekuber_transaction_id')
            ->select(['id','ekuber_payment_file_id','amt','ekuber_dn_transaction_id',
                'ekuber_ack_rjct_transaction_id','ekuber_transaction_id','ekuber_sent_transaction_id','message']);
    }
    public function no_reponse_ekuber_file_status(){
        return $this->hasOne(EkuberFilesStatus::class,'ekuber_filename','ekuber_filename')
            ->whereRaw("dn_filename is null
                and rn_filename is null
                and case when ack_filename::text like '%RJCT%' then (reprocess_status=6 and ack_filename::text like '%ACCP%') else '1' end
                and (ack_filename::text like '%ACCP%'
                    or ack_filename=null
                    or ack_filename::text='[]'
                    or ack_filename::text='')"
            );
    }

    public function ack_response()
    {
        return $this->hasOne(EkuberTransactions::class, 'orgnl_msg_id', 'ekuber_filename')
            ->whereIn('transtype', [1])->select(['id','orgnl_msg_id','transtype','cre_dt']);
    }

    public function dn_response()
    {
        return $this->hasOne(EkuberTransactions::class, 'orgnl_msg_id', 'ekuber_filename')
            ->whereIn('transtype', [3])->select(['id','orgnl_msg_id','transtype','cre_dt']);
    }
    public function rn_response()
    {
        return $this->hasOne(EkuberTransactions::class, 'orgnl_msg_id', 'ekuber_filename')
            ->whereIn('transtype', [4])->select(['id','orgnl_msg_id','transtype','cre_dt']);
    }
    public function nck_response(){
        return $this->hasOne(EkuberTransactions::class,'orgnl_msg_id','ekuber_filename')
            ->where('transtype','=',2);
    }

}
