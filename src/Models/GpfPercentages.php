<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Rennokki\QueryCache\Traits\QueryCacheable;

class GpfPercentages extends Model
{

    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'gpf_percentages';

    protected $table = 'gpf_percentages';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getPercentagesByDates($fromDate, $toDate)
    {
        return DB::table('gpf_percentages')
            ->select('year', 'from_month', 'to_month', 'percentage')
            ->where(function ($query) use ($fromDate, $toDate) {
                $query->whereBetween('wef_date_from', [$fromDate, $toDate])->orWhereBetween('wef_date_to', [$fromDate, $toDate]);
            })
            ->get();
    }
}
