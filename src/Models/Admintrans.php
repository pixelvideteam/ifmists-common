<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Admintrans extends Model
{
    protected $table = "admintrans";
    public $timestamps = false;
    protected $guarded = ['id'];

    public function admintrans()
    {
        return $this->belongsTo('App\Transactions', 'transid', 'id');
    }

    public function querydata()
    {
        return $this->belongsTo('App\Query', 'query_id', 'id');
    }
}
