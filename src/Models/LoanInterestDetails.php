<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanInterestDetails extends Model 
{
	protected $table='loan_interest_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
