<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DwaUsersSections extends Model
{
    protected $table = 'dwa_users_sections';
    protected $guarded = ['id'];
}
