<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestSurrenderDoc extends Model
{
    protected $table = 'budget_request_surrender_docs';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
