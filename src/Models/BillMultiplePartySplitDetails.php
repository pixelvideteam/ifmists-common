<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BillMultiplePartySplitDetails extends Model {

	protected $table='bill_multiple_party_split_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
