<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NmrEngPayBillDetails extends Model
{
    protected $table ='dwa_nmr_eng_pay_bill_details';
}
