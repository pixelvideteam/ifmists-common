<?php namespace App;

use App\Models\Dwa\AccountType;
use App\Models\Dwa\BillGstDetails;
use App\Models\Dwa\GovtOrder;
use Illuminate\Database\Eloquent\Model;

class ServiceMajorLog extends Model
{
    protected $table = 'servicemajor_transaction_logs';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
