<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RbiClearance extends Model {
    protected $table='rbi_clearances';
    protected $guarded = ['id', 'created_at', 'updated_at'];
	public function datadetails() {
		return $this->hasMany('App\RbiClearanceDetails','clearance_id','id');
	}

}
