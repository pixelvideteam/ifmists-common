<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 04-02-2019
 * Time: 06:09
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppNotifications extends Model
{
    protected $table = "app_notifications";
    protected $guarded = ['id'];
    const APPROVE_REJECT_FLAG_APPROVED = 1;
    const APPROVE_REJECT_FLAG_REJECTED = 21;
    const NOTIFICATION_TYPE_MOBILE_PUSH_NOTIFICATION = 5;
    const USER_ROLE_ARRAY_GOVT_ACTION = [37];
}
