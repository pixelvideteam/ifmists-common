<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceTransfers extends Model
{
    protected $table = 'esr_service_transfers';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getAllEmployeeTransferDetails($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with([
                'olddept.designations.desg', 'newdept.designations.desg', 'olddesg', 'newdesg', 'oldhod', 'newhod'
            ])
            ->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)
            ->with([
                'olddept.designations.desg', 'newdept.designations.desg', 'olddesg', 'newdesg', 'oldhod', 'newhod'
            ])
            ->first();
    }

    public function olddept()
    {
        return $this->hasOne(Department::class, 'id', 'old_dept_id');
    }

    public function newdept()
    {
        return $this->hasOne(Department::class, 'id', 'new_dept_id');
    }

    public function olddesg()
    {
        return $this->hasOne(DesgCode::class, 'id', 'old_desg_id');
    }

    public function newdesg()
    {
        return $this->hasOne(DesgCode::class, 'id', 'new_desg_id');
    }

    public function oldhod()
    {
        return $this->hasOne(HODList::class, 'id', 'old_hod_id');
    }

    public function newhod()
    {
        return $this->hasOne(HODList::class, 'id', 'new_hod_id');
    }
}
