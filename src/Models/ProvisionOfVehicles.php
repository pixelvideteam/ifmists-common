<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvisionOfVehicles extends Model
{
    protected $table = 'provision_of_vehicles';
    protected $guarded = ['id'];
}
