<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeAuthStoDistDdo extends Model
{
    protected $table = 'be_auth_sto_dist_ddo';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function stoDistribution(){
        return $this->hasOne(BeAuthStoDistribution::class, 'id', 'be_auth_sto_dist_id');
    }
}
