<?php

namespace Pixelvide\IFMISTS\Model;

use Illuminate\Database\Eloquent\Model;

class ShortUrl extends Model {
    const MAX_LENGTH = 10;
    const URL = 'pixv.in/';

    protected $table = 'short_urls';

    protected $guarded = [
        'id',
    ];

    /**
     * @param $url
     * @param $employee_id
     * @return string
     * @throws \Exception
     */
    public static function generate($url, $employee_id = null)
    {
        $m = new self;
        $m->url = trim($url);
        $m->hash = self::buildHash();
        $m->save();
        return self::URL . $m->hash;
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected static function buildHash()
    {
        $length = self::MAX_LENGTH;
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max - 1)];
        }

        return strtoupper($token);
    }
}