<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestHoaDdo extends Model
{
    protected $table = 'budget_request_hoa_ddo';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function ddo()
    {
        return $this->hasOne('App\User', 'username', 'ddocode');
    }

    public function budReqHoa()
    {
        return $this->hasOne(BudgetRequestHoa::class, 'id', 'budget_request_hoa_id');
    }
}
