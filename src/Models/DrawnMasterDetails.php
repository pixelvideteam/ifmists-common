<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DrawnMasterDetails extends Model 
{
	protected $table='drawn_master_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function paytype()
	{	
		return $this->hasOne('App\DrawnMasterDetailsTypes','type','type');
	}

	public function recoveries()
	{	
		return $this->hasMany('App\DrawnMasterDetails','rec_parent_id','id');
	}

	public function emprecdetails()
	{
		return $this->hasMany('App\EmployeeRecoveriesDetails', 'drawn_master_dets_id', 'id');
	}
}
