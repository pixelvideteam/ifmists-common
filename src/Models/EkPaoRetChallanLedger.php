<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 25-09-2019
 * Time: 07:38
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class EkPaoRetChallanLedger extends Model
{
    protected $table = 'ekuber_pao_return_challans_ledger';
    protected $fillable = ['challan_id', 'ddocode', 'transaction_id', 'bill_generation_time'];
}