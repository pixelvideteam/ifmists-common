<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewEkuberProcessCheques extends Model
{
    protected $table = "new_ekuber_process_cheques";
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
