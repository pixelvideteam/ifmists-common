<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class MongoBillTempSubmitData extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'bill_temp_submit_data';
    //protected $hidden=["created_at","updated_at"];
    protected $fillable = ['empListData','ddocode','formtype','cDate','billid','formno','hoa','emp_data_type','billdesc','frompercent','topercent','lastmonth_lastday','daarrear_wef_date','maindesc','supparr','sequence'];
}
