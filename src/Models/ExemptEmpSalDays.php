<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ExemptEmpSalDays extends Model
{
    protected $table="exempt_emp_sal_days";
    protected $guarded = ['id', 'created_at', 'updated_at'];
}