<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CloseAccounts extends Model
{
    protected $table= 'close_accounts';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
