<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class OfctypeRoleMapping extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'ofctype_role_mapping';

    protected $table = 'ofctype_role_mapping';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
