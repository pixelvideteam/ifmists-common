<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavesLTCFamilyMap extends Model
{
    protected $table='esr_leaves_ltc_family_mapping';
    protected $guarded = ['id','created_at','updated_at'];

    public function family(){
        return $this->hasOne('App\FamilyDetails','id','family_relation_id');
    }

}
