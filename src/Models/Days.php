<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class Days extends Model{

	protected $table="days";
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
