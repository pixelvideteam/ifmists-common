<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestSurrender extends Model
{
    protected $table = 'budget_request_surrender';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function surrenderDet()
    {
        return $this->hasMany('App\BudgetRequestSurrenderDetail', 'budget_request_surrender_id', 'id');
    }
    public function docs()
    {
        return $this->hasMany(BudgetRequestSurrenderDoc::class, 'budget_request_surrender_id', 'id');
    }
}
