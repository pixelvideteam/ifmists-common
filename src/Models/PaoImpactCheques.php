<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 02-02-2019
 * Time: 12:05
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class PaoImpactCheques extends Model
{
    protected $table='pao_imapact_cheques';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}