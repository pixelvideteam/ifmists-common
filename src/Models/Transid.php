<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transid extends Model
{
    protected $table = 'transid';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
