<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class HoaUpdateInBill extends Model{

	protected $table="hoa_update_in_bill";
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function billdets() {

		return $this->hasOne('App\Transactions','id','transactions_id');
	}
}
