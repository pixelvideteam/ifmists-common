<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestDoc extends Model
{
    protected $table = 'be_auth_budget_request_docs';
}
