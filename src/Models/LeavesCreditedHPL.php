<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavesCreditedHPL extends Model
{
    protected $table = 'esr_leaves_credited_hpl';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    const LEAVES_LIST = [12,17];

    public static function getEmployeeAllHPLLeaves($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with('leavesavailed.leavetype')
            ->orderBy('from_date','ASC')
            ->get();
    }

    public function leavesavailed()
    {
        return $this->hasMany(LeavesAvailedHPL::class, 'hpl_credited_id', 'id');
    }

}
