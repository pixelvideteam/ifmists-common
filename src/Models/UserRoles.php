<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
    const ROLE_DDO = 2;
    const ROLE_PD_GOV_IRRIGATION = 3;
    const ROLE_BANKER = 4;
    const ROLE_PD_GOV = 7;
    const ROLE_PD_DD = 8;
    const ROLE_PD_ATO = 9;
    const ROLE_PD_STO = 10;
    const ROLE_PD_SA = 11;
    const ROLE_PD_AG_USER = 12;
    const ROLE_PD_ADMIN_CHECKER = 20;
    const ROLE_PD_ADMIN_AUTH = 21;
    const ROLE_PD_DEPARTMENT = 22;
    const ROLE_RBI_USER = 30;
    const ROLE_BILL_SECTION = 32;
    const ROLE_EKBER_ADMIN = 70;
    const ROLE_AUDITOR = 33;
    const ROLE_SUPERINTENDENT = 34;
    const ROLE_OFFICER = 35;
    const ROLE_PAO = 36;
    const ROLE_GOV = 37;
    const ROLE_CHEQUE_SECTION_COUNTER = 38;
    const ROLE_CHEQUE_SECTION_APO = 39;
    const ROLE_CHEQUE_ADMIN = 40;
    const ROLE_HOD = 41;
    const ROLE_HOD_MAKER = 42;
    const ROLE_HOD_AUTH = 56;
    const ROLE_GOV_DASHBOARD_REPORTS = 47;
    const ROLE_PAO_BANKER = 48;
    const ROLE_ADMIN = 50;
    const ROLE_BILL_ADMIN = 60;
    const ROLE_HR_DDO_AUTHORISER = 77;
    const ROLE_APP_USER = 80;
    const ROLE_CODE_ADMIN = 99;
    const ROLE_DWA_AUDITOR = 6;
    const ROLE_DWA_SUPDT = 55; // or 86
    const ROLE_DWA_PAO = 5;
    const ROLE_DWA_GOVT = 37;
    const ROLE_DWA_CASHIER = 86;
    const ROLE_DWA_PAO_PAY = 87;
    const ROLE_DWA_DDO = 85;
    const ROLE_DWA_JD = 16;
    const ROLE_DWA_ADMIN = 14;
    const ROLE_DWA_DEO = 107;
    const ROLE_BE_SA = 43;
    const ROLE_BE_JAO = 44;
    const ROLE_BE_DD = 45;
    const ROEL_DWA_EKUBER_ADMIN = 73;
    const ROLE_DWA_JD_FINANCE = 17;
    const ROLE_RECEIPTS_ADMIN = 46;
    const ROLE_RECEIPTS_BANK_HEAD = 82;
    const ROLE_DD_DTA_EKUBER = 111;
    const ROLE_CPS_BILL_ADMIN = 301;
    const ROLE_DTAADMIN_EKUBER = 126;
    const ROLE_DTABILLSADMIN_EKUBER = 114;
    const ROLE_JDPPO_SA = 125;
    const ROLE_JDPPO_JAO = 124;
    const ROLE_JDPPO_APPO = 123;
    const ROLE_JDPPO_PPO = 122;
    const ROLE_EKUBER_SUPER_ADMIN = 127;
    const DWA_ENC_HOD = 129;
    const COMM_CLGTEDU = 29;
    const DWA_MD_TS = 132; // Managing Director, TS
    const DWA_DDO_LOGINS = [
        85,
    ];
    const DWA_ADMIN_LOGINS = [
        5,
        6,
        14,
        55,
        37,
        16,
        108,
        109,
        110,
    ];
    const ROLE_SA_PENSION = 125;
    const ROLE_JAO_PENSION = 124;
    const ROLE_APPO_PENSION = 123;
    const ROLE_PPO_PENSION = 122;
    const ROLE_DWA = 108;
    const ROLE_DWA_ADMIN_JD = 109;
    const ROLE_DWA_ADMIN_SUPERINTENDENT = 110;
    const ROLE_TSE = 97;
    const ROLE_CODE_ADMIN_DEV = 98;
    const ROLE_TSE_MANAGER = 101;
    const ROLE_OPERATIONS_MANAGER = 112;
    const ROLE_FOREIGN_SERVICE_ADMIN = 303;

    const ROLE_DWA_SECTION_OFFICER = 304;
    const ROLE_DWA_JOINT_SECRETARY = 305;
    const ROLE_DD_ADMIN = 130;
    const ROLE_STO_FUNDS = 131;

    /*BRO Officers ROles*/
    const ROLE_BRO_SECTION_OFFICER = 317;
    const ROLE_BRO_ASSISTANT_SECTION_OFFICER = 310;
    const ROLE_BRO_ASSISTANT_SECRETARY = 311;
    const ROLE_BRO_DEPUTY_SECRETARY = 312;

    const ROLE_GOVT_DUMMY = 313;
    const ROLE_BRO_ADMIN = 314;
    const ROLE_DCM = 315;
    const ROLE_AG_ADMIN= 333;
    const DWA_TSE_LEAD_ROLE = 135;
    const DWA_KALESHWARAM_MD = 136;

    protected $table = 'user_roles';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
}
