<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoSectionMap extends Model {

	protected $table='pao_section_map';
	public function suptuser() {

		return $this->hasOne('App\User','username','supt');
	}

	public function offuser() {

		return $this->hasOne('App\User','username','officer');
	}

	public function counter() {

		return $this->hasMany('App\PaoBranchCounter','branch_id','branch_id');
	}
	public function auditors() {

		return $this->hasMany('App\MapTable','mappeduser','supt');
	}
}
