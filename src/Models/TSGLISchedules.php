<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 09-02-2019
 * Time: 14:56
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class TSGLISchedules extends Model
{
    protected $table = "tsglischedules";
    protected $guarded = ['id'];
    // Add DDO codes for whom to reject the TSGLI validation on placing the salary bill
    const TSGLI_VAL_SKIP_DDO = [
        '25001002018',
        '21011002066',
        '23011002002',
    ];
    const TSGLI_VAL_SKIP_HOD = [
        '69',
        '75',
        '74',
        '81',
        '83',
        '82',
        '76',
        '78',
        '80',
    ];
    const TSGLI_VAL_SKIP_DEPT = [
        '60',
        '241',
        '283',
        '242',
        '321',
        '314',
        '239',
        '311',
    ];
    const TSGLI_VAL_SKIP_ROLE = 2;
    const TSGLI_VAL_SKIP_MONTHS = [
        '01-2020',
        '02-2020',
    ];

    public function transaction()
    {
        return $this->hasOne('App\Transactions', 'id', 'transaction_id');
    }

}
