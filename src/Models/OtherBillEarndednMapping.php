<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherBillEarndednMapping extends Model
{
    protected $table='other_bill_earndedn_mapping';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
