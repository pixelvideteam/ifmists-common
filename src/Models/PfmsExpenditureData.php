<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PfmsExpenditureData extends Model
{
    protected $table = 'pfms_expenditure_data';

    const PAO = 1;
    const DTA = 2;
    const DWA = 3;

}
