<?php namespace App;

use Illuminate\Database\Eloquent\Model;

	class Cheques extends Model{

		protected $table="chequelist";
		// public $timestamps = false;
		protected $guarded=['id'];
		public function pendingchqs() {

			return $this->hasMany('App\Chequeleaves','ddocode','reciptuser');
		}
	}
