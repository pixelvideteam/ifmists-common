<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class AudEmpApprovalsEarndedn extends Model
{
	protected $table="aud_emp_approvals_earndedn";
	protected $guarded=['id'];
	
	public function earndedn()
	{	
		return $this->belongsTo('App\EarnDednList','earndedn_id','id');
	}

	// public function querydata()
	// {
	// 	return $this->belongsTo('App\Query','query_id','id');
	// }
}