<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EKPaoReturnChallans extends Model
{
    protected $table = 'ekuber_pao_return_challans';
    public function old_bank()
    {
        return $this->hasOne(BankIfsc::class, 'ifsccode', 'old_ifsc');
    }
    public function new_bank()
    {
        return $this->hasOne(BankIfsc::class, 'ifsccode', 'new_ifsc');
    }
    public function ddo()
    {
        return $this->hasOne(User::class, 'username', 'ddocode');
    }
    public function bill_multiple_party()
    {
        return $this->hasOne(BillMultipleParty::class, 'id', 'bill_multiple_party_id');
    }
}
