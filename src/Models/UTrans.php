<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UTrans extends Model
{
    protected $table = "uniquetrans";
    public $timestamps = false;
    protected $guarded = ['id'];
}
