<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ElHplFinalEncashmentArrears extends Model {

	protected $table='el_hpl_final_encashment_arrears';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
