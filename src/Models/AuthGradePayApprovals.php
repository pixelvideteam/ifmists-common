<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthGradePayApprovals extends Model {

	protected $table='auth_grade_pay_approvals';
	protected $guarded = ['id', 'created_at', 'updated_at'];


	public function scale() 
	{
		return $this->hasOne('App\Scale','id','scale_id');
	}

	public function empmaster() 
	{
		return $this->hasOne('App\EmployeeMaster','id','employee_id');
	}

	public function basicdets() 
	{
		return $this->hasOne('App\MasterScaleDetails','id','master_scale_details_id');
	}

	public function empearndedn() 
	{
		return $this->hasMany('App\AuthGradePayApprovalsEarnDedn','auth_grade_pay_app_id','id');
	}

	public function empgradepay()
    {
        return $this->hasOne('App\EmployeeGradePay','auth_grade_pay_app_id','id');
    }

    /*public function authgradepay()
    {
        return $this->hasOne('App\AuthGradePayApprovals','id','auth_grade_pay_app_id');
    }*/


}
