<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 25-02-2019
 * Time: 15:18
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class EkuberMainBankDetails extends Model
{
    protected $table = 'ekuber_main_bankdetails';

    public function ifsc()
    {
        return $this->hasOne(BankIfsc::class, 'id', 'ifsc_id');
    }

    public function trectpaylog()
    {
        return $this->hasOne(TrecTpayMonthlyLog::class, 'ekuber_bank_detail_id', 'id');
    }
}
