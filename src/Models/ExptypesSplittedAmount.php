<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ExptypesSplittedAmount extends Model {

	protected $table = "exptypes_splittedamount";
	protected $guarded = ['id', 'created_at', 'updated_at'];
}