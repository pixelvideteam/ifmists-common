<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class MasterScaleDetails extends Model
{

	protected $table="master_scale_details";
	protected $guarded=['id'];
	
	// public function empmaster()
	// {	
	// 	return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	// }

	// // public function querydata()
	// // {
	// // 	return $this->belongsTo('App\Query','query_id','id');
	// // }
}