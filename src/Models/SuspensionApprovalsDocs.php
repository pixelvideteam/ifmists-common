<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class SuspensionApprovalsDocs extends Model
{
	protected $table="suspension_approvals_docs";
	protected $guarded=['id'];
	
	// public function empmaster()
	// {	
	// 	return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	// }

	// public function querydata()
	// {
	// 	return $this->belongsTo('App\Query','query_id','id');
	// }
}