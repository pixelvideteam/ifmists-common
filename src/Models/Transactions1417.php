<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions1417 extends Model
{
    protected $table = "transactions_1417";
    public $timestamps = false;
    protected $guarded = ['id'];

    public function requser()
    {
        return $this->belongsTo('App\User', 'ddocode', 'username');
    }

    public function laptrans()
    {
        return $this->hasMany('App\Transactions1417', 'laprecid', 'id');
    }

    public function accountdet()
    {
        return $this->belongsTo('App\Pdaccount', 'ddocode', 'ddocode');
    }

    public function usernames()
    {
        return $this->belongsTo('App\User', 'ddocode', 'username');
    }

    public function admintrans()
    {
        return $this->hasOne('App\Admintrans', 'id', 'transid');
    }

    public function schemes()
    {
        return $this->belongsTo('App\Schemes', 'hoa', 'hoa');
    }

    public function lapsing()
    {
        return $this->belongsTo('App\Lapsing', 'transid', 'transid');
    }

    public function fundstransfer()
    {
        return $this->belongsTo('App\FundsTransfer', 'id', 'transaction_id');
    }

    public function acinfoall()
    {
        return $this->hasMany('App\Pdaccount', 'ddocode', 'ddocode');
    }
}
