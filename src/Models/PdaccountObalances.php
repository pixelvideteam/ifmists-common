<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PdaccountObalances extends Model {

	protected $table='pdaccount_obalances';
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
