<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class IncrementApprovals extends Model
{
	protected $table="increment_approvals";
	protected $guarded=['id'];
	
	public function incempearndedn() 
	{
		return $this->hasMany('App\IncrementApprovalsEarndedn','increment_approval_id','id');
	}

	public function empmaster()
	{	
		return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	}

	public function from_scale_detail()
	{	
		return $this->belongsTo('App\ScaleDetails','from_scale_details_id','id');
	}

	public function to_scale_detail()
	{	
		return $this->belongsTo('App\ScaleDetails','to_scale_details_id','id');
	}

}