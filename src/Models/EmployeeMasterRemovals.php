<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeMasterRemovals extends Model {

	protected $table = 'employee_master_removals';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
