<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MCoDdo extends Model
{
    protected $table='mcoddo';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function ddo()
    {
        return $this->hasOne('App\User','username','ddocode');
    }
}
