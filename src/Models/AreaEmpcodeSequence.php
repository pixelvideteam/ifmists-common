<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaEmpcodeSequence extends Model
{
    protected $table = "area_empcode_sequence";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getEmpcodeIncrementedSerialByAreaCode($areaCode)
    {
        $areaEmpcodeModel = self::where('areacode', $areaCode)->first();
        if (!$areaEmpcodeModel) {
            throw new \Exception("Sequence not found for this Area code");
        }
        $sequence = $areaEmpcodeModel->empcode_serial;
        $sequence++;
        $areaEmpcodeModel->empcode_serial = str_pad($sequence, 5, 0, STR_PAD_LEFT);
        $areaEmpcodeModel->save();
        return $areaEmpcodeModel->empcode_serial;
    }
}
