<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EKuberReturnsDdoform extends Model {

	protected $table = "ekuber_returns_ddoform";
	public $timestamps = false;

	protected $guarded = ["id"];
	public function chqdetails() {
		return $this->hasOne('App\Transactions','id','transaction_id');
	}
	public function multipleparty() {
		return $this->hasOne('App\BillMultipleParty','id','bill_multiple_party_id');
	}
	public function ekubermultipleparty() {
		return $this->hasOne('App\EkuberMultipleParty','id','ekuber_multiple_party_id');
	}
}