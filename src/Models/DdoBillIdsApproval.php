<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DdoBillIdsApproval extends Model
{
    protected  $table = 'ddo_bill_ids_approvals';

    const PENDING = 0;
    const APPROVED = 1;
    const REJECTED = 21;

    public function hoa()
    {
        return $this->belongsTo('App\DdoBillIdsHoa','ddo_bill_ids_hoa_id','id');
    }

    public function sctype()
    {
        return $this->belongsTo('App\ScaleTypes','scale_type_id','id');
    }



}
