<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DashboardCategory extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'dashboard_categories';

    protected $table = 'dashboard_categories';

    const TYPE_PAO = 1;
    const TYPE_DTA = 2;
    const TYPE_PD = 3;
    const TYPE_DWA = 4;
    const TYPE_DTA_EKUBER = 5;
    const TYPE_PD_LESSTEN = 6;
    const TYPE_DTA_PAO_EKUBER_SALARY = 7;
    const TYPE_EKUBER_PENSION_PAYBANK = 8;
}
