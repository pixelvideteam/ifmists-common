<?php

namespace App\Models\Dwa;

use App\Transactions;
use Illuminate\Database\Eloquent\Model;

class DwaEnhanceTransactions extends Model
{
    protected $table = 'dwa_enhance_transactions';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function transaction()
    {
        return $this->belongsTo(Transactions::class, 'transaction_id', 'id');
    }
}
