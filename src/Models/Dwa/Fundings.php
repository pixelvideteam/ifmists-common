<?php

namespace App\Models\Dwa;

use App\Models\Dwa\Dwa\TechnicalSanctionValue;
use Illuminate\Database\Eloquent\Model;

class Fundings extends Model
{
    protected $table = 'dwa_fundings';
    protected $appends = ['type_description'];

    private $fundingCodes = [
        'CHEQUE' => 'Cheque',
        'DD' => 'Demand Draft',
        'CHALAAN' => 'Challan'
    ];

    public function projects ()
    {
        return $this->hasMany(Projects::class,'dwa_fundings_id','id');
    }

    public function technicalSanction ()
    {
        return $this->hasOne(TechnicalSanctionValue::class, 'id', 'dwa_technical_sanction_id');
    }

    public function getTypeDescriptionAttribute()
    {
        $typeDesciption = isset($this->fundingCodes[$this->type])?$this->fundingCodes[$this->type]:$this->type;
        return "{$typeDesciption}";
    }
}
