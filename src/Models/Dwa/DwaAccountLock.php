<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaAccountLock extends Model
{
    const DWA_ACCOUNT_LOCK_APPROVED_STATUS = 1;
    const DWA_ACCOUNT_LOCK_PENDING_STATUS = 0;

    protected $table='dwa_account_lock';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function paoList(){
        return $this->belongsTo('App\Models\Dwa\DwaAreas', 'pao', 'area_code');
    }
}
