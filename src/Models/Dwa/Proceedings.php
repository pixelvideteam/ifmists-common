<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class Proceedings extends Model
{
    const PROCEEDING_APPROVED = 1;
    const PROCEEDING_REJECTED = 21;
    const PROCEEDING_PENDING = 0;
    const PROCEEDING_DELETED = 22;

    protected $table="dwa_proceedings";

    protected $guarded=['id'];

    public function bill_data_entry(){
        return $this->hasMany(BillDataEntry::class, 'dwa_proceeding_id', 'id');
    }

    public function govtOrder()
    {
        return $this->hasOne(GovtOrder::class, 'id', 'dwa_govt_order_id');
    }
}
