<?php

namespace App\Models\Dwa;

use App\HeadsDesc;
use Illuminate\Database\Eloquent\Model;

class GovtOrderHoa extends Model
{
    const DWA_GOVT_ORDER_HOA_PENDING = 0;
    const DWA_GOVT_ORDER_HOA_APPROVED = 1;
    const DWA_GOVT_ORDER_HOA_REJECTED = 21;
    const DWA_GOVT_ORDER_HOA_DELETED = 22;

    protected $table = 'dwa_govt_order_hoa';

    public function goList()
    {
        return $this->belongsTo(GovtOrder::class, 'dwa_govt_order_id', 'id');
    }

    public function heads_desc()
    {
        return $this->belongsTo(HeadsDesc::class, 'hoa', 'hoa');
    }
}
