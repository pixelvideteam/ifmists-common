<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class AccountType extends Model
{
    use QueryCacheable;

    public $cacheFor = 3600; // cache time, in seconds / month
    public $cachePrefix = 'dwa_account_types';

    const ACTIVE_FLAG = 1;
    const INACTIVE_FLAG = 0;

    protected $table = 'dwa_account_types';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
