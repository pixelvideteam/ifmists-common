<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class ScrutinyItems extends Model
{
    protected $table="dwa_scrutiny_items";

    protected $guarded=['id'];

    public function YesExtraOptions() {
        return $this->hasMany('App\Models\Dwa\ScrutinyItemsExtraOptions','scrutiny_item_id','id')
            ->where('aggregate_flag','=','0')->where('yes_flag','=','1');
    }

    public function NoExtraOptions() {
        return $this->hasMany('App\Models\Dwa\ScrutinyItemsExtraOptions','scrutiny_item_id','id')
            ->where('aggregate_flag','=','0')->where('no_flag','=','1');
    }

    public function aggregateOptions(){
        return $this->hasMany('App\Models\Dwa\ScrutinyItemsExtraOptions','scrutiny_item_id','id')
            ->where('aggregate_flag','=','1');
    }
}
