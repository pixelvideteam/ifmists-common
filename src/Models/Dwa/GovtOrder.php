<?php

namespace App\Models\Dwa;

use App\Department;
use App\Models\Dwa\Dwa\TechnicalSanctionValue;
use App\User;
use Illuminate\Database\Eloquent\Model;

class GovtOrder extends Model
{
    const GOVT_ORDER_APPROVED = 1;
    const GOVT_ORDER_REJECTED = 21;
    const GOVT_ORDER_PENDING = 0;
    const GOVT_ORDER_DELETED = 22;
    const GOVT_ORDER_SANCTION_TYPE = 1;
    const PROCEEDING_MEMO_SANCTION_TYPE = 2;
    const GOVT_ORDER_SANCTION_TYPE_TEXT = 'Govt Order';
    const PROCEEDING_MEMO_SANCTION_TYPE_TEXT = 'Proc/Memo';

    protected $table = 'dwa_govt_orders';

    public function hoas()
    {
        return $this->hasMany(GovtOrderHoa::class, 'dwa_govt_order_id', 'id');
    }

    public function proceedings()
    {
        return $this->hasMany(Proceedings::class, 'dwa_govt_order_id', 'id');
    }

    public function projects ()
    {
        return $this->hasMany(Projects::class, 'dwa_govt_order_id', 'id');
    }

    public function revised()
    {
        return $this->hasMany(GovtOrderRevise::class, 'dwa_govt_order_id', 'id')->latest();
    }

    public function technical_sanctions()
    {
        return $this->hasMany(TechnicalSanctionValue::class, 'dwa_govt_order_id', 'id')->with('revised');
    }

    public function departments()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function createdby()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
