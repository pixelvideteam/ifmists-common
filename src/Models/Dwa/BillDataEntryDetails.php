<?php

namespace App\Models\Dwa;

use App\BankIfsc;
use Illuminate\Database\Eloquent\Model;

class BillDataEntryDetails extends Model
{
    protected $table = 'dwa_bill_data_entry_details';

	const  AWARD_TYPE_PD = 1;
	const  AWARD_TYPE_DDO_ACCOUNT = 2;
	const  AWARD_TYPE_BENEFICIERY = 3;

    public function beneficiery_details()
    {
        return $this->hasOne(Beneficieries::class, 'id', 'dwa_benificiery_id');
    }

    public function benificiery_bank()
    {
        return $this->hasOne(BeneficieryBanks::class, 'id', 'dwa_benificiery_bank_id');
    }
}
