<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class LocBudgetDetails extends Model
{

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;

    protected $table = "dwa_loc_budget_details";

    protected $guarded = ["id"];

    public function  ddoBudget()
    {
        return $this->hasMany(LocDdoBudget::class, 'dwa_locbudget_id','id');
    }

    public function ddoloc()
    {
        return $this->hasMany(LocDdoBudget::class, 'dwa_locbudget_id', 'id');
    }

}
