<?php

namespace App\Models\Dwa\Accounts;

use Illuminate\Database\Eloquent\Model;

class DwaChequeTransactions extends Model
{
    protected $table = 'dwa_cheques_transactions';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function transactionDetails()
    {
        return $this->hasOne('App\Transactions','id','transaction_id');
    }
}
