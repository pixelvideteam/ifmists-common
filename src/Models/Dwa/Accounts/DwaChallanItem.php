<?php

namespace App\Models\Dwa\Accounts;

use Illuminate\Database\Eloquent\Model;

class DwaChallanItem extends Model
{
    protected $table = 'dwa_challan_items';

    public function dwaChallan()
    {
        return $this->belongsTo('App\Models\Dwa\Accounts\DwaChallan');
    }

    public function headOfAccount()
    {
        return $this->belongsTo('App\HeadsDesc', 'hoa', 'hoa');
    }
}
