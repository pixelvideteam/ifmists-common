<?php

namespace App\Models\Dwa\Accounts;

use Illuminate\Database\Eloquent\Model;

class DwaChallan extends Model
{
    const FIXED_DEPOSIT_ACCOUNT_HOA = "8675001060001000000NVN";

    protected $table = 'dwa_challans';

    public function dwaChallanItems()
    {
        return $this->hasMany('App\Models\Dwa\Accounts\DwaChallanItem');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'ddocode', 'username');
    }

    public function accountType()
    {
        return $this->belongsTo('App\Models\Dwa\AccountType', 'dwa_account_type_id','id');
    }
}
