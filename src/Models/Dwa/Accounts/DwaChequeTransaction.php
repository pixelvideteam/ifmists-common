<?php

namespace App\Models\Dwa\Accounts;

use App\DWACheques;
use Illuminate\Database\Eloquent\Model;
use App\Transactions;

class DwaChequeTransaction extends Model
{
    protected $table = 'dwa_cheques_transactions';

    const STATUS_ZERO = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 21;

    public function cheque()
    {
        return $this->belongsTo(DWACheques::class, 'dwa_cheques_id', 'id');
    }

    public function transaction()
    {
        return $this->belongsTo(Transactions::class, 'transaction_id', 'id');
    }
}
