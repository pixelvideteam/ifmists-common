<?php

namespace App\Models\Dwa\Accounts;

use Illuminate\Database\Eloquent\Model;

class DwaTeoAdjustmentItem extends Model
{
	protected $table = 'dwa_teo_adjustment_items';

    public function dwaTeoAdjustment()
    {
        return $this->belongsTo('App\Models\Dwa\Accounts\DwaTeoAdjustment');
    }

    public function headOfAccount()
    {
        return $this->belongsTo('App\HeadsDesc', 'hoa', 'hoa');
    }
}
