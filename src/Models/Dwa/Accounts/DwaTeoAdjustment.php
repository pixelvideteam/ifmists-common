<?php

namespace App\Models\Dwa\Accounts;

use Illuminate\Database\Eloquent\Model;

class DwaTeoAdjustment extends Model
{
	protected $table = 'dwa_teo_adjustments';

	public function user()
	{
		return $this->belongsTo('App\User', 'ddocode', 'username');
	}

    public function dwaTeoAdjustmentItems()
    {
        return $this->hasMany('App\Models\Dwa\Accounts\DwaTeoAdjustmentItem');
    }

    public function accountType()
    {
        return $this->belongsTo('App\Models\Dwa\AccountType', 'dwa_account_type_id', 'id');
    }
}
