<?php

namespace App\Models\Dwa\Accounts;

use Illuminate\Database\Eloquent\Model;

class DwaCheque extends Model
{
    protected $table = 'dwa_cheques_mannual';

    const STATUS_ZERO = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 21;

    public function transaction()
    {
        return $this->belongsTo('App\Transactions', 'transaction_id');
    }

    public function chequeTransactions()
    {
        return $this->hasMany(DwaChequeTransaction::class, 'dwa_cheques_id', 'id');
    }

    public function dwaChequeTransactions()
    {
        return $this->hasMany('App\Models\Dwa\Accounts\DwaChequeTransactions','dwa_cheques_id','id');
    }

    public function getDnFile()
    {
            return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')->where('transtype', '=', '3')->orderBy('created_at');
    }

    public function ekuberPaymentFiles()
    {
        return $this->hasOne('App\EkuberPaymentFiles',trim('ekuber_filename'),trim('ekuber_filename'))->where('client_id','=','3');
    }

    public function users()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
