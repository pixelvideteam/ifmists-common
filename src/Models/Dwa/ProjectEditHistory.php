<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class ProjectEditHistory extends Model
{
    protected $table= 'dwa_projects_edit_history';
}
