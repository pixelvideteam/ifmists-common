<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaGstEarnings extends Model
{
    protected $table="dwa_gst_earnings";

    protected $guarded=['id'];

    public function GstDeductionDetails(){
        return $this->belongsTo('App\Models\Dwa\Deductions','dwa_deductions_list_id','id');
    }

    public function transaction()
    {
        return $this->hasOne('App\Transactions', 'id', 'transaction_id');
    }
}
