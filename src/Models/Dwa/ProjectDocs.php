<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class ProjectDocs extends Model
{
    const EOT_TYPE_IN_WORDS = 'Extention Of Time';
    const BANKGUARANTEE_TYPE_IN_WORDS = 'Bank Guarantee';

    protected $table="dwa_project_docs";

    protected $guarded=['id'];
}
