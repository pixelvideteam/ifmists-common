<?php

namespace App\Models\Dwa;

use App\Transactions;
use Illuminate\Database\Eloquent\Model;

class BillDataEntry extends Model
{
    protected $table = 'dwa_bill_data_entry';

    const STATUS_PENDING_AT_PAO = 0;
    const STATUS_PENDING_AT_JD = 2;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;
    const STATUS_USED = 3; // only one time uses

    public function details()
    {
        return $this->hasMany(BillDataEntryDetails::class, 'dwa_bill_data_entry_id', 'id');
    }

    public function proceeding()
    {
        return $this->hasOne(Proceedings::class, 'id', 'dwa_proceeding_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transactions::class, 'transid', 'ref_no');
    }
}
