<?php

namespace App\Models\Dwa;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{

    const AGENCY_STATUS_PENDING = 0;
    const AGENCY_STATUS_APPROVED = 1;
    const AGENCY_STATUS_REJECTED = 21;
    const AGENCY_STATUS_EDIT_IN_APPROVAL = 2;
    const AGENCY_STATUS_FORWARDED = 3;

    protected $table = "dwa_agencies";

    protected $guarded = ['id'];

    public function bankDetails()
    {
        return $this->hasMany('App\Models\Dwa\AgencyBanks', 'agency_id', 'id')->with('ifsc');
    }

    public function bankDetailOne()
    {
        return $this->hasOne('App\Models\Dwa\AgencyBanks', 'agency_id', 'id')->with('ifsc');
    }

    public function supportingDocs()
    {
        return $this->hasMany('App\Models\Dwa\AgencyDocs', 'agency_id', 'id');
    }

    public function gstinList()
    {
        return $this->hasMany(AgencyGstinList::class, 'dwa_agency_id', 'id');
    }

    public function ddo()
    {
        return $this->hasOne(User::class, 'username', 'ddocode');
    }
}
