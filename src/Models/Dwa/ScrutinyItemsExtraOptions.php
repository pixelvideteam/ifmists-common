<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class ScrutinyItemsExtraOptions extends Model
{
    const TYPE_INPUT = 1;
    const TYPE_ATTACHMENT = 2;

    protected $table="dwa_scrutiny_items_extraoptions";

    protected $guarded=['id'];
}
