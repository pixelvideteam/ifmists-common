<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaBillMultiplePartyAudit extends Model
{
    protected $table = "dwa_bill_multiple_party_audit";

    protected $guarded = ['id', 'created_at', 'updated_at'];
}
