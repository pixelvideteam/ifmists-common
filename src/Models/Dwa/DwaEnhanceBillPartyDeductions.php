<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DwaEnhanceBillPartyDeductions extends Model
{
    use SoftDeletes;

    protected $table = 'dwa_enhance_bill_party_deductions';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function DeductionDetails()
    {
        return $this->belongsTo(Deductions::class, 'dwa_deductions_list_id', 'id');
    }

    public function billMultipleParty()
    {
        return $this->belongsTo(BillMultipleParty::class, 'bill_multiple_party_id', 'id');
    }
}
