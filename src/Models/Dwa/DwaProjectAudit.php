<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaProjectAudit extends Model
{
    protected $table='dwa_project_audit';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function user(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
