<?php

namespace App\Models\Dwa;

use App\Models\Dwa\Corporations\KaleswaramPhaseWorkidDetails;
use App\Models\Dwa\Dwa\TechnicalSanctionValue;
use Illuminate\Database\Eloquent\Model;
use App\Transactions;

class Projects extends Model
{
    protected $table="dwa_projects";

    protected $guarded=['id'];

    const PROJECT_TYPE_WORK = 0;
    const PROJECT_TYPE_LA = 1;
    const PROJECT_TYPE_RR = 2;

    const PROJECT_TYPE_WORK_IN_WORDS = 'WORKS';
    const PROJECT_TYPE_LA_IN_WORDS = 'LA';
    const PROJECT_TYPE_RR_IN_WORDS = 'RR';
    const PROJECT_TYPE_PD_ADJUSTMENT_IN_WORDS = 'PD Adjustment';

    const PROJECT_STATUS_PENDING_AT_PAO = 0;
    const PROJECT_STATUS_PENDING_AT_JD = 2;
    const PROJECT_STATUS_PENDING_AT_DWA = 4;
    const PROJECT_STATUS_APPROVED = 1;
    const PROJECT_STATUS_REJECTED = 21;
    const PROJECT_STATUS_EDITTED = 3;
    const PROJECT_STATUS_RETURNED = 5;

    const PROJECT_STATUS_PENDING_AT_PAO_IN_WORDS = 'Pending At PAO';
    const PROJECT_STATUS_PENDING_AT_JD_IN_WORDS = 'Pending At JD';
    const PROJECT_STATUS_PENDING_AT_DWA_IN_WORDS = 'Pending At Director';
    const PROJECT_STATUS_APPROVED_IN_WORDS = 'Approved';
    const PROJECT_STATUS_REJECTED_IN_WORDS = 'Rejected';
    const PROJECT_STATUS_EDITTED_IN_WORDS = 'Editted & Pending For Approval';
    const PROJECT_STATUS_RETURNED_IN_WORDS = 'Returned to DDO';

    const WORKS_WORK_TYPE = 'works';
    const DCWWORKS_WORK_TYPE = 'dcw_works';
    const BANKGUARANTEE_CODE = 'BG';
    const THRESHOLD_VALUE = 5000000;

    const SUPPLEMENTAL_AGREEMENT_NOT_AVAILABLE = 0;
    const SUPPLEMENTAL_AGREEMENT_AVAILABLE = 1;

    const PD_ADJUSTMENT_CATEGORY_ID = 92;

    const NO_CORPORATION_TYPE_PROJECT = 0;
    // corporation types
    const CORPORATION_TYPE_KALESWARAM = 1;

    // Payment modes
    const BUDGET_PAYMENT_MODE = 1;
    const LOAN_PAYMENT_MODE = 2;
    const BUDGET_PAYMENT_MODE_IN_WORDS = "Budget";
    const LOAN_PAYMENT_MODE_IN_WORDS = "Loan";

    const DEFAULT_HOA = "0000000000000000000NVN";

    public function agency(){
        return $this->hasOne('App\Models\Dwa\ProjectAgencies','project_id','id');
    }

    public function agencies(){
        return $this->hasMany('App\Models\Dwa\ProjectAgencies','project_id','id');
    }

    public function supportingDocs(){
        return $this->hasMany('App\Models\Dwa\ProjectDocs','project_id','id');
    }

    public function agenciesWithBanks(){
        return $this->hasMany('App\Models\Dwa\ProjectAgencies','project_id','id');
    }

    public function govt_order(){
        return $this->hasOne(GovtOrder::class,'id', 'dwa_govt_order_id');
    }

    public function proceeding(){
        return $this->hasOne(Proceedings::class,'id', 'dwa_proceeding_id');
    }

    public function funding_details()
    {
        return $this->hasOne(Fundings::class,'id', 'dwa_fundings_id');
    }

    public function deposit_details ()
    {
        return $this->hasMany(ProjectDepositDetails::class, 'dwa_projects_id', 'id');
    }

    public function supplementAgreementList ()
    {
        return $this->hasMany(ProjectSupplementalAgreement::class, 'dwa_project_id', 'id');
    }

    public function technical_sanction ()
    {
        return $this->hasOne(TechnicalSanctionValue::class, 'id', 'technical_sanction_id');
    }

    public function transaction(){
        return $this->hasMany(Transactions::class, 'project_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'ddocode', 'username');
    }

    public function audit(){
        return $this->hasMany('App\Models\Dwa\DwaProjectAudit', 'dwa_project_id', 'id');
    }

    public function retention_details ()
    {
        return $this->hasMany(ProjectDepositDetails::class, 'dwa_projects_id', 'id')
            ->where('category_type', '=', ProjectDepositDetails::RETENTION_CATEGORY_TYPE);
    }

    public function comments(){
        return $this->hasMany('App\Models\Dwa\DwaProjectAudit', 'dwa_project_id', 'id')
            ->where('field', '=', 'remarks')
            ->latest();
    }

    public function emd_deposit_details ()
    {
        return $this->hasMany(ProjectDepositDetails::class, 'dwa_projects_id', 'id')
            ->where('category_type', '=', ProjectDepositDetails::EMD_CATEGORY_TYPE);
    }

    public function asd_deposit_details ()
    {
        return $this->hasMany(ProjectDepositDetails::class, 'dwa_projects_id', 'id')
            ->where('category_type', '=', ProjectDepositDetails::ASD_CATEGORY_TYPE);
    }


    public function phaseWorkIdDetails()
    {
        return $this->hasOne(KaleswaramPhaseWorkidDetails::class, 'work_id', 'id');
    }

}
