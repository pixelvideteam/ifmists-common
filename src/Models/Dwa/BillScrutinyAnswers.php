<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class BillScrutinyAnswers extends Model
{
    protected $table="dwa_bill_scrutiny_answers";

    protected $guarded=['id'];

    public function extraOptionAnswers(){
        return $this->hasMany('App\Models\Dwa\BillScrutinyExtraItemAnswers', 'scrutiny_item_id', 'id')
            ->where('aggregate_flag','=',0);
    }
    public function AggregateOptionAnswers(){
        return $this->hasMany('App\Models\Dwa\BillScrutinyExtraItemAnswers', 'scrutiny_item_id', 'id')
            ->where('aggregate_flag','=',1)->with('optionDetails');
    }

    public function ScrutinyDetails(){
        return $this->hasOne('App\Models\Dwa\ScrutinyItems', 'id', 'scrutiny_id');
    }

    public function extraOptions(){
        return $this->hasMany('App\Models\Dwa\ScrutinyItemsExtraOptions', 'scrutiny_item_id', 'scrutiny_id');
    }

    public function optionsCount(){
        return $this->hasOne('App\Models\Dwa\ScrutinyItemsExtraOptions', 'scrutiny_item_id', 'scrutiny_id');
    }

    public function options(){
        return $this->hasMany('App\Models\Dwa\ScrutinyItemsExtraOptions', 'scrutiny_item_id', 'scrutiny_id')
            ->where('aggregate_flag','=',0);
    }

    public function aggregateOptions(){
        return $this->hasMany('App\Models\Dwa\ScrutinyItemsExtraOptions', 'scrutiny_item_id', 'scrutiny_id')
            ->where('aggregate_flag','=',1);
    }
}
