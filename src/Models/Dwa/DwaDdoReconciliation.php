<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaDdoReconciliation extends Model
{
    const DWA_DDO_RECONCILIATION_PENDING_STATUS = 0;
    const DWA_DDO_RECONCILIATION_APPROVED_STATUS = 1;
    const DWA_DDO_RECONCILIATION_REJECTED_STATUS = 21;
    const DWA_DDO_RECONCILIATION_CHECK_MONTHS = 2;
    const DWA_DDO_RECONCILIATION_START_DATE = '2020-04-01';
    const DWA_DDO_RECONCILIATION_CHECK_DATE_FROM = '2020-06-01';

    protected $table = "dwa_ddo_reconciliation";

    protected $guarded = ['id'];

    public function ddoUser()
    {
        return $this->belongsTo('App\User', 'ddocode', 'username');
    }
}
