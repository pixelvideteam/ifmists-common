<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'dwa_bill_categories';
}
