<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class ProjectDepositDetails extends Model
{
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;
    const STATUS_PENDING_FOR_APPROVAL = 0;

    const EMD_CATEGORY_TYPE = 1;
    const ASD_CATEGORY_TYPE = 2;
    const RETENTION_CATEGORY_TYPE = 3;

    protected $table = 'dwa_project_deposit_details';
}
