<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class AgAccountType extends Model
{
    use QueryCacheable;

    public $cacheFor = 3600; // cache time, in seconds / month
    public $cachePrefix = 'dwa_ag_account_type';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE =0;

    protected $table = 'dwa_ag_account_type';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
