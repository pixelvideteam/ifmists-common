<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class BmsBillTypeCode extends Model
{
    protected $table ='dwa_bms_bill_type_code';

    protected $guarded=['id'];
    public $timestamps = false;
}
