<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaPpForm extends Model
{
    CONST PP_FORM_ACTIVE_STATUS = 1;
    CONST PP_FORM_DISABLE_STATUS = 0;

    protected $table='dwa_pp_form';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function adminSanction(){
        return $this->belongsTo('App\Models\Dwa\GovtOrder', 'dwa_govt_order_id', 'id');
    }

    public function technicalSanction(){
        return $this->belongsTo('App\Models\Dwa\Dwa\TechnicalSanctionValue', 'dwa_technical_sanction_id', 'id');
    }
}
