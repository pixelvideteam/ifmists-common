<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class BeneficieryBanks extends Model
{
    protected $table="dwa_beneficieries_banks";

    protected $guarded=['id'];

    public function beneficieryDetails() {
        return $this->belongsTo('App\Models\Dwa\Beneficieries','beneficiery_id','id')
            ->with('district')
            ->with('mandal')
            ->with('village');
    }

    public function BankDetails() {
        return $this->hasOne('App\BankIfsc','id','ifsc');
    }
}
