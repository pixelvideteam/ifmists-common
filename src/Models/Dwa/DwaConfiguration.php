<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaConfiguration extends Model
{
    protected $table = 'dwa_configuration';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    // FORMATS
    // TIME - HH:MM - 24 HOURS FORMAT
    // Eg: 10:10, 14:15, 20:00, etc
}
