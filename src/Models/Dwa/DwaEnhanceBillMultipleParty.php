<?php

namespace App\Models\Dwa;

use App\BillMultipleParty;
use Illuminate\Database\Eloquent\Model;

class DwaEnhanceBillMultipleParty extends Model
{
    protected $table = 'dwa_enhance_bill_multiple_party';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function billMultipleParty()
    {
        return $this->belongsTo(BillMultipleParty::class, 'bill_multiple_party_id', 'id');
    }
}
