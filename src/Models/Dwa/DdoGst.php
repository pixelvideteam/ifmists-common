<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DdoGst extends Model
{

    const DDO_GSTREG_PENDING_STATUS = 0;
    const DDO_GSTREG_PENDING_STATUS_IN_WORDS = 'Pending For Approval';
    const DDO_GSTREG_APPROVE_STATUS = 1;
    const DDO_GSTREG_APPROVE_STATUS_IN_WORDS = 'Approved';
    const DDO_GSTREG_REJECT_STATUS = 21;
    const DDO_GSTREG_REJECT_STATUS_IN_WORDS = "Rejected";
    const PAN_CARD_CODE = 'pan';
    const TAN_CARD_CODE = 'tan';

    protected $table = 'dwa_ddo_gst';

    protected $guarded=['id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'ddocode', 'username');
    }

}
