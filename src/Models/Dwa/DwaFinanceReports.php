<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaFinanceReports extends Model
{
    const DWA_FINANCE_REPORT = 'financereport';

    const STATUS_PENDING = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_FAILED = 21;

    protected $table = 'dwa_finance_reports';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];
}
