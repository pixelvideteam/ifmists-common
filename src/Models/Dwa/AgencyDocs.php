<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class AgencyDocs extends Model
{
    protected $table="dwa_agency_docs";

    protected $guarded=['id'];
}
