<?php

namespace App\Models\Dwa\MK;

use Illuminate\Database\Eloquent\Model;

class DwaMKDetails extends Model
{
    protected $table = 'dwa_mk_details';
    protected $guarded = ['id'];
}
