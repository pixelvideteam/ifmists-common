<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class Mandals extends Model
{
    protected $table="dwa_la_mandals";

    protected $guarded=['id'];
}
