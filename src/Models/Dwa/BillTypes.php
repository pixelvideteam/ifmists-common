<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class BillTypes extends Model
{

    const TDS_GST_BILL_CODE = 'TDSGST';
    const WORKS_BILL_CODE = 'works';
    const DCW_OTHERS_BILL_CODE = 'others';
    const DCW_CASH_BILL_CODE = 'cash';
    const HANDRECEIPTS_BILL_CODE = 'HReciepts';
    const LANDACQUISITION_BILL_CODE = 'LA';
    const RR_WORKS_BILL_CODE = 'RRW';
    const REHABILITATION_BILL_CODE = 'REH';


    const BILL_TYPE_ID_RR = 6;
    const BILL_TYPE_ID_LA = 4;
    const BILL_TYPE_ID_GST = 67;

    protected $table = "dwa_bill_types";
    protected $guarded=['id'];

    public function categories(){
        return $this->hasMany(Category::class, 'bill_type_id', 'id');
    }
}
