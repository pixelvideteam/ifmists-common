<?php

namespace App\Models\Dwa;

use App\TransactionsTransstatuses;
use Illuminate\Database\Eloquent\Model;

class BillGstDetails extends Model
{
    protected $table = "dwa_bill_gst_details";

    protected $guarded = ['id'];

    public $replacePanGst = [
        [
            'tx_id' => 9881940,
            'panno' => 'AAKFV4641C',
            'gstin' => '36AAKFV4641C1Z7',
        ]
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Transactions', 'transaction_id');
    }

    public function scopeDwaBillGstDetailTransactions($query, $request)
    {
        $dwaPaoCode = $request->paoCode ?? null;
        $accType = $request->accType ?? null;
        $fromDate = $request->fromDate ?? null;
        $toDate = $request->toDate ?? null;
        $ddocode = $request->ddocode ?? null;

        return $query
            ->where(function ($gQuery) {
                $gQuery->where('sgst_amount', '!=', 0);
                $gQuery->orWhere('igst_amount', '!=', 0);
                $gQuery->orWhere('cgst_amount', '!=', 0);
            })
            ->select('id', 'transaction_id', 'gst_amount', 'gst_percentage', 'cgst_amount', 'sgst_amount', 'igst_amount')
            ->whereHas('transaction', function ($q) use ($dwaPaoCode, $accType, $fromDate, $toDate, $ddocode) {
                $q->where('transstatus', TransactionsTransstatuses::DWA_BILL_PAID);
                if ($fromDate && $toDate) {
                    $q->whereBetween('voucherdate', [$fromDate, $toDate]);
                }
                if ($accType) {
                    $q->where('dwa_account_type_id', $accType);
                }
                if ($ddocode) {
                    $q->where('ddocode', $ddocode);
                }
                $q->whereHas('users', function ($uq) use ($dwaPaoCode, $ddocode) {
                    if ($dwaPaoCode) {
                        $uq->where('dwa_pao_code', $dwaPaoCode);
                    }
                    // if ($ddocode) {
                    //     $uq->where('username', $ddocode);
                    // }
                });
            })
            ->with(['transaction' => function ($q) use ($dwaPaoCode, $accType, $fromDate, $toDate, $ddocode) {

                $q->select('id', 'ddocode', 'dwa_account_type_id', 'tokenno', 'bill_year', 'voucherno', 'voucherdate', 'gross', 'net', 'hoa', 'transtype');

                $q->where('transstatus', TransactionsTransstatuses::DWA_BILL_PAID);
                if ($fromDate && $toDate) {
                    $q->whereBetween('voucherdate', [$fromDate, $toDate]);
                }
                if ($accType) {
                    $q->where('dwa_account_type_id', $accType);
                }
                if ($ddocode) {
                    $q->where('ddocode', $ddocode);
                }

                $q
                    ->whereHas('users', function ($uq) use ($dwaPaoCode, $ddocode) {
                        if ($dwaPaoCode) {
                            $uq->where('dwa_pao_code', $dwaPaoCode);
                        }
                        // if ($ddocode) {
                        //     $uq->where('username', $ddocode);
                        // }
                    })
                    ->with(['users' => function ($uq) use ($dwaPaoCode, $ddocode) {
                        $uq->select('id', 'dwa_pao_code_number', 'username', 'ddo_office', 'dwa_pao_code', 'gstno');
                        $uq->with('arealist:id,pao_code,areaname');
                        if ($dwaPaoCode) {
                            $uq->where('dwa_pao_code', $dwaPaoCode);
                        }
                        // if ($ddocode) {
                        //     $uq->where('username', $ddocode);
                        // }
                    }])
                    ->with('accountType:id,name')
                    ->with('BillPartyDetails:id,transaction_id,dwa_agency_id,bankacno,gstno')
                    ->with('BillPartyDetails.agencydet:id,bankacno,gstno,panno,partyname')
                    ->with('BillPartyDetails.agency:id,pan_no,party_name,gstin');
            }])
            ->get();
    }
}
