<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class BackupGoTechSanFunding extends Model
{
    protected $table = 'dwa_go_techsan_funding_backup';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}