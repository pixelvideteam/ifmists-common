<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaTransactionAudit extends Model
{
    protected $table = "dwa_transaction_audits";
    protected $guarded = ['id'];

    public function transaction()
    {
        return $this->belongsTo('App\Transactions', 'transaction_id');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
