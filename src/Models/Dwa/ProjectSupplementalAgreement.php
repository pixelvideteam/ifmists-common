<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class ProjectSupplementalAgreement extends Model
{
    const STATUS_PENDING_FOR_APPROVAL = 0;
    const STATUS_REJECTED = 21;
    const STATUS_APPROVED = 1;

    protected $table="dwa_project_supplemental_agreement";

    protected $guarded=['id'];
}
