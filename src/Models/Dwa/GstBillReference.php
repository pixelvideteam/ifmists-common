<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class GstBillReference extends Model
{
    protected $table = 'dwa_gstbill_reference';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function ddoTransactions()
    {
        return $this->belongsTo('App\Transactions', 'transaction_id');
    }

    public function scopeCpinExist($query, $cpin)
    {
        return $query->where('cpin', $cpin)->exists();
    }
}
