<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class AddlFundsOrReAppropriateBudget extends Model
{
    const FIRST_HALF = 1;
    const SECOND_HALF = 2;
    const TABLE_PREFIX = 'dwa_budget_reapprop_addlfunds_';
    const REAPPROPRIATION_TYPE = 1;
    const ADDITIONAL_FUNDS_TYPE = 2;

    protected $table;

    public function __construct(array $attributes = [])
    {
        $this->table = $this->getTableName();
    }

    protected function getTableName()
    {
        $budgetYear = $this->getCurrentYear();
        $partNumber = $this->getYearPartNumber();

        return self::TABLE_PREFIX.$budgetYear.'_'.$partNumber;
    }

    protected function getCurrentYear()
    {
        return date('Y');
    }

    protected function getYearPartNumber()
    {
        return (date('m') > '06') ? self::SECOND_HALF : self::FIRST_HALF;
    }
}
