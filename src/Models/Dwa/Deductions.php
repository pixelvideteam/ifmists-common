<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class Deductions extends Model
{
    const DEDN_GST_TYPE = 1;
    const DEDN_CGST_TYPE = 1;
    const DEDN_SGST_TYPE = 2;
    const DEDN_IGST_TYPE = 3;

    const COMMON_RECOVERY_TYPES = [1, 2, 3];

    protected $table = "dwa_deductions_list";

    protected $guarded = ['id'];

    public function billMultipleParties()
    {
        return $this->belongsToMany('App\BillMultipleParty', 'dwa_bill_party_deductions', 'dwa_deductions_list_id', 'bill_multiple_party_id')
            ->withPivot('amount');
    }

    public function hoaDesc()
    {
        return $this->belongsTo('App\HeadsDesc', 'hoa', 'hoa');
    }
}
