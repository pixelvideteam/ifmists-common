<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaMobilisationAmountDetails extends Model
{
    protected $table='dwa_mobilisation_amount_details';

    protected $guarded = ['id', 'created_at', 'updated_at'];
}
