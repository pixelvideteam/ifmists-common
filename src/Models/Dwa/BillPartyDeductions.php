<?php

namespace App\Models\Dwa;

use App\BillMultipleParty;
use Illuminate\Database\Eloquent\Model;

class BillPartyDeductions extends Model
{
    protected $table = "dwa_bill_party_deductions";

    protected $guarded = ['id'];

    public function DeductionDetails()
    {
        return $this->belongsTo('App\Models\Dwa\Deductions', 'dwa_deductions_list_id', 'id');
    }

    public function billMultipleParty()
    {
        return $this->belongsTo(BillMultipleParty::class, 'bill_multiple_party_id', 'id');
    }
}
