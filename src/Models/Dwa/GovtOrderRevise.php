<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class GovtOrderRevise extends Model
{
    const GOVT_ORDER_REVISE_PENDING_STATUS = 0;
    const GOVT_ORDER_REVISE_APPROVED_STATUS = 1;
    const GOVT_ORDER_REVISE_REJECTED_STATUS = 21;
    const GOVT_ORDER_REVISE_DELETED_STATUS = 22;

    protected $table = 'dwa_govt_order_revises';
}
