<?php

namespace App\Models\Dwa\Corporations\Kaleshwaram;

use Illuminate\Database\Eloquent\Model;

class DwaKaleshwaramBillPartyDeductions extends Model
{
    protected $table = "dwa_kaleshwaram_bill_party_deductions";

    protected $guarded = ['id'];

    public function DeductionDetails()
    {
        return $this->belongsTo('App\Models\Dwa\Deductions', 'dwa_deductions_list_id', 'id');
    }

    public function billMultipleParty()
    {
        return $this->belongsTo('App\BillMultipleParty', 'bill_multiple_party_id', 'id');
    }
}
