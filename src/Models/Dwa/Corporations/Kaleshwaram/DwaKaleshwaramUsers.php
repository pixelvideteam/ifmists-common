<?php

namespace App\Models\Dwa\Corporations\Kaleshwaram;

use Illuminate\Database\Eloquent\Model;

class DwaKaleshwaramUsers extends Model
{
    protected $table='dwa_kaleshwaram_users';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
