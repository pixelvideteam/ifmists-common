<?php

namespace App\Models\Dwa\Corporations\Kaleshwaram;

use Illuminate\Database\Eloquent\Model;

class DwaKaleshwaramBillProjectDetails extends Model
{
    protected $table="dwa_kaleshwaram_bill_project_details";

    protected $guarded=['id'];

    public function Department()
    {
        return $this->hasOne('App\Department','id','department_id');
    }

    public function billDetails()
    {
        return $this->hasOne('App\Transactions','id','transaction_id');
    }
}
