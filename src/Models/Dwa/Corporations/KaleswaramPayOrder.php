<?php

namespace App\Models\Dwa\Corporations;

use App\Transactions;
use Illuminate\Database\Eloquent\Model;

class KaleswaramPayOrder extends Model
{
    const STATUS_PAYORDER_GENERATED = 0;
    const STATUS_PAYORDER_BILL_CONFIRMED = 1; // by MD
    const STATUS_PAYORDER_BILL_PAID = 2; // by MD

    protected $table = 'dwa_kaleswaram_pay_order';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function transaction()
    {
        return $this->belongsTo(Transactions::class, 'transaction_id', 'id');
    }
}
