<?php

namespace App\Models\Dwa\Corporations;

use App\Models\Dwa\Projects;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KaleswaramPhaseWorkidDetails extends Model
{
    use SoftDeletes;

    protected $table = 'dwa_kaleswaram_phase_workid_details';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function phase()
    {
        return $this->belongsTo(KaleswaramPhases::class, 'phase_id', 'id');
    }

    public function workid()
    {
        return $this->belongsTo(Projects::class, 'work_id', 'id');
    }
}
