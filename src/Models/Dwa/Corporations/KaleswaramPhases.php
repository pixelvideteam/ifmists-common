<?php

namespace App\Models\Dwa\Corporations;

use Illuminate\Database\Eloquent\Model;

class KaleswaramPhases extends Model
{
    const PAHSE_STATUS_ACTIVE = 1;
    const PAHSE_STATUS_INACTIVE = 2;

    protected $table = 'dwa_kaleswaram_phases';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function phase_users()
    {
        return $this->hasMany('\App\Models\Dwa\Corporations\Kaleshwaram\DwaKaleshwaramPhaseUsers', 'phase_id', 'id');
    }
}
