<?php

namespace App\Models\Dwa\Budget;

use App\Http\Controllers\Dwa\Budget\DwaBudgetController;
use Illuminate\Database\Eloquent\Model;

class DwaBudgetSurrenderLocDdo extends Model
{
    const SURRENDER_LOC_DDO_PENDING = 0;
    const SURRENDER_LOC_DDO_APPROVED = 1;
    const SURRENDER_LOC_DDO_REJECTED = 21;
    const SURRENDER_LOC_DDO_DELETED = 22;

    protected $table = "dwa_budget_surrender_loc_ddo";

    protected $guarded = ['id'];

    public function dwaBudget()
    {
        return $this->belongsTo('App\Models\Dwa\Budget\DwaBudgetEstimation', 'dwa_budget_estimation_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'ddocode', 'username');
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = (new DwaBudgetController())->getAmountInRupees($value);
    }

    public function getAmountAttribute($value)
    {
        return (new DwaBudgetController())->getAmountAsPerBudgetFormat($value);
    }
}
