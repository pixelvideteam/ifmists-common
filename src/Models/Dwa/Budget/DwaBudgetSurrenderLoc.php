<?php

namespace App\Models\Dwa\Budget;

use App\Http\Controllers\Dwa\Budget\DwaBudgetController;
use Illuminate\Database\Eloquent\Model;

class DwaBudgetSurrenderLoc extends Model
{
    const SURRENDER_LOC_PENDING = 0;
    const SURRENDER_LOC_APPROVED = 1;
    const SURRENDER_LOC_REJECTED = 21;
    const SURRENDER_LOC_DELETED = 22;
    const SURRENDER_LOC_PREFIX = "S";
    const SURRENDER_LOC_DELIMITER = "-";

    protected $table = "dwa_budget_surrender_loc";

    protected $guarded = ['id'];
    protected $appends = ['code'];

    public function ddos()
    {
        return $this->hasMany('App\Models\Dwa\Budget\DwaBudgetSurrenderLocDdo', 'dwa_budget_surrender_loc_id', 'id');
    }

    public function dwaBudget()
    {
        return $this->belongsTo('App\Models\Dwa\Budget\DwaBudgetEstimation', 'dwa_budget_estimation_id', 'id');
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = (new DwaBudgetController())->getAmountInRupees($value);
    }

    public function getAmountAttribute($value)
    {
        return (new DwaBudgetController())->getAmountAsPerBudgetFormat($value);
    }

    public function getCodeAttribute()
    {
        return self::SURRENDER_LOC_PREFIX.self::SURRENDER_LOC_DELIMITER.$this->attributes['id'];
    }
}
