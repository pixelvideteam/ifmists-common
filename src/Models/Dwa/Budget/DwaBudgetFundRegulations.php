<?php

namespace App\Models\Dwa\Budget;

use App\Http\Controllers\Dwa\Budget\DwaBudgetController;
use Illuminate\Database\Eloquent\Model;

class DwaBudgetFundRegulations extends Model
{
    const ADDITIONAL_FUND_TYPE = 1;
    const RE_APPROPRIATION_TYPE = 2;
    const RESUMPTION_TYPE = 3;

    const PENDING_STATUS = 0;
    const APPROVED_STATUS = 1;
    const REJECTED_STATUS = 21;
    const DELETED_STATUS = 22;

    const ADDITIONAL_FUND_CODE_PREFIX = 'A';
    const RE_APPROPRIATION_CODE_PREFIX = 'R';
    const RESUMPTION_CODE_PREFIX = 'RS';
    const FUND_REGULATION_DELIMITER = '-';

    protected $table = "dwa_budget_fund_allocations";

    protected $guarded = ['id'];
    protected $appends = ['code', 'amount_in_rupees'];

    public function hoas()
    {
        return $this->hasMany('App\Models\Dwa\Budget\DwaBudgetFundRegulationDetails', 'dwa_budget_fund_allocation_id', 'id');
    }

    public function additionalFundHoas()
    {
        return $this->hasMany('App\Models\Dwa\Budget\DwaBudgetFundRegulationDetails','dwa_budget_fund_allocation_id', 'id')
            ->where('allocation_type', self::ADDITIONAL_FUND_TYPE);
    }

    public function reAppropriationHoas()
    {
        return $this->hasMany('App\Models\Dwa\Budget\DwaBudgetFundRegulationDetails','dwa_budget_fund_allocation_id', 'id')
            ->where('allocation_type', self::RE_APPROPRIATION_TYPE);
    }

    public function resumptionHoas()
    {
        return $this->hasMany('App\Models\Dwa\Budget\DwaBudgetFundRegulationDetails','dwa_budget_fund_allocation_id', 'id')
            ->where('allocation_type', self::RESUMPTION_TYPE);
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] =  (new DwaBudgetController())->getAmountInRupees($value);
    }

    public function getAmountAttribute($value)
    {
        return (new DwaBudgetController())->getAmountAsPerBudgetFormat($value);
    }

    public function getCodeAttribute()
    {
        $codePrefix = $this->attributes['allocation_type'] == 1 ? self::ADDITIONAL_FUND_CODE_PREFIX : ($this->attributes['allocation_type'] == 2 ? self::RE_APPROPRIATION_CODE_PREFIX : self::RESUMPTION_CODE_PREFIX);
        return $codePrefix.self::FUND_REGULATION_DELIMITER.$this->attributes['id'];
    }

    public function getAmountInRupeesAttribute(){
        return $this->attributes['amount'];
    }
}
