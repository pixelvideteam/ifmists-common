<?php

namespace App\Models\Dwa\Budget;

use App\Http\Controllers\Dwa\Budget\DwaBudgetController;
use Illuminate\Database\Eloquent\Model;

class DwaBudgetLoc extends Model
{
    const DWA_BUDGET_LOC_PENDING_STATUS = 0;
    const DWA_BUDGET_LOC_APPROVED_STATUS = 1;
    const DWA_BUDGET_LOC_REJECTED_STATUS = 21;
    const DWA_BUDGET_LOC_DELETED_STATUS = 22;
    const DWA_BUDGET_LOC_CODE_PREFIX = "L";
    const DWA_BUDGET_LOC_DELIMITER = "-" ;
    const DWA_BUDGET_AUTHORIZATION_PREFIX = 'AUTH';

    protected $table = "dwa_budget_loc";

    protected $guarded = ['id'];
    protected $appends = ['code', 'amount_in_rupees'];

    public function ddos()
    {
        return $this->hasMany('App\Models\Dwa\Budget\DwaBudgetLocDdo', 'dwa_budget_loc_id', 'id');
    }

    public function dwaBudget()
    {
        return $this->belongsTo('App\Models\Dwa\Budget\DwaBudgetEstimation', 'dwa_budget_estimation_id', 'id');
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] =  (new DwaBudgetController())->getAmountInRupees($value);
    }

    public function getAmountAttribute($value)
    {
        return (new DwaBudgetController())->getAmountAsPerBudgetFormat($value);
    }

    public function setGrantReleasedByGovtAttribute($value)
    {
        $this->attributes['grant_released_by_govt'] =  (new DwaBudgetController())->getAmountInRupees($value);
    }

    public function getGrantReleasedByGovtAttribute($value)
    {
        return (new DwaBudgetController())->getAmountAsPerBudgetFormat($value);
    }

    public function setGrantReleasedNowAttribute($value)
    {
        $this->attributes['grant_released_now'] = (new DwaBudgetController())->getAmountInRupees($value);
    }

    public function getGrantReleasedNowAttribute($value)
    {
        return (new DwaBudgetController())->getAmountAsPerBudgetFormat($value);
    }

    public function getCodeAttribute()
    {
        return self::DWA_BUDGET_LOC_CODE_PREFIX.self::DWA_BUDGET_LOC_DELIMITER.$this->attributes['id'];
    }

    public function getAuthorizationNumberAttribute()
    {
        return ($this->attributes['authorization_number'] == NULL )? "-" : self::DWA_BUDGET_AUTHORIZATION_PREFIX.self::DWA_BUDGET_LOC_DELIMITER.$this->attributes['authorization_number'];
    }

    public function getAmountInRupeesAttribute(){
        return $this->attributes['amount'];
    }

    public function print()
    {
        return $this->hasMany('App\Models\Dwa\Budget\DwaLocPrint', 'dwa_budget_loc_id', 'id');
    }

}
