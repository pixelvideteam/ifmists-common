<?php

namespace App\Models\Dwa\Budget;

use Illuminate\Database\Eloquent\Model;

class DwaBudgetPercentageRegulations extends Model
{
    const DEFAULT_PERCENTAGE = 100;

    protected $table = "percentage_regulations";

    protected $guarded = ['id'];
}
