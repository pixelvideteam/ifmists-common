<?php

namespace App\Models\Dwa\Budget;

use App\Http\Controllers\Dwa\Budget\DwaBudgetController;
use Illuminate\Database\Eloquent\Model;

class DwaBudgetLocDdo extends Model
{
    const DWA_BUDGET_LOC_DDO_PENDING_STATUS = 0;
    const DWA_BUDGET_LOC_DDO_APPROVED_STATUS = 1;
    const DWA_BUDGET_LOC_DDO_REJECTED_STATUS = 21;
    const DWA_BUDGET_LOC_DDO_DELETED_STATUS = 22;

    protected $table = "dwa_budget_loc_ddo";

    protected $guarded = ['id'];

    public function loc()
    {
        return $this->belongsTo('App\Models\Dwa\Budget\DwaBudgetLoc', 'dwa_budget_loc_id', 'id');
    }

    public function dwaBudget()
    {
        return $this->belongsTo('App\Models\Dwa\Budget\DwaBudgetEstimation', 'dwa_budget_estimation_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'ddocode', 'username');
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = (new DwaBudgetController())->getAmountInRupees($value);
    }

    public function getAmountAttribute($value)
    {
        return (new DwaBudgetController())->getAmountAsPerBudgetFormat($value);
    }
}
