<?php

namespace App\Models\Dwa\Budget;

use Illuminate\Database\Eloquent\Model;

class DwaDeficit extends Model
{
    protected $table = 'dwa_deficits';
    protected $guarded = [];

    public function deficitDetails()
    {
        return $this->hasMany(DwaDeficitDetail::class);
    }
}
