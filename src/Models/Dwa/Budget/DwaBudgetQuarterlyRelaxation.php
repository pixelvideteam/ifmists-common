<?php

namespace App\Models\Dwa\Budget;

use App\Http\Controllers\Dwa\Budget\DwaBudgetController;
use Illuminate\Database\Eloquent\Model;

class DwaBudgetQuarterlyRelaxation extends Model
{
    const APPROVED_STATUS = 1;
    const PENDING_STATUS = 0;
    const REJECTED_STATUS = 21;
    const DELETED_STATUS = 22;
    const QUARTERLY_RELAXATION_CODE_PREFIX = "Q";
    const QUARTERLY_RELAXATION_DELIMITER = "-";

    protected $table = "dwa_budget_quarterly_relaxation";

    protected $guarded = ['id'];
    protected $appends = ['code', 'amount_in_rupees'];

    public function dwaBudget()
    {
        return $this->belongsTo('App\Models\Dwa\Budget\DwaBudgetEstimation', 'dwa_budget_estimation_id', 'id');
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = (new DwaBudgetController())->getAmountInRupees($value);
    }

    public function getAmountAttribute($value)
    {
        return (new DwaBudgetController())->getAmountAsPerBudgetFormat($value);
    }

    public function getCodeAttribute()
    {
        return self::QUARTERLY_RELAXATION_CODE_PREFIX.self::QUARTERLY_RELAXATION_DELIMITER.$this->attributes['id'];
    }

    public function getAmountInRupeesAttribute(){
        return $this->attributes['amount'];
    }

}
