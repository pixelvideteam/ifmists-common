<?php

namespace App\Models\Dwa\Budget;

use App\Http\Controllers\Dwa\Budget\DwaBudgetController;
use Illuminate\Database\Eloquent\Model;

class DwaBudgetFundRegulationDetails extends Model
{
    const CREDIT_TYPE = 1;
    const DEBIT_TYPE = 2;

    protected $table = "dwa_budget_fund_allocation_details";

    protected $guarded = ['id'];
    protected $appends = ['amount_in_rupees'];

    public function fundAllocation()
    {
        return $this->belongsTo('App\Models\Dwa\Budget\DwaBudgetFundRegulations', 'dwa_budget_fund_allocation_id', 'id');
    }

    public function dwaBudget()
    {
        return $this->belongsTo('App\Models\Dwa\Budget\DwaBudgetEstimation', 'dwa_budget_estimation_id', 'id');
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = (new DwaBudgetController())->getAmountInRupees($value);
    }

    public function getAmountAttribute($value)
    {
        return (new DwaBudgetController())->getAmountAsPerBudgetFormat($value);
    }

    public function getAmountInRupeesAttribute(){
        return $this->attributes['amount'];
    }
}
