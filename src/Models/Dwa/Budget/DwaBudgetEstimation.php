<?php

namespace App\Models\Dwa\Budget;

use App\Http\Controllers\Dwa\Budget\DwaBudgetController;
use Illuminate\Database\Eloquent\Model;

class DwaBudgetEstimation extends Model
{
    const DEFAULT_PERCENTAGE = 100;
    
    protected $table = "dwa_budget_estimation";

    protected $guarded = ['id'];

    public function hoa()
    {
        return $this->hasOne('App\HeadsDesc', 'id', 'head_desc_id');
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = (new DwaBudgetController())->getAmountInRupees($value);
    }

    public function getAmountAttribute($value)
    {
        return (new DwaBudgetController())->getAmountAsPerBudgetFormat($value);
    }
}
