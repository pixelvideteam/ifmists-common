<?php

namespace App\Models\Dwa\Budget;

use App\Transactions;
use Illuminate\Database\Eloquent\Model;

class DwaDeficitDetail extends Model
{
    protected $table = 'dwa_deficit_details';
    protected $guarded = [];

    public function transaction()
    {
        return $this->belongsTo(Transactions::class, 'transaction_id');
    }
}
