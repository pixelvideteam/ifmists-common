<?php

namespace App\Models\Dwa\Budget;

use Illuminate\Database\Eloquent\Model;

class DwaLocPrint extends Model
{
    protected $table = "dwa_budget_loc_print";

    protected $guarded = ['id'];
}
