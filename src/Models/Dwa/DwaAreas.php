<?php

namespace App\Models\Dwa;

use App\PaoDdoAuditorMap;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DwaAreas extends Model
{
    use QueryCacheable;

    public $cacheFor = 3600; // cache time, in seconds / month
    public $cachePrefix = 'dwa_arealist_';

    protected $table = "dwa_arealist";
    public $timestamps = false;

    protected $guarded = ["id"];

    public function ddo_list()
    {
        return $this->hasMany(PaoDdoAuditorMap::class, 'dwa_area_id', 'id');
    }

    public function ddoUsers()
    {
        return $this->hasMany('App\User', 'dwa_pao_code', 'area_code')
            ->where('user_role', User::DWA_DDO_ROLE_ID);
    }

    public function jdwa()
    {
        return $this->belongsTo('App\Models\Dwa\JDList', 'dwa_jdwa_id', 'id');
    }

    public function auditors()
    {
        return $this->hasMany('App\User', 'dwa_pao_code', 'area_code')
            ->where('user_role', User::DWA_AUDITOR_ROLE_ID);
    }
}
