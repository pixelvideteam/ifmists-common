<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaBudget extends Model
{
    const FIRST_HALF = 1;
    const SECOND_HALF = 2;
    const TABLE_PREFIX = 'dwa_budget_';
    const YEARLY_BUDGET_DURATION_TYPE = 1;
    const HALFYEARLY_BUDGET_DURATION_TYPE = 2;
    const DDHEAD_FLAG = 1;
    const LOCHEAD_FLAG = 0;

    protected $table;

    public function __construct(array $attributes = [])
    {
        $this->table = $this->getTableName();
    }

    public static function getTableName()
    {
        $budgetYear = self::getCurrentYear();
        $partNumber = self::getYearPartNumber();

        return self::TABLE_PREFIX.$budgetYear.'_'.$partNumber;
    }

    protected static function getCurrentYear()
    {
        return date('Y');
    }

    protected static function getYearPartNumber()
    {
        return (date('m') > '09') ? self::SECOND_HALF : self::FIRST_HALF;
    }

    public function reappropriationBudget()
    {
        return $this->hasMany(AddlFundsOrReAppropriateBudget::class,'dwa_budget_id','id')
            ->where('type',AddlFundsOrReAppropriateBudget::REAPPROPRIATION_TYPE);
    }
    public function addlBudget()
    {
        return $this->hasMany(AddlFundsOrReAppropriateBudget::class,'dwa_budget_id','id')
            ->where('type',AddlFundsOrReAppropriateBudget::ADDITIONAL_FUNDS_TYPE);
    }

    public function quatRelaxBudget()
    {
        return $this->hasMany(QuaterlyRelaxationFunds::class,'dwa_budget_id','id');
    }
}
