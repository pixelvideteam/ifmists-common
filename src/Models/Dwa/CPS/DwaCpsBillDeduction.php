<?php

namespace App\Models\Dwa\CPS;

use Illuminate\Database\Eloquent\Model;

class DwaCpsBillDeduction extends Model
{
    protected $table = 'dwa_cps_bill_deduction';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];
}
