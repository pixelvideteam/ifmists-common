<?php

namespace App\Models\Dwa\CPS;

use Illuminate\Database\Eloquent\Model;

class DwaCpsBillParty extends Model
{
    protected $table = 'dwa_cps_bill_party';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];
}
