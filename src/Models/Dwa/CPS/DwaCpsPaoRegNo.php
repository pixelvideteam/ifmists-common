<?php

namespace App\Models\Dwa\CPS;

use App\BankIfsc;
use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DwaCpsPaoRegNo extends Model
{
     use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'dwa_cps_pao_reg_no';

    protected $table = 'dwa_cps_pao_reg_no';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function bankifsc()
    {
        return $this->belongsTo(BankIfsc::class, 'ifsccode', 'ifsccode');
    }

    public function dmfbankifsc()
    {
        return $this->belongsTo(BankIfsc::class, 'dmf_ifsccode', 'ifsccode');
    }

    public function smetbankifsc()
    {
        return $this->belongsTo(BankIfsc::class, 'smet_ifsccode', 'ifsccode');
    }

    public function nacbankifsc()
    {
        return $this->belongsTo(BankIfsc::class, 'nac_ifsccode', 'ifsccode');
    }

    public function labourbankifsc()
    {
        return $this->belongsTo(BankIfsc::class, 'labour_ifsccode', 'ifsccode');
    }
}
