<?php

namespace App\Models\Dwa\CPS;

use App\Models\Dwa\Accounts\DwaTeoAdjustment;
use App\Transactions;
use App\User;
use Illuminate\Database\Eloquent\Model;

class DwaCpsBillTransaction extends Model
{
    protected $table = 'dwa_cps_bill_transaction';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'ddocode', 'username');
    }

    public function transaction()
    {
        return $this->belongsTo(Transactions::class, 'transaction_id');
    }

    public function parties()
    {
        return $this->hasMany(DwaCpsBillParty::class, 'cps_bill_transaction_id');
    }

    public function teoAdjustment()
    {
        return $this->hasOne(DwaTeoAdjustment::class, 'transaction_id', 'transaction_id');
    }
}
