<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class AgencyGstinList extends Model
{
    const AGENCY_GSTIN_APPROVED = 1;
    const AGENCY_GSTIN_PENDING = 0;
    const AGENCY_GSTIN_REJECTED = 21;

    protected $table = 'dwa_agency_gstin_list';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function agency()
    {
        return $this->belongsTo(Agency::class,'dwa_agency_id','id');
    }
}
