<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class JDList extends Model
{
    protected $table = "dwa_jdwa_list";
    public $timestamps = false;

    protected $guarded = ["id"];

    public function pao_list(){
        return $this->hasMany(DwaAreas::class, 'dwa_jdwa_id', 'id');
    }

}
