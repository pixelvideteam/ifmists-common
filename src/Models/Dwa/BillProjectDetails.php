<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class BillProjectDetails extends Model
{
    protected $table="dwa_bill_project_details";

    protected $guarded=['id'];

    public function Department()
    {
        return $this->hasOne('App\Department','id','department_id');
    }

    public function trancheDetails()
    {
        return $this->hasOne('App\Models\Dwa\NabardProjectCode','id','dwa_nabard_project_code_id');
    }

    public function billDetails()
    {
        return $this->hasOne('App\Transactions','id','transaction_id');
    }

}
