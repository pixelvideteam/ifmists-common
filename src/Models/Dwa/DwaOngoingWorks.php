<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class DwaOngoingWorks extends Model
{

    const DWA_ONGOING_WORKS_PENDING_STATUS = 0;
    const DWA_ONGOING_WORKS_APPROVED_STATUS = 1;
    const DWA_ONGOING_WORKS_DDO_STATUS = 10;
    const DWA_ONGOING_WORKS_REJECT_STATUS = 21;
    const DWA_ONGOING_WORKS_DELETED_STATUS = 22;

    protected $table = 'dwa_ongoing_works';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function arealist()
    {
        return $this->belongsTo('App\Models\Dwa\DwaAreas', 'pao', 'area_code');
    }
}
