<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class AgencyBanks extends Model
{
    const AGENCY_BANK_APPROVED = 1;
    const AGENCY_BANK_REJECTED = 21;
    const AGENCY_BANK_PENDING = 0;

    protected $table="dwa_agencies_banks";

    protected $guarded=['id'];

    public function ifsc(){
        return $this->hasOne('App\BankIfsc','id','ifsccode')->select('id','bankname','ifsccode','branch');
    }

    public function agency()
    {
        return $this->belongsTo(Agency::class,'agency_id','id')->select('id');
    }

    public function agencyDetails()
    {
        return $this->belongsTo(Agency::class,'agency_id','id');
    }
}
