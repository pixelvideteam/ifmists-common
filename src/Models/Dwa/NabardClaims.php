<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class NabardClaims extends Model
{
    protected $table = "dwa_nabard_claims";

    protected $guarded = ['id'];

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
}
