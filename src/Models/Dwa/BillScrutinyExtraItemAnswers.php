<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class BillScrutinyExtraItemAnswers extends Model
{
    protected $table="dwa_bill_scrutiny_extraitems_answers";

    protected $guarded=['id'];

    public function optionDetails(){
        return $this->hasOne('App\Models\Dwa\ScrutinyItemsExtraOptions','id','extraoption_id');
    }
}
