<?php

namespace App\Models\Dwa\HeadsDesc;

use Illuminate\Support\Facades\Auth;

class ManagingDirectorHeads
{
    const MD_TSIDC_HEADS = [
        '4702-00-101-07-15-530-531',
        '4702-00-789-07-15-530-531',
        '4702-00-796-07-15-530 531',
        '4702-00-101-25-15-530-531',
        '4702-00-101-25-17-530-531',
        '4702-00-101-25-18-530-531',
        '4702-00-789-25-15-530-531',
        '4702-00-796-25-15-530-531',
        '4702-00-101-21-15-530-531',
        '4702-00-789-21-15-530-531',
        '4702-00-789-11-05-540-000',
        '4702-00-796-11-05-540-000',
        '4702-00-800-11-13-540-000',
    ];

    const MD_TSPHCL_HEADS = [
        // non-plan
        '2040-00-001-00-01-270-272-N',
        '2055-00-003-00-05-270-271-N',
        '2055-00-003-00-04-270-271-N',
        '2055-00-003-00-05-270-272-N',
        '2055-00-001-00-01-270-271-N',
        '2055-00-101-00-04-270-271-N',
        '2055-00-101-00-05-270-272-N',
        '2055-00-104-00-01-270-271-N',
        '2055-00-104-00-04-270-271-N',
        '2055-00-104-00-06-270-271-N',
        '2055-00-104-00-06-270-272-N',
        '2055-00-108-00-05-270-272-N',
        '2055-00-109-00-03-270-271-N',
        '2055-00-109-00-04-270-272-N',
        '2055-00-109-00-11-270-272-N',
        '2055-00-116-00-04-270-272-N',
        '2055-00-117-00-04-270-271-N',
        '2055-00-117-00-06-270-272-N',
        '2055-00-800-00-74-270-272-N',
        '2056-00-001-00-01-270-271-N',
        '2056-00-001-00-01-270-272-N',
        '2056-00-001-00-02-270-271-N',
        '2056-00-001-00-74-270-271-N',
        '2056-00-001-00-74-270-272-N',
        '2056-00-101-00-04-270-271-N',
        '2056-00-101-00-04-270-272-N',
        '2056-00-102-00-04-270-272-N',
        '2056-00-800-00-70-270-271-N',
        '2056-00-800-00-70-270-272-N',
        '2058-00-001-00-74-270-271-N',
        '2070-00-108-00-01-270-272-N',

        // plan
        '4055-00-003-04-05-530-531-P',
        '4055-00-003-11-05-530-531-P',
        '4055-00-003-25-05-530-531-P',
        '4055-00-115-12-05-530-531-P',
        '4055-00-207-11-04-530-531-P',
        '4055-00-207-11-05-530-531-P',
        '4055-00-207-11-06-530-531-P',
        '4055-00-207-11-07-530-531-P',
        '4055-00-207-11-10-530-531-P',
        '4055-00-207-11-11-530-531-P',
        '4055-00-207-11-12-530-531-P',
        '4055-00-207-11-13-530-531-P',
        '4055-00-207-11-16-530-531-P',
        '4055-00-207-11-28-530-531-P',
        '4055-00-207-12-05-530-531-P',
        '4055-00-207-12-07-530-531-P',
        '4055-00-207-12-27-530-531-P',
        '4055-00-207-25-04-530-531-P',
        '4055-00-207-25-05-530-531-P',
        '4055-00-207-25-06-530-531-P',
        '4055-00-207-25-07-530-531-P',
        '4055-00-207-25-09-530-531-P',
        '4055-00-207-25-11-530-531-P',
        '4055-00-207-25-12-530-531-P',
        '4055-00-207-25-14-530-531-P',
        '4055-00-207-25-16-530-531-P',
        '4055-00-207-25-22-530-531-P',
        '4055-00-207-25-27-530-531-P',
        '4055-00-207-25-28-530-531-P',
        '4055-00-207-25-33-530-531-P',
        '4055-00-207-25-38-530-531-P',
        '4055-00-207-25-42-530-531-P',
        '4055-00-207-25-46-530-531-P',
        '4055-00-208-04-05-530-531-P',
        '4055-00-208-11-06-530-531-P',
        '4055-00-208-25-06-530-531-P',
        '4055-00-800-11-05-530-531-P',
        '4055-00-800-25-05-530-531-P',
        '4070-00-800-04-05-530-531-P',
        '4070-00-800-11-04-530-531-P',
        '4070-00-800-11-10-530-531-P',
        '4070-00-800-11-12-530-531-P',
        '4070-00-800-11-17-530-531-P',
        '4070-00-800-11-23-530-531-P',
        '4070-00-800-25-10-530-531-P',
        '4070-00-800-25-12-530-531-P',
        '4070-00-800-25-17-530-531-P',
        '4070-00-800-25-23-530-531-P',
        '4070-00-800-25-25-530-531-P',
        '4070-00-800-25-26-530-531-P',
        '4070-00-800-25-38-530-531-P',
        '4235-60-800-25-05-530-531-P',
        '4403-00-101-07-05-530-531-P',
        '4405-00-800-25-05-530-531-P',
        '4406-01-070-25-05-530-531-P',
        '6075-00-800-00-07-001-000-P',
    ];

    const MD_TSEWIDC_MINOR_HEADS = [
        '2203-00-105-00-09-270-272',
        '2203-00-105-00-04-270-271',
        '2203-00-105-00-04-270-272',
        '2203-00-003-00-04-270-271',
        '2202-00-003-00-04-270-272',
        '2202-03-001-00-01-270-271',
        '2202-03-001-00-01-270-272',
        '2202-03-103-00-46-270-271',
        '2202-03-103-00-46-270-272',
        '2202-03-103-00-48-270-272',
        '2202-03-103-00-04-270-272',
        '2225-03-277-00-07-270-272',
        '2225-01-277-00-07-270-272',
        '2225-01-277-25-07-270-272',
        '2225-01-277-00-31-270-272',
        '2202-80-001-00-01-270-272',
    ];
    const MD_TSEWIDC_CAPITAL_HEADS = [
        '4225-01-277-07-32-530-531',
        '4225-01-277-25-34-530-531',
        '4225-01-800-25-07-530-531',
        '4202-01-203-25-70-530-531',
        '4202-02-104-25-05-530-531',
        '4202-01-202-25-74-530-531',
        '4202-01-201-25-77-530-531',
        '4202-01-202-25-83-530-531',
        '4202-01-202-25-87-530-531',
        '4202-01-202-12-05-530-531',
        '4202-02-104-25-74-530-531',
        '4202-02-104-07-74-530-531',
        '4202-01-202-07-07-530-531',
        '4202-01-202-07-06-530-531',
        '4202-01-202-25-48-530-531',
        '4225-03-277-25-74-530-531',
        '4225-01-277-25-74-530-531',
        '4202-01-203-07-74-530-531',
        '4202-01-203-25-10-530-531',
        '4202-01-203-25-77-530-531',
        '4235-02-102-07-04-530-531',
        '4202-01-201-25-81-530-531',
        '4250-00-203-25-76-530-531',
    ];


    public static function getHeads()
    {
        $user = Auth::user();

        switch ($user->username) {
            case 'md-tsidc':
                return self::mdTsidcHeads();
                break;

            case 'md-tsphcl':
                return self::mdTsphclHeads();
                break;

            case 'md-tsewidc':
                return self::mdTsewidcHeads();
                break;

            default:
                return [];
                break;
        }
    }

    public static function mdTsidcHeads()
    {
        $output = [];
        foreach (self::MD_TSIDC_HEADS as $head) {
            array_push($output, str_replace('-', '', $head) . 'NVN');
            array_push($output, str_replace('-', '', $head) . 'PVN');
            array_push($output, str_replace('-', '', $head) . 'NCN');
            array_push($output, str_replace('-', '', $head) . 'PCN');
        }

        return $output;
    }



    public static function mdTsphclHeads()
    {
        $output = [];
        foreach (self::MD_TSPHCL_HEADS as $head) {
            array_push($output, str_replace('-', '', $head) . 'VN');
            array_push($output, str_replace('-', '', $head) . 'CN');
        }

        return $output;
    }

    public static function mdTsewidcHeads()
    {
        $output = [];

        // Minor heads
        foreach (self::MD_TSEWIDC_MINOR_HEADS as $head) {
            array_push($output, str_replace('-', '', $head) . 'NVN');
            array_push($output, str_replace('-', '', $head) . 'PVN');
            array_push($output, str_replace('-', '', $head) . 'NCN');
            array_push($output, str_replace('-', '', $head) . 'PCN');
        }

        // Capital Heads
        foreach (self::MD_TSEWIDC_CAPITAL_HEADS as $head) {
            array_push($output, str_replace('-', '', $head) . 'NVN');
            array_push($output, str_replace('-', '', $head) . 'PVN');
            array_push($output, str_replace('-', '', $head) . 'NCN');
            array_push($output, str_replace('-', '', $head) . 'PCN');
        }

        return $output;
    }
}
