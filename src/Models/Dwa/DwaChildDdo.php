<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DwaChildDdo extends Model
{
    use QueryCacheable;

    public $cacheFor = 3600; // cache time, in seconds / month
    public $cachePrefix = 'dwa_child_ddo_';

    protected $table = "dwa_child_ddo";

    protected $guarded = ['id'];
}
