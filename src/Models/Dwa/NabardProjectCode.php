<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class NabardProjectCode extends Model
{
    protected $table = 'dwa_nabard_project_code';
}
