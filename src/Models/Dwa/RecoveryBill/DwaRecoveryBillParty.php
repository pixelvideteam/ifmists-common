<?php

namespace App\Models\Dwa\RecoveryBill;

use Illuminate\Database\Eloquent\Model;

class DwaRecoveryBillParty extends Model
{
    protected $table = 'dwa_recovery_bill_party';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];
}
