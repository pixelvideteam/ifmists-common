<?php

namespace App\Models\Dwa\RecoveryBill;

use App\Models\Dwa\Deductions;
use App\Transactions;
use App\User;
use Illuminate\Database\Eloquent\Model;

class DwaRecoveryBillTransaction extends Model
{
    const BILL_SUBMITION_STARTING_MONTH = 4;
    const BILL_SUBMITION_STARTING_YEAR = 2020;

    protected $table = 'dwa_recovery_bill_transaction';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'ddocode', 'username');
    }

    public function transaction()
    {
        return $this->belongsTo(Transactions::class, 'transaction_id');
    }

    public function parties()
    {
        return $this->hasMany(DwaRecoveryBillParty::class, 'recovery_bill_transaction_id');
    }

    public function dwaDeductionList()
    {
        return $this->hasOne(Deductions::class, 'id', 'dwa_deductions_list_id');
    }
}
