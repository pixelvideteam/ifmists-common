<?php

namespace App\Models\Dwa\Dwa;

use App\Models\Dwa\GovtOrder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Dwa\Projects;
use App\User;

class TechnicalSanctionValue extends Model
{
    const TECH_SANCTION_APPROVED = 1;
    const TECH_SANCTION_REJECTED = 21;
    const TECH_SANCTION_PENDING = 0;
    const TECH_SANCTION_DELETED = 22;
    const TS_REVISION_TYPE = 1;
    const TS_REVISION_TYPE_TEXT = 'Revised TS';
    const WORK_SLIP_REVISION_TYPE = 2;
    const WORK_SLIP_REVISION_TEXT = 'Work Slip';

    protected $table = 'dwa_technical_sanctions';

    public function revised()
    {
        return $this->hasMany(TechnicalSanctionValueRevised::class, 'dwa_technical_sanction_id', 'id')->latest();
    }

    public function go()
    {
        return $this->hasOne(GovtOrder::class, 'id', 'dwa_govt_order_id');
    }

    public function dwaProjects()
    {
        return $this->hasMany(Projects::class, 'technical_sanction_id', 'id');

    }

    public function fundings()
    {
        return $this->hasMany('App\Models\Dwa\Fundings', 'dwa_technical_sanction_id');
    }

    public function createdby()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
