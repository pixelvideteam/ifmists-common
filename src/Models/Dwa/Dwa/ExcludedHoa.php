<?php

namespace App\Models\Dwa\Dwa;

use App\Repository\ApiRepository\BMS\Helpers;

class ExcludedHoa
{
    // THIS IS LOC HEAD - exclude this Hoa in checking
    const EXCLUDE_HOA = [
        '8658-00-101-00-78-003-000'
    ];

    const HOA = [
        '2071-01-101-00-04-040-041',
        '2071-01-104-00-04-040-042',
        '2235-60-104-00-04-500-503'
    ];

    const DH = [
        '010'
    ];

    const DH_SDH = [
        '310-318',
        '060-000',
        '040-041',
        '040-043'
    ];

    public function isExcluded($hoa)
    {
        $formattedHoa = (new Helpers)->getFormattedHoa($hoa);

        // EXCLUDE HOA checking
        // return false - this need to be proceed for check budget
        if (in_array($formattedHoa, self::EXCLUDE_HOA)) {
            return false;
        }

        // HOA checking
        if (in_array($formattedHoa, self::HOA)) {
            return true;
        }

        list($mj_h, $s_mj_h, $mi_h, $gr_s_h, $s_h, $d_h, $s_d_h) = explode('-', $formattedHoa);

        // MJH >= 7000 checking
        if ($mj_h >= 7000) {
            return true;
        }

        // DH checking
        if (in_array($d_h, self::DH)) {
            return true;
        }

        // DH-SDH checking
        if (in_array($d_h . '-' . $s_d_h, self::DH_SDH)) {
            return true;
        }

        return false;
    }
}
