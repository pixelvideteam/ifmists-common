<?php

namespace App\Models\Dwa\Dwa;

use Illuminate\Database\Eloquent\Model;

class TechnicalSanctionValueRevised extends Model
{
    const TS_REVISE_PENDING_STATUS = 0;
    const TS_REVISE_APPROVED_STATUS = 1;
    const TS_REVISE_REJECTED_STATUS = 21;

    protected $table = 'dwa_technical_sanction_revises';
}
