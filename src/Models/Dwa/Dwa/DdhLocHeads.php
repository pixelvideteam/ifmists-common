<?php


namespace App\Models\Dwa\Dwa;

use App\Models\Dwa\Budget\DwaBudgetEstimation;
use App\Repository\ApiRepository\BMS\Helpers;
use App\Traits\Dwa\HeadOfAccountTrait;

class DdhLocHeads
{
    use HeadOfAccountTrait;

    const DCW_MJ_HEAD = '8443';

    public static function locMajHeadsRange()
    {
        return [
            'from' => '2000',
            'to' => '3999'
        ];
    }

    public static function locDetHeads()
    {
        return array_merge(self::locDetWorkHeads(), self::locDetNonWorkHeads());

        // return [
        //     // Work Heads
        //     '270',
        //     '270',
        //     '270',
        //     '270',
        //     '310',
        //     '310',
        //     '500',
        //     '500',
        //     '520',
        //     '520',
        //     '530',
        //     '530',
        //     '530',
        //     '530',
        //     '540',

        //     // Non-work Heads
        //     '020',
        //     '020',
        //     '020',
        //     '020',
        //     '110',
        //     '130',
        //     '130',
        //     '130',
        //     '130',
        //     '130',
        //     '140',
        //     '160',
        //     '200',
        //     '240',
        //     '260',
        //     '280',
        //     '280',
        //     '290',
        //     '290',
        //     '290',
        //     '290',
        //     '290',
        //     '290',
        //     '300',
        //     '300',
        //     '310',
        //     '510',
        //     '510'
        // ];
    }

    public static function locDetWorkHeads()
    {
        return [
            // Work Heads
            '270',
            '270',
            '270',
            '270',
            '310',
            '310',
            '500',
            '500',
            '520',
            '520',
            '530',
            '530',
            '530',
            '530',
            '540',
        ];
    }

    public static function locDetNonWorkHeads()
    {
        return [
            // Non-work Heads
            '020',
            '020',
            '020',
            '020',
            '110',
            '130',
            '130',
            '130',
            '130',
            '130',
            '140',
            '160',
            '200',
            '240',
            '260',
            '280',
            '280',
            '290',
            '290',
            '290',
            '290',
            '290',
            '290',
            '300',
            '300',
            '310',
            '510',
            '510'
        ];
    }

    public static function locSdHeads()
    {
        return array_merge(self::locSdWorkHeads(), self::locSdNonWorkHeads());

        // return [
        //     // Work heads
        //     '271',
        //     '272',
        //     '274',
        //     '275',
        //     '312',
        //     '319',
        //     '501',
        //     '503',
        //     '521',
        //     '522',
        //     '531',
        //     '532',
        //     '533',
        //     '535',
        //     '000',

        //     // Non-work Heads
        //     '000',
        //     '021',
        //     '022',
        //     '023',
        //     '111',
        //     '131',
        //     '132',
        //     '133',
        //     '134',
        //     '135',
        //     '000',
        //     '000',
        //     '000',
        //     '000',
        //     '000',
        //     '281',
        //     '284',
        //     '291',
        //     '292',
        //     '293',
        //     '294',
        //     '295',
        //     '296',
        //     '301',
        //     '302',
        //     '311',
        //     '511',
        //     '512'
        // ];
    }

    public static function locSdWorkHeads()
    {
        return [
            // Work heads
            '271',
            '272',
            '274',
            '275',
            '312',
            '319',
            '501',
            '503',
            '521',
            '522',
            '531',
            '532',
            '533',
            '535',
            '000',
        ];
    }

    public static function locSdNonWorkHeads()
    {
        return [
            // Non-work Heads
            '000',
            '021',
            '022',
            '023',
            '111',
            '131',
            '132',
            '133',
            '134',
            '135',
            '000',
            '000',
            '000',
            '000',
            '000',
            '281',
            '284',
            '291',
            '292',
            '293',
            '294',
            '295',
            '296',
            '301',
            '302',
            '311',
            '511',
            '512'
        ];
    }

    public static function locHoaTreatAsDdhHoa()
    {
        return [
            '2059-01-053-00-09-270-271-V',
            '2059-01-053-00-09-270-272-V'
        ];
    }

    public static function ddhMajHeadsRange()
    {
        return [
            'from' => '4000',
            'to' => '5999',
        ];
    }

    public static function ddhDetHeads()
    {
        return [
            '270',
            '270',
            '270',
            '270',
            '310',
            '310',
            '500',
            '500',
            '520',
            '520',
            '530',
            '530',
            '530',
            '530',
            '540',
        ];
    }

    public static function ddhSdHeads()
    {
        return [
            '271',
            '272',
            '274',
            '275',
            '312',
            '319',
            '501',
            '503',
            '521',
            '522',
            '531',
            '532',
            '533',
            '535',
            '000',
        ];
    }

    public static function isDdhHead($hoa)
    {
        $budgetEstimate = DwaBudgetEstimation::whereHoa($hoa)->first();

        if ($budgetEstimate) {
            return $budgetEstimate->ddh_loc == 'ddh';
        }

        $helpers = new Helpers();
        list($mj_h, $s_mj_h, $mi_h, $gr_s_h, $s_h, $d_h, $s_d_h) = explode('-', $helpers->getFormattedHoa($hoa));

        // LOC HOA treat as DDH - VOTED
        $votedHoa = $mj_h . '-' . $s_mj_h . '-' . $mi_h . '-' . $gr_s_h . '-' . $s_h . '-' . $d_h . '-' . $s_d_h . '-V';
        if (in_array($votedHoa, self::locHoaTreatAsDdhHoa())) {
            return true;
        }

        // LOC HOA treat as DDH - CHARGED
        $chargedHoa = $mj_h . '-' . $s_mj_h . '-' . $mi_h . '-' . $gr_s_h . '-' . $s_h . '-' . $d_h . '-' . $s_d_h . '-C';
        if (in_array($chargedHoa, self::locHoaTreatAsDdhHoa())) {
            return true;
        }

        $headRange = self::ddhMajHeadsRange();
        $mjHeadExists = false;
        if ($headRange['from'] <= $mj_h && $mj_h <= $headRange['to']) {
            $mjHeadExists = true;
        }

        $detHeads = self::ddhDetHeads();
        $detHeadExists = false;
        if (in_array($d_h, $detHeads)) {
            $detHeadExists = true;
        }

        $subDetHeads = self::ddhSdHeads();
        $subDetHeadExists = false;
        if (in_array($s_d_h, $subDetHeads)) {
            $subDetHeadExists = true;
        }

        if ($mjHeadExists && $detHeadExists && $subDetHeadExists) {
            return true;
        }
        return false;
    }

    public static function isLocHead($hoa)
    {
        $budgetEstimate = DwaBudgetEstimation::whereHoa($hoa)->first();

        if ($budgetEstimate) {
            return $budgetEstimate->ddh_loc == 'loc';
        }

        $dbhSdhArray = ['300302', '020021', '300301'];
        $helpers = new Helpers();
        list($mj_h, $s_mj_h, $mi_h, $gr_s_h, $s_h, $d_h, $s_d_h) = explode('-', $helpers->getFormattedHoa($hoa));

        $votedHoa = $mj_h . '-' . $s_mj_h . '-' . $mi_h . '-' . $gr_s_h . '-' . $s_h . '-' . $d_h . '-' . $s_d_h . '-V';
        if (in_array($votedHoa, self::locHoaTreatAsDdhHoa())) {
            return false;
        }

        // Check is it Dcw MJ Head
        if ($mj_h == self::DCW_MJ_HEAD) {
            return true;
        }

        if ((new static())->isWorkCharged($hoa, 2020)) {
            return true;
        }

        $dhSdh = substr($hoa, -9, 6);
        if (in_array($dhSdh, $dbhSdhArray)) {
            return true;
        }

        $headRange = self::locMajHeadsRange();
        $mjHeadExists = false;
        if ($headRange['from'] <= $mj_h && $mj_h <= $headRange['to']) {
            $mjHeadExists = true;
        }

        $detHeads = self::locDetHeads();
        $detHeadExists = false;
        if (in_array($d_h, $detHeads)) {
            $detHeadExists = true;
        }

        $subDetHeads = self::locSdHeads();
        $subDetHeadExists = false;
        if (in_array($s_d_h, $subDetHeads)) {
            $subDetHeadExists = true;
        }

        if ($mjHeadExists && $detHeadExists && $subDetHeadExists) {
            return true;
        }
        return false;
    }
}
