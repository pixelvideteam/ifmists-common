<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class Beneficieries extends Model
{
    protected $table = "dwa_beneficieries";

    protected $guarded = ['id'];

    public function district()
    {
        return $this->hasOne('App\Models\Dwa\Districts', 'id', 'district_id');
    }

    public function mandal()
    {
        return $this->hasOne('App\Models\Dwa\Mandals', 'id', 'mandal_id');
    }

    public function village()
    {
        return $this->hasOne('App\Models\Dwa\Villages', 'id', 'village_id');
    }

    public function bankAccounts()
    {
        return $this->hasMany(BeneficieryBanks::class, 'beneficiery_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'username', 'ddocode');
    }
}
