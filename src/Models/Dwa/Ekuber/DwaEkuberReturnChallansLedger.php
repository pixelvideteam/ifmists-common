<?php

namespace App\Models\Dwa\Ekuber;


use Illuminate\Database\Eloquent\Model;

class DwaEkuberReturnChallansLedger extends Model
{
    protected $table = 'dwa_ekuber_return_challans_ledger';
}
