<?php

namespace App\Models\Dwa\Ekuber;

use App\BankIfsc;
use App\BillMultipleParty;
use App\User;
use Illuminate\Database\Eloquent\Model;

class DwaEkuberReturnChallans extends Model
{
    protected $table = 'dwa_ekuber_return_challans';

    public function old_bank()
    {
        return $this->hasOne(BankIfsc::class, 'ifsccode', 'old_ifsc');
    }

    public function new_bank()
    {
        return $this->hasOne(BankIfsc::class, 'ifsccode', 'new_ifsc');
    }

    public function ddo()
    {
        return $this->hasOne(User::class, 'username', 'ddocode');
    }

    public function bill_multiple_party()
    {
        return $this->hasOne(BillMultipleParty::class, 'id', 'bill_multiple_party_id');
    }

    public function billmultipleparty()
    {
        return $this->belongsTo(BillMultipleParty::class, 'bill_multiple_party_id');
    }

    public function newbillmultipleparty()
    {
        return $this->belongsTo(BillMultipleParty::class, 'new_bill_multiple_party_id');
    }
}
