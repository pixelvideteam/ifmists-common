<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
    protected $table="dwa_la_districts";

    protected $guarded=['id'];
}
