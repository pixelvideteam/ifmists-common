<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class LocDdoBudget extends Model
{
    protected $table = "dwa_loc_ddo_budget";

    protected $guarded = ["id"];
}
