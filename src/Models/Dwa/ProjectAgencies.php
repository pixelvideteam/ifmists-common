<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class ProjectAgencies extends Model
{

    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 21;
    const STATUS_PENDING_FOR_APPROVAL = 0;

    protected $table = "dwa_project_agencies";

    protected $guarded = ['id'];

    public function agencyDetails()
    {
        return $this->hasOne('App\Models\Dwa\Agency', 'id', 'agency_id');
    }

    public function agencyDetailOne()
    {
        return $this->hasOne('App\Models\Dwa\Agency', 'id', 'agency_id');
    }

    public function Banks()
    {
        return $this->hasMany('App\Models\Dwa\AgencyBanks', 'agency_id', 'agency_id');
    }

    public function agencyDetailsWithBanks()
    {
        return $this->hasOne('App\Models\Dwa\Agency', 'id', 'agency_id');
    }

}
