<?php

namespace App\Models\Dwa;

use Illuminate\Database\Eloquent\Model;

class Villages extends Model
{
    protected $table="dwa_la_villages";

    protected $guarded=['id'];
}
