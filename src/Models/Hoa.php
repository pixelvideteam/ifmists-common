<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Hoa extends Model {

	protected $table='hoa';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function detailed() {

		return $this->hasOne('App\Formtype','detailed_head','detailed_head');
	}

	public function subdetailed() {

		return $this->hasOne('App\Formtype','subdetailed_head','sub_detailed_head');
	}

	public function paobudget() {

		return $this->hasMany('App\PaoBudget','hoa','hoa');
	}

	public function desc() {

		return $this->hasOne('App\HeadsDesc','hoa','hoa');
	}
}
