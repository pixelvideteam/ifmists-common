<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyDetails extends Model
{
    protected $table = 'agency_details';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
    const DWA_BMS_FLAG_NOT_DWA = 0;
    const DWA_BMS_FLAG_ADDED = 1;
    const DWA_BMS_FLAG_SENT_TO_BMS = 2;
    const STATUS_INACTIVE = 21;
    const STATUS_ACTIVE = 1;

    public static function boot()
    {
        parent::boot();
        static::creating(function ($post) {
            $nm = strtoupper($post->partyname);
            $name = preg_replace("/[^A-Za-z0-9 ]+/", " ", $nm);
            $post->partyname = $name;
        });
    }

    public function ifscdetail()
    {
        return $this->hasOne('App\BankIfsc', 'id', 'ifsc_id')->where('status','1');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'username', 'ddocode');
    }
}
