<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class HeavyAdjBillsFilesStatus extends Model
{
    protected $table = "heavy_adj_bill_files";
    public $timestamps = false;
    protected $guarded = ['id'];

    public function usernames(){
        return $this->belongsTo('App\User','id','users_id');
    }
}
