<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class SuspensionApprovals extends Model
{
	protected $table="suspension_approvals";
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function empmaster()
	{	
		return $this->belongsTo('App\EmployeeMaster','employee_id','id');
	}

	public function aud()
	{	
		return $this->belongsTo('App\User','auduser','username');
	}

	public function empearndedn() 
	{
		return $this->hasMany('App\SuspensionEarndedn','suspension_approval_id','id');
	}

	public function docs() 
	{
		return $this->hasMany('App\SuspensionApprovalsDocs','susp_approvals_id','id');
	}
}
