<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SubscriptionNominees extends Model
{
    protected $table = 'esr_subscription_nominees';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    const TYPE_GPF = 1;
    const TYPE_TSGLI = 2;
    const TYPE_GIS = 3;
    const TYPE_SERVICE_PENSION = 4;

    public static function getEmployeeNomineesForSubscription($employeeId,$type)
    {
        return self::where('employee_id', $employeeId)
            ->where('subscription_type',$type)
            ->with('familyDetails.familyRelations')
            ->get();
    }

    public function familyDetails()
    {
        return $this->hasOne('App\FamilyDetails', 'id', 'family_details_id');
    }
}
