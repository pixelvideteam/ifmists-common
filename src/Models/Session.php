<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Session extends Model
{
    const SESSION_PREFIX = 'sessions_';
    const CLIENT_TYPE_WEB = 1;
    const CLIENT_TYPE_MOBILE = 2;
    protected $table = 'sessions';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     *
     */
    protected static function boot()
    {
        parent::boot();

        static::saved(function ($m) {
            if (!empty($m->refreshtoken)) {
                Cache::forget(self::SESSION_PREFIX . $m->refreshtoken);
                static::fetch($m->refreshtoken);
            }
        });
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * @param string $token
     * @return mixed
     */
    public static function fetch(string $token)
    {
        if (!$token) {
            return null;
        }

        $date = Carbon::now();

        // min(18 hours, session_expiry_time) cache
        $userdata = Cache::remember(self::SESSION_PREFIX . $token, 1440, function () use ($token, $date) {
            return Session::where('refreshtoken', '=', $token)
                ->where('expiry_time', '>', $date)
                ->orderby('id', 'desc')
                ->with([
                    "users" => function ($qu) {
                        $qu->with('ddoifsc')
                            ->with('modules')
                            ->with('userfp')
                            ->with('alternate_phone_no');
                    },
                ])
                ->first();
        });

        return $userdata;
    }
}
