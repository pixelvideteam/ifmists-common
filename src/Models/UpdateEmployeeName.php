<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UpdateEmployeeName extends Model
{
    protected $table = "employee_name_updates";

    public function EmployeeMaster()
    {
        return $this->belongsTo('App\EmployeeMaster', 'id', 'employee_master_id');
    }
}
