<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionArrearsMain extends Model {

	protected $table='promotion_arrears_main';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
