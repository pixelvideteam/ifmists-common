<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SuppBillDaywisePayDetails extends Model {

	protected $table='supp_bill_daywise_pay_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];
    public function billmultiple()
    {
        return $this->belongsTo('App\BillMultipleParty','bill_multiple_party_id','id');
    }
}
