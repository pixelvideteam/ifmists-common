<?php namespace App;

use App\Models\Dwa\AccountType;
use App\Models\Dwa\BillGstDetails;
use App\Models\Dwa\GovtOrder;
use Illuminate\Database\Eloquent\Model;

class HOATransactionModifyLog extends Model
{
    protected $table = 'hoa_transaction_modify_log';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
