<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GisCatg;


class HomeTownDeclaration extends Model
{
    protected $table = 'esr_home_town_declaration';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getEmployeeHomeTownDeclarations($employeeId)
    {
        return self::where('employee_id', $employeeId)->first();
    }
}
