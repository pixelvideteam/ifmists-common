<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavesAvailedEL extends Model
{
    protected $table='esr_leaves_availed_el';
    protected $guarded = ['id','created_at','updated_at'];

    public function leavetype(){
        return $this->hasOne(LeavesMaster::class, 'id', 'leave_type');
    }
}
