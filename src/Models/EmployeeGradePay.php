<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeGradePay extends Model {

	protected $table='employee_grade_pay';
	protected $guarded = ['id', 'created_at', 'updated_at'];


	// public function scale() 
	// {
	// 	return $this->hasOne('App\Scale','id','scale_id');
	// }

	// public function empmaster() 
	// {
	// 	return $this->hasOne('App\EmployeeMaster','id','employee_id');
	// }

	public function basicdets() 
	{
		return $this->hasOne('App\MasterScaleDetails','id','from_master_scale_details_id');
	}

    public function to_basicdets()
    {
        return $this->hasOne('App\MasterScaleDetails','id','master_scale_details_id');
    }

    /*public function authgradepay()
    {
        return $this->hasOne('App\AuthGradePayApprovals','id','auth_grade_pay_app_id');
    }*/


}
