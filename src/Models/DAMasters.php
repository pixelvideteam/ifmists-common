<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DAMasters extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'da_masters';

    protected $table = 'da_masters';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    // public function emps()
    // {
    // 	return $this->hasMany('App\EmployeeDa','rec_parent_id','id');
    // }
}
