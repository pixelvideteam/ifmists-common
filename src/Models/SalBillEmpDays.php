<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
	class SalBillEmpDays extends Model{

		protected $table="sal_bill_emp_days";
		protected $guarded=['id'];
		
		public function billmultiple()
		{	
			return $this->belongsTo('App\BillMultipleParty','bill_multiple_party_id','id');
		}

		// public function querydata()
		// {
		// 	return $this->belongsTo('App\Query','query_id','id');
		// }
	}