<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class MaritalStatus extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'marital_statuses';

    protected $table = 'marital_statuses';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
