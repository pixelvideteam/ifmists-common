<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestReappropriation extends Model
{
    protected $table = 'budget_request_reappropriation';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function details(){
        return $this->hasMany(BudgetRequestReappropriationDetail::class,
            'budget_request_reappropriation_id', 'id');
    }

    public function go(){
        return $this->hasOne(BeAuthGo::class, 'id', 'go_id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function docs()
    {
        return $this->hasMany(BudgetRequestReappropriationDoc::class,
            'budget_request_reappropriation_id', 'id');
    }
}
