<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class IncrementTypes extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'increment_types';

    protected $table = 'increment_types';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
