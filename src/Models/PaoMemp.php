<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoMemp extends Model {

	protected $table='pao_memp';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}