<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeRecoveries extends Model {

	protected $table='employee_recoveries';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
