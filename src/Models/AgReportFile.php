<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgReportFile extends Model
{
    protected $table = 'ag_report_files';
    const TYPE_SUB_PAY_REPORT = 1;
    const TYPE_SUB_REC_REPORT = 2;
    const TYPE_SUB_FILE_REPORT = 3;
}
