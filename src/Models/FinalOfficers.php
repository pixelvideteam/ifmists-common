<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalOfficers extends Model {

	protected $table = "final_officers";
	public $timestamps = false;

	protected $guarded = ["id"];
}