<?php namespace App;

use App\Jobs\QuerySystem\QuerySystemCategoryIdenfityJob;
use Illuminate\Database\Eloquent\Model;

class Query extends Model
{

    const STATUS_PENDING = 0;
    const STATUS_CLOSED = 1;
    const STATUS_FORWARDED_TO_DEV = 2;
    const STATUS_RESOLVED = 3;
    const STATUS_JIRA_CREATED = 4;

    const TREASURY_TYPE_PD = '1'; // Type Column
    const TREASURY_TYPE_PAO = '2';
    const TREASURY_TYPE_DWA = '3';
    const TREASURY_TYPE_DTA = '4';

    protected $table = "queries";
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
    protected $dates = ['query_date'];
    protected $guarded = ["id"];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::saved(function ($m) {
            $changes = $m->getDirty();

            if (!empty($changes['query'])) {
                QuerySystemCategoryIdenfityJob::dispatch($m);
            }
        });
    }

    public function users()
    {
        return $this->hasOne('App\User', 'id', 'userid');
    }

    public function querydocs()
    {
        return $this->hasMany('App\QueriesDocs', 'queries_id', 'id');
    }

    public function latestComment()
    {
        return $this->hasOne('App\QueryComments', 'query_id', 'id')->select(['id', 'query_id', 'comment', 'comment_by'])
            ->selectRaw('LEFT(comment, 100) as min_comment')->where('is_sqlquery', '=', 0)->orderBy('id', 'desc')->with('commented_user');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'userid')->select(['id', 'name']);
    }

    public function tseuser()
    {
        return $this->hasOne('App\User', 'id', 'tse_user_id');
    }

    public function forwarduser()
    {
        return $this->hasOne('App\User', 'id', 'forward_by');
    }

}
