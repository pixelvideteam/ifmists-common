<?php

/**
 * Created by PhpStorm.
 * User: Prakash Sah
 * Date: 01-07-2020
 * Time: 11:13
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppNotificationApprovedRejectItems extends Model
{
    protected $table = "app_notification_approved_reject_items";

    const APPROVE_REJECT_FLAG_APPROVED = 1;
    const APPROVE_REJECT_FLAG_REJECTED = 21;
    const NOTIFICATION_TYPE_MOBILE_PUSH_NOTIFICATION = 5;
    const USER_ROLE_ARRAY_GOVT_ACTION = [37];


    public function notification(){
        return $this->hasOne('App\Models\Notification\AppNotification', 'id', 'notification_id');
    }

}
