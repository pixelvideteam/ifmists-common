<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SuspensionArrearsMain extends Model {

	protected $table='suspension_arrears_main';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
