<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class ScaleSubTypes extends Model
{
    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'scale_sub_types';

    protected $table = "scale_sub_types";
    protected $guarded = ['id'];
}
