<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PdMultipleParty extends Model {

	protected $table='multiple_party';
	public $timestamps = false;
	protected $guarded=['id'];

	public function ekubermultiplepartynew()
    {
        return $this->hasMany('App\EkuberMultiplePartyNew','multiple_party_id','id')->latest();
    }
}
