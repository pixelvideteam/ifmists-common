<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class MissingCpsBill extends Model
{
    protected $table = 'missing_cps_bills_list';

    const PENDING_FOR_SCRUTINY = 0;
    const APPROVED = 1;
    const REJECTED = 2;

    public function cpsbillparties()
    {
        return $this->hasMany(MissingCpsBillParty::class, 'cps_bill_list_id', 'id');
    }

    public function cpsdtoreg() {
        return $this->hasOne(CpsDtoReg::class, 'stocode', 'stocode');
    }
}
