<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeMasterUpdateLog extends Model
{
    protected $table="employee_master_update_log";
    protected $guarded=['id'];

    public function empDetails()
    {
        return $this->hasOne(EmployeeMaster::class, 'id', 'employee_id');
    }
}
