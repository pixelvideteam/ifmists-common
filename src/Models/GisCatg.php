<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GisCatg extends Model {

	protected $table='giscatg';
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
