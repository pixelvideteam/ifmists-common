<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SuppBillDaterangeDetails extends Model {

	protected $table='supp_bill_daterange_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
