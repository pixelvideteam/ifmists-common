<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DdoBillIds extends Model
{
//    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'ddo_bill_ids';

    protected $table = "ddo_bill_ids";
    protected $guarded = ['id'];

    public function hoa()
    {
        return $this->belongsTo('App\DdoBillIdsHoa', 'ddo_bill_ids_hoa_id', 'id');
    }

    public function sctype()
    {
        return $this->belongsTo('App\ScaleTypes', 'scale_type_id', 'id');
    }

    public function employees()
    {
        return $this->hasMany('App\EmployeeMaster', 'bill_id', 'id');
    }

    public function strength()
    {
        return $this->hasMany('App\BDWiseCadreStrength', 'ddo_bill_id', 'id');
    }

    public function ddo()
    {
        return $this->hasOne('App\User', 'username', 'ddocode');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transactions', 'bill_id', 'id');
    }

    public function billidhoa()
    {
        return $this->belongsTo('App\DdoBillIdsHoa', 'ddo_bill_ids_hoa_id', 'id');
    }
}
