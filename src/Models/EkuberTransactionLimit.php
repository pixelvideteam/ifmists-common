<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 07-03-2019
 * Time: 11:35
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class EkuberTransactionLimit extends Model
{
    protected $table='ekuber_transaction_limits';
}