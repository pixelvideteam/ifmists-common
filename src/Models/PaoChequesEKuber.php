<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoChequesEKuber extends Model {

	protected $table = "pao_cheques_ekuber";
	public $timestamps = false;

	protected $guarded = ["id"];
	public function paochqtrans()
	{	
		return $this->hasMany('App\PaoChequesEKuberParties','pao_cheques_ekuber_id','id');
	}
	public function approvedby()
	{	
		return $this->hasOne('App\User','id','approved_by');
	}
	public function branch()
	{	
		return $this->hasOne('App\PaoBranch','id','branch_id');
	}
    public function paymentfilesmap()
    {
        return $this->hasMany('App\ChequePaymentReturnMapping','cheque_id','id')->latest();
    }
    public function getdnfile()
    {
        return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')->where('transtype', '=', '3')->orderBy('created_at');
    }
    public function ekuberfilesent()
    {
        return $this->hasOne('App\EkuberTransactions','orgnl_msg_id','ekuber_filename')->where('transtype', '=', '5')->orderBy('created_at');
    }
}