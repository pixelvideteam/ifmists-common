<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 20-04-2019
 * Time: 11:59
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdjustmentBillBulkTemp extends Model
{
    protected $table = 'adjustment_bill_bulk_temp';
}
