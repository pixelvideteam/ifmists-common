<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayBookClosingMonthlyLog extends Model
{
    protected $table = 'day_book_closing_monthly_log';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $casts = [
        'amounts' => 'array',
    ];

}
