<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class ManualReceiptsResponse extends Model
{
    protected $table = 'manual_receipts_response';

    const INITIATED = 0;
    const SUCCESS = 1;
    const FAILED = 2;
    const PENDING = 3;
}
