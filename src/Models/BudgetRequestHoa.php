<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetRequestHoa extends Model
{
    protected $table = 'budget_request_hoa';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function budReqHoaDdo()
    {
        return $this->hasMany('App\BudgetRequestHoaDdo', 'budget_request_hoa_id', 'id');
    }
    public function budRequest()
    {
        return $this->hasOne('App\BudgetRequest', 'id', 'budget_request_id');
    }
    public function godetail()
    {
        return $this->hasOne('App\BeAuthGo', 'id', 'budget_request_go_id');
    }
}
