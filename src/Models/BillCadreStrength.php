<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class BillCadreStrength extends Model
{
	protected $table="bill_cadre_strength";
	protected $guarded=['id'];
	
	public function desg() 
	{
		return $this->hasOne('App\DesgCode','id','desg_id');
	}
}