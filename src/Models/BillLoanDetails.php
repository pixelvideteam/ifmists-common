<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BillLoanDetails extends Model {

	protected $table='bill_loan_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	public function billparty() {
		return $this->hasOne('App\BillMultipleParty','id','bill_multiple_party_id');
	}
}
