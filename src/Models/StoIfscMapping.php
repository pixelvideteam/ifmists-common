<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoIfscMapping extends Model
{
    protected $table = 'sto_ifsc_mapping';
}
