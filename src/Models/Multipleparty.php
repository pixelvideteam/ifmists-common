<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Multipleparty extends Model
{

    protected $table = "multiple_party";
    public $timestamps = false;

    protected $guarded = ['id'];

    public function transaction()
    {
        return $this->hasOne(Transactions::class, 'id', 'tran_id');
    }
    public function ekuber_multiple_party_new(){
        return $this->hasMany(EkuberMultiplePartyNew::class , 'multiple_party_id','id')->latest();
    }

    public function ekuber_multiple_party_new_one(){
        return $this->hasOne(EkuberMultiplePartyNew::class , 'multiple_party_id','id')->latest();
    }
    public function partydets()
    {
        return $this->hasOne(Party::class,  'partyacno','acno');
    }
    public function mpbankacnotracking()
    {
        return $this->hasOne(MultiplePartyTracking::class, 'multiple_party_id', 'id')->where("type","acno");
    }

}
