<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class ScaleTypes extends Model
{

    const SCALE_STATE = 1;
    const SCALE_AIS = 2;
    const SCALE_JUDICIAL = 3;
    const SCALE_UGC = 4;
    const SCALE_OTHERS = 5;
    const SCALE_AICTE = 6;

    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'scale_types';

    protected $table = 'scale_types';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function prcdets()
    {
        return $this->hasMany('App\Prc', 'scale_type_id', 'id');
    }

    public function subtypes()
    {
        return $this->hasMany('App\ScaleSubTypes', 'scale_type_id', 'id');
    }

    public function billids()
    {
        return $this->hasMany('App\DdoBillIds', 'scale_type_id', 'id');
    }
}
