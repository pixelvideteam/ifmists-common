<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaoBranch extends Model {

	protected $table='pao_branch';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	const WEF_COMPILATION_DATE ='01-07-2019'; //1st Jul, 2019 for IFMIS compilation WEF date

	public function users() {
		return $this->hasOne('App\User','id','chqapao_userid');
	}
	public function branchddos() {
		return $this->hasMany('App\User','pao_branch_id','id');
	}
	public function branchsections() {
		return $this->hasMany('App\PaoSectionMap','branch_id','id');
	}
}
