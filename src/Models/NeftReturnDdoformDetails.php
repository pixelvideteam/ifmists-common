<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NeftReturnDdoformDetails extends Model {

	protected $table = "neftreturn_ddoform_details";
	public $timestamps = false;

	protected $guarded = ["id"];
	public function chqdetails() {
		return $this->hasOne('App\Transactions','id','transaction_id');
	}
	public function multipleparty() {
		return $this->hasOne('App\BillMultipleParty','id','bill_multiple_party_id');
	}
	public function neftreturnuploadsdets() {
		return $this->hasOne('App\NeftReturnUploadsDetails','neftreturn_ddoform_details_id','id');
	}
}