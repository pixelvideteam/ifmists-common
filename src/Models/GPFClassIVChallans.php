<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class GPFClassIVChallans extends Model
{
    use QueryCacheable;

    protected $table = 'gpf_class_iv_challans';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function employeechallan()
    {
        return $this->hasOne(GPFClassIVChallanEmployees::class, 'id', 'challan_employee_id');
    }

    public function ifmisemployeechallan()
    {
        return $this->hasOne(GPFOpeningBalance::class, 'id', 'ifmis_challan_employee_id');
    }
}
