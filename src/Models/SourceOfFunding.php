<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 17-06-2019
 * Time: 20:51
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class SourceOfFunding extends Model
{
    protected $table = 'source_of_funding';

    public function sofcatg()
    {
        return $this->hasMany(SofCatg::class, 'sof_id', 'id');
    }

}