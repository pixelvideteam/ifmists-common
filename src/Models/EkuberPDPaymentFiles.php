<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EkuberPDPaymentFiles extends Model
{
    protected $table = "ekuber_pd_payment_files";
    public function transcheque(){
        return $this->hasOne(PDChequePaymentFileMapping::class,'ekuber_pd_payment_file_id','id');
    }
}
