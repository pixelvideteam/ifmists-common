<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sections extends Model
{
    protected $table = 'dwa_sections';
    protected $guarded = ['id'];
    
    const USER_MAX_SECTIONS = 3;
    
    public function users(){
        return $this->belongsToMany('App\User', 'dwa_users_sections', 'dwa_section_id', 'user_id');
    }
}
