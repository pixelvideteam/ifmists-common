<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class BillFilterTypes extends Model{

	protected $table="bill_filter_types";

	protected $guarded=['id'];
	
	public function formtypes()
	{	
		return $this->hasMany('App\BillFilterFormTypeMapping','bill_filter_formtypes_id','id');
	}

}