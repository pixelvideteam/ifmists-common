<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 29-12-2018
 * Time: 09:50
 */
declare(strict_types=1);

namespace App;
use Illuminate\Database\Eloquent\Model;

class ChequePaymentReturnMapping extends Model
{
    protected $table = "cheque_payment_return_mapping";

    public function paocheques (){
        return $this->hasOne(PaoChequesEKuber::class,'id','cheque_id');
    }
    public function paymentfiles(){
        return $this->hasOne(EkuberPaymentFiles::class,'id','ekuber_payment_file_id');
    }
    public function chqTrans(){
        return $this->hasMany(PaoChequesTransactions::class,'pao_cheques_id','cheque_id');
    }
}