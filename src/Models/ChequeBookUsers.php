<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ChequeBookUsers extends Model {

	protected $table = "chequebookusers";
	public $timestamps = false;

	protected $guarded = ["id"];
}