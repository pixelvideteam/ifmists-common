<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class LoansRecoveriesIds extends Model
{
	protected $table="loans_recoveries_ids";
	protected $guarded=['id'];
	
}