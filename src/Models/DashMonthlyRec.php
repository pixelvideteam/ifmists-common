<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DashMonthlyRec extends Model {

	//
	protected $table = "dashmonthlyrec";
	// public $timestamps = false;rans

	protected $guarded = ["id"];

	public function dailyrec()
	{	
		return $this->hasMany('App\DashDailyRec','dash_monthlyrec_table_id','id');
	}

}
