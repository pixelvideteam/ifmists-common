<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 22-06-2019
 * Time: 09:38
 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class DWAChequePaymentFileMapping extends Model
{

    protected $table = "dwa_cheque_payment_file_mapping";

    public function cheque()
    {
        return $this->hasOne(DWACheques::class, 'id', 'dwa_cheques_id');
    }

    public function dwacheques()
    {
        return $this->cheque();
    }

    public function paymentfiles()
    {
        return $this->hasOne(EkuberPaymentFiles::class, 'id', 'ekuber_dwa_payment_file_id');
    }

}