<?php
/**
 * Created by PhpStorm.
 * User: shreyan
 * Date: 5/13/2019
 * Time: 1:14 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class OTPLimit extends Model
{
    protected $table='otp_limit';
    protected $fillable=['cdate','user_id','count'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public $timestamps = false;

}