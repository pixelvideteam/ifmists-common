<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class PdExpTypes extends Model
{

    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'exptypes';

    protected $table = 'exptypes';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function scheme()
    {
        return $this->hasOne('App\Schemes', 'id', 'scheme_id');
    }
}
