<?php
/**
 * Created by PhpStorm.
 * User: yoges
 * Date: 12-07-2019
 * Time: 19:50
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormTypeRestrictedDdocode extends Model {

    protected $table='formtype_restricted_ddocodes';
    protected $guarded = ['id', 'created_at', 'updated_at', 'formtype_id', 'ddocode', 'hoa', 'type'];
}
