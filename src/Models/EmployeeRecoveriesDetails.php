<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeRecoveriesDetails extends Model 
{
	protected $table='employee_recoveries_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];
	public function emprec()
	{	
		return $this->hasOne('App\EmployeeRecoveries','id','emp_rec_id');
	}
}
