<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionCategoriesTraining extends Model
{
    protected $table = 'transaction_categories_training';
    public $timestamps = false;
}
