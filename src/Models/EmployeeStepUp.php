<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri

 */
declare(strict_types=1);

namespace App;


use Illuminate\Database\Eloquent\Model;

class EmployeeStepUp extends Model {

    protected $table='employee_stepup';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function emp()
    {
        return $this->hasOne('App\EmployeeMaster', 'id', 'employee_id');
    }

    public function monthdesc()
    {
        return $this->hasOne('App\Months', 'month_no', 'month');
    }
}
