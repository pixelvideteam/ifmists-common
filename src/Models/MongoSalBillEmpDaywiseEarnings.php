<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class MongoSalBillEmpDaywiseEarnings extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'sal_bill_emp_daywise_earnings';
    protected $hidden=["created_at","updated_at"];
    public function earndedn()
    {
        return $this->belongsTo('App\EarnDednList','earndedn_id','id');
    }
}
