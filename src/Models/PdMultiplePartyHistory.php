<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PdMultiplePartyHistory extends Model {

	protected $table='multiple_party_history';
	public $timestamps = false;
	protected $guarded=['id'];
}
