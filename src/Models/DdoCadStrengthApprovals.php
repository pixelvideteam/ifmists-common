<?php namespace App;

use Illuminate\Database\Eloquent\Model;
	
class DdoCadStrengthApprovals extends Model
{
	protected $table="ddo_cad_strength_approvals";
	protected $guarded=['id'];
	
	public function dets()
	{	
		return $this->hasMany('App\DdoCadStrengthApprovalsDets','ddo_cad_strength_app_id','id');
	}

	public function docs()
	{	
		return $this->hasMany('App\DdoCadStrengthApprovalsDocs','ddo_cad_strength_app_id','id');
	}

	public function dept()
	{	
		return $this->belongsTo('App\Department','dept_id','id');
	}

	public function aud()
	{	
		return $this->belongsTo('App\User','auduser','username');
	}
	public function ddo()
	{	
		return $this->belongsTo('App\User','ddocode','username');
	}
	
}