<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeDaDecrement extends Model {

	protected $table='employee_da_decrement';
	protected $guarded = ['id', 'created_at', 'updated_at'];

}
