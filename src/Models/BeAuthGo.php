<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeAuthGo extends Model
{
    protected $table="be_auth_go";
    protected $guarded=['id'];
}
