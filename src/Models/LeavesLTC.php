<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavesLTC extends Model
{
    protected $table = 'esr_leaves_ltc';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getEmployeeLeavesLTC($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with('familymap.family.familyRelations')
            ->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)
            ->with('familymap.family.familyRelations')
            ->first();
    }

    public function familymap()
    {
        return $this->hasMany('App\LeavesLTCFamilyMap', 'esr_leaves_ltc_id', 'id');
    }

}
