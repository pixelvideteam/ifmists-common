<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceSuspensions extends Model
{
    protected $table = 'esr_service_suspensions';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getAllEmployeeSuspensionDetails($employeeId)
    {
        return self::where('employee_id', $employeeId)
            ->with([
                'dept.designations.desg', 'desg', 'percentage'
            ])
            ->get();
    }

    public static function getById($id)
    {
        return self::where('id', $id)
            ->with([
                'dept.designations.desg', 'desg', 'percentage'
            ])
            ->first();
    }

    public function dept()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function desg()
    {
        return $this->hasOne(DesgCode::class, 'id', 'designation_id');
    }

    public function percentage()
    {
        return $this->hasOne(SuspensionPercentages::class, 'id', 'pay_percentage');
    }
}
