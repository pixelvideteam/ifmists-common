<?php
/**
 * Created by PhpStorm.
 * User: Praneeth Kalluri
 * Date: 21-11-2019
 * Time: 21:28
 */
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class RuleClientMapping extends Model
{
    protected $table = 'rule_client_mapping';
}