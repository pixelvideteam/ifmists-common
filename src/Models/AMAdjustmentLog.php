<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AMAdjustmentLog extends Model
{
    const TYPE_PAYMENT = 1;
    const TYPE_RECEIPT = 2;
    protected $table = 'am_adjustments_log';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
