<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LpcDetails extends Model 
{
	protected $table='lpc_details';
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function earndedn() {

		return $this->hasOne('App\EarnDednList','id','earndedn_id');
	}
}
