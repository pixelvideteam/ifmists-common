<?php
/**
 * Created by PhpStorm.
 * User: yoges
 * Date: 16-07-2019
 * Time: 17:20
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class EpassRequestTracker extends Model
{
    protected $table='epass_request_tracker';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
