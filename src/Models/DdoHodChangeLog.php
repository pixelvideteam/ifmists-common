<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DdoHodChangeLog extends Model
{
    protected $table = 'ddo_hod_change_log';
}
