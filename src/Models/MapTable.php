<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class MapTable extends Model
{

    use QueryCacheable;

    public $cacheFor = 2592000; // cache time, in seconds / month
    public $cachePrefix = 'maptable';

    protected $table = 'maptable';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function nextuser()
    {
        return $this->hasOne('App\User', 'username', 'mappeduser');
    }
}
