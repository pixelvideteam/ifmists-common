<?php

namespace App\Contracts;

use App\User;
use Illuminate\Foundation\Auth\User as AuthenticatableUser;

class Authenticatable extends AuthenticatableUser
{

    /**
     * @return string
     * Used in MainController and LoginPage for redirection
     */
    public function getDashboardRoute(): string
    {
        $route = '/#/home';
        if (!empty($this->user_role)) {
            switch ($this->user_role) {
                case User::ROLE_GOVT:
                    $route = '/pfs/#/home';
                    break;
                case 500:
                    $route = '/debt/#/home';
                    break;
            }
        }

        return $route;
    }
}
