<?php
/**
 * @author Mir Adnan
 * @use Backwards compatibility for FMSNEW repo autoloader
 */
spl_autoload_register(function ($class) {

    # Autoloader for Models
    if (strpos($class, 'App') !== false) {
        $parts = explode('\\', $class);

        if ($parts) {
            unset($parts[0]);
        }

        $file = __DIR__ . '/src/' . implode('/', $parts) . '.php';

        if (!file_exists($file)) {
            $file = __DIR__ . '/src/Models/' . implode('/', $parts) . '.php';
        }

        if (file_exists($file)) {
            include_once($file);
        }
    }

});
